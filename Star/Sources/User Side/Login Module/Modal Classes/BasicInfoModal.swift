//
//  BasicInfoModal.swift
//  Star
//
//  Created by ios-deepak b on 29/01/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class BasicInfoModal:NSObject{
    var title : String?
    var isSelect : Bool?
    var isGender:Bool? = false
    
    
    var strLanguageId = ""
    var strLanguageName = ""
    var strLanguageCode = ""
    
    init(title:String?,isSelect:Bool?) {
        self.isSelect = isSelect
        self.title = title
    }
    init(title:String?,isSelect:Bool?,isGender:Bool?)
    {
        self.isSelect = isSelect
        self.title = title
        self.isGender = isGender
    }
    
    init(dict:[String:Any]) {
           if let languageID = dict["languageID"] as? String{
               self.strLanguageId = languageID
           }
           else if let languageID = dict["languageID"] as? Int{
                      self.strLanguageId = String(languageID)
                  }
           
           
           if let name = dict["name"] as? String{
               self.strLanguageName = name
           }
           
           if let code = dict["code"] as? String{
               self.strLanguageCode = code
           }
    
}

}
