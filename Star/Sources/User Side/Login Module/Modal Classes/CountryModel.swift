//
//  CompanyInvitationListModel.swift
//  Reservision Drive
//
//  Created by Mindiii on 9/13/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CountryModel: NSObject {
    
    var CountryID = ""
    var Country_name = ""
    var Country_code = ""
    
    
    init(dict:[String:Any]) {
        if let languageID = dict["countryID"] as? String{
            self.CountryID = languageID
        }
        else if let countryID = dict["countryID"] as? Int{
                   self.CountryID = String(countryID)
               }
        
        
        if let country_name = dict["country_name"] as? String{
            self.Country_name = country_name
        }
        
        if let country_code = dict["country_code"] as? String{
            self.Country_code = country_code
        }
        
       
        
    }
}
