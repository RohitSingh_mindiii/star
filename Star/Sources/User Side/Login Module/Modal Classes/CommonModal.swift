//
//  CommonModal.swift
//  Star
//
//  Created by ios-deepak b on 18/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import Foundation
import UIKit

class CommonModal: NSObject {
    //var strGenderID:Int = 0
    var strID: String = ""
    var strParendID: String = ""
    var strName: String = ""
    var strCountryCode: String = ""
    var strCurrencyName: String = ""
    var strCurrencyCode: String = ""
    var strCurrencySymbol: String = ""
    var strDialCode: String = ""
    var strLanguageID: String = ""
    var strLanguageCode: String = ""
    var strGender: String = ""
    var strAge: String = ""
    var imgCountryLogo = UIImage()
    var arrSubCategory = [CommonModal]()
    var strValue: String = ""
    
    init(dict : [String:Any]) {
        
        if let countryID = dict["countryID"]  as? String{
            strID = countryID
        }else if let countryID = dict["countryID"]  as? Int{
            strID = "\(countryID)"
        }
        
        if let countryName = dict["country_name"] as? String{
            strName = countryName
        }
        if let countryCode = dict["country_code"] as? String{
            strCountryCode = countryCode
        }
        if let currencyCode = dict["currency_code"] as? String{
            strCurrencyCode = currencyCode
        }
        
        if let currencyName = dict["currency_name"] as? String{
            strCurrencyName = currencyName
        }
        
        if let currencySymbol = dict["currency_symbol"] as? String{
            strCurrencySymbol = currencySymbol
        }
        if let dialCode = dict["dial_code"] as? String{
            strDialCode = dialCode
        }
        if let currencyName = dict["languageID"] as? String{
            strLanguageID = currencyName
        }
        
        if let currencySymbol = dict["code"] as? String{
            strLanguageCode = currencySymbol
        }
        
        if let languageName = dict["name"] as? String{
            strName = languageName
        }
        
        if let languageID = dict["languageID"] as? String{
            strID = languageID
        }
        
        if let GenderId = dict["id"] as? String{
            strID = GenderId
        }else if let GenderId = dict["id"] as? Int{
            strID = "\(GenderId)"
        }
        
        
        if let GenderId = dict["value"] as? String{
            strName = GenderId
        }
        
        
        if let parentCategoryId = dict["parentCategoryId"]  as? String{
            strID = parentCategoryId
        }else if let parentCategoryId = dict["parentCategoryId"]  as? Int{
            strID = "\(parentCategoryId)"
        }
        if let categoryName = dict["category_name"] as? String{
            strName = categoryName
        }
        
        ////
        if let categoryId = dict["categoryId"] as? String{
            strID = categoryId
        }else if let countryID = dict["categoryId"]  as? Int{
            strID = "\(countryID)"
        }
        if let categoryName = dict["category_name"] as? String{
            strName = categoryName
        }
        if let parentCategoryId = dict["parent_category_id"] as? String{
            strParendID = parentCategoryId
        }else if let parentCategoryId = dict["parent_category_id"]  as? Int{
            strParendID = "\(parentCategoryId)"
        }
        ////
        
        if let arrSubCat = dict["sub_category"] as? [[String:Any]]{
            for subCat in arrSubCat{
                let objSub = CommonModal.init(dict: subCat)
                self.arrSubCategory.append(objSub)
            }
            
        }
        
        if let creditName = dict["name"] as? String{
                          strName = creditName
                      }
               if let creditID = dict["id"]  as? String{
                         strID = creditID
                     }else if let creditID = dict["id"]  as? Int{
                         strID = "\(creditID)"
                     }
               
               if let creditValue = dict["value"]  as? String{
                         strValue = creditValue
                     }else if let creditValue = dict["value"]  as? Int{
                         strValue = "\(creditValue)"
                     }else if  let creditValue = dict["value"]  as? Double{
                       strValue = "\(creditValue)"
        }
    }
    
}
