//
//  ViewController.swift
//  Star
//
//  Created by ios-deepak b on 27/01/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Foundation
import CoreLocation
class LoginVC: UIViewController {
    
    
    // MARK:- Outlets
    @IBOutlet weak var vwEmail: UIView!
    @IBOutlet weak var vwPassword: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var btnForgotPwd: UIButton!
    @IBOutlet weak var btnInfluencer: UIButton!
    @IBOutlet weak var btnCreateAccount: UIButton!
    @IBOutlet weak var lblRememberMe: UILabel!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var lblLine: UILabel!
    
    
    
    // MARK:- Varibles
    var iconClick = true
    var isRemember = true
    var dictDataGmail = [AnyHashable: Any]()
    var dictFaceBookData : [String : AnyObject]!
    var strUserSocialId = ""
    var strUserSocialEmail = ""
    var strUserSocialType = ""
    var strUsername = ""
    var strPic = ""
    var strSocialStatus = 0
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.isRememberData()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        GIDSignIn.sharedInstance().delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        //print("objAppShareData.arrCountryData.strCountryID.count is \(objAppShareData.arrCountryData.strCountryID.count)")
        
        //              if objAppShareData.UserDetail.stronboarding_step == "2"
        //              {
        //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "UploadPhotoVC") as! UploadPhotoVC
        //                self.navigationController?.pushViewController(vc, animated: true)
        //              }
        let color = UIColor(named: "bordercolor")
        self.vwEmail.setCornerRadBoarder(color: color!, cornerRadious: 4)
        self.vwPassword.setCornerRadBoarder(color: color!, cornerRadious: 4)
        self.btnLogin.setCornerRadius(cornerRadious: 4)
        self.btnFacebook.setCornerRadius(cornerRadious: 4)
        self.btnGoogle.setCornerRadius(cornerRadious: 4)
        // self.imgCheck.setCornerRadiusWithBoarder(color:#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1),cornerRadious: 4 ,photoImageView: self.imgCheck)
        self.txtEmail.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Email ID or Phone Number")
        self.txtPassword.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Password")
        
        
    }
    // MARK:- Buttons Action
    @IBAction func btnShowPwd(_ sender: UIButton) {
        self.view.endEditing(true)
        
        
        if(iconClick == true) {
            self.imgPassword.image = #imageLiteral(resourceName: "open_view_ico")
            txtPassword.isSecureTextEntry = false
        } else {
            self.imgPassword.image = #imageLiteral(resourceName: "close_view_ico")
            txtPassword.isSecureTextEntry = true
        }
        
        iconClick = !iconClick
        
        
    }
    @IBAction func btnCreateAccount(_ sender: UIButton) {
        self.view.endEditing(true)
       let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationVC") as! OtpVerificationVC
        // let vc = self.storyboard?.instantiateViewController(withIdentifier: "BasicInfoVC") as! BasicInfoVC
       // let vc = self.storyboard?.instantiateViewController(withIdentifier: "UploadPhotoVC") as! UploadPhotoVC
        self.navigationController?.pushViewController(vc, animated: true)
        
      // objAppDelegate.showUserTabbar()
    }
    
    @IBAction func btnLoginAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.imgCheck.image == #imageLiteral(resourceName: "inactive_check_ico"){
            self.rememberEmailPassword(value: false)
        }else{
            self.rememberEmailPassword(value: true)
        }
        if self.validateEmailMobile(with: self.txtEmail.text ?? "") {
        }
    }
    @IBAction func btnForgotPwdAction(_ sender: UIButton) {
        self.view.endEditing(true)
        //  objAlert.showAlert(message: Development, title: kAlertTitle, controller: self)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        //                let sb = UIStoryboard(name: "Discover", bundle: nil)
        //                let vc = sb.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        //                self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    @IBAction func btnRememberPwdAction(_ sender: UIButton) {
        if self.imgCheck.image == #imageLiteral(resourceName: "inactive_check_ico"){
            self.rememberEmailPassword(value: true)
        }else{
            self.rememberEmailPassword(value: false)
        }
    }
    
    func rememberEmailPassword(value:Bool){
        self.view.endEditing(true)
        let ud = UserDefaults.standard
       // let value = ud.value(forKey:"Alwayssign") as? String ?? ""
        
        if value == false{
            self.imgCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
           // ud.removeObject(forKey: "Alwayssign")
            ud.removeObject(forKey: "AlwasysEmail")
            ud.removeObject(forKey: "AlwaysPassword")
        }else{
            self.imgCheck.image = #imageLiteral(resourceName: "active_check_ico")
            //ud.set("true", forKey:"Alwayssign")
            ud.set(self.txtEmail.text, forKey:"AlwasysEmail")
            ud.set(self.txtPassword.text, forKey:"AlwaysPassword")
        }
    }
    
    @IBAction func btnFacebookAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.faceBookLoginMethod()
        
        if self.dictFaceBookData != nil {
            for (key, value) in self.dictFaceBookData {
                print(key, value)
            }
        }
        
        // self.getFacebookData()
        
    }
    
    
    @IBAction func btnGoogleAction(_ sender: UIButton) {
        self.view.endEditing(true)
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    
    @IBAction func btnInfluencerAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let sb = UIStoryboard.init(name: "Influencer", bundle:Bundle.main)
        let vc = sb.instantiateViewController(withIdentifier:"LoginInfluencerVC") as! LoginInfluencerVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func saveUserLoggedState() {
        // UserDefaults.standard.set(strUsername, forKey: “username”)
        UserDefaults.standard.set(1, forKey: "Key") //Integer
        UserDefaults.standard.set(1, forKey: "Button") //Integer
        UserDefaults.standard.synchronize()
        
        let id = UserDefaults.standard.integer(forKey: "Key")
        print("id is \(id)")
    }
    
    //    func getFacebookData(){
    //        let objFBSDKLoginManager : LoginManager = LoginManager()
    //        objFBSDKLoginManager.logOut()
    //        SocialUtility.shared.getFacebookData(sender: self, completionHandler: {[weak self] (sucess, userDict) in
    //            guard let weakSelf = self else {return}
    //            if sucess == true, let responseDict = userDict{
    //                //   objWebServiceManager.showIndicator()
    //                self!.strUserSocialType = "2"
    //                //                     objAppShareData.isfromFacebookLgin = true
    //                self!.dictFaceBookData = responseDict as! [String : AnyObject]
    //                if let socialData = self!.dictFaceBookData{
    //                    self?.strUserSocialEmail = socialData["email"] as? String ?? ""
    //                    self?.strUserSocialId = socialData["id"] as? String ?? ""
    //                    // self?.strSocialIdGender = socialData["gender"] as? String ?? ""
    //                    self?.strUsername = socialData["name"] as? String ?? ""
    //
    //                    //            self!.strPic = "http://graph.facebook.com/" + self!.strUserSocialId + "/picture?width=500&height=500"
    //                    //
    //                    //                               print("str pic is\(self!.strPic)")
    //
    //                    //                    let url = socialData["picture"] as? String ?? ""
    //                    //                              if let pic = url["data"] as? [String:Any]{
    //                    //
    //                    //                                self?.strPic = pic["url"]  as? String ?? ""
    //                    //                    print(socialData)
    //                    //                    }
    //                    print(socialData)
    //                }
    //                self?.callWebserviceForCheckSocailLogin()
    //
    //            }
    //            }
    //            , isForFBFriends: false, completionHandlerFriends: nil)
    //
    //    }
    
    func navigateToTabbar(){
//        if #available(iOS 13.0, *) {
//            objSceneDelegate.showUserTabbar()
//        }else{
//            objAppDelegate.showUserTabbar()
//        }
         objAppDelegate.showUserTabbar()
    }
    
    func navigateToSignup(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BasicInfoVC") as! BasicInfoVC
        vc.strUsername = self.strUsername
        vc.strUserSocialId = self.strUserSocialId
        vc.strUserSocialEmail = self.strUserSocialEmail
        vc.strUserSocialType = self.strUserSocialType
        vc.strPic = self.strPic
        vc.isFromSocial = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //    func CheckEmail(){
    //        if self.strUserSocialEmail.isEmpty{
    //           navigateToSignup()
    //        }else{
    //             self.callWebserviceForCheckSocailLogin()
    //        }
    //
    //    }
}

extension LoginVC : UITextFieldDelegate{
    
    // TextField delegate method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtEmail{
            txtEmail.resignFirstResponder()
            txtPassword.becomeFirstResponder()
        }
            
        else if textField == txtPassword{
            txtPassword.resignFirstResponder()
        }
        
        return true
    }
    
    
}
// MARK:- Google SignIn
extension LoginVC : GIDSignInDelegate
{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if (error) != nil {
            // handle here the error case
        }else {
            let userId = user.userID
            // For client-side use only!
            _ = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let email = user.profile.email
            let dimension = round(100 * UIScreen.main.scale)
            let pic = user.profile.imageURL(withDimension: UInt(dimension))
            let urlString = pic!.absoluteString
            // dictDataGmail = ["userId": userId!,"fullName":fullName!,"email":email!,"pic":urlString]
            // print("Gmail-----",dictDataGmail)
            // print("urlString-----",urlString)
            
            self.strUserSocialId = user.userID
            self.strUserSocialEmail = user.profile.email
            self.strUserSocialType = "1"
            self.strUsername = user.profile.name
            self.strPic = urlString
            
            self.callWebserviceForCheckSocailLogin()
            
            
            GIDSignIn.sharedInstance().signOut()
            
        }}
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
    }
    
    //Google sign
    func sign(_ signIn: GIDSignIn!,present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: false, completion: nil)
    }
}
// MARK:- Facebook SignIn
extension LoginVC
{
    func faceBookLoginMethod(){
        //action of the custom button in the storyboard
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    print("Succesfull Login")
                    self.getFBUserData()
                }
            }
        }
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result ?? "")
                    self.dictFaceBookData = (result as! [String : AnyObject])
                    if self.dictFaceBookData != nil {
                        if let socialData = self.dictFaceBookData{
                            self.strUserSocialEmail = socialData["email"] as? String ?? ""
                            self.strUserSocialId = socialData["id"] as? String ?? ""
                            self.strUsername = socialData["name"] as? String ?? ""
                            
                            self.strPic = "http://graph.facebook.com/" + self.strUserSocialId + "/picture?width=500&height=500"
                            
                            //   print("str pic is\(self.strPic)")
                            
                            self.strUserSocialType = "2"
                            self.callWebserviceForCheckSocailLogin()
                        }
                        
                    }
                }
            })
        }
    }
}

extension LoginVC {
    
    
    func isRememberData(){
        let ud = UserDefaults.standard
      
        print(ud.value(forKey:"AlwasysEmail") as? String ?? "")
        self.txtEmail.text = ud.value(forKey:"AlwasysEmail") as? String ?? ""
        self.txtPassword.text = ud.value(forKey:"AlwaysPassword") as? String ?? ""
        //     let value = ud.value(forKey: "Alwayssign") as? String ?? ""
        
        if self.txtEmail.text != ""{
            self.imgCheck.image = #imageLiteral(resourceName: "active_check_ico")
        }else{
            self.imgCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
        }
    }
    
    
    func validateEmailMobile(with Email: String) -> Bool {
        
        self.txtEmail.text = self.txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtPassword.text = self.txtPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (txtEmail.text?.isEmpty)! {
            objAlert.showAlert(message: BlankEmailAndPhoneNumber, title:kAlertTitle, controller: self)
        }
        else{
            if objValidationManager.validateEmail(with: txtEmail.text ?? ""){
                self.SucessReminder(strMsg: "Email")
            }
            else if objValidationManager.isvalidatePhone(value: txtEmail.text ?? ""){
                self.SucessReminder(strMsg: "Phone")
            }
            else{
                objAlert.showAlert(message: ValidEmailPhone, title:kAlertTitle, controller: self)
            }
        }
        return  true
    }
    
    func SucessReminder(strMsg : String) {
        
        if (txtPassword.text?.isEmpty)! {
            objAlert.showAlert(message: BlankPassword, title:kAlertTitle, controller: self)
        }
        else if  objValidationManager.isPwdLenth(password: txtPassword.text!) {
            objAlert.showAlert(message: LengthPassword, title: kAlertTitle, controller: self)
        }
            
        else{
            self.callWebserviceForLogin()
        }
    }
    
}

extension LoginVC{
    
    func callWebserviceForLogin(){
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        var strLoginType = ""
        if (txtEmail.text?.contains("@"))!{
            strLoginType = "email"
        }else{
            strLoginType = "phone"
        }
        
        let param = [WsParam.Login_identifier:self.txtEmail.text ?? "",
                     WsParam.Password:self.txtPassword.text ?? "",
                     WsParam.deviceToken:objAppShareData.strDeviceToken,
                     WsParam.Dial_code:"",
                     WsParam.Login_type:strLoginType,
                     WsParam.User_type: "user"
            ] as [String : Any]
        
        print(param)
        
        objWebServiceManager.requestPost(strURL: WsUrl.Login, params: param, strCustomValidation: "", showIndicator: false, success: {response in
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if status == k_success{
                objWebServiceManager.hideIndicator()
                if let data = response["data"]as? [String:Any]{
                    
                    if status == "success" ||  status == "Success"
                    {
                        let dic  = response["data"] as? [String:Any]
                        
                        let user_details  = dic!["user_details"] as? [String:Any]
                        self.saveDataInDeviceLogin(dict:user_details!)
                        objAppShareData.SaveUpdateUserInfoFromAppshareData(userDetail: user_details ?? [:])
                        
                        if let contents = dic?["content"]as? [String:Any]{
                            UserDefaults.standard.setValue(contents, forKey: UserDefaults.KeysDefault.contentURLs)
                        }
                        
                        objAppShareData.fetchUserInfoFromAppshareData()
                        
                        self.navigateToTabbar()
                        
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                    }
                }
            }
            else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
    }
    
    
    func saveDataInDeviceLogin(dict : [String:Any])
    {
    UserDefaults.standard.setValue(dict["auth_token"], forKey:"Authtoken")
    UserDefaults.standard.setValue(dict["total"], forKey:"Stars")
        UserDefaults.standard.setValue(dict["avatar"], forKey:"ProfilePic")
    }
    
    func callWebserviceForCheckSocailLogin(){
        
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        var param = [String:Any]()
        
        param = ["email":self.strUserSocialEmail,
                 "social_id":self.strUserSocialId,
                 "social_type":self.strUserSocialType ,
                 "device_token": objAppShareData.strDeviceToken,
                 "profile_image":self.strPic
            
        ]
        
        print(param)
        
        objWebServiceManager.requestPut(strURL: WsUrl.CheckSocialSignup, params: param, strCustomValidation: "", showIndicator: false, success: {
            response in
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if status == k_success{
                objWebServiceManager.hideIndicator()
                if let data = response["data"]as? [String:Any]{
                    
                    if status == "success" ||  status == "Success"
                    {
                        let dic  = response["data"] as? [String:Any]
                        
                        // self.strSocialStatus  = dic!["social_status"] as? Int ?? 0
                        let socialStatus  = dic!["social_status"] as? Int ?? 0
                        print("social_status is \(socialStatus)")
                        
                        if socialStatus == 0{
                            objWebServiceManager.hideIndicator()
                            self.navigateToSignup()
                            
                        }else{
                            objWebServiceManager.hideIndicator()
                            
                            if status == "success" ||  status == "Success"
                            {
                                let dic  = response["data"] as? [String:Any]
                                
                                let user_details  = dic!["user_details"] as? [String:Any]
                                self.saveDataInDeviceLogin(dict:user_details!)
                                objAppShareData.SaveUpdateUserInfoFromAppshareData(userDetail: user_details ?? [:])
                                objAppShareData.fetchUserInfoFromAppshareData()
                                self.navigateToTabbar()
                            }
                        }
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                    }
                }
            }
            else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
        
        
    }
        
    
    
}


class CustomTextField: UITextField {
var enableLongPressActions = false
required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
}
override init(frame: CGRect) {
    super.init(frame: frame)
}
override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    return enableLongPressActions
}
}

