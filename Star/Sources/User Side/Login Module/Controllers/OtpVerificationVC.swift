//
//  OtpVerificationVC.swift
//  Star
//
//  Created by ios-deepak b on 28/01/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//


import UIKit

class OtpVerificationVC: UIViewController , RSCountrySelectedDelegate{
    
    // MARK:- Outlets
    
    @IBOutlet weak var vwNumber: UIView!
    @IBOutlet weak var vwVerifyOtp: UIView!
    @IBOutlet weak var vwGetOtp: UIView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblOtp: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var btnGetOtp: UIButton!
    @IBOutlet weak var btnVerifyOtp: UIButton!
    @IBOutlet weak var txtNumber: UITextField!
    //    @IBOutlet weak var txtOtp1: UITextField!
    //    @IBOutlet weak var txtOtp2: UITextField!
    //    @IBOutlet weak var txtOtp3: UITextField!
    //    @IBOutlet weak var txtOtp4: UITextField!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var imgback: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet var txtOTP: [UITextField]!
    
    
    // MARK:- Varibles
    let strSendotp = "We will send you an One Time Password on this mobile number."
    
    var strCountryName = ""
    var textFieldsIndexes:[UITextField:Int] = [:]
    var strCountryCode = ""
    var strCountryDialCode = ""
    
    var digitArray = [String]()
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblCode.text = "+1"
        
        for index in 0 ..< txtOTP.count {
            textFieldsIndexes[txtOTP[index]] = index
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.imgHeader.image = #imageLiteral(resourceName: "mobile_verify_ico")
        self.txtNumber.resignFirstResponder()
        for index in txtOTP{
            index.text = ""
        }
        //  print("count is \( self.txtOTP.count)")
        self.vwGetOtp.isHidden = false
        self.vwVerifyOtp.isHidden = true
        let color = UIColor(named: "bordercolor")
        self.vwNumber.setCornerRadBoarder(color: color!, cornerRadious: 8)
        self.btnGetOtp.setCornerRadius(cornerRadious: 4)
        self.btnVerifyOtp.setCornerRadius(cornerRadious: 4)
        self.txtNumber.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Phone Number")
        let colorOtp = UIColor(named: "button_background")
        self.lblOtp.attributedText = coloredLabel(str: self.strSendotp, color: colorOtp! , location: 20, length: 18, size: 15.0)
        
    }
    // MARK:- Buttons Action
    @IBAction func btnCountryCodeAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.txtNumber.resignFirstResponder()
        let sb = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RSCountryPickerController")as! RSCountryPickerController
        sb.RScountryDelegate = self
        sb.strCheckCountry = self.strCountryName
        sb.modalPresentationStyle = .fullScreen
        self.navigationController?.present(sb, animated: true, completion: nil)
    }
    
    @IBAction func btnGetOtpAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.validationPhoneNo()
        //  self.vwGetOtp.isHidden = true
        //  self.vwVerifyOtp.isHidden = false
    }
    
    
    @IBAction func btnResendOtpAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.callWebserviceForSendVerficationCode()
        
    }
    @IBAction func btnVerifyOtpAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.validation()
        //  self.navigationForConfirmation()
        
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        
        if self.vwGetOtp.isHidden == true {
            self.vwVerifyOtp.isHidden = true
            self.vwGetOtp.isHidden = false
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    func coloredLabel(str : String , color : UIColor ,location : Int,length : Int,size : Float) -> NSAttributedString{
        
        var myMutableString = NSMutableAttributedString(string: str, attributes: [NSAttributedString.Key.font :UIFont(name: "Montserrat-Regular", size: CGFloat(size))!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSRange(location: location,length: length))
        
        
        return myMutableString
    }
    
    func RScountrySelected(countrySelected country: CountryInfo) {
        
        let imagePath = "CountryPicker.bundle/\(country.country_code).png"
        self.imgFlag.image = UIImage(named: imagePath)
        strCountryDialCode = country.dial_code
        self.lblCode.text = country.dial_code
        strCountryCode = country.country_code
        self.strCountryName = country.country_name
        self.txtNumber.becomeFirstResponder()
    }
    
    func validation(){
        ///*
        self.digitArray.removeAll()
        for i in 0...3{
            digitArray.append(txtOTP[i].text!)
        }
        
        if digitArray.contains("") {
            objAlert.showAlert(message:VerifyCode, title: "Alert", controller: self)
        }else{
            
            self.callWebserviceForVerficationCode()
        }
        
    }
    
    func VerifyOTP()
    {
        self.vwGetOtp.isHidden = true
        self.vwVerifyOtp.isHidden = false
        self.imgHeader.image = #imageLiteral(resourceName: "otp_ico")
        let strNumber = self.txtNumber.text!
        let codeno =  self.lblCode.text!
        let no = codeno + " - " + strNumber
        self.lblNumber.text = no
    }
    
    func navigationForConfirmation(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BasicInfoVC") as! BasicInfoVC
        vc.strPhoneNumber = self.txtNumber.text ?? ""
        vc.strPhoneDialCode = self.strCountryDialCode
        vc.strCountryCode = self.strCountryCode
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func validationPhoneNo(){
        
        self.txtNumber.text = self.txtNumber.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if (txtNumber.text?.isEmpty)! {
            objAlert.showAlert(message: BlankPhoneNo, title:kAlertTitle, controller: self)
            
        }else if !objValidationManager.isValidId(Id: txtNumber.text ?? ""){
            objAlert.showAlert(message: ValidPhone, title:kAlertTitle, controller: self)
        }
        else if  objValidationManager.isPwdLenth(password: txtNumber.text!) {
            objAlert.showAlert(message: ValidPhone, title: kAlertTitle, controller: self)
        }
        else if  self.lblCode.text == "+1" {
            objAlert.showAlert(message: CountrySelection, title: kAlertTitle, controller: self)
        }
            
        else{
            self.callWebserviceForSendVerficationCode()
            
        }
    }
}
extension OtpVerificationVC : UITextFieldDelegate{
    
    
    enum Direction { case left, right }
    
    func setNextResponder(_ index:Int?, direction:Direction) {
        
        guard let index = index else { return }
        
        if direction == .left {
            index == 0 ?
                (_ = txtOTP.first?.resignFirstResponder()) :
                (_ = txtOTP[(index - 1)].becomeFirstResponder())
        } else {
            index == txtOTP.count - 1 ?
                (_ = txtOTP.last?.resignFirstResponder()) :
                (_ = txtOTP[(index + 1)].becomeFirstResponder())
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.length == 0 {
            textField.text = string
            setNextResponder(textFieldsIndexes[textField], direction: .right)
            return true
        } else if range.length == 1 {
            textField.text = ""
            setNextResponder(textFieldsIndexes[textField], direction: .left)
            return false
        }
        
        return false
        
    }
    
}

//MARK:- Signup and social signup Api
extension OtpVerificationVC {
    
    func callWebserviceForSendVerficationCode(){
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        let param = [WsParam.Phone:self.txtNumber.text ?? "",
                     WsParam.Dial_code:self.strCountryDialCode,
                     WsParam.Phone_country_code:self.strCountryCode
            ] as [String : Any]
        
        print(param)
        
        objWebServiceManager.requestPost(strURL: WsUrl.SendVerificationCode, params: param, strCustomValidation: "", showIndicator: false, success: {response in
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if status == k_success{
                objWebServiceManager.hideIndicator()
                if let data = response["data"]as? [String:Any]{
                    
                    if status == "success" ||  status == "Success"
                    {
                        self.txtOTP = self.txtOTP.map({ (fld) -> UITextField in
                            fld.text = nil
                            return fld
                        })
                        self.VerifyOTP()
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                    }
                }
                
            }
            else{
                objWebServiceManager.hideIndicator()
                self.txtOTP = self.txtOTP.map({ (fld) -> UITextField in
                    fld.text = nil
                    return fld
                })
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
    }
    
    func callWebserviceForVerficationCode(){
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        let strCode = (digitArray.map{String($0)}).joined(separator: "")
        let param = [WsParam.Phone:self.txtNumber.text ?? "",
                     WsParam.Dial_code:self.strCountryDialCode,
                     WsParam.Phone_country_code:self.strCountryCode,
                     WsParam.Verify_code:strCode
            ] as [String : Any]
        
        print(param)
        
        objWebServiceManager.requestPost(strURL: WsUrl.VerifyCode, params: param, strCustomValidation: "", showIndicator: false, success: {response in
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if status == k_success{
                objWebServiceManager.hideIndicator()
                if let data = response["data"]as? [String:Any]{
                    
                    if status == "success" ||  status == "Success"
                    {
                        self.navigationForConfirmation()
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                    }
                }
            }
            else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
    }
    
    
}


