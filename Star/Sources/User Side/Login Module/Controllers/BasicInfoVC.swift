//
//  BasicInfoVC.swift
//  Star
//
//  Created by ios-deepak b on 28/01/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit
//MARK: step 4 conform the protocol here.
class BasicInfoVC: UIViewController , BottomSheetVCDelegate{
    
    
    // MARK:- Outlets
    @IBOutlet weak var vwEmail: UIView!
    @IBOutlet weak var vwPassword: UIView!
    @IBOutlet weak var vwUsername: UIView!
    @IBOutlet weak var vwGender: UIView!
    @IBOutlet weak var vwDOB: UIView!
    @IBOutlet weak var vwLanguage: UIView!
    @IBOutlet weak var vwSignIn: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtLanguage: UITextField!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var btnGender: UIButton!
    @IBOutlet weak var btnLanguage: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var imgGender: UIImageView!
    @IBOutlet weak var imgLanguage: UIImageView!
    @IBOutlet weak var vwBlur: UIView!
    @IBOutlet weak var vwSelectGender: UIView!
  //  @IBOutlet var btnDone: UIButton!
    @IBOutlet var imgMaleCheck: UIImageView!
    @IBOutlet var imgFemaleCheck: UIImageView!

    
    // MARK:- Varibles
    var iconClick = true
    let datePicker = UIDatePicker()
    var strBottomSheetFrom = ""
    var strIndex = 0
    var strPhoneNumber = ""
    var strPhoneDialCode = ""
    var strCountryCode = ""
    var arrCountry = [CountryModel]()
    var strCountry_Id = ""
    var strCountry_Code = ""
    var strDial_Code = ""
    
    // var strCountry_Id = 0
    var arrLanguages = [CommonModal]()
    var arrGender = [CommonModal]()
    var arrTxtID:[Int] = []
    var arrTxtName:[String] = []
    var strGenderName =  ""
    var strGender = ""
    var strUserSocialId = ""
    var strUserSocialEmail = ""
    var strUserSocialType = ""
    var strUsername = ""
    var isFromSocial = false
    var strPic = ""
    var dictSocial = [String:Any]()
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadCountryArray()
        // self.callWebserviceGetLangvages()
        self.callWebserviceGetCountries()
        //  self.loadArray()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let color = UIColor(named: "bordercolor")
        
        self.vwEmail.setCornerRadBoarder(color: color!, cornerRadious: 8)
        self.vwPassword.setCornerRadBoarder(color: color!, cornerRadious: 8)
        self.vwUsername.setCornerRadBoarder(color: color!, cornerRadious: 8)
        self.vwDOB.setCornerRadBoarder(color: color!, cornerRadious: 8)
        self.vwGender.setCornerRadBoarder(color: color!, cornerRadious: 8)
        self.vwLanguage.setCornerRadBoarder(color: color!, cornerRadious: 8)
        self.btnNext.setCornerRadius(cornerRadious: 4)
        self.txtUsername.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Fullname")
        self.txtEmail.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Email Id")
        self.txtPassword.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Password")
        self.txtGender.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Select Gender")
        self.txtDOB.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Select date of birth")
        self.txtLanguage.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Select Languages")
         self.vwBlur.isHidden = true
        self.vwSelectGender.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
        showDatePicker()
        self.arrLanguages = objAppShareData.arrLanguageData
        self.arrGender = objAppShareData.arrGenderData
        //self.arrGender = objAppShareData.arrCountryData
        
        if self.isFromSocial == true  {
            self.vwPassword.isHidden = true
            self.txtUsername.text = self.strUsername
            self.txtEmail.text = self.strUserSocialEmail
        }
        
    }
    
    func loadArray(){
        //        let arrGender1 = BasicInfoModal(title: "Male", isSelect: false,isGender: true)
        //        let arrGender2 = BasicInfoModal(title: "Female", isSelect: false,isGender: true)
        //        arrGender.append(arrGender1)
        //        arrGender.append(arrGender2)
    }
    
    
    
    
    // MARK:- Buttons Action
    
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.ValidationForSingIn()
        //    navigationForUpload()
    }
    
    @IBAction func btnShowPwd(_ sender: UIButton) {
        self.view.endEditing(true)
        
      
         if(iconClick == true) {
                 self.imgPassword.image = #imageLiteral(resourceName: "open_view_ico")
                 txtPassword.isSecureTextEntry = false
             } else {
                 self.imgPassword.image = #imageLiteral(resourceName: "close_view_ico")
                 txtPassword.isSecureTextEntry = true
             }
             
             iconClick = !iconClick
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnMaleAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
         self.imgMaleCheck.image = #imageLiteral(resourceName: "active_check_ico")
         self.imgFemaleCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
        
    }
    @IBAction func btnFemaleAction(_ sender: UIButton) {
           self.view.endEditing(true)

        self.imgFemaleCheck.image = #imageLiteral(resourceName: "active_check_ico")
        self.imgMaleCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
        
        
        
       }
    @IBAction func btnDoneAction(_ sender: UIButton) {
        self.view.endEditing(true)
           
        self.vwBlur.isHidden = true
            
      if self.imgMaleCheck.image == #imageLiteral(resourceName: "active_check_ico"){
            self.txtGender.text = "Male"
        self.strGenderName = "Male"
        strGender = "1"
      }else if self.imgFemaleCheck.image == #imageLiteral(resourceName: "active_check_ico"){
        self.txtGender.text = "Female"
        self.strGenderName = "Female"
        strGender = "2"
      }else{
            objAlert.showAlert(message: "Please select any option", title: "Alert", controller: self)
            return
        }
        
        
       // self.imgMaleCheck.image = #imageLiteral(resourceName: "inactive_check_ico")

          
       }
    @IBAction func btnCancleAction(_ sender: UIButton) {
           self.view.endEditing(true)
         self.vwBlur.isHidden = true
           
       }
    @IBAction func btnSelectGender(_ sender: UIButton) {
        self.view.endEditing(true)
//        let str = "Select Gender"
//        self.strBottomSheetFrom = "gender"
//        print("self.arrGender count is \(self.arrGender.count)")
//        self.showBotttomSheet(arr: self.arrGender,str: str)
        
        self.vwBlur.isHidden = false
        
    }
    @IBAction func btnSelectLanguage(_ sender: UIButton) {
        self.view.endEditing(true)
        self.strBottomSheetFrom = "language"
        let str = "Select Languages"
        print("self.arrLanguages count is \(self.arrLanguages.count)")
        
        ////
        let strLanguageID = (arrTxtID.map{String($0)}).joined(separator: ",")
        let arr = strLanguageID.components(separatedBy: ",")
        if arr.count>0{
            let arrInt = arr.map { Int($0) }
            if arrInt.count>0{
                let int = arrInt[0]
                if int != nil{
                    objAppShareData.arrSelectedBottomIds = arrInt as! [Int]
                }
            }
        }
        let arrName = self.txtLanguage.text!.components(separatedBy: ",")
        if arrName.count>0{
            objAppShareData.arrSelectedBottomNames = arrName
        }
        ////
        
        self.showBotttomSheet(arr: self.arrLanguages,str: str)
        
    }
    
    
    func showBotttomSheet(arr : Array<Any>, str: String ){
        
        let sb = UIStoryboard.init(name: "Main", bundle:Bundle.main)
              let vc = sb.instantiateViewController(withIdentifier:"BottomSheetVC") as! BottomSheetVC
        
        vc.arrBottom = arr as! [CommonModal]
        vc.strHeader = str
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
        
    }
    //MARK: step 6 finally use the method of the contract
    // func sendDataToFirstViewController(myData: [Int]) {
    func sendDataToFirstViewController(arrId:[Int], arrName:[String]) {
//        if self.strBottomSheetFrom == "gender"{
//            print(" my gender is \(arrId)")
//            print(" my gender  is \(arrName)")
//            let varText = arrName.joined(separator:", ")
//
//            self.txtGender.text = varText
//        }else if self.strBottomSheetFrom == "language"{
            print(" my language is \(arrId)")
            print(" my language is \(arrName)")
            let varText = arrName.joined(separator:",")
            self.arrTxtID = arrId
            self.txtLanguage.text = varText
            
             let strLanguageID = (arrTxtID.map{String($0)}).joined(separator: ",")
            print("strLanguageID is \(strLanguageID)")
    //    }
        
    }
    
    
    //MARK:- Get Country ID //Rohit
    func loadCountryArray(){
        var userCountryCode = ""
        if let countryCode = objAppShareData.dictUserLoction["countryCode"]as? String{
            userCountryCode = countryCode
        }
        if userCountryCode == ""{
            if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
                userCountryCode = countryCode
            }
        }
        
        if objAppShareData.arrCountryData.count != 0{
            let filteredArray = objAppShareData.arrCountryData.filter() { $0.strCountryCode == userCountryCode }
            if filteredArray.count != 0{
                let obj = filteredArray[0]
               // print(obj.strCountryCode)
                self.strCountry_Id = obj.strID
                self.strCountry_Code  = obj.strCountryCode
                self.strDial_Code   = obj.strDialCode
                
                
                
            }
            //            for dict in objAppShareData.arrCountryData{
            //                let objCountryCode = dict.strCountryCode
            //
            //                if objCountryCode == userCountryCode{
            //                    self.strCountry_Id = dict.strCountryID
            //                }
            //            }
        }
//        print(self.strCountry_Id)
//         print(self.strDial_Code)
//         print(self.strCountry_Code)
//
        
    }
    
}
// MARK:- Validation
extension BasicInfoVC {
    func ValidationForSingIn(){
        self.txtEmail.text = self.txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtPassword.text = self.txtPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtDOB.text = self.txtDOB.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtGender.text = self.txtGender.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtUsername.text = self.txtUsername.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtLanguage.text = self.txtLanguage.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        //
        if (txtUsername.text?.isEmpty)! {
            objAlert.showAlert(message: BlankUsername, title:kAlertTitle, controller: self)
        }else if !objValidationManager.validateNameStrings(txtUsername.text!){
            objAlert.showAlert(message: InvalidName, title:kAlertTitle, controller: self)
        }
        else if objValidationManager.isNamedLenth(password: txtUsername.text!) {
            objAlert.showAlert(message: LengthName, title: kAlertTitle, controller: self)
        }
            //
        else if (txtEmail.text?.isEmpty)! {
            objAlert.showAlert(message: BlankEmail, title:kAlertTitle, controller: self)
        }else if !objValidationManager.validateEmail(with: txtEmail.text!){
            objAlert.showAlert(message: ValidEmail, title:kAlertTitle, controller: self)
        }
            //
        else  if (txtPassword.text?.isEmpty)! && self.isFromSocial == false {
            objAlert.showAlert(message: BlankPassword, title:kAlertTitle, controller: self)
        }
        else if  objValidationManager.isPwdLenth(password: txtPassword.text!) && self.isFromSocial == false {
            objAlert.showAlert(message: LengthPassword, title: kAlertTitle, controller: self)
        }
            //
        else if (txtGender.text?.isEmpty)! {
            objAlert.showAlert(message: BlankGender, title:kAlertTitle, controller: self)
        }
            
        else if (txtDOB.text?.isEmpty)! {
            objAlert.showAlert(message: BlankDOB, title:kAlertTitle, controller: self)
//        }else if !objValidationManager.isValidDate(dateString: txtDOB.text!){
//            objAlert.showAlert(message: InvalidDOB, title:kAlertTitle, controller: self)
        }else if (txtLanguage.text?.isEmpty)! {
            objAlert.showAlert(message: BlankLanguage, title:kAlertTitle, controller: self)
        }
        else{
            if self.isFromSocial == true{
                self.callWebserviceForSocialSignup()
            }
            else {
                self.callWebserviceForSingup()
            }
        }
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
    
    func navigationForUpload()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UploadPhotoVC") as! UploadPhotoVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
// MARK:- DatePicker
extension BasicInfoVC {
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        let currentDate = NSDate()
        //   datePicker.minimumDate = currentDate as Date
        
        datePicker.maximumDate = currentDate as Date
        datePicker.backgroundColor = UIColor(named: "background")
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(btnDone));
        doneButton.tintColor = UIColor(named: "button_background")
        
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(btnCancel));
        cancelButton.tintColor = UIColor.red
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        txtDOB.inputAccessoryView = toolbar
        txtDOB.inputView = datePicker
    }
    
    @objc func btnDone()
    {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        txtDOB.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func btnCancel(){
        self.view.endEditing(true)
    }
    
    
}
extension BasicInfoVC : UITextFieldDelegate{
    
    // TextField delegate method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtUsername{
            txtUsername.resignFirstResponder()
            txtEmail.becomeFirstResponder()
        }
        if textField == txtEmail{
            txtEmail.resignFirstResponder()
            txtPassword.becomeFirstResponder()
        }
        if textField == txtPassword{
            txtPassword.resignFirstResponder()
            txtGender.becomeFirstResponder()
        }
        if textField == txtGender{
            txtGender.resignFirstResponder()
            txtDOB.becomeFirstResponder()
        }
        if textField == txtDOB{
            txtDOB.resignFirstResponder()
            txtLanguage.becomeFirstResponder()
        }
            
        else if textField == txtLanguage{
            txtLanguage.resignFirstResponder()
        }
        
        return true
    }
    
    
}

extension BasicInfoVC{
    
    func callWebserviceForSingup(){
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        //        var strGender = ""
        //        if txtGender.text == "Male"
        //        {
        //            strGender = "1"
        //        }else{
        //            strGender = "2"
        //        }
        
        ///  let strLanguage = (arrLanguages.map{String($0.strLanguageName)}).joined(separator: ",")
        let strLanguage = (arrLanguages.map{String($0.strLanguageID)}).joined(separator: ",")
        
        let formattedArray = objAppShareData.arrGenderData
        
     //   let strGender = (formattedArray.map{String($0.strID)}).joined(separator: ",")
        
         let strLanguageID = (arrTxtID.map{String($0)}).joined(separator: ",")
        
        
        let strDoB = self.convertDateFormater(self.txtDOB.text ?? "")
        
        let param = [WsParam.Phone_number:strPhoneNumber,
                     WsParam.Phone_dial_code:strPhoneDialCode,
                     WsParam.Phone_country_code:strCountryCode,
                     WsParam.UserName:txtUsername.text ?? "",
                     WsParam.Email:txtEmail.text ?? "",
                     WsParam.Password:txtPassword.text ?? "",
                     WsParam.Gender:strGender,
                     WsParam.Language:strLanguageID,
                     WsParam.deviceToken:objAppShareData.strDeviceToken,
                     WsParam.DoB:strDoB,
                     WsParam.Country_Id:strCountry_Id,
            ] as [String : Any]
        
        print(param)
        
        objWebServiceManager.requestPost(strURL: WsUrl.Signup, params: param, strCustomValidation: "", showIndicator: false, success: {response in
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if status == k_success{
                objWebServiceManager.hideIndicator()
                if (response["data"]as? [String:Any]) != nil{
                    
                    if status == "success" ||  status == "Success"
                    {
                        let dic  = response["data"] as? [String:Any]
                        
                        let user_details  = dic!["user_details"] as? [String:Any]
                        self.saveDataInDeviceLogin(dict:user_details!)
                        objAppShareData.SaveUpdateUserInfoFromAppshareData(userDetail: user_details ?? [:])
                        objAppShareData.fetchUserInfoFromAppshareData()
                        self.navigationForUpload()
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                    }
                }
            }
            else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
    }
    
   func saveDataInDeviceLogin(dict : [String:Any])
    {
    UserDefaults.standard.setValue(dict["auth_token"], forKey:"Authtoken")
    UserDefaults.standard.setValue(dict["total"], forKey:"Stars")
    }
    
    
    
    func callWebserviceGetCountries(){
        objWebServiceManager.showIndicator()
        objWebServiceManager.requestGet(strURL: WsUrl.Countries, params: nil, strCustomValidation: "", success: {response in
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if status == k_success{
                objWebServiceManager.hideIndicator()
                if let data = response["data"]as? [String:Any]{
                    
                    if status == "success" ||  status == "Success"
                    {
                        let arrAllReqData = data["countries"] as! [[String: Any]]
                        
                        //  let countryCode = Locale.current.regionCode
                        for dictGetData in arrAllReqData
                        {
                            let objPendingData = CountryModel.init(dict: dictGetData)
                            
                            if objPendingData.Country_code == self.strCountryCode
                            {
                                self.strCountry_Id = objPendingData.CountryID
                            }
                            self.arrCountry.append(objPendingData)
                        }
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                    }
                }
            }
            else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
    }
    
    
}


//MARK:- Webservice Calling
extension BasicInfoVC {
    
    //    func callWebserviceGetLangvages(){
    //        objWebServiceManager.showIndicator()
    //        objWebServiceManager.requestGet(strURL: WsUrl.Languages, params: nil, strCustomValidation: "", success: {response in
    //            print(response)
    //            let status = (response["status"] as? String)
    //            let message = (response["message"] as? String)
    //            if status == k_success{
    //                objWebServiceManager.hideIndicator()
    //                if let data = response["data"]as? [String:Any]{
    //
    //                    if status == "success" ||  status == "Success"
    //                    {
    //                        let arrAllReqData = data["languages"] as! [[String: Any]]
    //                        for dictGetData in arrAllReqData
    //                        {
    //                            let objPendingData = BasicInfoModal.init(dict: dictGetData)
    //
    //                            self.arrLanguages.append(objPendingData)
    //                            print("dict type \(dictGetData)")
    //
    //                        }
    //                        print("founded all Languages counts befor append \(self.arrLanguages.count)")
    //                        print("founded all Languages \(self.arrLanguages)")
    //                        //["languageID": 1, "code": en, "name": English]
    //                        let objAllLang:[String:Any] = ["languageID": -1, "code": "allLang", "name": "All Languages"]
    //                        let objAllLngBasicInfo:BasicInfoModal = BasicInfoModal(dict: objAllLang)
    //                        self.arrLanguages = [objAllLngBasicInfo] + self.arrLanguages
    //
    //                        print("founded all Languages counts after append \(self.arrLanguages.count)")
    //                    }else{
    //                        objWebServiceManager.hideIndicator()
    //                        objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
    //                    }
    //                }
    //            }
    //            else{
    //                objWebServiceManager.hideIndicator()
    //                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
    //            }
    //        }, failure: { (error) in
    //            print(error)
    //            objWebServiceManager.hideIndicator()
    //            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
    //        })
    //    }
    
    
    func callWebserviceForSocialSignup(){
        
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
            
        }
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
       // let strLanguage = (arrLanguages.map{String($0.strLanguageID)}).joined(separator: ",")
        
        let formattedArray = objAppShareData.arrGenderData
        
        let strLanguageID = (arrTxtID.map{String($0)}).joined(separator: ",")
        
        
        var param = [String:Any]()
        let strDoB = self.convertDateFormater(self.txtDOB.text ?? "")
        param = ["email":self.txtEmail.text!,
                 "name":self.strUsername,
                 "social_id":self.strUserSocialId,
                 "social_type":self.strUserSocialType ,
                 "device_token": objAppShareData.strDeviceToken,
                 "avatar": self.strPic,
                 //"phone_number": "",
                 //"phone_dial_code": self.strDial_Code,
                 //"phone_dial_code": "",
                 //"phone_country_code": self.strCountry_Code,
                 "language": strLanguageID,
                 "country_id": self.strCountry_Id,
                 "gender":strGender,
                 "dob":strDoB
        ]
        
        print(param)
        
        objWebServiceManager.requestPost(strURL: WsUrl.SocialSignup, params: param, strCustomValidation: "", showIndicator: false, success: {response in
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if let data = response["data"] as? [String:Any]{
                objWebServiceManager.hideIndicator()
                if let userData = data["user_details"] as? [String:Any]{
                    let token = userData["auth_token"] as? String ?? ""
                    UserDefaults.standard.setValue(userData["auth_token"], forKey:"Authtoken")
                    print(" auth token is \(UserDefaults.standard.setValue(userData["auth_token"], forKey:"Authtoken"))")
                    
                    
                    //  self.saveDataInDeviceLogin(dict:token)
                    objAppShareData.SaveUpdateUserInfoFromAppshareData(userDetail: userData ?? [:])
                    objAppShareData.fetchUserInfoFromAppshareData()
                }
                //
                
                //
                self.navigateToTabbar()
                
            }
                
            else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
    }
    
    func navigateToTabbar(){
        //        if #available(iOS 13.0, *) {
        //            objSceneDelegate.showUserTabbar()
        //        }else{
        //            objAppDelegate.showUserTabbar()
        //        }
        objAppDelegate.showUserTabbar()
    }
}


