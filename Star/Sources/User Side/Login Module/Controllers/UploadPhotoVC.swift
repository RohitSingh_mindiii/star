//
//  UploadPhotoVC.swift
//  Star
//
//  Created by ios-deepak b on 28/01/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class UploadPhotoVC: UIViewController ,UIImagePickerControllerDelegate ,UINavigationControllerDelegate{
    
    // MARK:- Outlets
    
    
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var imgUpload: UIImageView!
    @IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var vwCamera: UIView!
    
    var imagePicker = UIImagePickerController()
    var pickedImage:UIImage?
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.imagePicker.delegate = self
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.vwCamera.backgroundColor = UIColor.clear
        self.btnUpload.setCornerRadius(cornerRadious: 4)
        //self.cornerImage(image: self.imgCamera,color:#colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1) ,width: 0.7 )
        
        //            self.vwCamera.layer.borderWidth = 0.7
        //            self.vwCamera.layer.borderColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        //            self.vwCamera.layer.cornerRadius = self.vwCamera.layer.frame.size.height / 2.1
    }
    // MARK:- Buttons Action
    
    
    @IBAction func btnUploadAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if self.pickedImage != nil{
            self.callWebserviceForUploadUserImage()
        }else{
            objAlert.showAlert(message: UploadImage, title:kAlertTitle, controller: self)      }
        
    }
    
    @IBAction func btnSkipAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.callWebserviceForUploadUserImage()
        
        
       // objAppDelegate.showUserTabbar()
        
    }
    
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCameraAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.setImage()
        
    }
    //
    
    // MARK:- UIImage Picker Delegate
    func setImage(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
    }
    
    // Open camera
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.modalPresentationStyle = .fullScreen
            self .present(imagePicker, animated: true, completion: nil)
        } else {
            self.openGallery()
        }
    }
    
    // Open gallery
    func openGallery()
    {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[.editedImage] as? UIImage {
            self.pickedImage = editedImage
            self.imgUpload.image = self.pickedImage
            //  self.cornerImage(image: self.imgUpload,color:#colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1) ,width: 0.5 )
            
            self.makeRounded()
            if self.imgUpload.image == nil{
                // self.viewEditProfileImage.isHidden = true
            }else{
                // self.viewEditProfileImage.isHidden = false
            }
            imagePicker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            self.pickedImage = originalImage
            self.imgUpload.image = pickedImage
            self.makeRounded()
            // self.cornerImage(image: self.imgUpload,color:#colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1) ,width: 0.5 )
            if self.imgUpload.image == nil{
                // self.viewEditProfileImage.isHidden = true
            }else{
                //self.viewEditProfileImage.isHidden = false
            }
            imagePicker.dismiss(animated: true, completion: nil)
        }
    }
    
    func cornerImage(image: UIImageView, color: UIColor ,width: CGFloat){
        image.layer.cornerRadius = image.layer.frame.size.height / 2
        image.layer.masksToBounds = false
        image.layer.borderColor = color.cgColor
        image.layer.borderWidth = width
        
    }
    
    func makeRounded() {
        
        self.imgUpload.layer.borderWidth = 1
        self.imgUpload.layer.masksToBounds = false
        //self.imgUpload.layer.borderColor = UIColor.blackColor().CGColor
        self.imgUpload.layer.cornerRadius = self.imgUpload.frame.height/2 //This will change with corners of image and height/2 will make this circle shape
        self.imgUpload.clipsToBounds = true
    }
    
}

extension UploadPhotoVC{
    
    func callWebserviceForUploadUserImage(){
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        var imageData : Data?
        if self.pickedImage != nil{
            imageData = (self.pickedImage?.jpegData(compressionQuality: 1.0))!
        }
        
        objWebServiceManager.uploadMultipartData(strURL: WsUrl.AvatarUpload, params: nil, showIndicator: false, imageData: imageData, fileName: WsParam.Profile_picture, mimeType: "image/jpeg", success: { response in
            
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            
            if status == k_success{
                objWebServiceManager.hideIndicator()
                let dic  = response["data"] as? [String:Any]
                let user_details  = dic!["user_details"] as? [String:Any]
                objAppShareData.SaveUpdateUserInfoFromAppshareData(userDetail: user_details ?? [:])
                objAppShareData.fetchUserInfoFromAppshareData()

                objAppDelegate.showUserTabbar()
            }else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
    }
    
}

