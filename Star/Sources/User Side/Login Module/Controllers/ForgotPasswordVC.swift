//
//  ForgotPasswordVC.swift
//  Star
//
//  Created by ios-deepak b on 08/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var vwEmail: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    
    
    // MARK:- Varibles
    
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let color = UIColor(named: "bordercolor")
        self.vwEmail.setCornerRadBoarder(color: color!, cornerRadious: 4)
        self.btnSend.setCornerRadius(cornerRadious: 4)
        self.txtEmail.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Email ID or Phone Number")
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.txtEmail.text  = self.txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (txtEmail.text?.isEmpty)! {
                   objAlert.showAlert(message: BlankEmail, title:kAlertTitle, controller: self)
               }else if !objValidationManager.validateEmail(with: txtEmail.text!){
                   objAlert.showAlert(message: ValidEmail, title:kAlertTitle, controller: self)
        }else{
            self.view.endEditing(true)
            call_wsForgotPassword(strEmail: self.txtEmail.text!)
        }
        
       // objAlert.showAlert(message: Development, title: kAlertTitle, controller: self)
    }
    
}


extension ForgotPasswordVC{
    
    func call_wsForgotPassword(strEmail:String){
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        var strLoginType = ""
        if (txtEmail.text?.contains("@"))!{
            strLoginType = "1" //email
        }else{
            strLoginType = "2" //Phone
        }
        //sender_identifier  type  phone_dial_code
        let param = ["sender_identifier":strEmail,"type":strLoginType]as [String:AnyObject]
        
        objWebServiceManager.requestPatch(strURL: WsUrl.Url_ForgotPassword, params: param, strCustomValidation: "", success: { (response) in
             objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            print(response)
            if status == "success"{
                
                self.showSimpleAlert(strMessage: message, strTitle: "Success")
            
            }else{
                
                 objAlert.showAlert(message: message, title: "Alert", controller: self)
            }
        }) { (Error) in
            
            print(Error)
        
        }
        
        
    }
    
    func showSimpleAlert(strMessage:String,strTitle:String) {
        let alert = UIAlertController(title: strTitle, message: strMessage,preferredStyle: UIAlertController.Style.alert)
        
        //        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { _ in
        //            //Cancel Action
        //        }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
