//
//  BasicInfoCell.swift
//  Star
//
//  Created by ios-deepak b on 29/01/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class BasicInfoCell: UITableViewCell {

      @IBOutlet var lblTitleName: UILabel!
      @IBOutlet var btnCheck: UIButton!
      @IBOutlet var imgCheck: UIImageView!
     @IBOutlet var vwCell: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
