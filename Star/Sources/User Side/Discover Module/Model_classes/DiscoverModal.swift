//
//  DiscoverModal.swift
//  Star
//
//  Created by ios-deepak b on 21/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//


import Foundation
import UIKit
class DiscoverModal : NSObject{
    // var strUserID: Int = 0
    var strUserID: String = ""
    var strInfluecerID: String = ""
    var strCategoryId: String = ""
    var strSubCategoryId: String = ""
    var strParendCategoryId: String = ""
    var strIsFollow: String = ""
    var strStatus: String = ""
    var strScheduleID: String = ""
    var strPushAlert: String = ""
    var strFollowerId: String = ""
    var strFollowingId: String = ""
    var strCountryId: String = ""
    var strTutorialVideoUrl:String = ""
    var strTutorilVideoThumbUrl:String = ""
    var strUserType: String = ""
    var strName: String = ""
    var strFullName: String = ""
    var strIsImage: String = ""
    var strImageUrl: String = ""
    var strAudioRate: String = ""
    var strVideoRate: String = ""
    var strEmail: String = ""
    var strGender: String = ""
    var strAge: String = ""
    var strCountryCode: String = ""
    var strCountryName: String = ""
    var strDialCode: String = ""
    var strCategory: String = ""
    var strSubCategoryName: String = ""
    var imgCountryLogo = UIImage()
    
    
    
    
    
    init(dict : [String:Any]) {
        if let userID = dict["userID"]  as? Int{
            strUserID = "\(userID)"
        }else if let userID = dict["userID"]  as? String {
            strUserID = userID
        }
        if let influencerId = dict["influencer_id"]  as? String{
            strInfluecerID = influencerId
        }else if let influencerId = dict["influencer_id"]  as? Int {
            strInfluecerID = "\(influencerId)"
        }
        if let is_follow = dict["is_follow"]  as? String{
            strIsFollow = is_follow
        }else if let is_follow = dict["is_follow"]  as? Int {
            strIsFollow = "\(is_follow)"
        }
        
        
        if let TutorialVideo = dict["tutorial_video"]  as? String{
            strTutorialVideoUrl = TutorialVideo
        }
        if let TutorialVideoThumb = dict["tutorial_thumb"]  as? String{
            strTutorilVideoThumbUrl = TutorialVideoThumb
        }
        
        
        if let pushAlertStatus = dict["push_alert_status"]  as? String{
            strPushAlert = pushAlertStatus
        }
        if let categoryId = dict["categoryId"]  as? String{
            strCategoryId = categoryId
        }else if let categoryId = dict["categoryId"]  as? Int {
            strCategoryId = "\(categoryId)"
        }
        
        if let subCategoryId = dict["subcategory_id"]  as? String{
            strSubCategoryId = subCategoryId
        }else if let subCategoryId = dict["subcategory_id"]  as? Int {
            strSubCategoryId = "\(subCategoryId)"
        }
        if let parentCategoryId = dict["parent_category_id"]  as? String{
            strParendCategoryId = parentCategoryId
        }else if let parentCategoryId = dict["parent_category_id"]  as? Int {
            strParendCategoryId = "\(parentCategoryId)"
        }
        if let status = dict["status"]  as? String{
            strStatus = status
        }
        if let scheduleId = dict["scheduleID"]  as? String{
            strScheduleID = scheduleId
        }else if let scheduleId = dict["scheduleID"]  as? Int {
            strScheduleID = "\(scheduleId)"
        }
        if let followerId = dict["follower_id"]  as? String{
            strFollowerId = followerId
        }
        if let followingId = dict["following_id"]  as? String{
            strFollowingId = followingId
        }
        if let countryID = dict["country_id"]  as? String{
            strCountryId = countryID
        }else if let countryID = dict["country_id"]  as? Int {
            strCountryId = "\(countryID)"
        }
        if let image = dict["avatar"] as? String{
            strImageUrl = image
        }
        if let image = dict["influencer_avatar"] as? String{
            strImageUrl = image
        }
        if let category = dict["category"] as? String{
            strCategory = category
        }else if let category = dict["category"]  as? Int {
            strCategory = "\(category)"
        }
        if let subCategoryName = dict["subcategory_name"] as? String{
            strSubCategoryName = subCategoryName
        }
        if let fullname = dict["user_full_name"] as? String{
            strFullName = fullname
        }
        if let name = dict["name"] as? String{
            strName = name
        }
        if let countryCode = dict["country_code"] as? String{
            strCountryCode = countryCode
        }
        if let dialCode = dict["dial_code"] as? String{
            strName = dialCode
        }
        if let currencyName = dict["languageID"] as? String{
            strFullName = currencyName
        }
        if let ageGroup = dict["age_group"] as? String{
            strAge = ageGroup
        }else if let ageGroup = dict["age_group"]  as? Int {
            strAge = "\(ageGroup)"
        }
    }
}






