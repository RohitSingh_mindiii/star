//
//  BalancePlanListModal.swift
//  Star
//
//  Created by RohitSingh on 24/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class BalancePlanListModal: NSObject {

    
    var strAmount: String?
    var strCreditPlanID: String?
    var strStatus:String?
     
      
      init(dict : [String:Any]) {
        
        if let amount = dict["amount"] as? String{
            let doubleValue = Double(amount)
            let cleanValue = doubleValue?.clean ?? "00"
            let finalValue = String(cleanValue)
            self.strAmount = finalValue
        }else  if let amount = dict["amount"] as? Int{
            let doubleValue = Double(amount)
            let cleanValue = doubleValue.clean
            let finalValue = String(cleanValue)
            self.strAmount = finalValue
        }
        
        
        if let planID = dict["creditPlanID"] as? String{
            self.strCreditPlanID = planID
        }else  if let planID = dict["creditPlanID"] as? Int{
            self.strCreditPlanID = "\(planID)"
        }
        
        
        if let status = dict["status"] as? String{
            self.strStatus = status
        }else  if let status = dict["status"] as? Int{
            self.strStatus = "\(status)"
        }
        
        
        
       }
    
}
