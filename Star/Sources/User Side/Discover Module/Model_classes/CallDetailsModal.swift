//
//  CallDetailsModal.swift
//  Star
//
//  Created by RohitSingh on 22/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class CallDetailsModal: NSObject {
    
    var strCallID: String = ""
    var strCallType: String = ""
    var strInfluencer_id: String = ""
    var strInfluencer_media: String = ""
    var strInfluencer_video_thumb: String = ""
    var strStatus: String = ""
    var strUser_id: String = ""
    var strUser_media: String = ""
    var strUser_video_thumb: String = ""
    var strInfluencerName:String = ""
    var strInfluencerProfilePicUrl:String = ""
    var strUserImageUrl:String = ""
    var strTutorialVideoUrl:String = ""
    var strTutorialVideoThumb = ""
    var strUserName:String  = ""
    var strIsSeen:String = ""
    var strProgressStatus:String = ""
       
       init(dict : [String:Any]) {
           
           
        if let callID = dict["strCallID"] as? String{
            self.strCallID = callID
        }else  if let callID = dict["strCallID"] as? Int{
            self.strCallID = "\(callID)"
        }
        
        if let callType = dict["call_type"] as? String{
            self.strCallType = callType
        }else  if let callType = dict["call_type"] as? Int{
            self.strCallType = "\(callType)"
        }
        
        if let influencerID = dict["influencer_id"] as? String{
            self.strInfluencer_id = influencerID
        }else  if let influencerID = dict["influencer_id"] as? Int{
            self.strInfluencer_id = "\(influencerID)"
        }
        
        if let Seen = dict["seen_status"] as? String{
            self.strIsSeen = Seen
        }else  if let Seen = dict["seen_status"] as? Int{
            self.strIsSeen = "\(Seen)"
        }
        
        if let influencerMediaUrl = dict["influencer_media"] as? String{
            self.strInfluencer_media = influencerMediaUrl
        }
        
        if let influencerThumb = dict["influencer_video_thumb"] as? String{
            self.strInfluencer_video_thumb = influencerThumb
        }
        
        if let status = dict["status"] as? String{
            self.strStatus = status
        }else  if let status = dict["status"] as? Int{
            self.strStatus = "\(status)"
        }
        if let userName = dict["user_name"] as? String{
            self.strUserName = userName
        }
        
        if let userimage = dict["user_avatar"] as? String{
            self.strUserImageUrl = userimage
        }
        
        if let influencerName = dict["influencer_name"] as? String{
            self.strInfluencerName = influencerName
        }
        
        if let influencerImage = dict["influencer_avatar"] as? String{
            self.strInfluencerProfilePicUrl = influencerImage
        }
        
        if let tutorialThumb = dict["tutorial_thumb"] as? String{
            self.strTutorialVideoThumb = tutorialThumb
        }
        
        if let tutorialVideo = dict["tutorial_video"] as? String{
            self.strTutorialVideoUrl = tutorialVideo
        }
        if let userMediaUrl = dict["user_media"] as? String{
            self.strUser_media = userMediaUrl
        }
        
        if let userMediaThumb = dict["user_video_thumb"] as? String{
            self.strUser_video_thumb = userMediaThumb
        }
        
        if let userID = dict["user_id"] as? String{
            self.strUser_id = userID
        }else if let userID = dict["user_id"] as? Int{
            self.strUser_id = "\(userID)"
        }
        
        if let progressStatus = dict["progress_status"] as? String{
            self.strProgressStatus = progressStatus
        }else if let progressStatus = dict["progress_status"] as? Int{
            self.strProgressStatus = "\(progressStatus)"
        }
    }
    
/*
     "age_group" = "17-24";
            amount = "50.00";
            callID = 1;
            "call_type" = 2;
            "country_id" = 1;
            "created_at" = "2020-02-22 08:49:14";
            gender = Female;
            "influencer_id" = 30;
            "influencer_media" = "https://dev-stars-app.s3.us-east-1.amazonaws.com/uploads/call/IcgbXQxsJqFW3OHN.mp4";
            "influencer_video_thumb" = "https://dev-stars-app.s3.us-east-1.amazonaws.com/uploads/call/XM7Uc6SGV2jDlPfk.png";
            "" = 1;
            status = 1;
            "updated_at" = "2020-02-22 10:35:36";
            "user_id" = 33;
            "user_media" = "https://dev-stars-app.s3.us-east-1.amazonaws.com/uploads/call/5styFwTBX7EbrZ9C.mp4";
            "user_video_thumb" = "https://dev-stars-app.s3.us-east-1.amazonaws.com/uploads/call/qBZvKAh4JyNOtbYC.png";
     */
}
