

//
//  DiscoverVC.swift
//  Star
//
//  Created by ios-deepak b on 05/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit
import SDWebImage

class DiscoverVC: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var vwSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblPoints: UILabel!
   // @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var tblDiscover: UITableView!
    @IBOutlet weak var viewSearchFld: UIView!
       @IBOutlet weak var ViewContainer: UIView!
        @IBOutlet weak var viewNoDataFound: UIView!

    // TODO: for filter outlets
  
    var limit = 20
    var offset = 0
    
    // MARK:- Varibles
    var arrDiscoverData = [DiscoverModal]()
    //var influecerID: Int = 0
     var influecerID: String = ""
    var strSearch = ""
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self
        self.tblDiscover.delegate = self
        self.tblDiscover.dataSource = self

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
        
       // let color = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
  //    self.vwSearch.setViewShadowCornerRadius(color: color , cornerRadious: 8,shadowOpacity: 2)
        
//             self.vwSearch.layer.shadowColor = UIColor.red.cgColor
//               self.vwSearch.layer.shadowOffset = CGSize(width: -0.5, height: 0.3)
//               self.vwSearch.layer.shadowOpacity = 0.5
//               self.vwSearch.layer.cornerRadius = 20
        // #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1) ,
        
       // self.ViewContainer.setviewbottomShadow()
       /// self.viewSearchFld.setShadow()
        self.callWs_GetDiscoverList(strSearchText: "")
         self.tblDiscover.reloadData()
                self.viewNoDataFound.isHidden = true
        
        //let Stars = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.Stars)
        let star = objAppShareData.UserDetail.availableStars
        let cleanValue = Double(star)
        let finalPoints = cleanValue?.clean
        self.lblPoints.text = finalPoints
        
    }
    
    // MARK:- Buttons Action
    @IBAction func btnFilterAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnOnGoToBalanceVC(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "BalanceDetailsVC")as! BalanceDetailsVC
        self.navigationController?.pushViewController(sb, animated: true)
    }
    
    
}
// MARK:- TableView Delegate and DataSource
extension DiscoverVC : UITableViewDelegate ,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDiscoverData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscoverCell")as! DiscoverCell
      
        let obj = self.arrDiscoverData[indexPath.row]
        let subtitle = obj.strCategory + ", " + obj.strSubCategoryName
         
        cell.lblTitle.text = obj.strFullName
        cell.lblSubTitle.text = subtitle
        cell.btnFollow.tag = indexPath.row
        cell.btnFollow.addTarget(self,action:#selector(buttonCallClicked(sender:)), for: .touchUpInside)
        
//        if obj.strImageUrl == nil{
//            cell.imgProfile.image = #imageLiteral(resourceName: "placeholder_img")
//        }else{
//
        //        }
        if obj.strImageUrl != "" {
            let url = URL(string: obj.strImageUrl)
            cell.imgProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "basic_info_ico"))
        }else{
            
        }
        cell.imgCountry.image = obj.imgCountryLogo
        
        if "\(obj.strIsFollow)" == "1"{
           cell.imgFollow.image = #imageLiteral(resourceName: "follow_tick_ico")

        }else{
             cell.imgFollow.image = #imageLiteral(resourceName: "follow_ico")
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrDiscoverData[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CallDetail_VC")as! CallDetail_VC
        vc.objArrDiscover = obj
        print(obj.strInfluecerID)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    ///*
    @objc func buttonCallClicked(sender:UIButton) {
            
            
            let obj = self.arrDiscoverData[sender.tag]
     //   print(sender.tag)
        
             
                self.influecerID = obj.strInfluecerID
               
        print("obj.InfluecerID is \(obj.strInfluecerID)")
        print("before api callstrIsFollow is \(obj.strIsFollow)")
        
     self.callApiFollowUnFollow()
         print(" after api call strIsFollow is \(obj.strIsFollow)")
        self.tblDiscover.reloadData()
        
    }
/// */
}

// MARK:- Api calling
extension DiscoverVC{
    
    func callWs_GetDiscoverList(strSearchText: String){
    self.arrDiscoverData.removeAll()

            objWebServiceManager.showIndicator()
              
        //search_term=sachin&sort=price|desc&country=300&category=5&age_group=0&offset=0&limit=2&sub_category=8
        
       // "search_term="+strSearchText+"&sort="+""+"&country="+""+"&category="+""+"&age_group="+""+"&offset="+""+"&limit="+""+"&sub_category="+""
        
       /// var sort = "assa|gfgf"
        var strSort = ""
        if objAppShareData.objDiscoverFilter.strSortByPrice == "1"{
           strSort = "price|asc"
        }else if objAppShareData.objDiscoverFilter.strSortByPrice == "2"{
            strSort = "price|desc"
        }else{
        }
        
                    objWebServiceManager.requestGet(strURL: WsUrl.Url_GetDiscoverList+"search_term="+strSearchText+"&sort="+strSort+"&country="+objAppShareData.objDiscoverFilter.strCountry+"&category="+objAppShareData.objDiscoverFilter.strCategory+"&age_group="+""+"&offset="+""+"&limit="+""+"&sub_category="+objAppShareData.objDiscoverFilter.strSubCategory+"&for_call=true"+"&gender"+objAppShareData.objDiscoverFilter.strGender, params: nil, strCustomValidation: "", success: {response in
               print(response)
               let status = (response["status"] as? String)
                        
                        /*
                         ["status_code": 400, "data": {
                         }, "status": error, "error_type": USER_NOT_FOUND, "message": Your token has been expired]
                         {
                             data =     {
                             };
                             "error_type" = "USER_NOT_FOUND";
                             message = "Your token has been expired";
                             status = error;
                             "status_code" = 400;
                         }
                         ErrorCode: 400

                         "USER_NOT_FOUND"
                         "INVALID_TOKEN"
                         "SESSION_EXPIRED"
                         */
//                        if let errorCode = response["status_code"] as? String{
//                            if errorCode == "400"{
//                                let strErrorType = response["error_type"] as? String
//                                if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
//                                    objAlert.showSessionFailAlert(controller: self)
//                                    return
//                                }
//                            }
//                        }else if let errorCode = response["status_code"] as? Int{
//                            if errorCode == 400{
//                                let strErrorType = response["error_type"] as? String
//                                if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
//                                    objAlert.showSessionFailAlert(controller: self)
//                                    return
//                                }
//                            }
//                        }
                          
                        let message = (response["message"] as? String)
                           if status == k_success{
                               var dataFound:Int?
               
                               objWebServiceManager.hideIndicator()
                               if let data = response["data"]as? [String:Any]{
                                   dataFound = data["data_found"] as? Int ?? 0
                                   if status == "success" ||  status == "Success"
                                   {
                                       if dataFound == 0
                                       {
                                           self.viewNoDataFound.isHidden = false
               
                                       }
                                       else{
                                           let arrUserDetails  = data["discover_list"] as! [[String: Any]]
               
                                           for dictUserData in arrUserDetails
                                           {
                                               let objDiscover = DiscoverModal.init(dict: dictUserData)
               
                                               let imagestring = objDiscover.strCountryCode
                                               let imagePath = "CountryPicker.bundle/\(imagestring).png"
                                               objDiscover.imgCountryLogo = UIImage.init(named: imagePath) ?? UIImage()
               
               
                                               self.arrDiscoverData.append(objDiscover)
                                           }
                                           print("self.arrDiscoverData.count is \(self.arrDiscoverData.count)")
                                           self.tblDiscover.reloadData()
                                       }
                                       if self.arrDiscoverData.count == 0
                                       {
                                           self.viewNoDataFound.isHidden = false
                                       }else{
                                           self.viewNoDataFound.isHidden = true
                                       }
               
                                   }else{
                                       objWebServiceManager.hideIndicator()
                                       objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                                   }
                               }
                           }
                           else{
                               objWebServiceManager.hideIndicator()
                               objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                           }
                       }, failure: { (error) in
                           print(error)
                           objWebServiceManager.hideIndicator()
                           objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
                       })
                   }

    func callApiFollowUnFollow(){
            
            if !objWebServiceManager.isNetworkAvailable(){
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
                return
                
            }
        //    objWebServiceManager.showIndicator()
            self.view.endEditing(true)
            

        let StrUrl = WsUrl.Url_FollowUnfollow + String(self.influecerID)
        print(StrUrl)
        print(self.influecerID)
        
        
            objWebServiceManager.requestPut(strURL: StrUrl, params: nil, strCustomValidation: "", showIndicator: false, success: {
                response in
                print(response)
                let status = (response["status"] as? String)
                let message = (response["message"] as? String)
                if status == k_success{
                    //objWebServiceManager.hideIndicator()
                    self.callWs_GetDiscoverList(strSearchText: "")
                    if let data = response["data"]as? [String:Any]{
                        
                        if status == "success" ||  status == "Success"
                        {
                            //self.getDiscoverList()
                            
                        }else{
                            objWebServiceManager.hideIndicator()
                            objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                        }
                    }
                }
                else{
                    objWebServiceManager.hideIndicator()
                    objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                }
            }, failure: { (error) in
                print(error)
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
            })
            
            
        }
    
    
}

////MARK: - searching operation
extension DiscoverVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{

        do {
            let regex = try NSRegularExpression(pattern: ".*[^A-Za-z] .*", options: [])
            if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                return false
            }
        }
        catch {
            print("ERROR")
        }

        let text = textField.text! as NSString

        if (text.length == 1) && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            strSearch = ""
            self.arrDiscoverData.removeAll()
            self.limit = 20
            self.offset = 0
            self.perform(#selector(self.searchProduct), with: nil, afterDelay: 1)

        }
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }

    @objc func searchProduct(){
       print("called")
       self.callWs_GetDiscoverList(strSearchText: strSearch)
    }

    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            strSearch = substring
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 1)
        }
    }

    @objc func reload() {
        self.arrDiscoverData.removeAll()
        self.limit = 20
        self.offset = 0
        self.callWs_GetDiscoverList(strSearchText: strSearch)
    }
    
}



