//
// SecondVC.swift
// Design2
//
// Created by IOS-Sagar on 30/01/20.
// Copyright © 2020 IOS-Sagar. All rights reserved.
//

import UIKit

class FilterVC: UIViewController, BottomSheetVCDelegate{
    
    // MARK:- Outlets
    @IBOutlet weak var imgSelectMaleIcon: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgInactiveFemaleIcon: UIImageView!
    @IBOutlet weak var imgUnSelectedIconHighToLow: UIImageView!
    @IBOutlet weak var ImgunselectedLowToHigh: UIImageView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnApplyFiter: UIButton!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var txtAllCountry: UITextField!
    @IBOutlet weak var lblSelectSubCategory: UILabel!
    @IBOutlet weak var lblSelectCategory: UILabel!
    @IBOutlet weak var lblSelectCountry: UILabel!
    @IBOutlet weak var viewSelectMale: UIView!
    @IBOutlet weak var viewSelectFemale: UIView!
    @IBOutlet weak var viewSelectCountry: UIView!
    @IBOutlet weak var viewSelectSubCategory: UIView!
    @IBOutlet weak var viewSelectCategory: UIView!
    @IBOutlet weak var lblLowToHigh: UILabel!
    @IBOutlet weak var lblHighToLow: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    
    //var subCatList:[SubCategory]?
    var strBottomSheetFrom = ""
    var arrCountry = [CommonModal]()
    var arrCategory = [CommonModal]()
    var arrSubCategory = [CommonModal]()
    var arrSelectedGender = [String]()
    var strSortByPrice = ""
    var strCountry = ""
    var strCategory = ""
    var strSubCategory = ""
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    //    self.viewHeader.setviewbottomShadow()
        self.viewSelectCountry.setBorder()
        self.viewSelectCategory.setBorder()
        self.viewSelectSubCategory.setBorder()
         
        self.btnApplyFiter.backgroundColor = UIColor(named: "FliterButton")
        self.btnApplyFiter.backgroundColor = UIColor(named: "FliterButton")
        self.btnApplyFiter.setTitleColor(UIColor(named: "button_text"), for: .normal)
        if objAppShareData.isDiscoverFilterApply{
            self.setFilteredDataDesign()
        }else{
            self.setDefaultDataDesign()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.arrCountry = objAppShareData.arrCountryData
        self.arrCategory = objAppShareData.arrCategoryData
        self.arrSubCategory = objAppShareData.arrSubCategoryData
        
        //let Stars = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.Stars)
        let star = objAppShareData.UserDetail.availableStars
        let cleanValue = Double(star)
        let finalPoints = cleanValue?.clean
        self.lblPoints.text = finalPoints
    }
    func setDefaultDataDesign(){
        ////
        self.arrSelectedGender.append("1")
        self.arrSelectedGender.append("2")
        self.lblMale.textColor = UIColor(named: "Text_Color")
        self.imgSelectMaleIcon.image = UIImage(named:"active_male_ico")
        self.lblFemale.textColor = UIColor(named: "Text_Color")
        self.imgInactiveFemaleIcon.image = UIImage(named:"active_female_ico")
        
//        self.btnApplyFiter.backgroundColor = UIColor(named: "FliterButton")
//        self.btnApplyFiter.backgroundColor = UIColor(named: "FliterButton")
//        self.btnApplyFiter.setTitleColor(UIColor(named: "button_text"), for: .normal)
        
        self.strSortByPrice = "1"
        self.ImgunselectedLowToHigh.image = UIImage(named: "active_dots_ico")
        self.lblLowToHigh.textColor = UIColor(named: "Text_Color")
        self.imgUnSelectedIconHighToLow.image = UIImage(named: "inactive_dots_ico_w")
        self.lblHighToLow.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        ////
    }
    func setFilteredDataDesign(){
        if objAppShareData.objDiscoverFilter.strGender.contains("1"){
            self.arrSelectedGender.append("1")
            self.lblMale.textColor = UIColor(named: "Text_Color")
            self.imgSelectMaleIcon.image = UIImage(named:"active_male_ico")
        }
        if objAppShareData.objDiscoverFilter.strGender.contains("2"){
            self.arrSelectedGender.append("2")
            self.lblFemale.textColor = UIColor(named: "Text_Color")
            self.imgInactiveFemaleIcon.image = UIImage(named:"active_female_ico")
        }
        if objAppShareData.objDiscoverFilter.strSortByPrice.contains("1"){
            self.strSortByPrice = "1"
            self.ImgunselectedLowToHigh.image = UIImage(named: "active_dots_ico")
            self.lblLowToHigh.textColor = UIColor(named: "Text_Color")
            self.imgUnSelectedIconHighToLow.image = UIImage(named: "inactive_dots_ico_w")
            self.lblHighToLow.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
        if objAppShareData.objDiscoverFilter.strSortByPrice.contains("2"){
            self.strSortByPrice = "2"
            self.imgUnSelectedIconHighToLow.image = UIImage(named: "active_dots_ico")
            self.lblHighToLow.textColor = UIColor(named: "Text_Color")
            self.ImgunselectedLowToHigh.image = UIImage(named: "inactive_dots_ico_w")
            self.lblLowToHigh.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
        let varText = objAppShareData.objDiscoverFilter.strCountryName
        self.lblSelectCountry.text = varText
        if varText.count == 0{
           self.lblSelectCountry.text = "All Countries"
        }
        self.strCountry = objAppShareData.objDiscoverFilter.strCountry
        
        let varText1 = objAppShareData.objDiscoverFilter.strCategoryName
        self.lblSelectCategory.text = varText1
        if varText.count == 0{
           self.lblSelectCategory.text = "All Categories"
        }
        self.strCategory = objAppShareData.objDiscoverFilter.strCategory
        
        let varText2 = objAppShareData.objDiscoverFilter.strSubCategoryName
        self.lblSelectSubCategory.text = varText2
        if varText.count == 0{
           self.lblSelectSubCategory.text = "All Subcategories"
        }
        self.strSubCategory = objAppShareData.objDiscoverFilter.strSubCategory
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    // MARK:- Buttons Action
    
    @IBAction func btnOnGoToBalanceVC(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "BalanceDetailsVC")as! BalanceDetailsVC
        self.navigationController?.pushViewController(sb, animated: true)
    }
    
    
    @IBAction func btnSelectPriceLowToHight(_ sender: Any) {
        self.strSortByPrice = "1"
        self.ImgunselectedLowToHigh.image = UIImage(named: "active_dots_ico")
        self.lblLowToHigh.textColor = UIColor(named: "Text_Color")
        self.imgUnSelectedIconHighToLow.image = UIImage(named: "inactive_dots_ico_w")
        self.lblHighToLow.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    }
    
    @IBAction func btnSelectPriceHighToLow(_ sender: Any) {
        self.strSortByPrice = "2"
        self.imgUnSelectedIconHighToLow.image = UIImage(named: "active_dots_ico")
        self.lblHighToLow.textColor = UIColor(named: "Text_Color")
        self.ImgunselectedLowToHigh.image = UIImage(named: "inactive_dots_ico_w")
        self.lblLowToHigh.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    }
    
    @IBAction func btnSelectMale(_ sender: Any) {
//        self.imgSelectMaleIcon.image = UIImage(named: "active_male_ico")
//        self.lblMale.textColor = UIColor(named: "Text_Color")
//        self.imgInactiveFemaleIcon.image = UIImage(named: "inactive_female_icon")
//        self.lblFemale.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        if !self.arrSelectedGender.contains("1"){
            self.arrSelectedGender.append("1")
            self.lblMale.textColor = UIColor(named: "Text_Color")
            self.imgSelectMaleIcon.image = UIImage(named:"active_male_ico")
        }else{
            if self.arrSelectedGender.count == 1{
                return
            }
            let ind = self.arrSelectedGender.firstIndex(of:"1")
            self.arrSelectedGender.remove(at: ind ?? 0)
            self.lblMale.textColor = UIColor(named: "BookedTableCell_SecondryTXT")
            self.imgSelectMaleIcon.image = UIImage(named:"inactive_male_ico")
        }
    }
    
    @IBAction func btnSelectFemale(_ sender: Any) {
//        self.imgInactiveFemaleIcon.image = UIImage(named: "active_female_ico")
//        self.lblFemale.textColor = UIColor(named: "Text_Color")
//        self.imgSelectMaleIcon.image = UIImage(named: "inactive_male_icon")
//        self.lblMale.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        if !self.arrSelectedGender.contains("2"){
            self.arrSelectedGender.append("2")
            self.lblFemale.textColor = UIColor(named: "Text_Color")
            self.imgInactiveFemaleIcon.image = UIImage(named:"active_female_ico")
        }else{
            if self.arrSelectedGender.count == 1{
                return
            }
            let ind = self.arrSelectedGender.firstIndex(of:"2")
            self.arrSelectedGender.remove(at: ind ?? 0)
            self.lblFemale.textColor = UIColor(named: "BookedTableCell_SecondryTXT")
            self.imgInactiveFemaleIcon.image = UIImage(named:"inactive_female_ico")
        }
    }
    
    @IBAction func btnActionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: false)
    }
    
  @IBAction func btnActionReset(_ sender: Any){
         
/*
         self.btnReset.backgroundColor = UIColor(named: "FliterButton")
         self.btnApplyFiter.backgroundColor = .clear
         self.btnApplyFiter.setTitleColor(UIColor(named: "BookedTableCell_SecondryTXT"), for: .normal)
         self.btnReset.setTitleColor(UIColor(named: "button_text"), for: .normal)
         self.btnApplyFiter.borderClr = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
         self.lblSelectCategory.text = "select Category"
         self.lblSelectSubCategory.text = "select subCategory"

         self.imgInactiveFemaleIcon.image = UIImage(named: "inactive_female_icon")
         self.lblFemale.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
         self.imgSelectMaleIcon.image = UIImage(named: "inactive_male_icon")
         self.lblMale.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
         
         self.ImgunselectedLowToHigh.image = UIImage(named: "inactive_dots_ico_w")
         self.lblLowToHigh.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
         
         self.imgUnSelectedIconHighToLow.image = UIImage(named: "inactive_dots_ico_w")
         self.lblHighToLow.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
 */
       objAppShareData.objDiscoverFilter = DiscoverFilterModal.init(dict: [:])
       objAppShareData.isDiscoverFilterApply = false
       self.navigationController?.popViewController(animated: true)
     }
     @IBAction func btnActionApplyFilter(_ sender: Any){
//         self.btnApplyFiter.backgroundColor = UIColor(named: "FliterButton")
//         self.btnReset.backgroundColor = .clear
//         btnReset.setTitleColor(.darkGray, for: .normal)
//         btnReset.backgroundColor = .clear
//          self.btnReset.borderClr = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
//         btnApplyFiter.backgroundColor = UIColor(named: "FliterButton")
//         btnApplyFiter.setTitleColor(UIColor(named: "button_text"), for: .normal)
//         btnReset.setTitleColor(UIColor(named: "BookedTableCell_SecondryTXT"), for: .normal)
        objAppShareData.objDiscoverFilter = DiscoverFilterModal.init(dict: [:])
        objAppShareData.objDiscoverFilter.strSortByPrice = self.strSortByPrice
        objAppShareData.objDiscoverFilter.strGender = self.arrSelectedGender.joined(separator: ",")
        objAppShareData.objDiscoverFilter.strCountry = self.strCountry
        objAppShareData.objDiscoverFilter.strCategory = self.strCategory
        objAppShareData.objDiscoverFilter.strSubCategory = self.strSubCategory
        objAppShareData.isDiscoverFilterApply = true
        objAppShareData.objDiscoverFilter.strCountryName = self.lblSelectCountry.text ?? "Select Country"
        objAppShareData.objDiscoverFilter.strCategoryName = self.lblSelectCategory.text ?? "Select Category"
        objAppShareData.objDiscoverFilter.strSubCategoryName = self.lblSelectSubCategory.text ?? "Select Subcategory"
        self.navigationController?.popViewController(animated: true)
     }
     
    
    @IBAction func btnSelectCountry(_ sender: Any) {
         self.view.endEditing(true)
         let str = "Select Country"
         print("self.arrCountry count is \(self.arrCountry.count)")
                objAppShareData.arrSelectedBottomNames.removeAll()
                objAppShareData.arrSelectedBottomIds.removeAll()
        self.strBottomSheetFrom = "1"
     ////
     let arr = self.strCountry.components(separatedBy: ",")
     if arr.count>0{
         let arrInt = arr.map { Int($0) }
         if arrInt.count>0{
             let int = arrInt[0]
             if int != nil{
                 objAppShareData.arrSelectedBottomIds = arrInt as! [Int]
             }
         }
     }
     let arrName = self.lblSelectCountry.text!.components(separatedBy: ",")
     if arrName.count>0{
         objAppShareData.arrSelectedBottomNames = arrName
     }
     ////
         self.showBotttomSheet(arr: self.arrCountry,str: str)
    }
    //MARK: step 6 finally use the method of the contract
    func sendDataToFirstViewController(arrId:[Int], arrName:[String]) {
            print(" my country is \(arrId)")
            print(" my Country is \(arrName)")
            let varText = arrName.joined(separator:",")
          
        if strBottomSheetFrom == "1"{
            self.lblSelectCountry.text = varText
            if varText.count == 0{
               self.lblSelectCountry.text = "All Countries"
            }
            self.strCountry = (arrId.map{String($0)}).joined(separator: ",")
        }else if strBottomSheetFrom == "2"{
            self.lblSelectCategory.text = varText
            if varText.count == 0{
               self.lblSelectCategory.text = "All Categories"
            }
            self.strCategory = (arrId.map{String($0)}).joined(separator: ",")
           
            //// Get sub category
            self.strSubCategory = ""
            objAppShareData.arrSubCategoryData.removeAll()
            self.lblSelectSubCategory.text = "Select Subcategory"
            if self.strCategory.count == 0 || self.strCategory == "0"{
                //self.strCategory = "0"
                for obj in objAppShareData.arrCategoryData{
                    objAppShareData.arrSubCategoryData.append(contentsOf: obj.arrSubCategory)
                }
            }else{
                let arrCatId = self.strCategory.components(separatedBy: ",")
                if arrCatId.count>0{
                let arrCatInt = arrCatId.map { Int($0) }
                 if arrCatInt.count>0{
                    for catId in arrCatInt{
                        let str = String(catId!)
                        let filteredArray = objAppShareData.arrCategoryData.filter() { $0.strID == str }
                        if filteredArray.count > 0{
                            let obj = filteredArray[0]
                            objAppShareData.arrSubCategoryData.append(contentsOf: obj.arrSubCategory)
                        }
                    }
                 }
                }
            }
            let objCatData:[String:Any] = ["id": 0, "name": "All Subcategories"]
            let objCatInfo:CommonModal = CommonModal(dict: objCatData)
            objAppShareData.arrSubCategoryData = [objCatInfo] + objAppShareData.arrSubCategoryData
            ////
        }else if strBottomSheetFrom == "3"{
           self.lblSelectSubCategory.text = varText
           if varText.count == 0{
              self.lblSelectSubCategory.text = "All Subcategories"
           }
           self.strSubCategory = (arrId.map{String($0)}).joined(separator: ",")
        }
        
    }
   
    @IBAction func btnSelectCategory(_ sender: Any) {
        self.view.endEditing(true)
        self.strBottomSheetFrom = "2"
        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
                     let str = "Select Category"
                     print("self.arrCategory count is \(self.arrCategory.count)")
        ////
        let arr = self.strCategory.components(separatedBy: ",")
        if arr.count>0{
            let arrInt = arr.map { Int($0) }
            if arrInt.count>0{
                let int = arrInt[0]
                if int != nil{
                    objAppShareData.arrSelectedBottomIds = arrInt as! [Int]
                }
            }
        }
        let arrName = self.lblSelectCategory.text!.components(separatedBy: ",")
        if arrName.count>0{
            objAppShareData.arrSelectedBottomNames = arrName
        }
        ////
        self.showBotttomSheet(arr: self.arrCategory,str: str)
    }
    
    @IBAction func btnSelectSubcategory(_ sender: Any) {
        self.view.endEditing(true)
        if self.strCategory.count == 0{
            objAlert.showAlert(message: "Please select category first", title: "Alert", controller: self)
            return
        }
        self.strBottomSheetFrom = "3"
        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
                     let str = "Select Subcategory"
                     print("self.arrSubCategory count is \(self.arrSubCategory.count)")
        ////
        let arr = self.strSubCategory.components(separatedBy: ",")
        if arr.count>0{
            let arrInt = arr.map { Int($0) }
            if arrInt.count>0{
                let int = arrInt[0]
                if int != nil{
                    objAppShareData.arrSelectedBottomIds = arrInt as! [Int]
                }
            }
        }
        let arrName = self.lblSelectSubCategory.text!.components(separatedBy: ",")
        if arrName.count>0{
            objAppShareData.arrSelectedBottomNames = arrName
        }
        ////
        self.showBotttomSheet(arr: objAppShareData.arrSubCategoryData,str: str)
    }
    
    
    func showBotttomSheet(arr : Array<Any>, str: String ){
           
           let sb = UIStoryboard.init(name: "Main", bundle:Bundle.main)
                 let vc = sb.instantiateViewController(withIdentifier:"BottomSheetVC") as! BottomSheetVC
           
           vc.arrBottom = arr as! [CommonModal]
           vc.strHeader = str
           vc.modalPresentationStyle = .overCurrentContext
           vc.delegate = self
           self.present(vc, animated: false, completion: nil)
           
       }
     
}
