//
//  CallDetail_VC.swift
//  Star
//
//  Created by RohitSingh on 21/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import MobileCoreServices
import Photos
import AVKit
import Social

class CallDetail_VC: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet var vwHeader: UIView!
    @IBOutlet var stackVwBottom: UIStackView!
    @IBOutlet var stackViewContainDetails: UIStackView!
    @IBOutlet var vwContainTutorialVid: UIView!
    @IBOutlet var vwMyAudio: UIView!
    @IBOutlet var vwMyVideo: UIView!
    @IBOutlet var vwInfluencerAudio: UIView!
    @IBOutlet var vwInfluencerVideo: UIView!
    @IBOutlet var imgVwMyVideo: UIImageView!
    @IBOutlet var imgVwMyAudio: UIImageView!
    @IBOutlet var imgVwTutorial: UIImageView!
    @IBOutlet var imgVwInfluencerAudio: UIImageView!
    @IBOutlet var imgVwInfluencerVideo: UIImageView!
    @IBOutlet var btnAudio: UIButton!
    @IBOutlet var btnVideo: UIButton!
    @IBOutlet var vwBottom: UIView!
    @IBOutlet var vwContainInfluAudioShadow: UIView!
    @IBOutlet var vwContainMyAudioShadow: UIView!
    @IBOutlet var btnPlayTutorialVideo: UIButton!//tag = 0
    @IBOutlet var btnPlayMyVideo: UIButton!//tag = 1
    @IBOutlet var btnPlayInfluencerVideo: UIButton!//tag = 2
    @IBOutlet var btnPayNowUser: UIButton!
    @IBOutlet var lblAudioPrice: UILabel!
    @IBOutlet var lblVideoPrice: UILabel!
    @IBOutlet var vwContainAudioPrice: UIView!
    @IBOutlet var vwContainVideoPrice: UIView!
    @IBOutlet var btnPayNowVideo: UIButton!
    @IBOutlet var imgVwUserHeader: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var vwAlert: UIView!
    @IBOutlet var vwContainAlertPric: UIView!
    @IBOutlet var cvBalance: UICollectionView!
    @IBOutlet var lblTotalStartOnSubvw: UILabel!
    @IBOutlet var imgVwUserTutorial: UIImageView!
    @IBOutlet var imgVwUserInfluencer: UIImageView!
    @IBOutlet var btnInfluencerAudioPlay: UIButton!
    @IBOutlet var imgVwFollowUnFollow: UIImageView!
    @IBOutlet var btnMyAudioPlay: UIButton!
    @IBOutlet var btnFollowUnfollow: UIButton!
    @IBOutlet var imgVwPlayStopMyAudio: UIImageView!
    @IBOutlet var imgVwPlayStopInfluencerAudio: UIImageView!
    @IBOutlet var btnShareTutorialVideo: UIButton!
    @IBOutlet var btnShareMyAudio: UIButton!
    @IBOutlet var btnShareMyVideo: UIButton!
    @IBOutlet var btnShareInfluencerAudio: UIButton!
    @IBOutlet var btnShareInfluencerVideo: UIButton!
    @IBOutlet var imgVwReadUnreadAudio: UIImageView!
    @IBOutlet var vwResponsein3Days: UIView!
    @IBOutlet var imgVwReadUnreadVideo: UIImageView!
    @IBOutlet var lblResponseIn3Days: UILabel!
    
    //SubvwOutlets
    @IBOutlet var vwContainerAudio: UIView!
    @IBOutlet var vwContainerInsiderAudio: UIView!
    @IBOutlet var imgVwCancelLeftSide: UIImageView!
    @IBOutlet var lblRecordingHeader: UILabel!
    @IBOutlet var btnLeftSideCancel: UIButton!
    @IBOutlet var lblDoneRightSide: UILabel!
    @IBOutlet var imgVwCancelRightSide: UIImageView!
    @IBOutlet var btnCancelRightSideTop: UIButton!
    @IBOutlet var imgVwWaveFormGif: UIImageView!
    @IBOutlet var lblTimeAudio: UILabel!
    @IBOutlet var stackVwAudio: UIStackView!
    @IBOutlet var vwContainRetakeBtn: UIView!
    @IBOutlet var vwRetakeBtnInside: UIView!
    @IBOutlet var btnRetake: UIButton!
    @IBOutlet var vwContainPlayBtn: UIView!
    @IBOutlet var vwContainPlayInside: UIView!
    @IBOutlet var btnPlayAudioSubVw: UIButton!
    
    
    //MARK:- Variables
    open var bitRate = 192000
    open var sampleRate = 44100.0
    open var channels = 1
    var imagePicker = UIImagePickerController()
    var player : AVAudioPlayer?
    var videoURL : URL?
    var compressedURL : URL?
    var pickedImage:UIImage?
    var pickedImageData:Data?
    var pickedVideoData:Data?
    var pickedVideo = ""
    var pickedVideoThumbData:Data?
    var isAudioRecordingGranted:Bool = false
    var recorder: AVAudioRecorder?
    var avPlayer : AVAudioPlayer!
    var recordingSession: AVAudioSession!
    var counter = 15
    var timer = Timer()
    var playBackTimer = Timer()
    var playBackTime = Int()
    var playBacktimeCount = 0
    var isFromRecord = false
    var isRecording = false
    var isPlaying = false
    var displayLink:CADisplayLink!
    var strAudioUrl = ""
    var audioData = Data()
    var arrSheduleData = [SheduleModel]()
    var callID = ""
    var objArrDiscover:DiscoverModal?
    var objArrBooked:CallListModel?
    var audioPrice = Double()
    var videoPrice = Double()
    var mediaData = Data()
    var strCallType = ""
    var arrBalance = [BalancePlanListModal]()
    var isFromAudio = Bool()
    var strMyAudioUrl = ""
    var strMyVideoUrl = ""
    var strTutorialVideoUrl = ""
    var strInfluencerAudioUrl = ""
    var strInfluencerVideoUrl = ""
    var isComingFromCrossSubVw:Bool?
    var audioUrl:URL?
    var isComingFrom = ""
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if objArrBooked?.strCallID != nil{
            self.callID = objArrBooked?.strCallID ?? ""
        }else{
            
        }
        
        if self.callID == ""{
            self.showTutorialVideoDiscover()
            self.setUserDataDiscover()
            if objArrDiscover?.strInfluecerID != ""{
                self.callWebserviceGetSheduleList(strInfulencerID: objArrDiscover!.strInfluecerID)
            }
        }else{
            self.setUserDataBooked()
//            if self.isComingFrom == "Notification"{
//
//            }else{
//               self.showTutorialVideoBooked()
//            }
            self.call_WsGetDetails(strCallDetailID: self.callID)
            
        }
        
        setviewbottomShadow()
        didLoadWork()
    }
    
    //MARK:- SetStyling
    func didLoadWork(){
        
        self.btnPlayTutorialVideo.setImage(#imageLiteral(resourceName: "video_ico"), for: .normal)
        self.btnPlayMyVideo.setImage(#imageLiteral(resourceName: "video_ico"), for: .normal)
        self.btnPlayInfluencerVideo.setImage(#imageLiteral(resourceName: "video_ico"), for: .normal)
        self.btnPayNowUser.setCornerRadius(cornerRadious: 4)
        self.btnPayNowVideo.setCornerRadius(cornerRadious: 4)
        
        self.call_WsGetBalancePlanList()
        self.vwAlert.isHidden = true
        self.vwContainAlertPric.setViewRadius()
        self.cvBalance.delegate = self
        self.cvBalance.dataSource = self
        
        self.imgVwMyVideo.setCornerRadiusWithBoarder(color: .clear, cornerRadious: 7, photoImageView: self.imgVwMyVideo)
        self.imgVwInfluencerVideo.setCornerRadiusWithBoarder(color: .clear, cornerRadious: 7, photoImageView: self.imgVwInfluencerVideo)
        self.imgVwTutorial.setCornerRadiusWithBoarder(color: .clear, cornerRadious: 7, photoImageView: self.imgVwTutorial)
        self.vwContainMyAudioShadow.setViewShadowAndLessCorner()
        self.vwContainInfluAudioShadow.setViewShadowAndLessCorner()
        
        //  self.vwAudioRecorder.isHidden = true
        // self.btnMyAudioPlay.isHidden = true
        self.btnInfluencerAudioPlay.isHidden = true
        //SubVw
        self.vwContainerAudio.isHidden = true
        self.vwContainPlayBtn.isHidden = true
        self.vwContainerInsiderAudio.setViewRadius()
        self.btnRetake.setTitle("RECORD", for: .normal)
        self.btnPlayAudioSubVw.setTitle("PLAY", for: .normal)
        self.vwContainPlayInside.setCornerRadius(radius: 3)
        self.vwRetakeBtnInside.setCornerRadius(radius: 3)
        self.lblDoneRightSide.isHidden = true
        self.imgVwCancelRightSide.isHidden = true
        self.btnCancelRightSideTop.isUserInteractionEnabled = false
        
        self.imgVwReadUnreadAudio.isHidden = true
        self.imgVwReadUnreadVideo.isHidden = true
        self.vwResponsein3Days.isHidden = true
        //self.lblResponseIn3Days.isHidden = true
    }
    
    func showTutorialVideoDiscover(){
        
        var strTutorialVideoUrl = String(objArrDiscover?.strTutorialVideoUrl ?? "")
        if strTutorialVideoUrl != "" {
            self.vwContainTutorialVid.isHidden = false
            strTutorialVideoUrl = objArrDiscover?.strTutorialVideoUrl ?? ""
            let videoThumbUrl =  objArrDiscover?.strTutorilVideoThumbUrl
            
            if videoThumbUrl != "" {
                let url = URL(string: videoThumbUrl!)
                self.imgVwTutorial.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "image_placeholder"))
            }else{
                self.imgVwTutorial.image = #imageLiteral(resourceName: "image_placeholder")
            }
            let tutorialImgVw = objArrDiscover?.strImageUrl
            if tutorialImgVw != "" {
                let url = URL(string: tutorialImgVw ?? "")
                self.imgVwUserTutorial.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
                self.imgVwUserInfluencer.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
            }else{
                self.imgVwUserTutorial.image = #imageLiteral(resourceName: "inactive_profile_ico")
                self.imgVwUserInfluencer.image = #imageLiteral(resourceName: "inactive_profile_ico")
            }
        }else{
            self.vwContainTutorialVid.isHidden = true
        }
    }
    
//    func showTutorialVideoBooked(){
//
//        if objArrBooked?.strTutorialVideoUrl != ""{
//            strTutorialVideoUrl = objArrBooked?.strTutorialVideoUrl ?? ""
//            let videoThumbUrl =  objArrBooked?.strTutorialVideoThumbUrl
//
//            if videoThumbUrl != "" {
//                let url = URL(string: videoThumbUrl!)
//                self.imgVwTutorial.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "image_placeholder"))
//            }else{
//                self.imgVwTutorial.image = #imageLiteral(resourceName: "image_placeholder")
//            }
//            let tutorialImgVw = objArrBooked?.strUserImage
//            if tutorialImgVw != "" {
//                let url = URL(string: tutorialImgVw ?? "")
//                self.imgVwUserTutorial.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
//                self.imgVwUserInfluencer.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
//            }else{
//                self.imgVwUserTutorial.image = #imageLiteral(resourceName: "inactive_profile_ico")
//                self.imgVwUserInfluencer.image = #imageLiteral(resourceName: "inactive_profile_ico")
//            }
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.imgVwPlayStopMyAudio.image = #imageLiteral(resourceName: "audio_play_ico")
        self.imgVwPlayStopInfluencerAudio.image = #imageLiteral(resourceName: "audio_play_ico")
        DispatchQueue.main.async {
            self.vwBottom.dropShadow(color: .gray, offSet: CGSize(width: -1, height: 1),radius:5,scale: true)
        }
    }
    
    func setUserDataDiscover(){
        self.vwInfluencerAudio.isHidden = true
        self.vwInfluencerVideo.isHidden = true
        self.vwMyAudio.isHidden = true
        self.vwMyVideo.isHidden = true
        self.imgVwUserHeader.setImgCircle()
        self.imgVwUserTutorial.setImgCircle()
        if objArrDiscover != nil{
            self.lblUserName.text = objArrDiscover?.strName
            if objArrDiscover?.strImageUrl != "" {
                let url = URL(string: objArrDiscover!.strImageUrl)
                self.imgVwUserHeader.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "basic_info_ico"))
            }else{
                self.imgVwUserHeader.image = #imageLiteral(resourceName: "placeholder_img")
            }
        }
        
        if objArrDiscover?.strIsFollow == "1"{
            self.imgVwFollowUnFollow.image = #imageLiteral(resourceName: "follow_tick_ico")
        }else{
            self.imgVwFollowUnFollow.image = #imageLiteral(resourceName: "follow_ico")
        }
        self.imgVwFollowUnFollow.isHidden = false
        self.btnFollowUnfollow.isHidden = false
        
    }
    
    func setUserDataBooked(){
        self.vwInfluencerAudio.isHidden = true
        self.vwInfluencerVideo.isHidden = true
        self.vwMyAudio.isHidden = true
        self.vwMyVideo.isHidden = true
        self.imgVwUserHeader.setImgCircle()
        self.imgVwUserTutorial.setImgCircle()
        if objArrBooked != nil{
            self.lblUserName.text = objArrBooked?.strUserName
            if objArrBooked?.strUserImage != "" {
                let url = URL(string: objArrBooked!.strUserImage)
                self.imgVwUserHeader.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "basic_info_ico"))
            }else{
                self.imgVwUserHeader.image = #imageLiteral(resourceName: "placeholder_img")
            }
        }
        self.vwBottom.isHidden = true
        self.imgVwFollowUnFollow.isHidden = true
        self.btnFollowUnfollow.isHidden = true
        
    }
    
    func setviewbottomShadow(){
        self.vwHeader.layer.shadowColor = UIColor(named: "Shadow")?.cgColor
        self.vwHeader.layer.masksToBounds = false
        self.vwHeader.layer.shadowOffset = CGSize(width: 0.0 , height: 5.0)
        self.vwHeader.layer.shadowOpacity = 0.2
        self.vwHeader.layer.shadowRadius = 4
    }
    
    //MARK:- Button Action
    @IBAction func btnBackonHeader(_ sender: Any) {
        self.isComingFrom = ""
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnFollowUnfollowUser(_ sender: Any) {
        //Add Follow API
        self.callApiFollowUnFollow()
    }
    //MARK:- Button Audio Recorder
    @IBAction func btnOpenAudioRecorder(_ sender: Any) {
        
        self.WebserviceForGetCreditList()
        self.check_record_permission()
        let userDict = UserDefaults.standard.value(forKey: UserDefaults.KeysDefault.userInfo)as? [String:Any] ?? [:]
        print(userDict)
        var userTotalStarsBalance = Double()
        let totalUserPoint = userDict["available"]as? String ?? ""
        if totalUserPoint != ""{
            userTotalStarsBalance = Double(totalUserPoint)!
        }
        
        if userTotalStarsBalance >= audioPrice {
            print("userTotalStarsBalance is greater than audioPrice")
            self.vwContainerAudio.isHidden = false
        }
        else {
            let newPrice = Double(totalUserPoint)
            let cleanValue = (newPrice?.clean)!
            let finalValue = String(cleanValue)
            self.lblTotalStartOnSubvw.text = finalValue
            self.vwAlert.isHidden = false
        }
    }
    
    @IBAction func btnOpenVideoRecorder(_ sender: Any) {
        self.WebserviceForGetCreditList()
        let userDict = UserDefaults.standard.value(forKey: UserDefaults.KeysDefault.userInfo)as? [String:Any] ?? [:]
        var userTotalStarsBalance = Double()
        let totalUserPoint = userDict["available"]as? String ?? ""
        if totalUserPoint != ""{
            userTotalStarsBalance = Double(totalUserPoint)!
        }
        
        if userTotalStarsBalance >= videoPrice {
            print("availableUserBalance is greater than second_value")
            checkCameraPermissions(handler: { [weak self](isGranted) in
                guard self != nil else{return}
                if isGranted{
                   // pickVideo()
                    gotoCameraVC()
                }
            })
        }
        else {
            let newPrice = Double(totalUserPoint)
            let cleanValue = newPrice?.clean
            let finalValue = String(cleanValue!)
            self.lblTotalStartOnSubvw.text = finalValue
            self.vwAlert.isHidden = false
        }
    }
    
    
    @IBAction func btnPlayVideo(_ sender: AnyObject) {
        var strVideoUrl = ""
        switch sender.tag {
        case 0:
            strVideoUrl = strTutorialVideoUrl
        case 1:
            strVideoUrl = strMyVideoUrl
        case 2:
            strVideoUrl = strInfluencerVideoUrl
        default:
            break
        }
        
        if strVideoUrl != ""{
            if let videoURL = URL(string:strVideoUrl){
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }else{
                // AppSharedClass.shared.showAlert(title: "Alert", message: "Something went wrong!")
            }
        }
        
        
    }
    
    @IBAction func btnMyAudioPlay(_ sender: Any) {
        
        if self.imgVwPlayStopInfluencerAudio.image == #imageLiteral(resourceName: "audio_stop_ico"){
                   self.player?.stop()
                   self.imgVwPlayStopInfluencerAudio.image = #imageLiteral(resourceName: "audio_play_ico")
               }
               
               
               if self.player?.isPlaying == true{
                   self.player?.stop()
                   self.imgVwPlayStopMyAudio.image = #imageLiteral(resourceName: "audio_play_ico")
               }else{
                 self.imgVwPlayStopMyAudio.image = #imageLiteral(resourceName: "audio_stop_ico")
                   let url:URL? = URL(string: strMyAudioUrl)
                   if url != nil{
                       // playAudio(strAudioUrl: url!)
                     //  play(url: url! as NSURL)
                       playMedia(strDestinationUrl: url!)
                   }
               }
               
        
//        if self.strMyAudioUrl != ""{
//            let audioUrl = NSURL(string: self.strMyAudioUrl)
//            self.playAudio(strAudioUrl: audioUrl! as URL)
//        }
    }
    @IBAction func btnOnInfluencerVideoPlay(_ sender: Any) {
        
        if self.imgVwPlayStopMyAudio.image == #imageLiteral(resourceName: "audio_stop_ico"){
                   self.player?.stop()
                   self.imgVwPlayStopMyAudio.image = #imageLiteral(resourceName: "audio_play_ico")
               }
               
               if self.player?.isPlaying == true{
                   self.imgVwPlayStopInfluencerAudio.image = #imageLiteral(resourceName: "audio_play_ico")
                   self.player?.stop()
               }else{
                   self.imgVwPlayStopInfluencerAudio.image = #imageLiteral(resourceName: "audio_stop_ico")
                   let url:URL? = URL(string: strInfluencerAudioUrl)
                   if url != nil{
                       playMedia(strDestinationUrl: url!)
                   }
               }
        
//        if self.strInfluencerAudioUrl != ""{
//            let audioUrl = NSURL(string: self.strInfluencerAudioUrl)
//            self.playAudio(strAudioUrl: audioUrl! as URL)
//        }
    }
    
    
    //MARK:- Button Pay Now
    @IBAction func btnPayNowAudio(_ sender: Any) {
        //self.vwAlert.isHidden = false
        self.call_WsGetCall()
    }
    
    @IBAction func btnPayNowVideo(_ sender: Any) {
        self.call_WsGetCall()
    }
    @IBAction func btnCloseBalanceAlert(_ sender: Any) {
        self.vwAlert.isHidden = true
    }
    
    //MARK:- SubVwButton Action
    @IBAction func btnOnCloseLeftSideSubVw(_ sender: Any) {
        self.btnPlayAudioSubVw.isUserInteractionEnabled = true
        playBacktimeCount = 0
        playBackTimer.invalidate()
        self.imgVwWaveFormGif.image = nil
        self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
        self.vwContainerAudio.isHidden = true
        self.vwContainPlayBtn.isHidden = true
        self.btnRetake.setTitle("RECORD", for: .normal)
        isComingFromCrossSubVw = true
        isRecording = false
        self.player?.stop()
        self.StopRecordingAndNavigation()
        self.timer.invalidate()
        self.lblTimeAudio.text = "00:00"
        self.lblDoneRightSide.isHidden = true
        self.vwContainPlayBtn.isHidden = true
    }
    @IBAction func btnOnCloseRightSideSubVw(_ sender: Any) {
         self.btnPlayAudioSubVw.isUserInteractionEnabled = true
        playBacktimeCount = 0
        playBackTimer.invalidate()
        self.imgVwWaveFormGif.image = nil
        self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
        self.vwContainerAudio.isHidden = true
        self.vwContainPlayBtn.isHidden = true
        self.isComingFromCrossSubVw = false
        self.btnRetake.setTitle("RECORD", for: .normal)
        self.timer.invalidate()
        self.lblTimeAudio.text = "00:00"
        self.player?.stop()
        self.vwContainPlayBtn.isHidden = true
        
    }
    
    @IBAction func btnOnRetakeStartStop(_ sender: Any) {
        
        if self.btnRetake.currentTitle == "RECORD"{
            playBacktimeCount = 0
            playBackTimer.invalidate()
            self.imgVwWaveFormGif.image = nil
            self.imgVwWaveFormGif.image = UIImage.gifImageWithName("audio")
            
            self.isComingFromCrossSubVw = false
            self.btnRetake.setTitle("STOP", for: .normal)
            self.vwContainPlayBtn.isHidden = true
            self.btnCancelRightSideTop.isUserInteractionEnabled = false
            if(isRecording)
            {
                isRecording = false
            }
            else
            {
                //self.imgVwWaveGIF.image = UIImage.gifImageWithName("sound_img")
                setup_recorder()
                counter = 15
                timer.invalidate()
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
                isRecording = true
            }
        }else if self.btnRetake.currentTitle == "STOP"{
            playBacktimeCount = 0
            playBackTimer.invalidate()
            self.imgVwWaveFormGif.image = nil
            self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
            self.isComingFromCrossSubVw = false
            self.btnRetake.setTitle("RETAKE", for: .normal)
            self.StopRecordingAndNavigation()
            self.lblDoneRightSide.isHidden = false
            self.imgVwCancelRightSide.isHidden = true
            self.vwContainPlayBtn.isHidden = false
            self.btnCancelRightSideTop.isUserInteractionEnabled = true
            
        }else if self.btnRetake.currentTitle == "RETAKE"{
             self.btnPlayAudioSubVw.isUserInteractionEnabled = true
            playBacktimeCount = 0
            playBackTimer.invalidate()
            self.imgVwWaveFormGif.image = nil
            self.imgVwWaveFormGif.image = UIImage.gifImageWithName("audio")
            self.player?.stop()
            self.isComingFromCrossSubVw = false
            self.btnCancelRightSideTop.isUserInteractionEnabled = false
            self.lblDoneRightSide.isHidden = true
            self.btnRetake.setTitle("STOP", for: .normal)
            self.vwContainPlayBtn.isHidden = true
            setup_recorder()
            counter = 15
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
            isRecording = true
        }
        
        
    }
    @IBAction func btnOnPlaySubVwAudio(_ sender: Any) {
        if self.audioUrl != nil{
            self.imgVwWaveFormGif.image = UIImage.gifImageWithName("audio")
             self.btnPlayAudioSubVw.isUserInteractionEnabled = false
            playBackTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(playBackTimerAction), userInfo: nil, repeats: true)
            
            playAudio(strAudioUrl: self.audioUrl!)
            
        }else{
            print("Url Not Found")
        }
        
    }
    
    
    @IBAction func btnOnShareContentOnSocial(_ sender: AnyObject) {
        
        switch sender.tag {
        case 0:
            share(strText: "Star App sharing", strUrl: strTutorialVideoUrl)
        case 1:
            share(strText: "Star App sharing", strUrl: strMyAudioUrl)
        case 2:
            share(strText: "Star App sharing", strUrl: strMyVideoUrl)
        case 3:
            share(strText: "Star App sharing", strUrl: strInfluencerAudioUrl)
        case 4:
           share(strText: "Star App sharing", strUrl: strInfluencerVideoUrl)
    
        default:
            break
        }
            
    }
    
    
    func share(strText:String, strUrl:String){
        let share = [strText, strUrl]
        let activityViewController = UIActivityViewController(activityItems: share, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    //MARK:- Play Audio
    func playAudio(strAudioUrl:URL){
        
        //12000
        _ = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 44100.0,
            AVNumberOfChannelsKey: 1,
            AVEncoderBitRateKey: 192000,
            
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        let settings: [String: AnyObject] = [
            AVFormatIDKey : NSNumber(value: Int32(kAudioFormatAppleLossless) as Int32),
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue as AnyObject,
            AVEncoderBitRateKey: bitRate as AnyObject,
            AVNumberOfChannelsKey: channels as AnyObject,
            AVSampleRateKey: sampleRate as AnyObject
        ]
        do
        {
            self.player = try AVAudioPlayer(contentsOf: strAudioUrl as URL, fileTypeHint: AVFileType.mp3.rawValue)
            //self.player = try AVAudioPlayer(url: self.strAudioUrl as URL, settings: settings)
            
        }catch let error{
            print(error.localizedDescription)
        }
        
        
        do {
            
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            
            
        } catch let error{
            print(error.localizedDescription)
        }
        self.player?.delegate = self
        self.player?.prepareToPlay()
        self.player?.isMeteringEnabled = true
        self.player?.play()
        self.player?.volume = 1
        
        // displayLink = CADisplayLink(target: self, selector: #selector(updateMetersPlay))
        //displayLink.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
    }
    
    
    //Function
    func checkCameraPermissions(handler:(Bool)->Void) {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized:
            print("Allowed")
            handler(true)
        case .denied:
            handler(false)
            alertPromptToAllowCameraAccessViaSetting(strTitle:"Camera access required",strMessage: "Camera access is disabled please allow from Settings.")
        default:
            print("Allowed")
            handler(true)
        }
    }
}

extension CallDetail_VC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    //MARK:- Pick video
    func gotoCameraVC(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CameraVC")as! CameraVC
        vc.closerForDictMedia = { dict
        in
            print(dict)
            if dict.count != 0{
                self.mediaData = dict["mediaData"]as! Data
                self.pickedVideoThumbData = (dict["videoThumb"]as! Data)
               
                let videoUrl = dict["videoUrl"]as! URL
                self.strMyVideoUrl = "\(String(describing: videoUrl))"
                 print(videoUrl)
                self.generateImage(videoUrl)
                self.vwMyVideo.isHidden = false
                self.vwMyAudio.isHidden = true
                self.strCallType = "2"
            }
            
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    func pickVideo(){
        objAppShareData.openImagePickerForVideo(controller: self, success: { (imageInfo) in
            if var videoUrl = imageInfo["imageUrl"] as? URL{
                let strUrl = "\(videoUrl)"
                
                self.imagePicker.videoQuality = UIImagePickerController.QualityType.typeHigh
                self.imagePicker.videoQuality = .typeHigh
                self.imagePicker.delegate = self
                self.imagePicker.videoMaximumDuration = 5.0
                self.imagePicker.allowsEditing = false
                
                self.encodeVideo(videoUrl: videoUrl) { (convertedUrl) in
                    videoUrl = convertedUrl!
                    print("MP4---VideoUrl>>>>>\(videoUrl)")
                    let data = NSData(contentsOf: videoUrl as URL)!
                    self.mediaData = data as Data
                    print("MediaData>>>>",self.mediaData)
                }
                
                self.vwMyVideo.isHidden = false
                self.vwMyAudio.isHidden = true
                self.btnPlayMyVideo.isHidden = true
                self.strCallType = "2"
                let thumbData = self.generateThumbImageData(videoUrl)
                print("Thumb Data>>>>>\(thumbData)")
                self.pickedVideoThumbData = thumbData
                self.compressedURL = videoUrl
                
                self.generateImage(videoUrl)
                
                
            }
        }){ (Error) in
            print("Error Occured")
        }
        
    }
    
    */
    
    //MARK:- Generate video thumbnail image
    func generateImage(_ URL: URL) -> UIImage {
        var image = UIImage()
        let asset = AVAsset.init(url: URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true;
        
        let maxSize = CGSize(width: 400, height: 400)
        assetImageGenerator.maximumSize = maxSize
        var time = asset.duration
        time.value = min(time.value, 2)
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            image =  UIImage.init(cgImage: imageRef)
            DispatchQueue.main.async {
                self.imgVwMyVideo.image = image
                //self.pickedImage = image
            }
            
            return image
        } catch {
        }
        return image
    }
    
    func generateThumbImageData(_ URL: URL) -> Data {
        var data:Data?
        let asset = AVAsset.init(url: URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true;
        
        let maxSize = CGSize(width: 300, height: 350)
        assetImageGenerator.maximumSize = maxSize
        var time = asset.duration
        time.value = min(time.value, 2)
        do {
            
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            let image =  UIImage.init(cgImage: imageRef)
            data = image.pngData()
            return data!
        } catch {
        }
        return data!
    }
    
    
    
    //Default PopUp
    func alertPromptToAllowCameraAccessViaSetting(strTitle:String,strMessage:String) {
        
        let alert = UIAlertController(title: strTitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel) { (alert) -> Void in
            
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        })
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

//MARK:-  AudioRecording Work
extension CallDetail_VC: AVAudioPlayerDelegate,AVAudioRecorderDelegate{
    
    
    
    func check_record_permission()
    {
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSessionRecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSessionRecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (allowed) in
                if allowed {
                    self.isAudioRecordingGranted = true
                } else {
                    self.isAudioRecordingGranted = false
                }
            })
            break
        default:
            break
        }
    }
    
    
    func setup_recorder()
    {
        
        
        if isAudioRecordingGranted
        {
            let session = AVAudioSession.sharedInstance()
            do
            {
                
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord)
                //try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.record)
                
                try session.setActive(true)
                
                let settings: [String: AnyObject] = [
                    AVFormatIDKey : NSNumber(value: Int32(kAudioFormatAppleLossless) as Int32),
                    AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue as AnyObject,
                    AVEncoderBitRateKey: bitRate as AnyObject,
                    AVNumberOfChannelsKey: channels as AnyObject,
                    AVSampleRateKey: sampleRate as AnyObject
                ]
                
                
                do{
                    
                    self.recorder = try AVAudioRecorder(url: self.getFileUrl(), settings: settings)
                    print(self.recorder)
                    
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
                
                self.recorder?.delegate = self
                self.recorder?.prepareToRecord()
                self.recorder?.isMeteringEnabled = true
                self.recorder?.record()
                
                //DispatchQueue.main.async {
                
                //                    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                //                        self.displayLink = CADisplayLink(target: self, selector: #selector(self.updateMeters))
                //                        DispatchQueue.main.async {
                //                            self.displayLink.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
                //                        }
                //                    }
                //                   }
                
            }
                
            catch let error {
                //  display_alert(msg_title: "Error", msg_desc: error.localizedDescription, action_title: "OK")
            }
            
        }
        else
        {
            //  display_alert(msg_title: "Error", msg_desc: "Don't have access to use your microphone.", action_title: "OK")
        }
    }
    
    func getDocumentsDirectory() -> URL {
        
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        
        let url = NSURL(fileURLWithPath: path )
        return url as URL
        
        
    }
    
    func getFileUrl() -> URL
    {
        let filename = "myRecording.m4a"
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        self.audioUrl = filePath
        self.strMyAudioUrl = "\(filePath)"
        return filePath
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    @objc func timerAction() {
        print(counter)
        if counter >= 1{
            counter -= 1
            self.lblTimeAudio.text = "\(self.timeFormatted(counter))"
            
        }
        else
        {
            
            self.StopRecordingAndNavigation()
        }
    }
    
    @objc func playBackTimerAction() {
        print(playBacktimeCount)
        if playBacktimeCount < self.playBackTime{
            playBacktimeCount += 1
            self.lblTimeAudio.text = "\(self.timeFormatted(playBacktimeCount))"
            
        }
        else
        {
             self.btnPlayAudioSubVw.isUserInteractionEnabled = true
            playBacktimeCount = 0
            playBackTimer.invalidate()
            self.imgVwWaveFormGif.image = nil
            self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
        }
    }
    
    
    
    
    
    func StopRecordingAndNavigation()
    {
        self.timer.invalidate()
        if self.isComingFromCrossSubVw == true{
            self.isComingFromCrossSubVw = false
            self.imgVwWaveFormGif.image = nil
            self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
            recorder?.stop()
            self.vwMyAudio.isHidden = true
            self.vwMyVideo.isHidden = true
            recorder = nil
            
        }else{
            if recorder != nil
            {
                self.imgVwWaveFormGif.image = nil
                self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
                let last2 = self.lblTimeAudio.text!.suffix(2)
                let remaining  = Int(last2) ?? 0
                let clipTime = 15 - remaining
                self.playBackTime = clipTime
                let clip = "\(clipTime)"
                if clip.count != 2{
                    self.lblTimeAudio.text  = "00:0\(clipTime)"
                }else{
                    self.lblTimeAudio.text  = "00:\(clipTime)"
                }
                recorder?.stop()
                print(recorder?.url.absoluteString ?? "")
                print(recorder.self ?? "")
                print(recorder?.url ?? "")
                if FileManager.default.fileExists(atPath: recorder?.url.path ?? ""){
                    
                    if let cert = NSData(contentsOfFile: recorder?.url.path ?? "") {
                        
                        self.audioData = cert as Data
                        self.mediaData = cert as Data
                        // print(cert)
                    }
                    print("Audio Recorder finished successfully")
                    //   self.vwAudioRecorder.isHidden = true
                    //self.lblTimeAudio.text = "00:00"
                    recorder = nil
                    self.strCallType = "1"
                    self.btnRetake.setTitle("RETAKE", for: .normal)
                    self.lblDoneRightSide.isHidden = false
                    self.imgVwCancelRightSide.isHidden = true
                    self.vwContainPlayBtn.isHidden = false
                    self.btnCancelRightSideTop.isUserInteractionEnabled = true
                    self.strCallType = "1"
                    self.vwMyAudio.isHidden = false
                    self.vwMyVideo.isHidden = true
                }
                
                
                
                //recorder = AVAudioRecorder()
            }
            
            timer.invalidate()
            
            if displayLink !=  nil
            {
                displayLink.invalidate()
            }
            //   self.vwStop.isHidden = false
            isRecording = false
            //  self.navigateToAnother()
        }
    }
    
}

//MARK:- Call Webservice
extension CallDetail_VC{
    //MARK:- Get Create call
    func call_WsGetCall(){
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        var mediaType = ""
        var strMediaFileName:String?
        var strThumbFileName:String?
        var strMimeType:String?
        var strAmount:String = ""
        var imageData:Data?
        
        
        if self.strCallType == "1"{
            mediaType = "Audio"
            strMediaFileName = "user_media"
            strThumbFileName = nil
            strMimeType = "audio/m4a"
            imageData = nil
            strAmount = "\(self.audioPrice)"
        }else{
            mediaType = "Video"
            strMediaFileName = "user_media"
            strThumbFileName = "user_video_thumb"
            imageData = pickedVideoThumbData
            strMimeType = "video/MOV"
            strAmount = "\(self.videoPrice)"
        }
        
        let param = ["call_type":strCallType,
                     "amount":strAmount,
                     "influencer_id":objArrDiscover?.strInfluecerID ?? ""]as [String:Any]
        
        print(param)
        
        objWebServiceManager.uploadMultipartDataAudioVideo(strURL: BASE_URL + "call", params: param, showIndicator: false, strMediaType: mediaType, imageData: imageData, mediaData: self.mediaData, mediaFileName: strMediaFileName, thumbDataFileName: strThumbFileName, mimeType: strMimeType, success: { (response) in
            //objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            print(response)
            if status == "success"{
                self.vwBottom.isHidden = true
                if let data = response["data"]as? [String:Any]{
                    if let callDetailID = data["callID"]as? String{
                        self.call_WsGetDetails(strCallDetailID: callDetailID)
                    }else if let callDetailID = data["callID"]as? Int{
                        self.call_WsGetDetails(strCallDetailID: "\(callDetailID)")
                    }
                }else{
                    objWebServiceManager.hideIndicator()
                    objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                }
            }else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
            }
        }) { (Error) in
            
            print(Error)
        }
        
        
    }
    
    
    //MARK:- Call WS GET Balance Plan List
    func call_WsGetBalancePlanList(){
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        
        objWebServiceManager.requestGet(strURL: WsUrl.Url_BalancePlanList, params: nil, strCustomValidation: "", success: { (response) in
            print(response)
            objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            
            if status == "success"{
                
                if let data = response["data"]as? [String:Any]{
                    
                    if let arrPlanList = data["plans"]as? [[String:Any]]{
                        
                        
                        for dictPlan in arrPlanList{
                            let obj = BalancePlanListModal.init(dict: dictPlan)
                            self.arrBalance.append(obj)
                        }
                        
                        self.cvBalance.reloadData()
                        
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                    }
                }else{
                    objWebServiceManager.hideIndicator()
                    objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                }
            }else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
            }
        }) { (Error) in
            print(Error)
        }
        
    }
    
    
    //MARK:- Call WS Purchase Plan
    func callWSPurchasePlan(strCreditPlanID:String){
        
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        
        let param = ["creditPlanID":strCreditPlanID]as [String:Any]
        
        objWebServiceManager.requestPost(strURL: WsUrl.Url_PurchasePlan, params: param, strCustomValidation: "", showIndicator: false, success: { (response) in
            objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            print(response)
            if status == "success"{
                self.vwAlert.isHidden = true
                objAlert.showAlert(message: "Purchase successfully" , title: "Success", controller: self)
                if let data = response["data"]as? [String:Any]{
                    if let dictCreditDetail = data["credit_detail"]as? [String:Any]{
                        
                        var userDict = UserDefaults.standard.value(forKey: UserDefaults.KeysDefault.userInfo)as? [String:Any] ?? [:]
                        
                        if let free = dictCreditDetail["free"]as? String{
                            userDict[UserDefaults.KeysDefault.freeStars] = free
                        }
                        
                        if let paid = dictCreditDetail["paid"]as? String{
                            userDict[UserDefaults.KeysDefault.paidStars] = paid
                        }
                        
                        if let total = dictCreditDetail[UserDefaults.KeysDefault.availableStars]as? String{
                            userDict[UserDefaults.KeysDefault.availableStars] = total
                        }
                        
                        UserDefaults.standard.setValue(userDict, forKey: UserDefaults.KeysDefault.userInfo)
                        
                        print(userDict)
                        
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                    }
                }else{
                    objWebServiceManager.hideIndicator()
                    objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                }
                
            }else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                
            }
        }) { (Error) in
            print(Error)
        }
        
    }
    
    
    //MARK:- Call WS Get Call Datails
    func call_WsGetDetails(strCallDetailID: String){
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        
        objWebServiceManager.requestGet(strURL: BASE_URL + "call/\(strCallDetailID)", params: nil, strCustomValidation: "", success: { (response) in
            print(response)
            objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            
            if status == "success"{
                
                if let data = response["data"]as? [String:Any]{
                    
                    if let callDetails = data["call_detail"]as? [String:Any]{
                        
                        let objData = CallDetailsModal.init(dict: callDetails)
                        
                       // if self.isComingFrom == "Notification"{
                            self.WebserviceForGetCreditList()
                            self.lblUserName.text = objData.strInfluencerName
                            if objData.strInfluencerProfilePicUrl != "" {
                                let url = URL(string: objData.strInfluencerProfilePicUrl)
                                self.imgVwUserHeader.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "basic_info_ico"))
                            }else{
                                self.imgVwUserHeader.image = #imageLiteral(resourceName: "placeholder_img")
                            }
                        
                        
                            if objData.strTutorialVideoThumb != ""{
                                self.vwContainTutorialVid.isHidden = false
                                let url = URL(string: objData.strTutorialVideoThumb)
                                self.imgVwTutorial.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "image_placeholder"))
                                self.strTutorialVideoUrl = objData.strTutorialVideoUrl
                            }else{
                                self.vwContainTutorialVid.isHidden = true
                                self.imgVwTutorial.image = #imageLiteral(resourceName: "image_placeholder")
                            }
//                        }else{
//
//                        }
                        
                        if objData.strProgressStatus == "2"{
                            self.lblResponseIn3Days.isHidden = true
                            self.vwResponsein3Days.isHidden = true
                        }else{
                            self.lblResponseIn3Days.isHidden = false
                            self.vwResponsein3Days.isHidden = false
                        }
                        
                        if objData.strCallType == "1"{
                            self.btnPayNowUser.isHidden = true
                            self.vwMyVideo.isHidden = true
                            self.vwInfluencerVideo.isHidden = true
                            self.vwMyAudio.isHidden = false
                            
                            self.strMyAudioUrl = objData.strUser_media
                            self.btnMyAudioPlay.isHidden = false
                            //save media in local
                            self.saveUserAudioInLoacal(strUrl: objData.strUser_media)
                            //DoubleTick
                                if objData.strIsSeen == "0"{
                                    self.imgVwReadUnreadAudio.image = #imageLiteral(resourceName: "gray_tick_ico")
                                    self.imgVwReadUnreadAudio.isHidden = false
                                }else{
                                    self.imgVwReadUnreadAudio.image = #imageLiteral(resourceName: "tick_ico")
                                    self.imgVwReadUnreadAudio.isHidden = false
                                }
                                
                            if objData.strInfluencer_media != ""{
                                self.strInfluencerAudioUrl = objData.strInfluencer_media
                                self.saveInfluencerAudioInLoacal(strUrl: objData.strInfluencer_media)
                                self.vwInfluencerAudio.isHidden = false
                                self.btnInfluencerAudioPlay.isHidden = false
                            }else{
                                self.vwInfluencerAudio.isHidden = true
                            }
                            
                        }else{
                            self.btnPayNowVideo.isHidden = true
                            self.vwMyVideo.isHidden = false
                            self.strMyVideoUrl = objData.strUser_media
                            self.btnPlayMyVideo.isHidden = false
                            if objData.strUser_video_thumb != "" {
                                let url = URL(string: objData.strUser_video_thumb)
                                self.imgVwMyVideo.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "image_placeholder"))
                            }else{
                                self.imgVwUserHeader.image = #imageLiteral(resourceName: "image_placeholder")
                            }
                            
                            //DoubleTick
                                if objData.strIsSeen == "0"{
                                    self.imgVwReadUnreadVideo.image = #imageLiteral(resourceName: "gray_tick_ico")
                                    self.imgVwReadUnreadVideo.isHidden = false
                                }else{
                                    self.imgVwReadUnreadVideo.image = #imageLiteral(resourceName: "tick_ico")
                                    self.imgVwReadUnreadVideo.isHidden = false
                                }
                            
                            if objData.strInfluencer_media != ""{
                                
                                self.strInfluencerVideoUrl = objData.strInfluencer_media
                                self.vwInfluencerVideo.isHidden = false
                                
                                if objData.strInfluencer_video_thumb != "" {
                                    let url = URL(string: objData.strInfluencer_video_thumb)
                                    self.imgVwInfluencerVideo.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "image_placeholder"))
                                }else{
                                    self.imgVwUserHeader.image = #imageLiteral(resourceName: "image_placeholder")
                                }
                                
                            }else{
                                self.vwInfluencerVideo.isHidden = true
                            }
                            
                            self.vwMyAudio.isHidden = true
                            self.vwInfluencerAudio.isHidden = true
                        }
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                    }
                }else{
                    objWebServiceManager.hideIndicator()
                    objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                }
            }else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
            }
        }) { (Error) in
            print(Error)
        }
        
    }
    
    //MARK:- WS Get Shedule List
    func callWebserviceGetSheduleList(strInfulencerID:String){
        
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        objWebServiceManager.showIndicator()
        
        let userOrInfluencer = "true"
        
        objWebServiceManager.requestGet(strURL: WsUrl.ScheduleList+"for_call="+userOrInfluencer+"&influencer_id="+strInfulencerID, params: nil, strCustomValidation: "", success: {response in
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if status == k_success{
                objWebServiceManager.hideIndicator()
                var dataFound:Int?
                if let data = response["data"]as? [String:Any]{
                    dataFound = data["data_found"] as? Int ?? 0
                    if status == "success" || status == "Success"
                    {
                        
                        if let arrData = data["schedule_list"]as? [[String:Any]]{
                            
                            if arrData.count != 0{
                                for dictData in arrData{
                                    let obj = SheduleModel.init(dict: dictData)
                                    self.arrSheduleData.append(obj)
                                }
                            }
                            
                            if self.arrSheduleData.count != 0{
                                let objArrData = self.arrSheduleData[0]
                                self.audioPrice = Double(objArrData.strAudioRate)!
                                self.videoPrice = Double(objArrData.strVideoRate)!
                                let audPrice = objArrData.strAudioRate
                                let vidPrice = objArrData.strVideoRate
                                let newPrice = Double(objArrData.strAudioRate)
                                let cleanValue = newPrice?.clean
                                let finalAudValue = String(cleanValue!)
                                
                                let newVidPrice = Double(objArrData.strVideoRate)
                                let cleanVidValue = newVidPrice?.clean
                                let finalVidValue = String(cleanVidValue!)
                                
                                self.lblAudioPrice.text = finalAudValue
                                self.lblVideoPrice.text = finalVidValue
                                
                                if audPrice == ""{
                                    self.vwContainAudioPrice.isHidden = true
                                }else if vidPrice == ""{
                                    self.vwContainVideoPrice.isHidden = true
                                }else if audPrice != "" && vidPrice != "" {
                                    self.vwContainAudioPrice.isHidden = false
                                    self.vwContainVideoPrice.isHidden = false
                                }
                            }
                        }else{
                            
                            objWebServiceManager.hideIndicator()
                            self.showSimpleAlert()
                            // objAlert.showAlert(message: message ?? "" , title: kAlertTitle, controller: self)
                            
                        }
                        
                    }else{
                        objWebServiceManager.hideIndicator()
                        // objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                    }
                }
            }
            else{
                objWebServiceManager.hideIndicator()
                if message == "Your token has been expired"
                {self.postAlert(strMessage: message ?? "")
                }else{
                    objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                }
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
    }
    
    
    //MARK:- Follow Unfollow API
    func callApiFollowUnFollow(){
        
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
            
        }
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        
        let StrUrl = WsUrl.Url_FollowUnfollow + String(objArrDiscover?.strInfluecerID ?? "")
        print(StrUrl)
        print(objArrDiscover?.strInfluecerID)
        
        
        objWebServiceManager.requestPut(strURL: StrUrl, params: nil, strCustomValidation: "", showIndicator: false, success: {
            response in
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if status == k_success{
                objWebServiceManager.hideIndicator()
                // self.callWs_GetDiscoverList(strSearchText: "")
                if let data = response["data"]as? [String:Any]{
                    
                    if status == "success" ||  status == "Success"
                    {
                        
                        if message == "Followed successfully"{
                            self.imgVwFollowUnFollow.image = #imageLiteral(resourceName: "follow_tick_ico")
                        }else{
                            self.imgVwFollowUnFollow.image = #imageLiteral(resourceName: "follow_ico")
                        }
                        //self.getDiscoverList()
                        
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                    }
                }
            }
            else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
        
        
    }
    
    
    func showSimpleAlert() {
        let alert = UIAlertController(title: "Alert", message: "Influencer is not available right now.",         preferredStyle: UIAlertController.Style.alert)
        
        //        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { _ in
        //            //Cancel Action
        //        }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func postAlert(strMessage: String)
    {
        // Create the alert controller
        let alertController = UIAlertController(title: "", message: strMessage , preferredStyle: .alert)
        
        let subview = alertController.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.layer.cornerRadius = 10
        alertContentView.alpha = 1
        alertContentView.layer.borderWidth = 1
        alertContentView.layer.borderColor = UIColor.gray.cgColor
        // alertController.view.tintColor = UIColor.red.cgColor
        
        // Create the actions
        let okAction = UIAlertAction(title:"Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            objAppDelegate.showLogInNavigation()
        }
        
        // Add the actions
        
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
}
extension UIView{
    func setViewShadowAndLessCorner() {
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        layer.shadowOpacity = 0.9
        layer.cornerRadius = 5
    }
}

extension CallDetail_VC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrBalance.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BalancePlansCVCell", for: indexPath as IndexPath) as! BalancePlansCVCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        
        let obj = arrBalance[indexPath.row]
        cell.lblAmount.text = obj.strAmount
        cell.lblAED.text = "AED " + (obj.strAmount ?? "")
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = arrBalance[indexPath.row]
        if obj.strCreditPlanID != ""{
            self.callWSPurchasePlan(strCreditPlanID: obj.strCreditPlanID!)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = CGFloat((self.cvBalance.frame.size.width ) / 3.9)
        let cellHeight = cellWidth/2
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    
}


extension CallDetail_VC {
    func encodeVideo(videoUrl: URL, outputUrl: URL? = nil, resultClosure: @escaping (URL?) -> Void ) {
        
        var finalOutputUrl: URL? = outputUrl
        
        if finalOutputUrl == nil {
            var url = videoUrl
            url.deletePathExtension()
            url.appendPathExtension("mp4")
            finalOutputUrl = url
        }
        
        if FileManager.default.fileExists(atPath: finalOutputUrl!.path) {
            print("Converted file already exists \(finalOutputUrl!.path)")
            resultClosure(finalOutputUrl)
            return
        }
        
        let asset = AVURLAsset(url: videoUrl)
        if let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetPassthrough) {
            exportSession.outputURL = finalOutputUrl!
            exportSession.outputFileType = AVFileType.mp4
            let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
            let range = CMTimeRangeMake(start: start, duration: asset.duration)
            exportSession.timeRange = range
            exportSession.shouldOptimizeForNetworkUse = true
            exportSession.exportAsynchronously() {
                
                switch exportSession.status {
                case .failed:
                    print("Export failed: \(exportSession.error != nil ? exportSession.error!.localizedDescription : "No Error Info")")
                case .cancelled:
                    print("Export canceled")
                case .completed:
                    resultClosure(finalOutputUrl!)
                default:
                    break
                }
            }
        } else {
            resultClosure(nil)
        }
    }
     func WebserviceForGetCreditList(){

           // objWebServiceManager.showIndicator()
        
                objWebServiceManager.requestGet(strURL: WsUrl.Url_GetCredits, params: nil, strCustomValidation: "", success: { (response) in
                    print(response)
               let status = (response["status"] as? String)
                           let message = (response["message"] as? String)
                           if status == k_success{
                               var dataFound:Int?
               
                               //objWebServiceManager.hideIndicator()
                               if let data = response["data"]as? [String:Any]{
                                   dataFound = data["data_found"] as? Int ?? 0
                                   if status == "success" ||  status == "Success"
                                   {
                                       if dataFound == 0
                                       {
                                        //   self.viewNoDataFound.isHidden = false
                                        
                                        //objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
               
                                       }
                                       else{
               
                                              let credit_details  = data["credit_details"] as? [String:Any]
                                                self.parseData(dict:credit_details!)
                                        
                                       }
                                       
               
                                   }else{
    //                                   objWebServiceManager.hideIndicator()
    //                                   objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                                   }
                               }
                           }
                           else{
    //                           objWebServiceManager.hideIndicator()
    //                           objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                           }
                       }, failure: { (error) in
                           print(error)
    //                       objWebServiceManager.hideIndicator()
    //                       objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
                       })
                   }
        func parseData(dict : [String:Any])
        {
           
            if let available = dict["available"] as? String{
                  
                  objAppShareData.UserDetail.availableStars = available
                if let userDic = UserDefaults.standard.value(forKey:  UserDefaults.KeysDefault.userInfo) as? [String : Any]{
                    var dict = userDic
                    dict["available"] = objAppShareData.UserDetail.availableStars
                    UserDefaults.standard.setValue(dict, forKey: UserDefaults.KeysDefault.userInfo)
                    UserDefaults.standard.synchronize()
                }
            }
            else if let available = dict["available"] as? Int{
                
                objAppShareData.UserDetail.availableStars = String(available)
                if let userDic = UserDefaults.standard.value(forKey:  UserDefaults.KeysDefault.userInfo) as? [String : Any]{
                    var dict = userDic
                    dict["available"] = objAppShareData.UserDetail.availableStars
                    UserDefaults.standard.setValue(dict, forKey: UserDefaults.KeysDefault.userInfo)
                    UserDefaults.standard.synchronize()
                }
            }
        }
}


//MARK:- Audio Playback Work
extension CallDetail_VC{
    
    func saveUserAudioInLoacal(strUrl:String){
        
        if let audioUrl = URL(string: strUrl) {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                self.strMyAudioUrl = "\(destinationUrl)"
               // self.playMedia(strDestinationUrl: destinationUrl)
                
                // if the file doesn't exist
            } else {
                
                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        self.strMyAudioUrl = "\(destinationUrl)"
                       // self.playMedia(strDestinationUrl: destinationUrl)
                        print("File moved to documents folder")
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }).resume()
            }
        }
        
    }
    
    func saveInfluencerAudioInLoacal(strUrl:String){
           
           if let audioUrl = URL(string: strUrl) {
               
               // then lets create your document folder url
               let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
               
               // lets create your destination file url
               let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
               print(destinationUrl)
               
               // to check if it exists before downloading it
               if FileManager.default.fileExists(atPath: destinationUrl.path) {
                   print("The file already exists at path")
                   self.strInfluencerAudioUrl = "\(destinationUrl)"
                 //  self.playMedia(strDestinationUrl: destinationUrl)
                   
                   // if the file doesn't exist
               } else {
                   
                   // you can use NSURLSession.sharedSession to download the data asynchronously
                   URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                       guard let location = location, error == nil else { return }
                       do {
                           // after downloading your file you need to move it to your destination url
                           try FileManager.default.moveItem(at: location, to: destinationUrl)
                            self.strInfluencerAudioUrl = "\(destinationUrl)"
                          // self.playMedia(strDestinationUrl: destinationUrl)
                           print("File moved to documents folder")
                       } catch let error as NSError {
                           print(error.localizedDescription)
                       }
                   }).resume()
               }
           }
           
       }
    
    
    func playMedia(strDestinationUrl:URL){
        //if let audioUrl = URL(string: "http://freetone.org/ring/stan/iPhone_5-Alarm.mp3") {
        if let audioUrl:URL = strDestinationUrl {
                   // then lets create your document folder url
                   let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                   
                   // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
        
               //let url = Bundle.main.url(forResource: destinationUrl, withExtension: "mp3")!
               
               do {
                self.player = try AVAudioPlayer(contentsOf: destinationUrl)
                guard let player = self.player else { return }
                   
                   player.prepareToPlay()
                   player.play()
               } catch let error {
                   print(error.localizedDescription)
               }
               }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.imgVwPlayStopMyAudio.image = #imageLiteral(resourceName: "audio_play_ico")
        self.imgVwPlayStopInfluencerAudio.image = #imageLiteral(resourceName: "audio_play_ico")
    }
    
}
