//
//  BalancePlansCVCell.swift
//  Star
//
//  Created by RohitSingh on 24/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class BalancePlansCVCell: UICollectionViewCell {
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblAED: UILabel!
    
}
