import UIKit

class DiscoverCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var imgFollow: UIImageView!
    @IBOutlet weak var btnFollow: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.imgProfile.setImgCircle()
            self.viewImg.setviewCirclewhite()
            self.viewImg.setshadowViewCircle2()
            self.imgCountry.setImgCircleColor()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
