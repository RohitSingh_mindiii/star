//
//  BookedVC.swift
//  Design2
//
//  Created by IOS-Sagar on 06/02/20.
//  Copyright © 2020 IOS-Sagar. All rights reserved.
//

import UIKit

class BookedVC: UIViewController {

    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewNoDataFound: UIView!
    @IBOutlet weak var lblPoints: UILabel!

   var arrBookList = [CallListModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if objAppShareData.isFromNotification{
            if objAppShareData.strNotificationType == "Calls" || objAppShareData.strNotificationType == "Call"{
                self.goToCallDetailFromNotification()
                return
            }
        }
      //  self.viewHeader.setviewbottomShadow()
      //  self.viewHeader.setviewbottomShadow()
        self.viewHeader.layer.shadowColor =  UIColor(named: "Shadow")?.cgColor
        self.viewHeader.layer.masksToBounds = false
        self.viewHeader.layer.shadowOffset = CGSize(width: 0.0 , height: 5.0)
        self.viewHeader.layer.shadowOpacity = 0.2
        self.viewHeader.layer.shadowRadius = 4
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.callWebserviceGetBookList()
        //let Stars = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.Stars)
        let star = objAppShareData.UserDetail.availableStars
        let cleanValue = Double(star)
        let finalPoints = cleanValue?.clean
        self.lblPoints.text = finalPoints
    }
    
    @IBAction func btnActionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOnGoToBalanceDetailVC(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BalanceDetailsVC")as! BalanceDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension BookedVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBookList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "BookedTableCell") as! BookedTableCell
        
        if self.arrBookList.count>0{
            
        let obj = self.arrBookList[indexPath.row]
               cell.lblName.text = obj.strUserName
         let subtitle = obj.strCategory + ", " + obj.strSubCategoryName
               cell.lblDetails.text = subtitle
          
        let profilePic = obj.strUserImage
                if profilePic != "" {
                    let url = URL(string: profilePic)
                    cell.imgProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_img"))
                } else{
                    cell.imgProfile.image = #imageLiteral(resourceName: "placeholder_img")
                }
        
        if obj.strProgressStatus == "2"
        {
           cell.btnStatus.setTitle("Sent", for: .normal)
           //cell.btnStatus.setTitle("Pending", for: .normal)
           cell.btnStatus.titleLabel?.font = UIFont(name: "Montserrat-Medium", size:12.0)
           cell.btnStatus.setTitleColor(UIColor(named: "Text_Color"), for: .normal)
           cell.btnStatus.setCornerRadWithBoarder(color: UIColor(named: "Text_Color")!, cornerRadious: 4)
        }else{
            let color = #colorLiteral(red: 1, green: 0.3960784314, blue: 0.003921568627, alpha: 1)
            cell.btnStatus.setTitle("Pending", for: .normal)
            cell.btnStatus.titleLabel?.font = UIFont(name: "Montserrat-Medium", size:12.0)
            cell.btnStatus.setTitleColor(color, for: .normal)
            cell.btnStatus.setCornerRadWithBoarder(color: color , cornerRadious: 2)
           
        }
        
//            cell.btnStatus.addTarget(self, action: #selector(btnStatus(sender:)), for: UIControl.Event.touchUpInside)
//
//             if obj.isSelected == true{
//                 cell.btnStatus.backgroundColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
//                 cell.btnStatus.setTitleColor(.white, for: .normal)
//                 cell.btnStatus.setTitle("default", for: .normal)
//             }else{
//                 cell.btnStatus.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//                 cell.btnStatus.setTitleColor(.black, for: .normal)
//                 cell.btnStatus.setTitle("Make default", for: .normal)
//
//             }

        return cell
        }else{
          return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.arrBookList[indexPath.row]
        let vc = UIStoryboard(name: "Discover", bundle: Bundle.main).instantiateViewController(withIdentifier: "CallDetail_VC")as! CallDetail_VC
        vc.objArrBooked = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func goToCallDetailFromNotification(){
        let obj = CallListModel.init(dict: [:])
        if let callId = objAppShareData.notificationDict["reference_id"] as? String{
            obj.strCallID = callId
        }else if let callId = objAppShareData.notificationDict["reference_id"] as? Int{
            obj.strCallID = String(callId)
        }
        objAppShareData.isFromNotification = false
        objAppShareData.strNotificationType = ""
        let vc = UIStoryboard(name: "Discover", bundle: Bundle.main).instantiateViewController(withIdentifier: "CallDetail_VC")as! CallDetail_VC
        vc.objArrBooked = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension BookedVC
{
   func callWebserviceGetBookList(){
         self.arrBookList.removeAll()
          objWebServiceManager.showIndicator()
    
      //let CallUrl = "https://dev.stars.app/api/v1/call/list?" + "is_booked=true"
    let CallUrl = BASE_URL + "call/list?" + "is_booked=true"
    
    objWebServiceManager.requestGet(strURL:CallUrl,params: nil, strCustomValidation: "", success: {response in
              print(response)
              let status = (response["status"] as? String)
              let message = (response["message"] as? String)
              if status == k_success{
                  var dataFound:Int?
                  objWebServiceManager.hideIndicator()
                  if let data = response["data"]as? [String:Any]{
                       dataFound = data["data_found"] as? Int ?? 0
                      if status == "success" ||  status == "Success"
                      {
                          if dataFound == 0
                        {
                            self.viewNoDataFound.isHidden = false
                            return
                        }else{
                            
                            let arrAllReqData = data["call_list"] as! [[String: Any]]
                              
                              for dictGetData in arrAllReqData
                              {
                                  let objPendingData = CallListModel.init(dict: dictGetData)
                                  
                                  self.arrBookList.append(objPendingData)
                              }
                            self.tableView.reloadData()
                            
                            if self.arrBookList.count == 0
                            {
                                self.viewNoDataFound.isHidden = false
                            }else{
                                self.viewNoDataFound.isHidden = true
                            }
                        }
                   
                      }else{
                          objWebServiceManager.hideIndicator()
                          objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                      }
                  }
              }
              else{
                  objWebServiceManager.hideIndicator()
                 if message == "Your token has been expired"
                  {self.postAlert(strMessage: message ?? "")
                  }else{
                      objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                  }
              }
          }, failure: { (error) in
              print(error)
              objWebServiceManager.hideIndicator()
              objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
          })
      }

func postAlert(strMessage: String)
   {
       // Create the alert controller
       let alertController = UIAlertController(title: "", message: strMessage , preferredStyle: .alert)
       
       let subview = alertController.view.subviews.first! as UIView
       let alertContentView = subview.subviews.first! as UIView
       alertContentView.layer.cornerRadius = 10
       alertContentView.alpha = 1
       alertContentView.layer.borderWidth = 1
       alertContentView.layer.borderColor = UIColor.gray.cgColor
       
       // Create the actions
       let okAction = UIAlertAction(title:"Ok", style: UIAlertAction.Style.default) {
           UIAlertAction in
           objAppDelegate.showLogInNavigation()
       }
       
       // Add the actions
       
       alertController.addAction(okAction)
       
       // Present the controller
       self.present(alertController, animated: true, completion: nil)
   }
}

protocol cornerProtocol
{
    func cornerRadius(_ rad:Float)->Void
}
extension UILabel : cornerProtocol
{
    func cornerRadius(_ rad: Float)
    {
        self.layer.cornerRadius = CGFloat(rad)
        print("delegation function called from ViewController1")
        print("value of corner radius =- =- \(rad)")
    }
}
