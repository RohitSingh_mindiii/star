//
//  ViewController.swift
//  Design
//
//  Created by IOS-Sagar on 28/01/20.
//  Copyright © 2020 IOS-Sagar. All rights reserved.
//

import UIKit
import FBSDKShareKit
import FacebookCore
import FBSDKCoreKit
import Social

class BalanceDetailsVC: UIViewController {
    
    var arrBalance = [BalancePlanListModal]()
    var arrCreditsType = [BalanceModel]()
    var dictCredit = [String:Any]()
    var arrCredit = [CommonModal]()
    var strType:String = ""
    var strCredit:String = ""
    
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var viewAlertContainer: UIView!
    @IBOutlet weak var lblTotalBalance: UILabel!
    @IBOutlet weak var collectionPlans: UICollectionView!
    @IBOutlet weak var viewStarLogoHeader: UIView!
    @IBOutlet weak var viewPaidBalanceLogo: UIView!
    @IBOutlet weak var lblRight: UILabel!
    @IBOutlet weak var lblLeft: UILabel!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewLeftPaidBalance: UIView!
    @IBOutlet weak var viewRightFreeBalances: UIView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var viewShareAppFaceBook: UIView!
    @IBOutlet weak var viewShareAppTwitter: UIView!
    @IBOutlet weak var viewShareAppWhatsapp: UIView!
    @IBOutlet weak var viewShareAppFBMessenger: UIView!
    @IBOutlet weak var imgFacebookview: UIImageView!
    @IBOutlet weak var imgTwitterView: UIImageView!
    @IBOutlet weak var imgWhatsappView: UIImageView!
    @IBOutlet weak var imgFBMessengerView: UIImageView!
    @IBOutlet weak var lblAvailable: UILabel!
    @IBOutlet weak var lblPaid: UILabel!
    @IBOutlet weak var lblFree: UILabel!
    @IBOutlet weak var btnBlurFacebook: UIButton!
    @IBOutlet weak var btnBlurTwitter: UIButton!
    
    //MARK:LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.viewAlert.isHidden = true
        self.lblRight.isHidden = true
        self.viewRightFreeBalances.isHidden = true
        self.viewLeftPaidBalance.isHidden = false
        self.viewAlertContainer.setViewRadius()
        self.viewStarLogoHeader.setviewbottomShadow()
        self.viewPaidBalanceLogo.setviewCircle()
        self.viewPaidBalanceLogo.setShadow()
        self.imgTwitterView.setShadow()
        self.imgFacebookview.setShadow()
        self.imgWhatsappView.setShadow()
        self.imgFBMessengerView.setShadow()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.arrCredit = objAppShareData.arrCreditData
        self.WebserviceForGetCreditList()
    }
    
    //MARK:Buttons Action
    
    @IBAction func btnActionRight(_ sender: Any) {
        self.viewLeftPaidBalance.isHidden = true
        self.viewRightFreeBalances.isHidden = false
        self.lblRight.isHidden = false
        self.lblLeft.isHidden = true
        btnRight.titleLabel?.textColor = #colorLiteral(red: 0.8039215686, green: 0.662745098, blue: 0.4901960784, alpha: 1)
        
        self.btnLeft.setTitleColor(UIColor(named: "Gray_Color"), for: .normal)
        self.btnRight.setTitleColor(#colorLiteral(red: 0.8039215686, green: 0.662745098, blue: 0.4901960784, alpha: 1), for: .normal)
        self.btnRight.titleLabel?.font = UIFont(name: "Montserrat-Medium", size:15.0)
        
    }
    
    @IBAction func btnActionLeft(_ sender: Any) {
        self.viewLeftPaidBalance.isHidden = false
        self.viewRightFreeBalances.isHidden = true
        self.lblRight.isHidden = true
        self.lblLeft.isHidden = false
        btnLeft.titleLabel?.textColor = #colorLiteral(red: 0.8039215686, green: 0.662745098, blue: 0.4901960784, alpha: 1)
        self.btnLeft.setTitleColor(#colorLiteral(red: 0.8039215686, green: 0.662745098, blue: 0.4901960784, alpha: 1), for: .normal)
        self.btnRight.setTitleColor(UIColor(named: "Gray_Color"), for: .normal)
        self.btnLeft.titleLabel?.font = UIFont(name: "Montserrat-Medium", size:15.0)
        
    }
    @IBAction func btnActionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        print("back button")
    }
    
    @IBAction func btnCloseAlert(_ sender: Any) {
        self.viewAlert.isHidden = true
        
        
    }
    
    @IBAction func btnAddCredit(_ sender: Any) {
        self.viewAlert.isHidden = false
        self.call_WsGetBalancePlanList()
        
    }
   
    @IBAction func btnShareAction(_ sender: UIButton) {
 
        if sender.tag == 0{
            
            let obj = self.arrCredit[0]
            self.strCredit = obj.strValue
            self.strType = obj.strID
            self.callWSFreeCredits(isComingFrom: "Facebook")
            
        }else if sender.tag == 1{
            let obj = self.arrCredit[1]
            self.strCredit = obj.strValue
            self.strType = obj.strID
            self.callWSFreeCredits(isComingFrom: "Twitter")
            
           
            
            
        }else if sender.tag == 2{
            print("Whatsapp")
            
            let obj = self.arrCredit[2]
            self.strCredit = obj.strValue
            self.strType = obj.strID
            self.callWSFreeCredits(isComingFrom: "Whatsapp")
            
            
        }else{
            print("Messenger")
            
            let obj = self.arrCredit[3]
            self.strCredit = obj.strValue
            self.strType = obj.strID
          //  self.callWSFreeCredits()
            self.callWSFreeCredits(isComingFrom: "Messenger")
            
                
            
                 
           
            
        }
        
   
    }
    
    func shareWhatsApp(){
        //       var urlString = "Hello Friends, Sharing some data here... !"
        //       var urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        //       var url  = NSURL(string: "whatsapp://send?text=\(urlStringEncoded!)")
        //////////////
        let urlWhats = "whatsapp://send?text=\("Star")"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.open(whatsappURL as URL)
                    //self.callWSFreeCredits()
                }
                else {
                    objAlert.showAlert(message: "Whatsapp app is not installed in your mobile please install first", title: "Alert", controller: self)
                }
            }
        }
        //////////////
        
        
        
    }
    func shareMessenger(){
        //       let userID = 4
        //       let urlStr = String(format: "fb-messenger://user-thread/%d", userID)
        //       let theUrl = NSURL(string: urlStr)
        //        [UIApplication.shared.openURL(theUrl! as URL as URL)]
        
        
        let id = "akbarkhanshinwar"
        // If you have User id the use this URL
        let urlWithId = "fb-messenger://user-thread/\(id)"
        // If you don't have User id then use this URL
        let urlWithoutId = "fb-messenger://user-thread"
        if let url = URL(string: urlWithId) {
            
            // this will open your Messenger App
            UIApplication.shared.open(url, options: [:], completionHandler: {
                (success) in
                
                if success == false {
                    // If Messenger App is not installed then it will open browser.
                    // If you have User id the use this URL
                    let urlWithIdForBrowser = "https://m.me/\(id)"
                    // If you don't have User id then use this URL
                    let urlWithoutIdForBrowser = "https://m.me"
                    let url = URL(string: urlWithIdForBrowser)
                    if UIApplication.shared.canOpenURL(url!) {
                        UIApplication.shared.open(url!)
                    }
                }
            })
        }
    }
    
    
    func checkFBStatus(){
        var strId = ""
        for obj in self.arrCreditsType{
            strId = obj.strType
            if String(strId) == "1"{
                self.imgFacebookview.image = #imageLiteral(resourceName: "facebook_btn_lock")
                self.btnBlurFacebook.isEnabled = true
                return
            }else{
                self.imgFacebookview.image = #imageLiteral(resourceName: "fb_img")
                self.btnBlurFacebook.isEnabled = true
            }
        }
        
        
    }
    
    func checkTwitterStatus(){
        var strId = ""
        for obj in self.arrCreditsType{
            strId = obj.strType
            if String(strId) == "2"{
                 self.imgTwitterView.image = #imageLiteral(resourceName: "twitter_btn_lock")
                 self.btnBlurTwitter.isEnabled = true
                return
            }else{
               self.imgTwitterView.image = #imageLiteral(resourceName: "twitter_img")
               self.btnBlurTwitter.isEnabled = true
            }
        }
    }
    
     }


//MARK:UICollectionViewDelegateFlowLayout

extension BalanceDetailsVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrBalance.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BalancePlansCVCell", for: indexPath as IndexPath) as! BalancePlansCVCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        
        let obj = arrBalance[indexPath.row]
        cell.lblAmount.text = obj.strAmount
        cell.lblAED.text = "AED " + (obj.strAmount ?? "")
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = arrBalance[indexPath.row]
        if obj.strCreditPlanID != ""{
            self.callWSPurchasePlan(strCreditPlanID: obj.strCreditPlanID!)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = CGFloat((self.collectionPlans.frame.size.width ) / 3.9)
        let cellHeight = cellWidth/2
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    
}





//MARK: Webservice for get credit list
extension BalanceDetailsVC {
    
    // TODO: Call WS GET Credit List
      
      func WebserviceForGetCreditList(){

        objWebServiceManager.showIndicator()
    
            objWebServiceManager.requestGet(strURL: WsUrl.Url_GetCredits, params: nil, strCustomValidation: "", success: { (response) in
                print(response)
           let status = (response["status"] as? String)
                       let message = (response["message"] as? String)
                       if status == k_success{
                           var dataFound:Int?
           
                           objWebServiceManager.hideIndicator()
                           if let data = response["data"]as? [String:Any]{
                               dataFound = data["data_found"] as? Int ?? 0
                               if status == "success" ||  status == "Success"
                               {
                                   if dataFound == 0
                                   {
                                    //   self.viewNoDataFound.isHidden = false
                                    
                                    objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
           
                                   }
                                   else{
           
                                          let credit_details  = data["credit_details"] as? [String:Any]
                                                        self.parseData(dict:credit_details!)
                                    
                                    let arrCreditData = data["free_credit_types"] as! [[String: Any]]
                                           for dictCreditData in arrCreditData
                                           {
                                           let objData = BalanceModel.init(dict: dictCreditData)
                                           self.arrCreditsType.append(objData)

                                           }
                                    self.checkFBStatus()
                                    self.checkTwitterStatus()
                                    print(" self.arrCreditsType.count is \( self.arrCreditsType.count)")
                                   }
                                   
           
                               }else{
                                   objWebServiceManager.hideIndicator()
                                   objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                               }
                           }
                       }
                       else{
                           objWebServiceManager.hideIndicator()
                           objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                       }
                   }, failure: { (error) in
                       print(error)
                       objWebServiceManager.hideIndicator()
                       objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
                   })
               }
        
    // TODO: Call WS GET Balance Plan List
    
    func call_WsGetBalancePlanList(){
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        
        
        objWebServiceManager.requestGet(strURL: WsUrl.Url_BalancePlanList, params: nil, strCustomValidation: "", success: { (response) in
            print(response)
            objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            
            if status == "success"{
                
                if let data = response["data"]as? [String:Any]{
                    
                    if let arrPlanList = data["plans"]as? [[String:Any]]{
                        
                        
                        for dictPlan in arrPlanList{
                            let obj = BalancePlanListModal.init(dict: dictPlan)
                            self.arrBalance.append(obj)
                        }
                        
                        self.collectionPlans.reloadData()
                        
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                    }
                }else{
                    objWebServiceManager.hideIndicator()
                    objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                }
            }else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
            }
        }) { (Error) in
            print(Error)
        }
        
    }
    
    
    // TODO: Call WS Purchase Plan
    func callWSPurchasePlan(strCreditPlanID:String){
        
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        
        let param = ["creditPlanID":strCreditPlanID]as [String:Any]
        
        objWebServiceManager.requestPost(strURL: WsUrl.Url_PurchasePlan, params: param, strCustomValidation: "", showIndicator: false, success: { (response) in
            objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            
            if status == "success"{
                /// self.vwAlert.isHidden = true
                if let data = response["data"]as? [String:Any]{
                    if let dictCreditDetail = data["credit_detail"]as? [String:Any]{
                        
                        var userDict = UserDefaults.standard.value(forKey: UserDefaults.KeysDefault.userInfo)as? [String:Any] ?? [:]
                        
                        if let free = dictCreditDetail["free"]as? String{
                            userDict[UserDefaults.KeysDefault.freeStars] = free
                        }
                        
                        if let paid = dictCreditDetail["paid"]as? String{
                            userDict[UserDefaults.KeysDefault.paidStars] = paid
                        }
                        
                        if let total = dictCreditDetail[UserDefaults.KeysDefault.availableStars]as? String{
                            userDict[UserDefaults.KeysDefault.availableStars] = total
                        }
                        
                        UserDefaults.standard.setValue(userDict, forKey: UserDefaults.KeysDefault.userInfo)
                        
                        print(userDict)
                        self.viewAlert.isHidden = true
                        self.WebserviceForGetCreditList()

                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                    }
                }else{
                    objWebServiceManager.hideIndicator()
                    objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                }
                
            }else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                
            }
        }) { (Error) in
            print(Error)
        }
        
    }
    // TODO: Call WS For Free Credits
    func callWSFreeCredits(isComingFrom:String){
        
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        
    //    let param = ["creditPlanID":strCreditPlanID]as [String:Any]
        
        
        
        let param = ["credit":strCredit,
                     "credit_type":strType,
            ]as [String:Any]
print("param is \(param)")
        
        objWebServiceManager.requestPost(strURL: WsUrl.Free_Credits, params: param, strCustomValidation: "", showIndicator: false, success: { (response) in
            
            print(response)
            //objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            //            let data = reponse["data"] as? [String:Any] ?? [:]
            //            let
            
            if status == "success"{
                self.WebserviceForGetCreditList()
                switch isComingFrom {
                case "Facebook":
                    if let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook) {
                        vc.setInitialText("Look at this great Star App!")
                        // vc.add(UIImage(named: "myImage.jpg")!)
                        vc.add(URL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4"))
                        self.present(vc, animated: true)
                    }
                case "Twitter":
                    if let vc = SLComposeViewController(forServiceType: SLServiceTypeTwitter){
                        vc.setInitialText("Look at this great Star App!")
                        // vc.add(UIImage(named: "myImage.jpg")!)
                        vc.add(URL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4"))
                        self.present(vc, animated: true)
                    }
                case "Whatsapp":
                    self.shareWhatsApp()
                case "Messenger":
                    self.shareMessenger()
                default:
                    break
                }
                if let data = response["data"]as? [String:Any]{
                    
                }else{
                    objWebServiceManager.hideIndicator()
                    objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                }
                
            }else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                
            }
        }) { (Error) in
            print(Error)
        }
        
    }
    
    
    func parseData(dict : [String:Any])
    {
        
        if let free = dict["free"] as? String{
            let cleanValue = Double(free)
            let finalValue = cleanValue?.clean
            let strFinal = String(finalValue!)
            self.lblFree.text = strFinal
        }
        else if let free = dict["free"] as? Int{
            let cleanValue = Double(free)
            let finalValue = cleanValue.clean
            let strFinal = String(finalValue)
            self.lblFree.text = String(strFinal)
        }
        
        if let paid = dict["paid"] as? String{
            let cleanValue = Double(paid)
            let finalValue = cleanValue?.clean
            let strFinal = String(finalValue!)
            self.lblPaid.text = strFinal
        }
        else if let paid = dict["paid"] as? Int{
            let cleanValue = Double(paid)
            let finalValue = cleanValue.clean
            let strFinal = String(finalValue)
            self.lblPaid.text = String(strFinal)
        }
        
        if let available = dict["available"] as? String{
            let cleanValue = Double(available)
            let finalValue = cleanValue?.clean
            let strFinal = String(finalValue!)
            self.lblAvailable.text = strFinal
            self.lblTotalBalance.text = strFinal
            objAppShareData.UserDetail.availableStars = available
            if let userDic = UserDefaults.standard.value(forKey:  UserDefaults.KeysDefault.userInfo) as? [String : Any]{
                var dict = userDic
                dict["available"] = objAppShareData.UserDetail.availableStars
                UserDefaults.standard.setValue(dict, forKey: UserDefaults.KeysDefault.userInfo)
                UserDefaults.standard.synchronize()
            }
        }
        else if let available = dict["available"] as? Int{
            let cleanValue = Double(available)
            let finalValue = cleanValue.clean
            let strFinal = String(finalValue)
            self.lblAvailable.text = strFinal
            self.lblTotalBalance.text = strFinal
            objAppShareData.UserDetail.availableStars = String(available)
            if let userDic = UserDefaults.standard.value(forKey:  UserDefaults.KeysDefault.userInfo) as? [String : Any]{
                var dict = userDic
                dict["available"] = objAppShareData.UserDetail.availableStars
                UserDefaults.standard.setValue(dict, forKey: UserDefaults.KeysDefault.userInfo)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    
}


