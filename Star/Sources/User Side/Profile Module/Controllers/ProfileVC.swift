//
//  ProfileVC.swift
//  Star
//
//  Created by ios-deepak b on 05/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet var vwHeader: UIView!
    @IBOutlet var vwContainUserProfile: UIView!
    @IBOutlet var imgVwUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblRealUserName: UILabel!
    @IBOutlet var btnEditProfile: UIButton!
    @IBOutlet var btnSetting: UIButton!
    @IBOutlet var btnDarkMode: UIButton!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet var imgDarkMode: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if objAppShareData.isFromNotification{
            if objAppShareData.strNotificationType == "Calls" || objAppShareData.strNotificationType == "Call"{
                self.goToBookedScreen()
                return
            }else if objAppShareData.strNotificationType == "Balance"{
                objAppShareData.isFromNotification = false
                objAppShareData.strNotificationType = ""
                self.goToBalanceScreen()
                return
            }
        }
        callWSGetProfileDetails()
      //  setviewbottomShadow()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callWSGetProfileDetails()
        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
        //let Stars = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.Stars)
        let star = objAppShareData.UserDetail.availableStars
        let cleanValue = Double(star)
        let finalPoints = cleanValue?.clean
        self.lblPoints.text = finalPoints
        let isDarkMode:Bool = userDefaults.bool(forKey: "isDarkMode")
        if isDarkMode == true{
            if #available(iOS 13.0, *) {
                self.imgDarkMode.image = #imageLiteral(resourceName: "on_toggle")
                self.btnDarkMode.isSelected = true
            }
        }else{
            if #available(iOS 13.0, *) {
                self.imgDarkMode.image = #imageLiteral(resourceName: "off_toggle")
                self.btnDarkMode.isSelected = false
            }
        }
    }
    
    func setviewbottomShadow(){
         self.vwHeader.layer.shadowColor = UIColor(named: "Shadow")?.cgColor
         self.vwHeader.layer.masksToBounds = false
         self.vwHeader.layer.shadowOffset = CGSize(width: 0.0 , height: 5.0)
         self.vwHeader.layer.shadowOpacity = 0.4
         self.vwHeader.layer.shadowRadius = 4
    }
    func goToBookedScreen(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookedVC")as! BookedVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func goToBalanceScreen(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BalanceDetailsVC")as! BalanceDetailsVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func btnOnGoTOBalanceVC(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BalanceDetailsVC")as! BalanceDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Button Actions
extension ProfileVC{
    @IBAction func btnOnEditProfile(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
      @IBAction func btnOnSetting(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Setting", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnOnDarkMode(_ sender: UIButton) {
        self.view.endEditing(true)

        if sender.isSelected == true{
            if #available(iOS 13.0, *) {
                UserDefaults.standard.setValue(true, forKey: "isDarkMode")
                self.imgDarkMode.image = #imageLiteral(resourceName: "on_toggle")
                objAppDelegate.window?.overrideUserInterfaceStyle = .dark
            }
            sender.isSelected = false
            
        }else{
            if #available(iOS 13.0, *) {
                self.imgDarkMode.image = #imageLiteral(resourceName: "off_toggle")
                UserDefaults.standard.setValue(false, forKey: "isDarkMode")

                objAppDelegate.window?.overrideUserInterfaceStyle = .light
            }
            sender.isSelected = true
        }
    }
    @IBAction func btnOnBalance(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BalanceDetailsVC") as! BalanceDetailsVC
               self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnOnBooked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookedVC") as! BookedVC
                 self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Call WebService
extension ProfileVC{
    func callWSGetProfileDetails(){
           let userDict = UserDefaults.standard.value(forKey: UserDefaults.KeysDefault.userInfo)as? [String:Any] ?? [:]
           
           let user_id = userDict["userID"]as? String ?? ""
           if !objWebServiceManager.isNetworkAvailable(){
               objWebServiceManager.hideIndicator()
               objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
               return
           }
           
           objWebServiceManager.showIndicator()
           self.view.endEditing(true)
           
           objWebServiceManager.requestGet(strURL: WsUrl.Url_GetUserProfile+user_id, params: nil, strCustomValidation: "", success: { (response) in
               print(response)
               objWebServiceManager.hideIndicator()
               let status = response["status"]as? String ?? ""
               let message = response["message"]as? String ?? ""
               
            if status == "success"{
                if let data = response["data"]as? [String:Any]{
                    
                    if let userDetails = data["user_details"]as? [String:Any]{
                        
                        
                        if let profilePic = userDetails["avatar"]as? String{
                            if profilePic != "" {
                                let url = URL(string: profilePic)
                                self.imgVwUser.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_img"))
                            }
                            
                        }
                        
                        if let name = userDetails["name"]as? String{
                            self.lblUserName.text = name
                        }
                        if let name = userDetails["username"]as? String{
                            self.lblRealUserName.text = "@" + name
                        }
                        
//                        if let nickName = userDetails["username"]as? String{
//                            self.tfUserNickName.text = nickName
//                        }
//
//                        if let email = userDetails["email"]as? String{
//                            self.tfEmail.text = email
//                        }
//
//                        if let gender = userDetails["gender"]as? String{
//                            self.lblGender.text = gender
//                        }
//
//                        self.tfPassword.text = "******"
//
//                        if let mobile = userDetails["phone_number"]as? String{
//                            self.tfMobile.text = mobile
//                        }
//
//                        if let dialCode = userDetails["phone_dial_code"]as? String{
//                            self.lblDialCode.text = dialCode
//                        }
//
//                        if let dob = userDetails["dob"]as? String{
//                            self.tfDOB.text = dob
//                        }
                        
                    }
                    
                }
                
            }else{
                
                
            }
               
           }) { (Error) in
               print(Error)
           }
           
           
       }
}
