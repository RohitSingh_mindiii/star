//
//  UpdateProfileVC.swift
//  Design2
//
//  Created by IOS-Sagar on 06/02/20.
//  Copyright © 2020 IOS-Sagar. All rights reserved.
//

import UIKit
import SDWebImage

class UpdateProfileVC: UIViewController,BottomSheetVCDelegate,RSCountrySelectedDelegate {
    
    //MARK:- Outlets
    @IBOutlet var vwContainer: UIView!
    @IBOutlet var vwContainUserImage: UIView!
    @IBOutlet var imgVwUserProfile: UIImageView!
    @IBOutlet var btnOpenImgPicker: UIButton!
    
    @IBOutlet var tfUserName: UITextField!
    @IBOutlet var tfUserNickName: UITextField!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var tfMobile: UITextField!
    @IBOutlet var tfDOB: UITextField!

    @IBOutlet var lblGender: UILabel!
    @IBOutlet var lblLanguage: UILabel!
    @IBOutlet var btnLanguage: UIButton!
    @IBOutlet var btnDOB: UIButton!
    @IBOutlet var btnGender: UIButton!
    @IBOutlet var lblDialCode: UILabel!
    @IBOutlet var vwContainGenderPicker: UIView!
    @IBOutlet var vwContainerGender: UIView!
    @IBOutlet var imgMaleCheck: UIImageView!
    @IBOutlet var imgFemaleCheck: UIImageView!
    @IBOutlet weak var lblPoints: UILabel!

    
    //MARK:- Variables
    let ImagePicker = UIImagePickerController()
    let datePicker = UIDatePicker()
    var strIndex = 0
    var arrGender:[BasicInfoModal] = []
    var arrLanguages = [CommonModal]()
    var pickedImage:UIImage?
    var email = ""
    var password = "******"
    var mobileNumber = ""
    var isAlreadyPassword = false
    var isValidUsername = true
    var seletedLanguage = ""
    var needToSessionExpired = Bool()
    var strCountryCode = ""
    var strFormatedDob = ""
    var strGender = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        callWSGetProfileDetails()
        self.vwContainUserImage.setViewprofileBorderEdit()
        setDelegateDatasource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showDatePicker()
//        DispatchQueue.main.async {
//             //self.vwContainer.dropShadow(color: .gray, offSet: CGSize(width: -1, height: 1),radius:5,scale: true)
        //          //  self.vwContainer.setviewbottomShadow()
        //        }
        self.vwContainGenderPicker.isHidden = true
        self.vwContainerGender.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
        self.arrLanguages = objAppShareData.arrLanguageData
        //let Stars = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.Stars)
        let star = objAppShareData.UserDetail.availableStars
        let cleanValue = Double(star)
        let finalPoints = cleanValue?.clean
        self.lblPoints.text = finalPoints
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    //Delegates and Datasource
    func setDelegateDatasource(){
        ImagePicker.delegate = self
        self.tfUserName.delegate = self
        self.tfUserNickName.delegate = self
        self.tfEmail.delegate = self
        self.tfPassword.delegate = self
        self.tfMobile.delegate = self
        
        ////
        self.tfUserNickName.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Username")
        self.tfUserName.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Fullname")
        self.tfEmail.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Email Id")
        self.tfPassword.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Password")
        self.tfMobile.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Phone Number")
        self.tfDOB.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Select Date of birth")
        ////
    }
    
    //MARK: Bottom Sheet
    func showBotttomSheet(arr : Array<Any>, str: String ){
        let vc = UIStoryboard.init(name: "Main", bundle:Bundle.main).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        vc.arrBottom = arr as! [CommonModal]
        vc.strHeader = str
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
        
    }
    
    func sendDataToFirstViewController(arrId:[Int], arrName:[String]) {
        //        if self.strBottomSheetFrom == "gender"{
        //            print(" my gender is \(arrId)")
        //            print(" my gender is \(arrName)")
        //            let varText = arrName.joined(separator:", ")
        //
        //            self.txtGender.text = varText
        //        }else if self.strBottomSheetFrom == "language"{
        print(" my language is \(arrId)")
        print(" my language is \(arrName)")
        let varText = arrName.joined(separator:",")
        let stringArray = arrId.map { String($0) }
        self.seletedLanguage = stringArray.joined(separator: ",")
        self.lblLanguage.text = varText
        //  }
        
    }
    //MARK:- IBAction Buttons
    
    @IBAction func btnOnGoToBalanceDetailVC(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BalanceDetailsVC")as! BalanceDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnActionOpenImagePicker(_ sender: Any) {
        openImagepicker()
    }
    
    @IBAction func btnOnOpenGenderPicker(_ sender: Any) {
        self.view.endEditing(true)
        ////
        if self.strGender == "1"{
            self.imgMaleCheck.image = #imageLiteral(resourceName: "active_check_ico")
            self.imgFemaleCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
        }else if self.strGender == "2"{
            self.imgFemaleCheck.image = #imageLiteral(resourceName: "active_check_ico")
            self.imgMaleCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
        }
        ////
        self.vwContainGenderPicker.isHidden = false
    }
    
    @IBAction func btnMaleAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgMaleCheck.image = #imageLiteral(resourceName: "active_check_ico")
        self.imgFemaleCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
    }
    
    @IBAction func btnFemaleAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgFemaleCheck.image = #imageLiteral(resourceName: "active_check_ico")
        self.imgMaleCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
    }
    
    @IBAction func btnDoneAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.vwContainGenderPicker.isHidden = true
        if self.imgMaleCheck.image == #imageLiteral(resourceName: "active_check_ico"){
            self.lblGender.text = "Male"
            self.strGender = "1"
        }else if self.imgFemaleCheck.image == #imageLiteral(resourceName: "active_check_ico"){
            self.lblGender.text = "Female"
            self.strGender = "2"
        }else{
            objAlert.showAlert(message: "Please select any option", title: "Alert", controller: self)
            return
        }
    }
    
    @IBAction func btnCancleAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.vwContainGenderPicker.isHidden = true
    }
    
    @IBAction func btnOnOpenDOBPicker(_ sender: Any) {
        
    }
    
    @IBAction func btnOnOpenLanguagePicker(_ sender: Any) {
        self.view.endEditing(true)
        //        let str = "Select Languages"
        //        // BottomSheetVC.delegate = self
        //        self.showBotttomSheet(arr: self.arrLanguages,str: str, index: 1)
        let str = "Select Languages"
        print("self.arrLanguages count is \(self.arrLanguages.count)")
        
        ////
        let arr = self.seletedLanguage.components(separatedBy: ",")
        if arr.count>0{
            let arrInt = arr.map { Int($0) }
            if arrInt.count>0{
                let int = arrInt[0]
                if int != nil{
                    objAppShareData.arrSelectedBottomIds = arrInt as! [Int]
                }
            }
        }
        let arrName = self.lblLanguage.text!.components(separatedBy: ",")
        if arrName.count>0{
            objAppShareData.arrSelectedBottomNames = arrName
        }
        ////
        
        self.showBotttomSheet(arr: self.arrLanguages,str: str)
    }
    
    
    @IBAction func btnActionBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionUpdateProfile(_ sender: Any) {
        _Validation()
    }
    
    
    @IBAction func btnOpenCountryPicker(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RSCountryPickerController")as! RSCountryPickerController
        sb.RScountryDelegate = self
        // sb.strCheckCountry = self.strCountryName
        sb.modalPresentationStyle = .fullScreen
        self.navigationController?.present(sb, animated: true, completion: nil)
    }
    
    /*
     self.view.endEditing(true)
     self.txtNumber.resignFirstResponder()
     let sb = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RSCountryPickerController")as! RSCountryPickerController
     sb.RScountryDelegate = self
     sb.strCheckCountry = self.strCountryName
     sb.modalPresentationStyle = .fullScreen
     self.navigationController?.present(sb, animated: true, completion: nil)
     */
    
    //MARK:- Country
    func RScountrySelected(countrySelected country: CountryInfo) {
        
        // let imagePath = "CountryPicker.bundle/\(country.country_code).png"
        //  self.imgFlag.image = UIImage(named: imagePath)
        self.lblDialCode.text = country.dial_code
        self.strCountryCode = country.country_code
        print(self.strCountryCode)
        // self.strCountryName = country.country_name
        //self.txtNumber.becomeFirstResponder()
    }
    
    //MARK:- Validation
    func _Validation(){
        self.tfUserName.text = self.tfUserName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.tfUserNickName.text = self.tfUserNickName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.tfEmail.text = self.tfEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.tfPassword.text = self.tfPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.tfMobile.text = self.tfMobile.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        // self.txtLanguage.text = self.txtLanguage.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        //
        if (tfUserName.text?.isEmpty)! {
            objAlert.showAlert(message: BlankUsername, title:kAlertTitle, controller: self)
        }else if !objValidationManager.validateNameStrings(tfUserName.text!){
            objAlert.showAlert(message: InvalidName, title:kAlertTitle, controller: self)
        }
        else if objValidationManager.isNamedLenth(password: tfUserName.text!) {
            objAlert.showAlert(message: LengthName, title: kAlertTitle, controller: self)
        }
            //
        else if (tfEmail.text?.isEmpty)! {
            objAlert.showAlert(message: BlankEmail, title:kAlertTitle, controller: self)
        }else if !objValidationManager.validateEmail(with: tfEmail.text!){
            objAlert.showAlert(message: ValidEmail, title:kAlertTitle, controller: self)
        }
            //
            
        else  if (tfPassword.text?.isEmpty)! && self.isAlreadyPassword{
            objAlert.showAlert(message: BlankPassword, title:kAlertTitle, controller: self)
        }
        else if objValidationManager.isPwdLenth(password: tfPassword.text!) && !(tfPassword.text?.isEmpty)!{
            objAlert.showAlert(message: LengthPassword, title: kAlertTitle, controller: self)
        }
            //
        else if (lblGender.text?.isEmpty)! {
            objAlert.showAlert(message: BlankGender, title:kAlertTitle, controller: self)
        }
//        else if (tfMobile.text?.isEmpty)! {
//            objAlert.showAlert(message: BlankPhoneNo, title:kAlertTitle, controller: self)
//       }
            
        else if (tfDOB.text?.isEmpty)! {
            objAlert.showAlert(message: BlankDOB, title:kAlertTitle, controller: self)
        }
            //        else if !objValidationManager.isValidDate(dateString: tfDOB.text!){
            //            objAlert.showAlert(message: InvalidDOB, title:kAlertTitle, controller: self)
            //        }
        else if (lblLanguage.text?.isEmpty)! {
            objAlert.showAlert(message: BlankLanguage, title:kAlertTitle, controller: self)
        }
        else{
            
            if email != self.tfEmail.text || mobileNumber != self.tfMobile.text || (password != tfPassword.text && tfPassword.text!.count > 0){
                let alertView = UIAlertController(title:"Alert" , message: "If you want to change email,password or mobile number\n You have to login again to continue", preferredStyle: .alert)
               
                alertView.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                
                alertView.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (UIAlertAction) in
                    if self.isValidUsername || self.tfUserNickName.text!.count == 0{
                        self.needToSessionExpired = true
                        self.callWSUpdateProfile()
                    }else{
                        objAlert.showAlert(message: "Username not available", title: "Alert", controller: self)
                    }
                    }))
                // alertView.show()
                self.present(alertView, animated:true, completion:nil)
            }else{
                if self.isValidUsername || self.tfUserNickName.text!.count == 0{
                    self.needToSessionExpired = false
                    self.callWSUpdateProfile()
                }else{
                    objAlert.showAlert(message: "Username not available", title: "Alert", controller: self)
                }
            }
        }
    }
}

// MARK:- DatePicker
extension UpdateProfileVC {
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        let currentDate = NSDate()
        //   datePicker.minimumDate = currentDate as Date
        
        datePicker.maximumDate = currentDate as Date
        datePicker.backgroundColor = UIColor(named: "background")
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(btnDone));
        doneButton.tintColor = UIColor(named: "button_background")
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(btnCancel));
        cancelButton.tintColor = UIColor.red
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        tfDOB.inputAccessoryView = toolbar
        tfDOB.inputView = datePicker
    }
    
    @objc func btnDone()
    {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        tfDOB.text = formatter.string(from: datePicker.date)
       
        print(strFormatedDob)
        self.view.endEditing(true)
    }
    
    @objc func btnCancel(){
        self.view.endEditing(true)
    }
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
    func convertDateFormater2(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return  dateFormatter.string(from: date!)
        
    }
    
}

extension UIView{
    func setViewprofileBorderEdit()
    {
        layer.cornerRadius = 65
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 0.8901208043, green: 0.8902459741, blue: 0.8900814056, alpha: 1)
    }
}

//Mark:- ImagePickerDelegate
extension UpdateProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func openImagepicker(){
        let alertController = UIAlertController(title: nil, message: "Choose Image", preferredStyle: .actionSheet)
        
        let defaultAction = UIAlertAction(title: "Camera", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        
        let deleteAction = UIAlertAction(title: "Gallery", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { (alert: UIAlertAction!) -> Void in
            
        })
        
        alertController.addAction(defaultAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        if let popoverController = alertController.popoverPresentationController {
            // popoverController.barButtonItem = sender
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            self.ImagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.ImagePicker.allowsEditing = true
            self .present(self.ImagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.savedPhotosAlbum){
            self.ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.ImagePicker.allowsEditing = true
            self.ImagePicker.delegate = self
            self.present(self.ImagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.editedImage] as? UIImage{
            self.imgVwUserProfile.image = image
            self.pickedImage = image
        }else{
            
        }
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Call WebService
extension UpdateProfileVC : UITextFieldDelegate{
    
    func callWSGetProfileDetails(){
        let userDict = UserDefaults.standard.value(forKey: UserDefaults.KeysDefault.userInfo)as? [String:Any] ?? [:]
        
        let user_id = userDict["userID"]as? String ?? ""
        print(user_id)
        //4899F5A9-52DD-44CD-9B82-9B2BF0AFEF6F
        //4899F5A9-52DD-44CD-9B82-9B2BF0AFEF6F
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        objWebServiceManager.requestGet(strURL: WsUrl.Url_GetUserProfile+user_id, params: nil, strCustomValidation: "", success: { (response) in
            print(response)
            objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            
            if status == "success"{
                if let data = response["data"]as? [String:Any]{
                    
                    if let userDetails = data["user_details"]as? [String:Any]{
                        
                        
                        if let profilePic = userDetails["avatar"]as? String{
                            if profilePic != "" {
                                let url = URL(string: profilePic)
                                self.imgVwUserProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_img"))
                            }
                            
                        }
                        
                        if let name = userDetails["name"]as? String{
                            self.tfUserName.text = name
                        }
                        if let name = userDetails["username"]as? String{
                            self.tfUserNickName.text = name
                        }
                        if let nickName = userDetails["username"]as? String{
                            self.tfUserNickName.text = nickName
                        }
                        
                        if let email = userDetails["email"]as? String{
                            self.tfEmail.text = email
                            self.email = email
                        }
                        
                        if let gender = userDetails["gender"] as? String{
                            self.lblGender.text = gender
                            if gender == "Male"{
                            self.strGender = "1"
                            }else{
                            self.strGender = "2"
                            }
                        }
                        
                        if let password = userDetails["password"] as? String{
                            self.isAlreadyPassword = true
                            self.tfPassword.text = "******"
                        }else{
                            self.isAlreadyPassword = false
                        }
                        
                        if let mobile = userDetails["phone_number"]as? String{
                            self.tfMobile.text = mobile
                            self.mobileNumber = mobile
                        }
                        
                        if let dialCode = userDetails["phone_dial_code"]as? String{
                            self.lblDialCode.text = dialCode
                        }
                        
                        if let countryCode = userDetails["phone_country_code"]as? String{
                            self.strCountryCode = countryCode
                        }
                        
                        if let dob = userDetails["dob"]as? String{
                            print(dob)
                            self.tfDOB.text = self.convertDateFormater2(dob)
                            self.strFormatedDob = self.convertDateFormater2(dob)
                        }
                        
                        var arrName = [String]()
                        if let arrLanguage = userDetails["languages"]as? [[String:Any]]{
                            for dictLang  in arrLanguage{
                                if let objName = dictLang["name"]as? String{
                                    arrName.append(objName)
                                }
                            }
                        }
                        let str = arrName.joined(separator: ",")
                        self.lblLanguage.text = str
                    }
                }
            }else{
            }
        }) { (Error) in
            print(Error)
        }
    }
    
    //MARK:- Update Profile
    func callWSUpdateProfile(){
        
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
            
        }
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        var param = [String:Any]()
        
        self.strFormatedDob = convertDateFormater(tfDOB.text!)
        
        var strPW = ""
        if self.tfPassword.text == "******"{
            strPW = ""
        }else{
            strPW = self.tfPassword.text!
        }
        
        param = ["email":self.tfEmail.text!,
                 "phone_dial_code":self.lblDialCode.text!,
                 "phone_number":self.tfMobile.text!,
                 "name":self.tfUserName.text!,
                 "password":strPW,
                 "phone_country_code":self.strCountryCode,
                 "gender":self.strGender,
                 "dob":self.self.strFormatedDob,
                 "language":self.seletedLanguage,
                 "username":self.tfUserNickName.text!,
                 //"country_id":"100"
            ]as [String:Any]
        
        self.view.endEditing(true)
        var imageData : Data?
        if self.pickedImage != nil{
            imageData = (self.pickedImage?.jpegData(compressionQuality: 1.0))!
        }
        print(param)
        objWebServiceManager.uploadMultipartData(strURL: WsUrl.Url_updateProfile, params: param, showIndicator: false, imageData: imageData, fileName: "profile_picture", mimeType: "image/jpeg", success: { (response) in
            
            objWebServiceManager.hideIndicator()
            print(response)
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
                          
            if status == "success"{
               if self.needToSessionExpired == true{
                let alertView = UIAlertController(title:"Alert" , message: "Your profile updated succesfully\n  Session expired\n Please login again", preferredStyle: .alert)
                
                alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                    //Logout
                    objAppShareData.callWebserviceForLogout()
                }))
                self.present(alertView, animated:true, completion:nil)
                
               }else{
                
                ////
                let dic = response["data"] as? [String:Any]
                let user_details = dic!["user_details"] as? [String:Any]
                objAppShareData.UserDetail.strProfilePicture = user_details!["avatar"] as? String ?? ""
                if let userDic = UserDefaults.standard.value(forKey:  UserDefaults.KeysDefault.userInfo) as? [String : Any]{
                    var dict = userDic
                    dict["avatar"] = objAppShareData.UserDetail.strProfilePicture
                    UserDefaults.standard.setValue(dict, forKey: UserDefaults.KeysDefault.userInfo)
                    UserDefaults.standard.synchronize()
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTabbarUserImage"), object: nil, userInfo: nil)
                ////
                
                let alertView = UIAlertController(title:"Success" , message: "Your profile updated succesfully.", preferredStyle: .alert)
                alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alertView, animated:true, completion:nil)
                }
            }else{
                objAlert.showAlert(message: message, title: "Alert", controller: self)
            }
        }) { (Error) in
            print("Error")
        }
    }
    func saveDataInDeviceLogin(dict : [String:Any])
    {
        UserDefaults.standard.setValue(dict["auth_token"], forKey:"Authtoken")
        UserDefaults.standard.setValue(dict["total"], forKey:"Stars")
        UserDefaults.standard.setValue(dict["avatar"], forKey:"ProfilePic")
    }
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text! as NSString
        if textField == self.tfUserNickName{
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        if substring.count > 3{
            self.callWebserviceForChaeckUserName(strUserNameCheck: substring)
        }else{
            self.isValidUsername = false
            self.tfUserNickName.textColor = UIColor.red
        }
        }
        return  true
    }
    
    func callWebserviceForChaeckUserName(strUserNameCheck:String){
           if !objWebServiceManager.isNetworkAvailable(){
               objWebServiceManager.hideIndicator()
               objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
               return
           }
           let param = ["username":strUserNameCheck ?? ""]
           print(param)
           objWebServiceManager.requestPost(strURL: WsUrl.checkUserName, params: param, strCustomValidation: "", showIndicator: false, success: {response in
               print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if status == "success"{
                if let data = response["data"] as? [String:Any]{
                    var strStatus = ""
                    if let statusU = data["username_status"] as? String{
                       strStatus = statusU
                    }else if let statusU = data["username_status"] as? Int{
                        strStatus = String(statusU)
                    }
                    if strStatus == "1"{
                        self.isValidUsername = true
                        self.tfUserNickName.textColor = UIColor(named: "Text_Color")
                    }else{
                        self.isValidUsername = false
                        self.tfUserNickName.textColor = UIColor.red
                    }
                }else{
                    
                }
            }else{
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
           }, failure: { (error) in
               print(error)
               objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
           })
       }
}
