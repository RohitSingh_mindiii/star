//
//  BookedModel.swift
//  Star
//
//  Created by MACBOOK-SHUBHAM V on 20/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//


import Foundation

class  BookedModel:NSObject{
   
    var Name: String?
    var ProfileImg: String?
    var Detail : String?
    var isSelected: Bool?
    
    
     
    init(Name:String,ProfileImg:String,Detail:String,isSelected:Bool) {
          self.Name = Name
          self.ProfileImg = ProfileImg
         self.Detail = Detail
          self.isSelected = isSelected
      }
    
    
    
    
}
