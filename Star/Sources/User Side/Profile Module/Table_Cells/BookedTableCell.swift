//
//  BookedTableCell.swift
//  Design2
//
//  Created by IOS-Sagar on 06/02/20.
//  Copyright © 2020 IOS-Sagar. All rights reserved.
//

import UIKit

class BookedTableCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var btnStatus: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.imgProfile.setImgCircle()
        self.viewImg.setviewCirclewhite()
        self.viewImg.setshadowViewCircle2()
        
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    @IBAction func btnStatus(_ sender: customButton) {
//       }
    
//    @IBAction func btnStatus(_ sender: UIButton) {
//    }
}

