//
//  TabBarVC.swift
//  Star
//
//  Created by ios-deepak b on 05/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

var imgUserTabbar = UIImageView()

let SEPARATOR_WIDTH = 1.0
var viewOverTabBar = UIView()
class TabBarVC: UITabBarController, UITabBarControllerDelegate{
    let appColor = UIColor(red: 0/255, green: 173/255, blue: 239/255, alpha: 1.0)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        //        self.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "refreshTabbarUserImage"), object: nil, queue: nil) { [weak self](Notification) in
            self?.manageUserImageSelected()
        }
        for viewController in self.viewControllers! {
            
            _ = viewController.view
        }
        //        //self.selectedIndex = selected_TabIndexServiceProvider
        //        //print(self.selectedIndex)
        //
        //        addUnderlinInTabBarItem()
        //        setTabarShadow()
        //
        //        viewOverTabBar = UIView(frame: CGRect(x: 0 , y: 0, width: tabBar.frame.size.width, height: 100))
        //        self.tabBar.addSubview(viewOverTabBar)
        //        viewOverTabBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        //        viewOverTabBar.alpha = 0.4
        //        viewOverTabBar.isHidden = true
        self.manageUserImage()
        if objAppShareData.isFromNotification{
            if objAppShareData.strNotificationType == "Calls" || objAppShareData.strNotificationType == "Balance" || objAppShareData.strNotificationType == "Call"{
                self.selectedIndex = 2
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        for vc in self.viewControllers! {
            if #available(iOS 13.0, *) {
                vc.tabBarItem.imageInsets = UIEdgeInsets(top: 1, left: 0, bottom:2, right: 0)
            }else{
                vc.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom:-6, right: 0)
            }
        }
    }
    
    func manageUserImage(){
        let tabWidth = (tabBar.frame.size.width/4)
        let tabBarWidth = tabBar.frame.size.width
        
        imgUserTabbar = UIImageView(frame: CGRect(x: (tabBarWidth - (tabBarWidth/3) + tabWidth/2)-15, y: 8, width: 30, height: 30))
        let profilePic = objAppShareData.UserDetail.strProfilePicture
        if profilePic != "" {
            let url = URL(string: profilePic)
            imgUserTabbar.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_img"))
        }else{
            imgUserTabbar.image = UIImage.init(named: "inactive_profile_ico")
        }
        imgUserTabbar.layer.cornerRadius = 15.0
        imgUserTabbar.layer.masksToBounds = true
        self.tabBar.addSubview(imgUserTabbar)
        imgUserTabbar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        viewOverTabBar = UIView(frame: CGRect(x: 0 , y: 0, width: tabBar.frame.size.width, height: 100))
        self.tabBar.addSubview(viewOverTabBar)
        viewOverTabBar.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        viewOverTabBar.isHidden = true
    }
    func manageUserImageSelected(){
        let tabWidth = (tabBar.frame.size.width/4)
        let tabBarWidth = tabBar.frame.size.width
        
        imgUserTabbar = UIImageView(frame: CGRect(x: (tabBarWidth - (tabBarWidth/3) + tabWidth/2)-15, y: 8, width: 30, height: 30))
        let profilePic = objAppShareData.UserDetail.strProfilePicture
        if profilePic != "" {
            let url = URL(string: profilePic)
            imgUserTabbar.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_img"))
        }else{
            imgUserTabbar.image = UIImage.init(named: "inactive_profile_ico")
        }
        
        imgUserTabbar.layer.cornerRadius = 15.0
        imgUserTabbar.layer.borderWidth = 1.0
        imgUserTabbar.layer.borderColor = UIColor.init(red: 218.0/255.0, green: 186.0/255.0, blue: 151.0/255.0, alpha: 1.0).cgColor
        imgUserTabbar.layer.masksToBounds = true
        self.tabBar.addSubview(imgUserTabbar)
        imgUserTabbar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        viewOverTabBar = UIView(frame: CGRect(x: 0 , y: 0, width: tabBar.frame.size.width, height: 100))
        self.tabBar.addSubview(viewOverTabBar)
        viewOverTabBar.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        viewOverTabBar.isHidden = true
    }
    func drawTabbar() {
        
        self.tabBar.items?[0].image = #imageLiteral(resourceName: "inactive_discover_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[1].image = #imageLiteral(resourceName: "inactive_notifications_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[2].image = #imageLiteral(resourceName: "inactive_profile_ico").withRenderingMode(.alwaysOriginal)
                
        self.tabBar.items?[0].selectedImage = #imageLiteral(resourceName: "active_discover_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[1].selectedImage = #imageLiteral(resourceName: "active_notifications_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[2].selectedImage = #imageLiteral(resourceName: "inactive_profile_ico").withRenderingMode(.alwaysOriginal)
        
        
        var dataImg:UIImage? = UIImage()
        DispatchQueue.main.async {
            
            let strProfilePic = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.ProfilePic)
            print(strProfilePic)
            Alamofire.request(strProfilePic!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseData { (res) in
                print("found data \(res)")
                //self.tabBar.items?[3].image = UIImage(data: res.data!)
                dataImg = UIImage(data: res.data!)
                var img = dataImg?.ResizeImage(image: dataImg!, targetSize: CGSize(width: 35, height: 35))
                dataImg = img
                print("size of image \(img?.size.width)")
                
                img = img?.roundedImage
                self.tabBar.items?[3].image = img?.withRenderingMode(.alwaysOriginal)
                
                img = img?.rounded(with: #colorLiteral(red: 0.7490196078, green: 0.6, blue: 0.3921568627, alpha: 1), width: 2)
                self.tabBar.items?[3].selectedImage = img?.withRenderingMode(.alwaysOriginal)
                
                self.tabBar.inputView?.addSubview(UIView())
                
                //                UIImage().resizableImage(withCapInsets: UIEdgeInsets()
                //
            }
        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        let indexOfTab = tabBar.items?.index(of: item)
        print("pressed tabBar: \(String(describing: indexOfTab))")
        if indexOfTab == 2{
            self.manageUserImageSelected()
        }else{
            self.manageUserImage()
        }
        //selected_TabIndexServiceProvider = (tabBar.items?.index(of: item))!
        
    }
}



