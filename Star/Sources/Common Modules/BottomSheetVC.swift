//
//  BottomSheetVC.swift
//  Star
//
//  Created by ios-deepak b on 31/01/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here
protocol BottomSheetVCDelegate: class {
    
    func sendDataToFirstViewController(arrId:[Int], arrName:[String])
}

class BottomSheetVC: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var tblBottom: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var vwtbl: UIView!
    
    //MARK: step 2 Create a delegate property here, don't forget to make it weak!
    //  weak var delegate: BottomSheetVCDelegate?
    var delegate: BottomSheetVCDelegate? = nil
    
    
    // MARK:- Varibles
    var arrBottom:[CommonModal] = []
    var SelectedArrBottom:[Int] = []
   // var arrID:[Int] = []
    var arrName:[String] = []
    var strHeader = ""
    var selectAll: Bool = false
   
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ////
        if objAppShareData.arrSelectedBottomIds.count>0{
            let obj = objAppShareData.arrSelectedBottomIds[0]
            if obj == 0{
                self.selectAll = true
            }else{
                self.SelectedArrBottom = objAppShareData.arrSelectedBottomIds
                self.arrName = objAppShareData.arrSelectedBottomNames
            }
        }
        ////
        
        self.lblHeader.text = self.strHeader
        self.vwtbl.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
        self.tblBottom.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        objAppShareData.isCountryInBottomSheet = false
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        //self.tblHeight.constant = self.tblBottom.contentSize.height + 30
        if self.arrBottom.count > 14{
            var bounds = UIScreen.main.bounds.size.height
            self.tblHeight.constant = bounds - 150
            self.tblBottom.isScrollEnabled = true
        }else{
            self.tblHeight.constant = self.tblBottom.contentSize.height + 30
            self.tblBottom.isScrollEnabled = false
        }
    }
    
    // MARK:- Buttons Action
    @IBAction func btnDoneAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.selectAll == true {
            let obj = self.arrBottom[0]
            self.arrName.removeAll()
            self.arrName.append(obj.strName)
            self.SelectedArrBottom.append(0)
        }else if self.selectAll == false && self.SelectedArrBottom.count == 0 {
            objAlert.showAlert(message: "Please select any option", title: "Alert", controller: self)
            return
        }else{
        }
        self.delegate?.sendDataToFirstViewController(arrId:self.SelectedArrBottom, arrName:self.arrName )
        self.dismiss(animated: false, completion: nil)
       
    }
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.dismiss(animated: false, completion: nil)
        //self.navigationController?.popViewController(animated: false)
    }
    
}
// MARK:- TableView Delegate and DataSource
extension BottomSheetVC : UITableViewDelegate ,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrBottom.count
      //  print("self.arrBottom count is \(self.arrBottom.count)")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicInfoCell")as! BasicInfoCell
        if objAppShareData.isCountryInBottomSheet && indexPath.row == 0{
            return UITableViewCell()
        }
        let obj = self.arrBottom[indexPath.row]
        cell.lblTitleName.text = obj.strName
        cell.btnCheck.tag = indexPath.row
        cell.btnCheck.addTarget(self,action:#selector(buttonClicked(sender:)), for: .touchUpInside)
        if self.selectAll{
            cell.imgCheck.image =  #imageLiteral(resourceName: "active_check_ico")
        //}else if SelectedArrBottom.contains(indexPath.row){
        }else if SelectedArrBottom.contains(Int(obj.strID) ?? 0){
            cell.imgCheck.image = #imageLiteral(resourceName: "active_check_ico")
        }else{
            cell.imgCheck.image =  #imageLiteral(resourceName: "inactive_check_ico")
        }
        return cell
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        if objAppShareData.isCountryInBottomSheet && indexPath.row == 0{
            return 0
        }else{
            return 44
        }
    }
    
    @objc func buttonClicked(sender:UIButton) {
        let obj = self.arrBottom[sender.tag]
        
        if !objAppShareData.isCountryInBottomSheet{
        if sender.tag == 0{
            if self.selectAll{
                if SelectedArrBottom.count > 0{
                    SelectedArrBottom.removeAll()
                    self.arrName.removeAll()
                }
                self.selectAll = false
            }else{
                if SelectedArrBottom.count > 0{
                    SelectedArrBottom.removeAll()
                    self.arrName.removeAll()
                }
                self.selectAll = true
            }
        }else{
            ////
            if self.selectAll{
                SelectedArrBottom.removeAll()
                self.arrName.removeAll()
                for obj in self.arrBottom{
                    if obj.strID.count != 0{
                        if obj.strID != "0"{
                            if !SelectedArrBottom.contains(Int(obj.strID)!){
                                SelectedArrBottom.append(Int(obj.strID)!)
                                self.arrName.append(obj.strName)
                            }
                        }
                    }
                }
            }
            ////
           
            self.selectAll = false
            let obj = self.arrBottom[sender.tag]
            if let index = SelectedArrBottom.firstIndex(of: Int(obj.strID)!) {
            //if let index = SelectedArrBottom.firstIndex(of: sender.tag) {
                SelectedArrBottom.remove(at: index)
                self.arrName.remove(at: index)
            }else{
//                if !SelectedArrBottom.contains(sender.tag){
//                    SelectedArrBottom.append(sender.tag)
//                }
                if !SelectedArrBottom.contains(Int(obj.strID)!){
                    SelectedArrBottom.append(Int(obj.strID)!)
                }
                if !self.arrName.contains(obj.strName){
                    self.arrName.append(obj.strName)
                }
                if SelectedArrBottom.count == arrBottom.count-1{
                    self.selectAll = true
                    SelectedArrBottom.removeAll()
                    //SelectedArrBottom.append(0)
                }
            }
        }
        }else{
           self.selectAll = false
            if let index = SelectedArrBottom.firstIndex(of: sender.tag) {
                SelectedArrBottom.remove(at: index)
                self.arrName.remove(at: index)
            }else{
                if !SelectedArrBottom.contains(sender.tag){
                    SelectedArrBottom.append(sender.tag)
                }
                if !self.arrName.contains(obj.strName){
                    self.arrName.append(obj.strName)
                }
            }
        }
        print(" self.SelectedArrBottom \(self.SelectedArrBottom)")
        print(" self.arrName \(self.arrName)")

        self.tblBottom.reloadData()
    }
    
}
