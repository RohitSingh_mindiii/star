//
//  SettingCell.swift
//  Hoggz
//
//  Created by IOS-CATELINA on 29/01/20.
//  Copyright © 2020 MACBOOK-SHUBHAM V. All rights reserved.
//

import UIKit

class SettingViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgLeftIcon: UIImageView!
    @IBOutlet weak var imgRightIcon: UIImageView!
    @IBOutlet weak var lblSeparator: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
