//
//  SettingVC.swift
//  Hoggz
//
//  Created by IOS-CATELINA on 29/01/20.
//  Copyright © 2020 MACBOOK-SHUBHAM V. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgToggle: UIImageView!
    
    //var arrSetting = [String]()
    var isSocialLogin = false
    var notiStatus = ""
    
    var arrSetting = ["Terms & Condition","Privacy Policy","Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uiDesign()
        
       // self.checkUserType()
        
        let userDict = UserDefaults.standard.value(forKey: UserDefaults.KeysDefault.userInfo)as? [String:Any] ?? [:]
        
        if let notificationStatus = userDict["push_alert_status"]as? String{
            notiStatus = notificationStatus
        }else if let notificationStatus = userDict["push_alert_status"]as? Int{
            notiStatus = "\(notificationStatus)"
        }
        
        if notiStatus == "0"{
            objAppShareData.isNotificationToggleOn = false
            self.imgToggle.image = #imageLiteral(resourceName: "OFF_TOGGLE_ICO")
        }else{
            objAppShareData.isNotificationToggleOn = true
           // self.imgToggle.image = #imageLiteral(resourceName: "ON_TOGGLE_ICO")
              self.imgToggle.image = #imageLiteral(resourceName: "on_toggle")
        }

//        if objAppShareData.isNotificationToggleOn == true{
//            self.imgToggle.image = #imageLiteral(resourceName: "ON_TOGGLE_ICO")
//        }else{
//            self.imgToggle.image = #imageLiteral(resourceName: "OFF_TOGGLE_ICO")
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.uiDesign()
    }
    

    
    @IBAction func btnActionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnToggleAction(_ sender: Any){
        
        if notiStatus == "0"{
            call_wsNotificationOnnOff(strStatus: "1")
        }else{
            call_wsNotificationOnnOff(strStatus: "0")
        }
    }
    
    func uiDesign(){
        //self.viewHeader.setviewbottomShadow()
    }
}

extension SettingVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSetting.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingViewCell") as! SettingViewCell
        
        let obj = self.arrSetting[indexPath.row]
        cell.lblTitle.text = self.arrSetting[indexPath.row]
        cell.imgRightIcon.image = #imageLiteral(resourceName: "right_arrow")
        
//        if arrSetting.count-1 == indexPath.row{
//            cell.imgRightIcon.isHidden = true
//            cell.lblSeparator.isHidden = true
//        }
        
        switch obj {
     
        case "Terms & Condition":
            cell.imgLeftIcon.image = #imageLiteral(resourceName: "term_conditions_ico")
        case "Privacy Policy":
            cell.imgLeftIcon.image = #imageLiteral(resourceName: "privacy_policy_ico")
       
        case "Logout":
            cell.imgLeftIcon.image = #imageLiteral(resourceName: "logout_ico")
        default:
            print("default")
        }
        return cell
    }
    
    //MARK: table view delegate method
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let obj = arrSetting[indexPath.row]
        switch obj {
        case "Terms & Condition":
            self.navigateTOTerms()
        case "Privacy Policy":
            self.navigateTOPrivacy()
        case "Logout":
            self.ShowAlertForLogout()
        default:
            print("default")
        }
    }
    
}

//MARK:- Custom methods
extension SettingVC{
    
    func navigateTOTerms(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsNConditionVC") as! TermsNConditionVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateTOPrivacy(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func ShowAlertForLogout(){
        let alertController = UIAlertController(title: kAlertTitle, message: "Are you sure you want to Logout?", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            self.view.endEditing(true)
            objAppShareData.callWebserviceForLogout()
        }
        let CancelAction = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction!) in
            self.view.endEditing(true)
            print("cancel button tapped");
        }
        alertController.addAction(CancelAction)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
        
}

extension SettingVC{
    
    func call_wsNotificationOnnOff(strStatus:String){
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        let param = ["status":strStatus]as [String:AnyObject]
        print(param)
        
        objWebServiceManager.requestPatch(strURL: WsUrl.Url_NotificationStatus, params: param, strCustomValidation: "", success: { (response) in
             objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            print(response)
            if status == "success"{
                var strStatus = ""
                if let data = response["data"]as? [String:Any]{
                    if let status = data["push_alert_status"]as? String{
                        strStatus = status
                    }else if let status = data["push_alert_status"]as? Int{
                        strStatus = "\(status)"
                    }
                    self.notiStatus = strStatus
                    if strStatus == "0"{
                        objAppShareData.isNotificationToggleOn = false
                        self.imgToggle.image = #imageLiteral(resourceName: "OFF_TOGGLE_ICO")
                    }else{
                        
                        objAppShareData.isNotificationToggleOn = true
                      //  self.imgToggle.image = #imageLiteral(resourceName: "ON_TOGGLE_ICO")
                        self.imgToggle.image = #imageLiteral(resourceName: "on_toggle")
                    }
                    var userDict = UserDefaults.standard.value(forKey: UserDefaults.KeysDefault.userInfo)as? [String:Any] ?? [:]
                    userDict["push_alert_status"] = strStatus
                    UserDefaults.standard.setValue(userDict, forKey: UserDefaults.KeysDefault.userInfo)
                }
            }else{
                
                 objAlert.showAlert(message: message, title: "Alert", controller: self)
            }
        }) { (Error) in
            
            print(Error)
        
        }
        
        
    }
    
    
    
}
