//
//  TermsNConditionVC.swift
//  Hoggz
//
//  Created by IOS-CATELINA on 29/01/20.
//  Copyright © 2020 MACBOOK-SHUBHAM V. All rights reserved.
//

import UIKit
import WebKit

class TermsNConditionVC: UIViewController {

    @IBOutlet weak var webkitView: WKWebView!

    @IBOutlet var viewHeader: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uiDesign()
        
//        if let pdf = Bundle.main.url(forResource: "Privacypolicy", withExtension: "pdf", subdirectory: nil, localization: nil){
//            let req = NSURLRequest(url: pdf)
//            self.webkitView.load(req as URLRequest)
//        }
        
        if let url = URL(string: "https://www.apple.com") {
                   let request = URLRequest(url: url)
                   self.webkitView.load(request)
               }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.uiDesign()
        //objWebServiceManager.showIndicator()
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func uiDesign(){
        self.viewHeader.setviewbottomShadow()
    }
    func webView(_: WKWebView, didFinish: WKNavigation!){
           objWebServiceManager.hideIndicator()
       }
}

extension TermsNConditionVC: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        objWebServiceManager.hideIndicator()
    }
}
