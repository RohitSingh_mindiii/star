//
//  NotificationVC.swift
//  Star
//
//  Created by ios-deepak b on 05/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit
import SDWebImage

class NotificationVC: UIViewController {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet weak var ViewContainer: UIView!
    @IBOutlet weak var viewNoDataFound: UIView!
    
    // MARK:- Varibles
    var arrNotification = [NotificationModal]()
    var userType = ""
//    //For Pagination
//       var isDataLoading:Bool=false
//       var limit:Int=20
//       var offset:Int=0
//       var totalRecords = Int()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHeader.setviewbottomShadow()
        
        
        // Do any additional setup after loading the view.
        //self.ViewContainer.dropShadow(color: .gray, offSet: CGSize(width: -1, height: 1),radius:5,scale: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.WSGetNotificationList()
        self.tblNotification.reloadData()
        self.viewNoDataFound.isHidden = true
        
        
        
    }
}
// MARK: - UITableViewDataSource & UITableViewDelegate

extension NotificationVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableCell")as! NotificationTableCell
        
        let obj = self.arrNotification[indexPath.row]
        
        // cell.lblMsg.text = obj.strMessage
        cell.lblLastSeen.text = obj.strRemainingTime
        
        
        if obj.strNotificationType == "Calls"{
            if obj.strCallType == "1"{
                cell.imgView.image = #imageLiteral(resourceName: "circle_audio_ico")
            }else{
                cell.imgView.image = #imageLiteral(resourceName: "circle_video_ico")
            }
        }else if obj.strNotificationType == "Balance"{
            cell.imgView.image = #imageLiteral(resourceName: "circle_star_ico")
        }else{
            cell.imgView.image = #imageLiteral(resourceName: "follow_ico1")
        }
        
      
        //AttributeWork
//
        var attr_UserName = NSMutableAttributedString()
        var attr_Sentence = NSMutableAttributedString()
//        if objNotification.notificationMessage.contains(objNotification.userName) {
//        objNotification.notificationMessage = objNotification.notificationMessage.replacingOccurrences(of: objNotification.userName, with: "")
//        }
        attr_UserName = NSMutableAttributedString(string: obj.strUserName, attributes: [NSAttributedString.Key.font:UIFont(name: "Montserrat-Medium", size: 16.0)!,NSAttributedString.Key.foregroundColor : UIColor(named: "Text_Color")])

        let strMsg = " " + obj.strMessage

        attr_Sentence = NSMutableAttributedString(string:strMsg, attributes: [NSAttributedString.Key.font:UIFont(name: "Montserrat-Medium", size: 14.0)!,NSAttributedString.Key.foregroundColor : UIColor.gray])

        let combination = NSMutableAttributedString()
        combination.append(attr_UserName)
        combination.append(attr_Sentence)
        cell.lblMsg.attributedText = combination
//
        
        //==============XXXX==================//
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.arrNotification[indexPath.row]
        
        
        
        switch obj.strNotificationType {
        case "Balance Deduct":
            let sb = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "BalanceDetailsVC")as! BalanceDetailsVC
            self.navigationController?.pushViewController(sb, animated: true)
            
        case "Balance":
            let sb = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "BalanceDetailsVC")as! BalanceDetailsVC
            self.navigationController?.pushViewController(sb, animated: true)
            
        case "Calls":
            if objAppShareData.UserDetail.strUserType == "user"{
                let sb = UIStoryboard.init(name: "Discover", bundle: Bundle.main).instantiateViewController(withIdentifier: "CallDetail_VC")as! CallDetail_VC
                sb.callID = obj.strCallID
                sb.isComingFrom = "Notification"
                self.navigationController?.pushViewController(sb, animated: true)
            }else{
                let sb = UIStoryboard.init(name: "Calling", bundle: Bundle.main).instantiateViewController(withIdentifier: "InfluencerCallDetails_VC")as! InfluencerCallDetails_VC
                sb.callID = obj.strCallID
                sb.isComingFrom = "Notification"
                self.navigationController?.pushViewController(sb, animated: true)
            }
        case "Call":
            if objAppShareData.UserDetail.strUserType == "user"{
                let sb = UIStoryboard.init(name: "Discover", bundle: Bundle.main).instantiateViewController(withIdentifier: "CallDetail_VC")as! CallDetail_VC
                sb.callID = obj.strCallID
                sb.isComingFrom = "Notification"
                self.navigationController?.pushViewController(sb, animated: true)
            }else{
                let sb = UIStoryboard.init(name: "Calling", bundle: Bundle.main).instantiateViewController(withIdentifier: "InfluencerCallDetails_VC")as! InfluencerCallDetails_VC
                sb.callID = obj.strCallID
                sb.isComingFrom = "Notification"
                self.navigationController?.pushViewController(sb, animated: true)
            }
            
        case "Following":
            print("redicrect to deduct")
        default:
            break
        }
        
        
    }
    
}

//MARK: Webservice for get Notification list
extension NotificationVC {
    
    func WSGetNotificationList(){
        
        self.arrNotification.removeAll()
        
        objWebServiceManager.showIndicator()
        
        objWebServiceManager.requestGet(strURL: WsUrl.Get_Notification, params: nil, strCustomValidation: "", success: {response in
            print(response)
            let status = (response["status"] as? String)
            let message = (response["message"] as? String)
            if status == k_success{
                var dataFound:Int?
                
                objWebServiceManager.hideIndicator()
                if let data = response["data"]as? [String:Any]{
                    dataFound = data["data_found"] as? Int ?? 0
                    if status == "success" ||  status == "Success"
                    {
                        if dataFound == 0
                        {
                            self.viewNoDataFound.isHidden = false
                            
                        }
                        else{
                            let arrAlertDetails  = data["notification_list"] as! [[String: Any]]
                            
                            for dictNotification in arrAlertDetails
                            {
                                let objNotification = NotificationModal.init(dict: dictNotification)
                                
                                self.arrNotification.append(objNotification)
                            }
                            print("self.arrNotification.count is \(self.arrNotification.count)")
                            self.tblNotification.reloadData()
                        }
                        if self.arrNotification.count == 0
                        {
                            self.viewNoDataFound.isHidden = false
                        }else{
                            self.viewNoDataFound.isHidden = true
                        }
                        
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                    }
                }
            }
            else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
        })
    }
}
