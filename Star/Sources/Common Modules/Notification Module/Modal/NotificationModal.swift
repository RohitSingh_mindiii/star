//
//  NotificationModal.swift
//  Star
//
//  Created by ios-deepak b on 27/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import Foundation
import UIKit

class NotificationModal : NSObject{
    
    var strTitle: String = ""
    var strUserName: String = ""
    var strMessage: String = ""
    var strUserImageUrl: String = ""
    var strCreateTime: String = ""
    var strCurrentTime: String = ""
    var strNotificationType: String = ""
    var strCallType:String = ""
    var strParentRefType: String = ""
    var strCallID:String = ""
    var strID: String = ""
    var strReadStatus: String = ""
    var date = Date()
    var currentDate = Date()
    var strRemainingTime = ""
    
    
    init(dict : [String:Any]) {
        
        super.init()
        
        if let title = dict["title"] as? String{
            strTitle = title
        }
        
        if let usrName = dict["name"] as? String{
//            let cleanValue = Double(usrName)
//            let finalValue = cleanValue?.clean
//            let strFinal = String(finalValue!)
            strUserName = usrName
        }
        
        if let calltype = dict["call_type"] as? String{
            self.strCallType = calltype
        }else if let calltype = dict["call_type"] as? Int{
            self.strCallType = "\(calltype)"
        }
        
        if let message = dict["message"] as? String{
            strMessage = message
        }
        if let created = dict["created_at"] as? String{
            strCreateTime = created
        }
        if let current = dict["current_time"] as? String{
            strCurrentTime = current
        }
        if let notification_type = dict["notification_type"] as? String{
            strNotificationType = notification_type
        }
        if let reference_type = dict["parent_reference_type"] as? String{
            strParentRefType = reference_type
        }
        
        
        if let callId = dict["reference_id"]  as? String{
            self.strCallID = callId
        }else if let callId = dict["reference_id"]  as? Int{
            self.strCallID = "\(callId)"
        }
        
       
        if let notificationID = dict["notificationID"]  as? String{
            strID = notificationID
        }else if let notificationID = dict["notificationID"]  as? Int{
            strID = "\(notificationID)"
        }
        if let is_read = dict["is_read"]  as? String{
                  strReadStatus = is_read
              }else if let is_read = dict["is_read"]  as? Int{
                  strReadStatus = "\(is_read)"
              }
      
        if self.strCreateTime != ""{
            self.date = self.strCreateTime.dateWithFormat(format: "yyyy-MM-dd HH:mm:ss")
            self.currentDate = self.strCurrentTime.dateWithFormat(format: "yyyy-MM-dd HH:mm:ss")
            self.strRemainingTime = relativePastTime(for: self.date, currentDate: self.currentDate)
        }

    }
}

extension NotificationModal{
    
    func relativePastTime(for date : Date,currentDate : Date) -> String {
        
        let units = Set<Calendar.Component>([.year, .month, .day, .hour, .minute, .second, .weekOfYear])
        let components = Calendar.current.dateComponents(units, from: date, to: currentDate)
        
        if components.year! > 0 {
            return "\(components.year!) " + (components.year! > 1 ? "years ago" : "year ago")
            
        } else if components.month! > 0 {
            return "\(components.month!) " + (components.month! > 1 ? "months ago" : "month ago")
            
        } else if components.weekOfYear! > 0 {
            return "\(components.weekOfYear!) " + (components.weekOfYear! > 1 ? "weeks ago" : "week ago")
            
        } else if (components.day! > 0) {
            return (components.day! > 1 ? "\(components.day!) days ago" : "Yesterday")
            
        } else if components.hour! > 0 {
            return "\(components.hour!) " + (components.hour! > 1 ? "hours ago" : "hour ago")
            
        } else if components.minute! > 0 {
            return "\(components.minute!) " + (components.minute! > 1 ? "minutes ago" : "minute ago")
            
        } else {
            return "\(components.second!) " + (components.second! > 1 ? "seconds ago" : "second ago")
        }
    }
    
}

extension String{
    func dateWithFormat(format:String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        return formatter.date(from:self) ?? Date()
    }
}
