

import UIKit
import Alamofire
import SDWebImage

class TabBarVCInfluencer: UITabBarController, UITabBarControllerDelegate{
    let appColor = UIColor(red: 0/255, green: 173/255, blue: 239/255, alpha: 1.0)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        //        self.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        for viewController in self.viewControllers! {
            _ = viewController.view
        }
      //  self.selectedIndex = selected_TabIndexServiceProvider
        self.manageUserImage()
        if objAppShareData.isFromNotification{
            if objAppShareData.strNotificationType == "Calls" || objAppShareData.strNotificationType == "Call"{
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           for vc in self.viewControllers! {
               if #available(iOS 13.0, *) {
                   vc.tabBarItem.imageInsets = UIEdgeInsets(top: 1, left: 0, bottom:2, right: 0)
               }else{
                   vc.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom:-5, right: 0)
               }
           }
       }
    
    func manageUserImage(){
        let tabWidth = (tabBar.frame.size.width/4)
        let tabBarWidth = tabBar.frame.size.width
        
        imgUserTabbar = UIImageView(frame: CGRect(x: (tabBarWidth - (tabBarWidth/4) + tabWidth/2)-15, y: 8, width: 30, height: 30))
        let profilePic = objAppShareData.UserDetail.strProfilePicture
        if profilePic != "" {
            let url = URL(string: profilePic)
            imgUserTabbar.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_img"))
        }else{
            imgUserTabbar.image = UIImage.init(named: "inactive_profile_ico")
        }
        imgUserTabbar.layer.cornerRadius = 15.0
        imgUserTabbar.layer.masksToBounds = true
        self.tabBar.addSubview(imgUserTabbar)
        imgUserTabbar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        viewOverTabBar = UIView(frame: CGRect(x: 0 , y: 0, width: tabBar.frame.size.width, height: 100))
        self.tabBar.addSubview(viewOverTabBar)
        viewOverTabBar.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        viewOverTabBar.isHidden = true
    }
    
    func manageUserImageSelected(){
        let tabWidth = (tabBar.frame.size.width/4)
        let tabBarWidth = tabBar.frame.size.width
        
        imgUserTabbar = UIImageView(frame: CGRect(x: (tabBarWidth - (tabBarWidth/4) + tabWidth/2)-15, y: 8, width: 30, height: 30))
        let profilePic = objAppShareData.UserDetail.strProfilePicture
        if profilePic != "" {
            let url = URL(string: profilePic)
            imgUserTabbar.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_img"))
        }else{
            imgUserTabbar.image = UIImage.init(named: "inactive_profile_ico")
        }
        
        imgUserTabbar.layer.cornerRadius = 15.0
        imgUserTabbar.layer.borderWidth = 1.0
        imgUserTabbar.layer.borderColor = UIColor.init(red: 218.0/255.0, green: 186.0/255.0, blue: 151.0/255.0, alpha: 1.0).cgColor
        imgUserTabbar.layer.masksToBounds = true
        self.tabBar.addSubview(imgUserTabbar)
        imgUserTabbar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        viewOverTabBar = UIView(frame: CGRect(x: 0 , y: 0, width: tabBar.frame.size.width, height: 100))
        self.tabBar.addSubview(viewOverTabBar)
        viewOverTabBar.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        viewOverTabBar.isHidden = true
    }
    func drawTabbar() {
        
        self.tabBar.items?[0].selectedImage = #imageLiteral(resourceName: "active_call_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[1].selectedImage = #imageLiteral(resourceName: "active_chart_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[2].selectedImage = #imageLiteral(resourceName: "active_notifications_ico").withRenderingMode(.alwaysOriginal)
    
        self.tabBar.items?[0].image = #imageLiteral(resourceName: "inactive_call_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[1].image = #imageLiteral(resourceName: "inactive_chart_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[2].image = #imageLiteral(resourceName: "inactive_notifications_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[3].image = #imageLiteral(resourceName: "placeholder_img").withRenderingMode(.alwaysOriginal)
 
        var dataImg:UIImage? = UIImage()
        DispatchQueue.main.async {
           
            let Stars = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.ProfilePic)

            Alamofire.request("https://dev-stars-app.s3.us-east-1.amazonaws.com/uploads/placeholders/user_profile_placeholder.png", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseData { (res) in
                print("found data \(res)")
                //self.tabBar.items?[3].image = UIImage(data: res.data!)
                dataImg = UIImage(data: res.data!)
                var img = dataImg?.ResizeImage(image: dataImg!, targetSize: CGSize(width: 35, height: 35))
                dataImg = img
                print("size of image \(img?.size.width)")
                
                img = img?.roundedImage
                self.tabBar.items?[3].image = img?.withRenderingMode(.alwaysOriginal)
                
                img = img?.rounded(with: #colorLiteral(red: 0.7490196078, green: 0.6, blue: 0.3921568627, alpha: 1), width: 2)
                self.tabBar.items?[3].selectedImage = img?.withRenderingMode(.alwaysOriginal)
                
                self.tabBar.inputView?.addSubview(UIView())
                
                //                UIImage().resizableImage(withCapInsets: UIEdgeInsets()
                //
            }
        }
        }
    
    func setupTabBarSeparators(){
//        let itemWidth = floor(self.tabBar.frame.size.width / CGFloat(self.tabBar.items!.count))
//        let bgView = UIView(frame: CGRect(x: 0, y: 0, width: tabBar.frame.size.width, height: tabBar.frame.size.height))
//
//
//        for i in 0..<(tabBar.items?.count)! - 1{
//            let separator = UIView(frame: CGRect(x: itemWidth * CGFloat(i + 1) - CGFloat(SEPARATOR_WIDTH / 2), y: 10, width: CGFloat(SEPARATOR_WIDTH), height: self.tabBar.frame.size.height-20))
//            separator.backgroundColor = UIColor(red: 98.0 / 255.0, green: 98.0 / 255.0, blue: 98.0 / 255.0, alpha: 1)
//            bgView.addSubview(separator)
//        }
//
//        UIGraphicsBeginImageContext(bgView.bounds.size)
//        bgView.layer.render(in: UIGraphicsGetCurrentContext()!)
//
//        let tabBarBackground = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        UITabBar.appearance().backgroundImage = tabBarBackground
    }

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        let indexOfTab = tabBar.items?.index(of: item)
        print("pressed tabBar: \(String(describing: indexOfTab))")
       if indexOfTab == 3{
           self.manageUserImageSelected()
       }else{
           self.manageUserImage()
       }
    }
}

