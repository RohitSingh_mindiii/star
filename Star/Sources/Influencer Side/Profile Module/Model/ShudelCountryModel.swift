//
//  CompanyInvitationListModel.swift
//  Reservision Drive
//
//  Created by Mindiii on 9/13/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ShudelCountryModel: NSObject {
    
    var strCountryCode = ""
    var strCountryId = ""
    var strCountryName = ""
    var imgLogo = UIImage()
    //var strFlag = UIImage
    
    ////
//       let objPendingData = CommonModal.init(dict: dictCountryData)
//       let imagestring = self.strCountryCode
//       let imagePath = "CountryPicker.bundle/\(imagestring).png"
//       objPendingData.imgCountryLogo = UIImage.init(named: imagePath) ?? UIImage()
//       objAppShareData.arrCountryData.append(objPendingData)
       ////
      
    init(dict:[String:Any]) {
        if let name = dict["country_code"] as? String{
            self.strCountryCode = name
        }
        
        if let name = dict["country_name"] as? String{
            self.strCountryName = name
        }
        
        if let countryID = dict["countryID"] as? String{
            self.strCountryId = countryID
        }else if let countryID = dict["countryID"] as? Int{
            self.strCountryId = String(countryID)
        }
               
        let imagestring = self.strCountryCode
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        self.imgLogo = UIImage.init(named: imagePath) ?? UIImage()
       
    }
}
