//
//  CompanyInvitationListModel.swift
//  Reservision Drive
//
//  Created by Mindiii on 9/13/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class SheduleModel: NSObject {
    
    var strAudioRate = ""
    var strVideoRate = ""
    var strGender = ""
    var strId = ""
    var strAgeGroup = ""
    var arrShedulLanguage = [ShudelLangModel]()
     var arrShedulCountry = [ShudelCountryModel]()
    
    init(dict:[String:Any]) {
        if let audio_rate = dict["audio_rate"] as? String{
            let newPrice = Double(audio_rate)
            let cleanPrice = newPrice?.clean
            let strFinalPrice = String(cleanPrice!)
            self.strAudioRate = strFinalPrice
        }
        else if let audio_rate = dict["audio_rate"] as? Int{
            let newPrice = Double(audio_rate)
            let cleanPrice = newPrice.clean
            let strFinalPrice = String(cleanPrice)
                   self.strAudioRate = strFinalPrice
               }
        
        if let schedule_id = dict["scheduleID"] as? String{
                   self.strId = schedule_id
               }
               else if let schedule_id = dict["scheduleID"] as? Int{
                          self.strId = String(schedule_id)
                      }
        
        if let video_rate = dict["video_rate"] as? String{
            let newPrice = Double(video_rate)
            let cleanPrice = newPrice?.clean
            let strFinalPrice = String(cleanPrice!)
            self.strVideoRate = strFinalPrice
        }
        else if let video_rate = dict["video_rate"] as? Int{
            let newPrice = Double(video_rate)
            let cleanPrice = newPrice.clean
            let strFinalPrice = String(cleanPrice)
            self.strVideoRate = strFinalPrice
        }
        
        
        if let gender = dict["gender"] as? String{
            self.strGender = gender
        }
        
        if let age_group = dict["age_group"] as? String{
            self.strAgeGroup = age_group
        }else if let age_group = dict["age_group"] as? Int{
            self.strAgeGroup = String(age_group)
        }
        
        
        if let arrReqData1 = dict["languages"] as? [Any]{
                   for dictGetData1 in arrReqData1
                   {
                       let obj = ShudelLangModel.init(dict: dictGetData1 as! Dictionary<String, Any>)
                       self.arrShedulLanguage.append(obj)
                   }
               }else{
                   arrShedulLanguage = [ShudelLangModel]()
               }
        
        if let arrReqData1 = dict["countries"] as? [Any]{
            for dictGetData1 in arrReqData1
            {
                let obj = ShudelCountryModel.init(dict: dictGetData1 as! Dictionary<String, Any>)
                self.arrShedulCountry.append(obj)
            }
        }else{
            arrShedulCountry = [ShudelCountryModel]()
        }
       
    }
}
