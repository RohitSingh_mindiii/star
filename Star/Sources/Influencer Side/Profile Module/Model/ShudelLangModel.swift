//
//  CompanyInvitationListModel.swift
//  Reservision Drive
//
//  Created by Mindiii on 9/13/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ShudelLangModel: NSObject {
    
    var strLangName = ""
    var strLangId = ""
    
    init(dict:[String:Any]) {
        if let name = dict["name"] as? String{
            self.strLangName = name
        }
        
        
        if let languageID = dict["languageID"] as? String{
            self.strLangId = languageID
        }
        else if let languageID = dict["languageID"] as? Int{
                   self.strLangId = String(languageID)
            }
       
    }
}
