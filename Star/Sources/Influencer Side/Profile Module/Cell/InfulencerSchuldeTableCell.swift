
//
//  ProfileTableCell.swift
//  Star
//
//  Created by IOS-Sagar on 11/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class InfulencerSchuldeTableCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var viewInner: UIView!
    @IBOutlet weak var FlagCollection: UICollectionView!
    @IBOutlet weak var lblAgeGroup: UILabel!
    @IBOutlet weak var lblAudio_rate: UILabel!
    @IBOutlet weak var lblVideo_rate: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var viewAllCountryFlag: UIView!
    var arrCountryLogo = [ShudelCountryModel]()
    
    func reloadCollectionCountry(){
        DispatchQueue.main.async {
            self.FlagCollection.reloadData()
        }
    }
   
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewInner.setShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrCountryLogo.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.FlagCollection.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionCell", for: indexPath) as! ProfileCollectionCell
        let obj = self.arrCountryLogo[indexPath.item]
        cell.imgFlag.image = obj.imgLogo
        cell.imgFlag.layer.cornerRadius = 15.0
        cell.imgFlag.clipsToBounds = true
        return cell
    }
           
}
