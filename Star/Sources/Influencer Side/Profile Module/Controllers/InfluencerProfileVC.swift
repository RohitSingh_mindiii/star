//
//  ProfileVC.swift
//  Design2
//
//  Created by IOS-Sagar on 05/02/20.
//  Copyright © 2020 IOS-Sagar. All rights reserved.
//

import UIKit

class InfluencerProfileVC: UIViewController {
    
      @IBOutlet weak var ScheduleListTableView: UITableView!
    
    @IBOutlet weak var viewImgProfile: UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var containerView: UIView!
    var arrShedul = [SheduleModel]()
    @IBOutlet var btnDarkMode: UIButton!
    @IBOutlet weak var viewAdd: UIView!
    @IBOutlet weak var tblHgtConstant: NSLayoutConstraint!
    @IBOutlet var imgDarkMode: UIImageView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUiDesign()
        self.setData()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
        self.callWebserviceGetSheduleList()
        let isDarkMode:Bool = userDefaults.bool(forKey: "isDarkMode")
           if isDarkMode == true{
               if #available(iOS 13.0, *) {
                  self.imgDarkMode.image = #imageLiteral(resourceName: "on_toggle")
                  self.btnDarkMode.isSelected = true
               }
           }else{
               if #available(iOS 13.0, *) {
                  self.imgDarkMode.image = #imageLiteral(resourceName: "off_toggle")
                  self.btnDarkMode.isSelected = false
               }
        }
    }
}

extension InfluencerProfileVC
    {
    
    func setData(){
         self.ScheduleListTableView.isHidden = true
         self.viewAdd.isHidden = true
        self.lblUserName.text = UserDefaults.standard.string(forKey:UserDefaults.InfulenceKeys.InfulencerName)
         let profilePic = UserDefaults.standard.string(forKey:UserDefaults.InfulenceKeys.InfulencerProfileImage)
        
            if profilePic != "" {
                let url = URL(string: profilePic!)
                self.imgUser.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_img"))
            }
       // self.imgUser.setImageFream()
    }
    func setUiDesign(){
        self.viewAdd.setviewCircle()
        //self.viewHeader.setviewbottomShadow()
        self.imgUser.setImgCircle()
        self.viewImgProfile.setshadowViewCircle2()
        
    }
   }

extension InfluencerProfileVC{
    @IBAction func btnAddSchedule(_ sender: Any) {
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddSchedulVC") as! AddSchedulVC
           self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnSettingAction(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Setting", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnOnDarkMode(_ sender: UIButton) {
        self.view.endEditing(true)

        if sender.isSelected == true{
            if #available(iOS 13.0, *) {
                UserDefaults.standard.setValue(true, forKey: "isDarkMode")
                self.imgDarkMode.image = #imageLiteral(resourceName: "on_toggle")
                objAppDelegate.window?.overrideUserInterfaceStyle = .dark
            }
            sender.isSelected = false
            
        }else{
            if #available(iOS 13.0, *) {
                self.imgDarkMode.image = #imageLiteral(resourceName: "off_toggle")
                UserDefaults.standard.setValue(false, forKey: "isDarkMode")

                objAppDelegate.window?.overrideUserInterfaceStyle = .light
            }
            sender.isSelected = true
        }
    }
    @IBAction func switchDarkMode(_ sender: UISwitch) {
           if sender.isOn
           {
               if #available(iOS 13.0, *) {
                 let us = UIApplication.shared
                   
               } else {
                   // Fallback on earlier versions
               }
           }
           else
           {
               if #available(iOS 13.0, *) {
                   self.view.overrideUserInterfaceStyle = .light
               } else {
                   // Fallback on earlier versions
               }
           }
           }
}

extension InfluencerProfileVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          self.tblHgtConstant.constant = CGFloat(self.arrShedul.count * 236)
      return self.arrShedul.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.ScheduleListTableView.dequeueReusableCell(withIdentifier: "InfulencerSchuldeTableCell", for: indexPath) as! InfulencerSchuldeTableCell
         let obj = arrShedul[indexPath.row]
       
        cell.lblAgeGroup.text = obj.strAgeGroup
        cell.lblGender.text = obj.strGender
        cell.lblAudio_rate.text = obj.strAudioRate
        cell.lblVideo_rate.text = obj.strVideoRate
        
        cell.btnMenu.tag = indexPath.row
        cell.btnMenu.addTarget(self,action:#selector(buttonClicked(sender:)), for: .touchUpInside)
        
        let arrLanguage = obj.arrShedulLanguage
        var strLang = ""
        for obLang in arrLanguage{
            strLang.append("\(obLang.strLangName),")
        }
        let finalLang = strLang.dropLast(1)
        if String(finalLang) == ""
        {
             cell.lblLanguage.text = "All"
        }else{
             cell.lblLanguage.text = String(finalLang)
        }
        cell.lblRating.text = "NA"
        if obj.arrShedulCountry.count == 0{
            cell.viewAllCountryFlag.isHidden = false
            cell.FlagCollection.isHidden = true
        }else{
            cell.viewAllCountryFlag.isHidden = true
            cell.FlagCollection.isHidden = false
            cell.arrCountryLogo = obj.arrShedulCountry
            cell.reloadCollectionCountry()
        }
        
        return cell
    }
    
   @objc func buttonClicked(sender:UIButton)
        {
            let actionSheetAlertController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "CANCEL", style: .cancel, handler: nil)
            actionSheetAlertController.addAction(cancelActionButton)
            
            actionSheetAlertController.view.tintColor = UIColor(named: "Text_Color")

                let  editAction = UIAlertAction(title: " Edit", style: .default){ (UIAlertAction) in
                      let objData = self.arrShedul[sender.tag]
                      self.updateShedule(objEvent: objData)
                      
                }
                let  attributedText = NSMutableAttributedString(string: " Edit")
                let range = NSRange(location: 0, length: attributedText.length)
            attributedText.addAttribute(NSAttributedString.Key.kern, value: 0.1, range: range)
            attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat-Regular", size: 18.0)!, range: range)
                
                
                let imageEdit = UIImageView(image: #imageLiteral(resourceName: "new_edit_ico_w"))
                actionSheetAlertController.view.addSubview(imageEdit)
                imageEdit.translatesAutoresizingMaskIntoConstraints = false
                actionSheetAlertController.view.addConstraint(NSLayoutConstraint(item: imageEdit, attribute: .centerX, relatedBy: .equal, toItem: actionSheetAlertController.view, attribute: .centerX, multiplier: 1.85, constant: 0))
                actionSheetAlertController.view.addConstraint(NSLayoutConstraint(item: imageEdit, attribute: .centerY, relatedBy: .equal, toItem: actionSheetAlertController.view, attribute: .centerY, multiplier: 0.33, constant: 0))
                actionSheetAlertController.view.addConstraint(NSLayoutConstraint(item: imageEdit, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 17.0))
                actionSheetAlertController.view.addConstraint(NSLayoutConstraint(item: imageEdit, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 17.0))
                
            editAction.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            
            actionSheetAlertController.addAction(editAction)
                
    //---------------- Delete action ------------------
            
             let obj = self.arrShedul[sender.tag]
            
                 var strButtonText = ""
            
                strButtonText = " Delete"
                
                let deleteAction = UIAlertAction(title:strButtonText, style: .default){ (UIAlertAction) in
                let objData = self.arrShedul[sender.tag]
                 self.deleteShedule(objEvent: objData)
                   
                }
                
                let imagdelete = UIImageView(image:#imageLiteral(resourceName: "delete_ico_w"))
                actionSheetAlertController.view.addSubview(imagdelete)
                imagdelete.translatesAutoresizingMaskIntoConstraints = false
                actionSheetAlertController.view.addConstraint(NSLayoutConstraint(item: imagdelete, attribute: .centerX, relatedBy: .equal, toItem: actionSheetAlertController.view, attribute: .centerX, multiplier: 1.85, constant: 0))
            actionSheetAlertController.view.addConstraint(NSLayoutConstraint(item: imagdelete, attribute: .centerY, relatedBy: .equal, toItem: actionSheetAlertController.view, attribute: .centerY, multiplier: 0.99, constant: 0))
                actionSheetAlertController.view.addConstraint(NSLayoutConstraint(item: imagdelete, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 20.0))
                actionSheetAlertController.view.addConstraint(NSLayoutConstraint(item: imagdelete, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 20.0))
                
            deleteAction.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                
                
                // for font  attributed text
                let attributedTextdelete = NSMutableAttributedString(string:strButtonText)
                let range1 = NSRange(location: 0, length: attributedTextdelete.length)
            attributedTextdelete.addAttribute(NSAttributedString.Key.kern, value:0.1, range: range1)
            attributedTextdelete.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat-Regular", size: 18.0)!, range: range1)
            
            actionSheetAlertController.addAction(deleteAction)

            self.present(actionSheetAlertController, animated: true, completion: nil)
        }
    
    func updateShedule (objEvent:SheduleModel)
    { let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateSchedulVC") as! UpdateSchedulVC
        vc.ObjShedulUpdateData = objEvent
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
       func deleteShedule (objEvent:SheduleModel)
    {
        objWebServiceManager.showIndicator()
        
        //let StrUrl = "https://dev.stars.app/api/v1/schedule/" + objEvent.strId
        let StrUrl = BASE_URL + "schedule/" + objEvent.strId
        
        objWebServiceManager.requestDelete(strURL: StrUrl, params: nil, strCustomValidation: "", success: {response in
            
            let status = (response["status"] as? String)
            // let message = (response["message"] as? String)
            if status == k_success{
                //objWebServiceManager.hideIndicator()
                self.callWebserviceGetSheduleList()
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            // objAppShareData.showAlert(title: kAlertTitle, message: message ?? "", view: self)
        })
    }
    
}

extension InfluencerProfileVC
{
   func callWebserviceGetSheduleList(){
    self.arrShedul.removeAll()
          objWebServiceManager.showIndicator()
          objWebServiceManager.requestGet(strURL: WsUrl.ScheduleList, params: nil, strCustomValidation: "", success: {response in
              print(response)
              let status = (response["status"] as? String)
              let message = (response["message"] as? String)
              if status == k_success{
                  objWebServiceManager.hideIndicator()
                  var dataFound:Int?
                  if let data = response["data"]as? [String:Any]{
                    dataFound = data["data_found"] as? Int ?? 0
                      if status == "success" ||  status == "Success"
                      {
                        if dataFound == 0
                        {
                            self.ScheduleListTableView.isHidden = true
                            self.viewAdd.isHidden = true
                            return
                           
                        }else{
                            let arrAllReqData = data["schedule_list"] as! [[String: Any]]
                              
                              //  let countryCode = Locale.current.regionCode
                              for dictGetData in arrAllReqData
                              {
                                  let objPendingData = SheduleModel.init(dict: dictGetData)
                                  self.arrShedul.append(objPendingData)
                              }
                            self.ScheduleListTableView.reloadData()
                            
                            if self.arrShedul.count == 0
                            {
                                self.ScheduleListTableView.isHidden = true
                                self.viewAdd.isHidden = true
                            }else{
                                self.ScheduleListTableView.isHidden = false
                                self.viewAdd.isHidden = false
                            }
                        }
                      }else{
                          objWebServiceManager.hideIndicator()
                          objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                      }
                  }
              }
              else{
                  objWebServiceManager.hideIndicator()
                 if message == "Your token has been expired"
                  {self.postAlert(strMessage: message ?? "")
                  }else{
                      objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                  }
              }
          }, failure: { (error) in
              print(error)
              objWebServiceManager.hideIndicator()
              objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
          })
      }
    func postAlert(strMessage: String)
    {
        // Create the alert controller
        let alertController = UIAlertController(title: "", message: strMessage , preferredStyle: .alert)
        
        let subview = alertController.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.layer.cornerRadius = 10
        alertContentView.alpha = 1
        alertContentView.layer.borderWidth = 1
        alertContentView.layer.borderColor = UIColor.gray.cgColor
       // alertController.view.tintColor =  UIColor.red.cgColor
        
        // Create the actions
        let okAction = UIAlertAction(title:"Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            objAppDelegate.showLogInNavigation()
        }
        
        // Add the actions
        
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
       
    }



//extension InfluencerProfileVC{
//
//    func collectCountries() {
//        for country in Localcountries  {
//            let code = country["code"] ?? ""
//            let name = country["name"] ?? ""
//            let dailcode = country["dial_code"] ?? ""
//            let FlagImage = country["dial_code"] ?? ""
//
//            RScountriesModel.append(CountryInfo(country_code:code,dial_code:dailcode, country_name:name, flag: FlagImage))
//        }
//    }
//}


