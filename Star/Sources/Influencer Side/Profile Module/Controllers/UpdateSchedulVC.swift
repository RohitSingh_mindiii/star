//
//  AddSchedulVC.swift
//  Design2
//
//  Created by IOS-Sagar on 05/02/20.
//  Copyright © 2020 IOS-Sagar. All rights reserved.
//

import UIKit

class UpdateSchedulVC: UIViewController,BottomSheetVCDelegate {

    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var ViewInner: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var ViewForVideo: UIView!
    @IBOutlet weak var ViewForVoice: UIView!
    @IBOutlet weak var ViewForGender: UIView!
    @IBOutlet weak var ViewForAge: UIView!
    @IBOutlet weak var ViewForLanguage: UIView!
    @IBOutlet weak var ViewForCountry: UIView!
    @IBOutlet weak var imgAllCountry: UIImageView!
    @IBOutlet weak var imgSpecificCountry: UIImageView!
    @IBOutlet weak var imgVoice: UIImageView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var btnAddSchedule: UIButton!
    
     @IBOutlet weak var txtVideoMessage: UITextField!
     @IBOutlet weak var txtVoice: UITextField!
     @IBOutlet weak var  lblVideo: UILabel!
    @IBOutlet weak var  lblVoice: UILabel!
     @IBOutlet weak var txtGender: UITextField!
     @IBOutlet weak var  txtAge: UITextField!
     @IBOutlet weak var  txtLanguage: UITextField!
     @IBOutlet weak var  txtAllCountry: UITextField!
     @IBOutlet weak var  viewSelectCountry: UIView!
    
    var strBottomSheetFrom = ""
    var arrLanguages = [CommonModal]()
    var arrGender = [CommonModal]()
    var arrCountry = [CommonModal]()
    var arrAgeGroup = [CommonModal]()
    var strGender = ""
    var strAge = ""
    var strCountry = ""
    var strLanguage = ""
    var ObjShedulUpdateData : SheduleModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.setUpdateData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.arrLanguages = objAppShareData.arrLanguageData
        self.arrGender = objAppShareData.arrGenderData
        self.arrCountry = objAppShareData.arrCountryData
        self.arrAgeGroup = objAppShareData.arrAgeGroupData
    }

}

extension UpdateSchedulVC{
    
    func setData() {
      //  self.viewHeader.setviewbottomShadow()
        self.ViewForVideo.setViewShadowCorner()
        self.ViewForVoice.setViewShadowCorner()
       // self.ViewForGender.setViewShadowCorner()
        self.ViewForLanguage.setViewShadowCorner()
      //  self.ViewForAge.setViewShadowCorner()
        self.ViewForLanguage.setViewShadowCorner()
     //   self.ViewForCountry.setViewShadowCorner()
        self.btnAddSchedule.setCornerRediues()
        self.ViewInner.setShadow()
        self.viewSelectCountry.isHidden = true
        self.imgVideo.image = #imageLiteral(resourceName: "inactive_check_ico")
        self.imgVoice.image = #imageLiteral(resourceName: "inactive_check_ico")
    }
    
    func setUpdateData() {
        print(ObjShedulUpdateData?.strAudioRate as Any)
        
        self.txtVoice.text = ObjShedulUpdateData?.strAudioRate
        self.txtVideoMessage.text = ObjShedulUpdateData?.strVideoRate
        self.txtAge.text = ObjShedulUpdateData?.strAgeGroup
        self.txtGender.text = ObjShedulUpdateData?.strGender
        
        /// Gender
        if ObjShedulUpdateData?.strGender == "Male"{
            self.strGender = "1"
        } else if ObjShedulUpdateData?.strGender == "Female"{
            self.strGender = "2"
        }else{
            self.strGender = "0"
        }
        
        //// Age
        if ObjShedulUpdateData?.strAgeGroup == "17-24"{
                self.strAge = "1"
        } else if ObjShedulUpdateData?.strAgeGroup == "25+"{
                self.strAge = "2"
        }else{
                self.strAge = "0"
        }
        
        if self.txtVideoMessage.text != ""
        {
             self.imgVideo.image = #imageLiteral(resourceName: "active_check_ico")
        }
        if self.txtVoice.text != ""
        {
             self.imgVoice.image = #imageLiteral(resourceName: "active_check_ico")
        }
                
        let arrLanguage = ObjShedulUpdateData!.arrShedulLanguage
               var strLang = ""
               var strLangId = ""
               for obLang in arrLanguage{
                   strLang.append("\(obLang.strLangName),")
                   strLangId.append("\(obLang.strLangId),")
               }
               let finalLang = strLang.dropLast(1)
               if String(finalLang) == ""
               {
                    self.txtLanguage.text = "All Languages"
               }else{
                if (objAppShareData.arrLanguageData.count-1) == arrLanguage.count{
                   self.txtLanguage.text = "All Languages"
                }else{
                   self.txtLanguage.text = String(finalLang)
                }
               }

               let finalLangId = strLangId.dropLast(1)
               strLanguage = String(finalLangId)
       
        if (objAppShareData.arrLanguageData.count-1) == arrLanguage.count{
            strLanguage = "0"
        }

//        strLanguage = ""
//        for obj in ObjShedulUpdateData!.arrShedulLanguage{
//            strLanguage = obj.strLangId + ","
//        }
//        strLanguage = String(strLanguage.dropLast())
        let arrCountry = ObjShedulUpdateData!.arrShedulCountry
                      var strCounty = ""
                      var strCountryId = ""
                      for obLang in arrCountry{
                          strCounty.append("\(obLang.strCountryName),")
                          strCountryId.append("\(obLang.strCountryId),")
                      }
                      let finalCountry = strCounty.dropLast(1)
                      if String(finalCountry) == ""
                      {
                       self.txtAllCountry.text = "All"
                      }else{
                           self.txtAllCountry.text = String(finalCountry)
                      }
        
        let finalCountryId = strCountryId.dropLast(1)
        strCountry = String(finalCountryId)
        strCounty = ""
        for obj in ObjShedulUpdateData!.arrShedulCountry{
            strCounty = obj.strCountryId + ","
        }
        strCounty = String(strCounty.dropLast())
        if String(finalCountry) == ""
        {
                   self.imgAllCountry.image = #imageLiteral(resourceName: "active_dots_ico")
                   self.imgSpecificCountry.image = #imageLiteral(resourceName: "inactive_dots_ico")
                   self.viewSelectCountry.isHidden = true
        }else{
                self.imgAllCountry.image = #imageLiteral(resourceName: "inactive_dots_ico")
                self.imgSpecificCountry.image = #imageLiteral(resourceName: "active_dots_ico")
                self.viewSelectCountry.isHidden = false
        }
 
    }
    
    //MARK: step 6 finally use the method of the contract
    // func sendDataToFirstViewController(myData: [Int]) {
    func sendDataToFirstViewController(arrId:[Int], arrName:[String]) {
    if self.strBottomSheetFrom == "gender"{
    print(" my gender is \(arrId)")
    print(" my gender is \(arrName)")
    let varText = arrName.joined(separator:",")
    self.txtGender.text = varText
    self.strGender = (arrId.map{String($0)}).joined(separator: ",")

    }
    else if self.strBottomSheetFrom == "language"{
    print(" my language is \(arrId)")
    print(" my language is \(arrName)")
    let varText = arrName.joined(separator:",")
    self.txtLanguage.text = varText
    self.strLanguage = (arrId.map{String($0)}).joined(separator: ",")
    }
     else if self.strBottomSheetFrom == "AgeGroup"{
     print(" my Age is \(arrId)")
     print(" my Age is \(arrName)")
     let varText = arrName.joined(separator:",")
     self.txtAge.text = varText
        self.strAge = (arrId.map{String($0)}).joined(separator: ",")
     }
        
    else if self.strBottomSheetFrom == "Country"{
            print(" my country is \(arrId)")
            print(" my Country is \(arrName)")
            let varText = arrName.joined(separator:",")
            self.txtAllCountry.text = varText
            self.strCountry = (arrId.map{String($0)}).joined(separator: ",")
            }
        
    }
    
    func showBotttomSheet(arr : Array<Any>, str: String ){

    let sb = UIStoryboard.init(name: "Main", bundle:Bundle.main)
    let vc = sb.instantiateViewController(withIdentifier:"BottomSheetVC") as! BottomSheetVC

    vc.arrBottom = arr as! [CommonModal]
    vc.strHeader = str
    vc.modalPresentationStyle = .overCurrentContext
    vc.delegate = self
    self.present(vc, animated: false, completion: nil)

    }}

extension UpdateSchedulVC{
    
    func ValidationForAddSchedule()
    {
        self.txtVoice.text = self.txtVoice.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtVideoMessage.text = self.txtVideoMessage.text!.trimmingCharacters(in: .whitespacesAndNewlines)
         self.txtAllCountry.text = self.txtAllCountry.text!.trimmingCharacters(in: .whitespacesAndNewlines)
         self.txtAge.text = self.txtAge.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtLanguage.text = self.txtLanguage.text!.trimmingCharacters(in: .whitespacesAndNewlines)
         self.txtGender.text = self.txtGender.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
       
        
        // if txt.isHidden == false {
        if (txtVoice.text?.isEmpty)! &&  (txtVideoMessage.text?.isEmpty)!{
                   objAlert.showAlert(message: BlankVoiceVideo, title:kAlertTitle, controller: self)
               }
       else if (txtGender.text?.isEmpty)! {
            objAlert.showAlert(message: BlankGender, title:kAlertTitle, controller: self)
        }
        else if (txtAge.text?.isEmpty)! {
                   objAlert.showAlert(message: BlankAge, title:kAlertTitle, controller: self)
               }
        else if (txtLanguage.text?.isEmpty)! {
            objAlert.showAlert(message: BlankLanguage, title:kAlertTitle, controller: self)
        }else{
            self.callWebserviceForUpdateShedule()
        }
    }
   
    func callWebserviceForUpdateShedule(){
           
           if !objWebServiceManager.isNetworkAvailable(){
               objWebServiceManager.hideIndicator()
               objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
               return
               
           }
           objWebServiceManager.showIndicator()
           self.view.endEditing(true)
                  
        var param = [String:Any]()
        var strCountrySetting = ""
        if strCountry == "" || strCountry == "0"{
            strCountrySetting = "0"
        }else{
             strCountrySetting = "1"
        }
           
        param = ["video_rate":txtVideoMessage.text ?? "",
                 "audio_rate":txtVoice.text ?? "",
                 "gender":strGender,
                 "age_group":strAge ,
                    "country_settings": strCountrySetting ,
                    "language": strLanguage,
                    "country": strCountry , "schedule_id":String(ObjShedulUpdateData?.strId ?? "" )
           ]
           
           print(param)
           
           objWebServiceManager.requestPut(strURL: WsUrl.AddSchedule, params: param, strCustomValidation: "", showIndicator: false, success: {
           response in
           print(response)
               let status = (response["status"] as? String)
               let message = (response["message"] as? String)
                if status == "success"{
                    objWebServiceManager.hideIndicator()
                                   self.postAlert(strMessage: message!)
                }
                else{
                    objWebServiceManager.hideIndicator()
objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                }
            
           }, failure: { (error) in
               print(error)
               objWebServiceManager.hideIndicator()
               objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
           })
       }
    
    
    func postAlert(strMessage: String)
       {
           // Create the alert controller
           let alertController = UIAlertController(title: "", message: strMessage , preferredStyle: .alert)
           
           let subview = alertController.view.subviews.first! as UIView
           let alertContentView = subview.subviews.first! as UIView
           alertContentView.layer.cornerRadius = 10
           alertContentView.alpha = 1
           alertContentView.layer.borderWidth = 1
           alertContentView.layer.borderColor = UIColor.gray.cgColor
          // alertController.view.tintColor =  UIColor.red.cgColor
           
           // Create the actions
           let okAction = UIAlertAction(title:"Ok", style: UIAlertAction.Style.default) {
               UIAlertAction in
              self.navigationController?.popViewController(animated: true)
           }
           
           // Add the actions
           
           alertController.addAction(okAction)
           
           // Present the controller
           self.present(alertController, animated: true, completion: nil)
       }
}

extension UpdateSchedulVC{
    
    @IBAction func btnActionBack(_ sender: Any) {
             self.navigationController?.popViewController(animated: true)
         }
      
      @IBAction func btnActionAddSchedul(_ sender: Any) {
         ValidationForAddSchedule()
        
    }
    
    @IBAction func btnActionSelectCountry(_ sender: Any) {
                 self.view.endEditing(true)
                   self.strBottomSheetFrom = "Country"
                   let str = "Select Country"
                   print("self.arrCountry count is \(self.arrCountry.count)")
        ////
               objAppShareData.isCountryInBottomSheet = true
               let arr = self.strCountry.components(separatedBy: ",")
               if arr.count>0{
                   let arrInt = arr.map { Int($0) }
                   if arrInt.count>0{
                       let int = arrInt[0]
                       if int != nil{
                           objAppShareData.arrSelectedBottomIds = arrInt as! [Int]
                       }
                   }
               }
               let arrName = self.txtAllCountry.text!.components(separatedBy: ",")
               if arrName.count>0{
                   objAppShareData.arrSelectedBottomNames = arrName
               }
               ////
                   self.showBotttomSheet(arr: self.arrCountry,str: str)
         }
    
    @IBAction func btnActionAllCountry(_ sender: Any) {
        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
        self.imgAllCountry.image = #imageLiteral(resourceName: "active_dots_ico")
        self.imgSpecificCountry.image = #imageLiteral(resourceName: "inactive_dots_ico")
        self.strCountry = ""
        self.viewSelectCountry.isHidden = true
    }
    
    @IBAction func btnSpecificCountry(_ sender: Any) {
        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
       self.imgAllCountry.image = #imageLiteral(resourceName: "inactive_dots_ico")
       self.imgSpecificCountry.image = #imageLiteral(resourceName: "active_dots_ico")
        self.viewSelectCountry.isHidden = false
    }
    
    @IBAction func btnLanguages(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
        self.strBottomSheetFrom = "language"
        let str = "Select Languages"
        print("self.arrLanguages count is \(self.arrLanguages.count)")
        
        ////
        let arr = self.strLanguage.components(separatedBy: ",")
        if arr.count>0{
            let arrInt = arr.map { Int($0) }
            if arrInt.count>0{
                let int = arrInt[0]
                if int != nil{
                    objAppShareData.arrSelectedBottomIds = arrInt as! [Int]
                }
            }
        }
        let arrName = self.txtLanguage.text!.components(separatedBy: ",")
        if arrName.count>0{
            objAppShareData.arrSelectedBottomNames = arrName
        }
        ////
        self.showBotttomSheet(arr: self.arrLanguages,str: str)
    }
    
    @IBAction func btnGender(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
        self.strBottomSheetFrom = "gender"
        let str = "Select Gender"
        print("self.arrgender count is \(self.arrGender.count)")
        
        ////
        let arr = self.strGender.components(separatedBy: ",")
        if arr.count>0{
            let arrInt = arr.map { Int($0) }
            if arrInt.count>0{
                let int = arrInt[0]
                if int != nil{
                    objAppShareData.arrSelectedBottomIds = arrInt as! [Int]
                }
            }
        }
        let arrName = self.txtGender.text!.components(separatedBy: ",")
        if arrName.count>0{
            objAppShareData.arrSelectedBottomNames = arrName
        }
        ////
        self.showBotttomSheet(arr: self.arrGender,str: str)
    }
    
    @IBAction func btnAge(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
        self.strBottomSheetFrom = "AgeGroup"
        let str = "Select Age"
        print("self.arrAgeGroup count is \(self.arrAgeGroup.count)")
        
        ////
               let arr = self.strAge.components(separatedBy: ",")
               if arr.count>0{
                   let arrInt = arr.map { Int($0) }
                   if arrInt.count>0{
                       let int = arrInt[0]
                       if int != nil{
                           objAppShareData.arrSelectedBottomIds = arrInt as! [Int]
                       }
                   }
               }
               let arrName = self.txtAge.text!.components(separatedBy: ",")
               if arrName.count>0{
                   objAppShareData.arrSelectedBottomNames = arrName
               }
               ////
        self.showBotttomSheet(arr: self.arrAgeGroup,str: str)
    }
    
    @IBAction func btnVideo(_ sender: Any) {
        if self.imgVideo.image == #imageLiteral(resourceName: "inactive_check_ico"){
           self.imgVideo.image = #imageLiteral(resourceName: "active_check_ico")
        }else{
            self.imgVideo.image = #imageLiteral(resourceName: "inactive_check_ico")
        }
           }
    
    @IBAction func btnVice(_ sender: Any) {
                 if self.imgVoice.image == #imageLiteral(resourceName: "inactive_check_ico"){
                           self.imgVoice.image = #imageLiteral(resourceName: "active_check_ico")
                        }else{
                            self.imgVoice.image = #imageLiteral(resourceName: "inactive_check_ico")
                        }
              }
}


// MARK: - Textfield Delegate Methods
extension UpdateSchedulVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if textField == self.txtVoice{
            
            let currentText = txtVoice.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            if replacementText.contains(" "){
                
                return false
            }else{
                let currentCharacterCount = txtVoice.text?.count ?? 0
                if range.length + range.location > currentCharacterCount {
                    return false
                }
                let newLength = currentCharacterCount + string.count - range.length
                return newLength <= 8
            }
        }
       else if textField == self.txtVideoMessage{
            
            let currentText = txtVideoMessage.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            
            if replacementText.contains(" "){
                
                return false
            }else{
                let currentCharacterCount = txtVideoMessage.text?.count ?? 0
                if range.length + range.location > currentCharacterCount {
                    return false
                }
                let newLength = currentCharacterCount + string.count - range.length
                return newLength <= 8
            }
        }
        return true
    }
    
}

