//
//  LoginInfluencerVC.swift
//  Star
//
//  Created by ios-deepak b on 27/01/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//
import UIKit
import Foundation

class LoginInfluencerVC: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var vwEmail: UIView!
    @IBOutlet weak var vwPassword: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var btnForgotPwd: UIButton!
    @IBOutlet weak var lblRememberMe: UILabel!
    @IBOutlet weak var lblLogin: UILabel!
    
    // MARK:- Varibles
    var iconClick = true
    var isRemember = true
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isRememberData()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let color = UIColor(named: "bordercolor")
        
        self.vwEmail.setCornerRadBoarder(color: color!, cornerRadious: 4)
        self.vwPassword.setCornerRadBoarder(color: color!, cornerRadious: 4)
        self.btnLogin.setCornerRadius(cornerRadious: 4)
        //  self.imgCheck.setCornerRadiusWithBoarder(color:#colorLiteral(red: 0.6352941176, green: 0.6352941176, blue: 0.6352941176, alpha: 1),cornerRadious: 4 ,photoImageView: self.imgCheck)
        self.txtEmail.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Email ID or Phone Number")
        self.txtPassword.attributedPlaceholder = NSAttributedString.attributedPlaceholderString(string: "Password")
        
        
    }
    // MARK:- Buttons Action
    @IBAction func btnShowPwd(_ sender: UIButton) {
        self.view.endEditing(true)
        
          if(iconClick == true) {
                 self.imgPassword.image = #imageLiteral(resourceName: "open_view_ico")
                 txtPassword.isSecureTextEntry = false
             } else {
                 self.imgPassword.image = #imageLiteral(resourceName: "close_view_ico")
                 txtPassword.isSecureTextEntry = true
             }
             
             iconClick = !iconClick
        
    }
    
    @IBAction func btnLoginAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.imgCheck.image == #imageLiteral(resourceName: "inactive_check_ico"){
            self.rememberEmailPassword(value: false)
        }else{
            self.rememberEmailPassword(value: true)
        }
        self.ValidationForLogIn()
        
    }
    @IBAction func btnForgotPwdAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let sb = UIStoryboard.init(name: "Main", bundle:Bundle.main)
        let vc = sb.instantiateViewController(withIdentifier:"ForgotPasswordVC") as! ForgotPasswordVC
        
        //  let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPwdInfluencerVC") as! ForgotPwdInfluencerVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnRememberPwdAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.imgCheck.image == #imageLiteral(resourceName: "inactive_check_ico"){
            self.rememberEmailPassword(value: true)
        }else{
            self.rememberEmailPassword(value: false)
        }
//        let ud = UserDefaults.standard
//        let value = ud.value(forKey:"AlwayssignInfu") as? String ?? ""
//
//        if value == "true"{
//            self.imgCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
//            ud.removeObject(forKey: "AlwayssignInfu")
//            ud.removeObject(forKey: "AlwasysEmailInfu")
//            ud.removeObject(forKey: "AlwaysPasswordInfu")
//        }else{
//            self.imgCheck.image = #imageLiteral(resourceName: "active_check_ico")
//            ud.set("true", forKey:"AlwayssignInfu")
//            ud.set(self.txtEmail.text, forKey:"AlwasysEmailInfu")
//            ud.set(self.txtPassword.text, forKey:"AlwaysPasswordInfu")
//        }
    }
    
    func rememberEmailPassword(value:Bool){
        self.view.endEditing(true)
        let ud = UserDefaults.standard
       // let value = ud.value(forKey:"Alwayssign") as? String ?? ""
        
        if value == false{
            self.imgCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
           // ud.removeObject(forKey: "Alwayssign")
            ud.removeObject(forKey: "AlwasysEmailInfu")
            ud.removeObject(forKey: "AlwaysPasswordInfu")
        }else{
            self.imgCheck.image = #imageLiteral(resourceName: "active_check_ico")
            //ud.set("true", forKey:"Alwayssign")
            ud.set(self.txtEmail.text, forKey:"AlwasysEmailInfu")
            ud.set(self.txtPassword.text, forKey:"AlwaysPasswordInfu")
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension LoginInfluencerVC : UITextFieldDelegate{
    
    // TextField delegate method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        if textField == txtEmail{
            txtEmail.resignFirstResponder()
            txtPassword.becomeFirstResponder()
        }
            
        else if textField == txtPassword{
            txtPassword.resignFirstResponder()
        }
        
        return true
    }
    
    
}
extension LoginInfluencerVC {
    func ValidationForLogIn()
    {
        self.txtEmail.text = self.txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtPassword.text = self.txtPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        // if txt.isHidden == false {
        if (txtEmail.text?.isEmpty)! {
            objAlert.showAlert(message: BlankEmail, title:kAlertTitle, controller: self)
        }else if !objValidationManager.validateEmail(with: txtEmail.text!){
            objAlert.showAlert(message: ValidEmail, title:kAlertTitle, controller: self)
        }
        else  if (txtPassword.text?.isEmpty)! {
            objAlert.showAlert(message: BlankPassword, title:kAlertTitle, controller: self)
        }
        else if  objValidationManager.isPwdLenth(password: txtPassword.text!) {
            objAlert.showAlert(message: LengthPassword, title: kAlertTitle, controller: self)
        }
        else{
            self.callWebserviceForLogin()
        }
    }
}

extension LoginInfluencerVC{

func callWebserviceForLogin(){
    objWebServiceManager.showIndicator()
    self.view.endEditing(true)
    var strLoginType = ""
    if (txtEmail.text?.contains("@"))!{
        strLoginType = "email"
    }else{
        strLoginType = "phone"
    }
    
    let param = [WsParam.Login_identifier:self.txtEmail.text ?? "",
                 WsParam.Password:self.txtPassword.text ?? "",
                 WsParam.deviceToken:objAppShareData.strDeviceToken,
                 WsParam.Dial_code:"",
                 WsParam.Login_type:strLoginType,
                 WsParam.User_type:"influencer"
        ] as [String : Any]
    
    print(param)
    
    objWebServiceManager.requestPost(strURL: WsUrl.Login, params: param, strCustomValidation: "", showIndicator: false, success: {response in
        print(response)
        let status = (response["status"] as? String)
        let message = (response["message"] as? String)
        if status == k_success{
            objWebServiceManager.hideIndicator()
            if let data = response["data"]as? [String:Any]{
                
                if status == "success" ||  status == "Success"
                {
                    let dic  = response["data"] as? [String:Any]
                    
                    let user_details  = dic!["user_details"] as? [String:Any]
                    self.saveDataInDeviceLogin(dict:user_details!)
                    objAppShareData.SaveUpdateUserInfoFromAppshareData(userDetail: user_details ?? [:])
                    objAppShareData.fetchUserInfoFromAppshareData()
                   
                    objAppDelegate.showInfluencerTabbar()
                    
                }else{
                    objWebServiceManager.hideIndicator()
                    objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                }
            }
        }
        else{
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
        }
    }, failure: { (error) in
        print(error)
        objWebServiceManager.hideIndicator()
        objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
    })
}
    
    func saveDataInDeviceLogin(dict : [String:Any])
       {
           UserDefaults.standard.setValue(dict["auth_token"], forKey:"Authtoken")
           UserDefaults.standard.setValue(dict["user_type"], forKey: UserDefaults.InfulenceKeys.user_type)
         UserDefaults.standard.setValue(dict["avatar"], forKey: UserDefaults.InfulenceKeys.InfulencerProfileImage)
           UserDefaults.standard.setValue(dict["name"], forKey: UserDefaults.InfulenceKeys.InfulencerName)
          
       }
    
    func isRememberData(){
        let ud = UserDefaults.standard
        
        print(ud.value(forKey:"AlwasysEmailInfu") as? String ?? "")
        self.txtEmail.text = ud.value(forKey:"AlwasysEmailInfu") as? String ?? ""
        self.txtPassword.text = ud.value(forKey:"AlwaysPasswordInfu") as? String ?? ""
        //     let value = ud.value(forKey: "Alwayssign") as? String ?? ""
        
        if self.txtEmail.text != ""{
            self.imgCheck.image = #imageLiteral(resourceName: "active_check_ico")
        }else{
            self.imgCheck.image = #imageLiteral(resourceName: "inactive_check_ico")
        }
    }


}


