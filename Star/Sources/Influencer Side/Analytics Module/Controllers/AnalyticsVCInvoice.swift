//
//  AnalyticsVC2.swift
//  Star
//
//  Created by IOS-Sagar on 18/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class AnalyticsVCInvoice: UIViewController {
    
    //MARK: Outlet
    
    
      @IBOutlet weak var viewCalender: UIView!
      @IBOutlet weak var viewDollerImage: UIView!
      @IBOutlet weak var viewDashedLine: UIView!
      @IBOutlet weak var viewRevenueInvoice: UIView!
      @IBOutlet weak var lblLineRevenue: UILabel!
      @IBOutlet weak var lblLineInvoice: UILabel!
      @IBOutlet weak var btnInvoice: UIButton!
      @IBOutlet weak var btnRevenue: UIButton!
    
    //MARK: All months label outlet
    
    @IBOutlet weak var lblJanuary: UILabel!
    @IBOutlet weak var lblFebruary: UILabel!
    @IBOutlet weak var lblMarch: UILabel!
    @IBOutlet weak var lblApril: UILabel!
    @IBOutlet weak var lblMay: UILabel!
    @IBOutlet weak var lblJun: UILabel!
    @IBOutlet weak var lblJuly: UILabel!
    @IBOutlet weak var lblAugust: UILabel!
    @IBOutlet weak var lblSeptember: UILabel!
    @IBOutlet weak var lblOctober: UILabel!
    @IBOutlet weak var lblNovember: UILabel!
    @IBOutlet weak var lblDecember: UILabel!
    
    
        override func viewDidLoad() {
            super.viewDidLoad()

            self.creatDashedLine()
            self.viewRevenueInvoice.setviewBottomShadowLight()
            self.viewCalender.setviewbottomShadow()
            self.viewDollerImage.setviewShadowLightDollerView()
    }
    
    
    
    //MARK: UIButton Action Method
    
        @IBAction func btnActionRevenue(_ sender: Any)
        {
    //        self.lblPending.isSelected = true
    //        self.lblCompleted.isSelected = false
           
//            self.lblLineRevenue.isHidden = false
//            self.lblLineInvoice.isHidden = true
//           btnInvoice.setTitleColor(UIColor(named: "Text_Color"), for: .normal)
//            btnRevenue.setTitleColor(UIColor(named: "Total_Balance_Color"), for: .normal)
            self.navigationController?.popViewController(animated: false)
            
        }
        @IBAction func btnActionInvoice(_ sender: Any)
        {
    //        self.lblPending.isSelected = false
    //        self.lblCompleted.isSelected = true
                
//            self.lblLineRevenue.isHidden = true
//            self.lblLineInvoice.isHidden = false
          
//            self.btnInvoice.setTitleColor(UIColor(named: "Total_Balance_Color"), for: .normal)
//            self.btnRevenue.setTitleColor(UIColor(named: "Text_Color"), for: .normal)
            
            
}
    

   
    
    //MARK: select month action method
    
  @IBAction func btnActionSelectMonth(_ sender:UIButton)
             {
                switch sender.tag {
                case 1:
                    self.setSelectedMonthBG(selectedLbl: self.lblJanuary)
                case 2:
                     self.setSelectedMonthBG(selectedLbl: self.lblFebruary)
                case 3:
                     self.setSelectedMonthBG(selectedLbl: self.lblMarch)
                case 4:
                     self.setSelectedMonthBG(selectedLbl: self.lblApril)
                case 5:
                     self.setSelectedMonthBG(selectedLbl: self.lblMay)
                case 6:
                    self.setSelectedMonthBG(selectedLbl: self.lblJun)
                    case 7:
                        self.setSelectedMonthBG(selectedLbl: self.lblJuly)
                    case 8:
                         self.setSelectedMonthBG(selectedLbl: self.lblAugust)
                    case 9:
                         self.setSelectedMonthBG(selectedLbl: self.lblSeptember)
                    case 10:
                         self.setSelectedMonthBG(selectedLbl: self.lblOctober)
                    case 11:
                         self.setSelectedMonthBG(selectedLbl: self.lblNovember)
                    case 12:
                        self.setSelectedMonthBG(selectedLbl: self.lblDecember)
                default:
                    print("")
                }
             }
    
    
    func setSelectedMonthBG(selectedLbl:UILabel)
        {
            self.lblJanuary.backgroundColor = .clear
            self.lblJanuary.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblFebruary.backgroundColor = .clear
            self.lblFebruary.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblMarch.backgroundColor = .clear
            self.lblMarch.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblApril.backgroundColor = .clear
            self.lblApril.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblMay.backgroundColor = .clear
            self.lblMay.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblJun.backgroundColor = .clear
            self.lblJun.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblJuly.backgroundColor = .clear
            self.lblJuly.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblAugust.backgroundColor = .clear
            self.lblAugust.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblSeptember.backgroundColor = .clear
            self.lblSeptember.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblOctober.backgroundColor = .clear
            self.lblOctober.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblNovember.backgroundColor = .clear
            self.lblNovember.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblDecember.backgroundColor = .clear
            self.lblDecember.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            
            selectedLbl.backgroundColor = UIColor(named:"AnalyticInvoiceSelctedMonthBGClr")?.withAlphaComponent(0.2)
            selectedLbl.textColor = UIColor(named: "AnalyticInvoiceSelctedMonthBGClr")
        }

    
    
    
    //MARK: Create Dash Line
func creatDashedLine()
    {
    let topPoint = CGPoint(x: self.viewDashedLine.bounds.minX, y: self.viewDashedLine.bounds.maxY)
           let bottomPoint = CGPoint(x: self.viewDashedLine.bounds.maxX, y: self.viewDashedLine.bounds.maxY)
self.viewDashedLine.createDashedLine(from: topPoint, to: bottomPoint, color: .lightGray, strokeLength: 8, gapLength: 4, width: 0.5)

    }
}
