//
//  AnalyticsVC.swift
//  Star
//
//  Created by IOS-Sagar on 17/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class AnalyticsVC: UIViewController {
    
  //MARK: Outlet
    
    @IBOutlet weak var btnCustom: UIButton!
    @IBOutlet weak var btnThirtyDays: UIButton!
    @IBOutlet weak var btnSevenDays: UIButton!
    @IBOutlet weak var btnOneDay: UIButton!
    @IBOutlet weak var lineCustom: UILabel!
    @IBOutlet weak var line30Days: UILabel!
    @IBOutlet weak var imageViewGreenArrow: UIImageView!
   
    
    @IBOutlet weak var lblNumbers: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lineSevenDays: UILabel!
    @IBOutlet weak var lineOneDay: UILabel!
    @IBOutlet weak var lblLineRevenue: UILabel!
    @IBOutlet weak var lblLineInvoice: UILabel!
    @IBOutlet weak var btnInvoice: UIButton!
    @IBOutlet weak var btnRevenue: UIButton!
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var viewDuration: UIView!
    @IBOutlet weak var viewRevenueInvoice: UIView!
    @IBOutlet weak var viewAge: UIView!
    @IBOutlet weak var viewGender: UIView!
    @IBOutlet weak var viewContainerStarImage: UIView!
    @IBOutlet weak var viewStarImage: UIView!
    
    //MARK: lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()


        setInitialView()
      
       
    }
   
    
   
    @IBAction func btnActionRevenue(_ sender: Any)
        {
           

            
        }
        @IBAction func btnActionInvoice(_ sender: Any)
        {
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "AnalyticsVCInvoice") as! AnalyticsVCInvoice
            self.navigationController?.pushViewController(vc, animated: false)
            
        }

    @IBAction func btnActionOneDay(_ sender: Any)
    {
        self.lineOneDay.isHidden = false
        self.line30Days.isHidden = true
        self.lineCustom.isHidden = true
        self.lineSevenDays.isHidden = true
        self.setSelectedButtonColor(selectedButton: self.btnOneDay)
    }
    @IBAction func btnActionSevenDay(_ sender: Any) {
        self.lineOneDay.isHidden = true
        self.line30Days.isHidden = true
        self.lineCustom.isHidden = true
        self.lineSevenDays.isHidden = false
        self.setSelectedButtonColor(selectedButton: self.btnSevenDays)
    }
    
    @IBAction func btnAction30Day(_ sender: Any) {
        self.lineOneDay.isHidden = true
        self.line30Days.isHidden = false
        self.lineCustom.isHidden = true
        self.lineSevenDays.isHidden = true
        self.setSelectedButtonColor(selectedButton: self.btnThirtyDays)
    }
    @IBAction func btnActionCustom(_ sender: Any) {
        self.lineOneDay.isHidden = true
        self.line30Days.isHidden = true
        self.lineCustom.isHidden = false
        self.lineSevenDays.isHidden = true
        self.setSelectedButtonColor(selectedButton: self.btnCustom)
    }
    
   
    
    
    @IBAction func btnActionPercentage(_ sender: Any)
    {
        
        lblNumbers.textColor = UIColor(named: "Text_Color")
        lblPercentage.textColor = UIColor(named: "Total_Balance_Color")
    }
    
    @IBAction func btnActionNumbers(_ sender: Any)
    {
        lblNumbers.textColor = UIColor(named: "Total_Balance_Color")
        lblPercentage.textColor = UIColor(named: "Text_Color")
    }
    
    //MARK: Custom Method
    
    func setSelectedButtonColor(selectedButton:UIButton)
    {
        self.btnOneDay.setTitleColor(UIColor(named: "Text_Color"), for: .normal)
        self.btnSevenDays.setTitleColor(UIColor(named: "Text_Color"), for: .normal)
        self.btnThirtyDays.setTitleColor(UIColor(named: "Text_Color"), for: .normal)
        self.btnCustom.setTitleColor(UIColor(named: "Text_Color"), for: .normal)
        selectedButton.setTitleColor(UIColor(named: "Total_Balance_Color"), for: .normal)

    }
    
    func setInitialView()
    {
        self.viewCountry.layer.cornerRadius = self.viewCountry.bounds.height/2
        self.viewGender.layer.cornerRadius = self.viewGender.bounds.height/2
        self.viewAge.layer.cornerRadius = self.viewAge.bounds.height/2
        self.viewDuration.setviewBottomShadowLight()
        self.viewRevenueInvoice.setviewBottomShadowLight()
        
        self.viewContainerStarImage.layer.cornerRadius = 10
        self.viewStarImage.layer.cornerRadius = self.viewStarImage.bounds.width/2
    }
    
    
}
