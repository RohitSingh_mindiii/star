//
//  CallsTableCell.swift
//  Star
//
//  Created by IOS-Sagar on 17/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class CallsTableCell: UITableViewCell {

    @IBOutlet weak var imgViewStarIcon: UIImageView!
    @IBOutlet weak var imgViewCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewImgProfile: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgUserFlag: UIImageView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var imgAudioVideo: UIImageView!
     @IBOutlet weak var viewVoice: UIView!
     @IBOutlet weak var viewVideo: UIView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      ///  self.imgViewCenterConstraint.constant = -10
        self.imgProfile.setImgCircle()
        self.imgUserFlag.setImgCircle()
        self.viewImgProfile.setviewCirclewhite()
        self.viewImgProfile.setshadowViewCircle2()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
