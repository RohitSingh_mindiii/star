//
//  InfluencerCallDetails_VC.swift
//  Star
//
//  Created by RohitSingh on 26/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import MobileCoreServices
import Photos
import AVKit

class InfluencerCallDetails_VC: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet var vwHeader: UIView!
    @IBOutlet var lblUserNameHeader: UILabel!
    @IBOutlet var imgVwUserHeader: UIImageView!
    @IBOutlet var btnFollowUnFollowHeader: UIButton!
    @IBOutlet var vwContainer: UIView!
    @IBOutlet var stackVw: UIStackView!
    @IBOutlet var vwContainTutorial: UIView!
    @IBOutlet var vwContainInfluencerAudio: UIView!
    @IBOutlet var vwContainInfluencerVideo: UIView!
    @IBOutlet var vwContainUserAudio: UIView!
    @IBOutlet var vwContainUserVideo: UIView!
    @IBOutlet var imgVwUserAudio: UIImageView!
    @IBOutlet var imgVwUserProfileVideo: UIImageView!
    @IBOutlet var imgVwUserTutorial: UIImageView!
    @IBOutlet var imgVwVideoTutorial: UIImageView!
    @IBOutlet var imgVwInfluencerVideo: UIImageView!
    @IBOutlet var imgVwUserVideo: UIImageView!
    @IBOutlet var vwConainUserAudioShadow: UIView!
    @IBOutlet var vwConatinInfluencerAudioShadow: UIView!
    @IBOutlet var vwContainBtnBottom: UIView!
    @IBOutlet var imgVwBottomAudioVideo: UIImageView!
    @IBOutlet var btnAudioVideoBottom: UIButton!
    @IBOutlet var btnPlayTutorialVideo: UIButton!
    @IBOutlet var btnPlayUserVideo: UIButton!
    @IBOutlet var btnPlayInfluencerVideo: UIButton!
    @IBOutlet var imgVwPlayStopUser: UIImageView!
    @IBOutlet var imgVwPlayStopInfluencer: UIImageView!
    @IBOutlet var imgVwReadUnreadAudio: UIImageView!
    @IBOutlet var imgVwReadUnreadVideo: UIImageView!
    
    //SubVwAudioPlayerOutlets
    @IBOutlet var vwContainerAudio: UIView!
    @IBOutlet var vwContainerInsiderAudio: UIView!
    @IBOutlet var imgVwCancelLeftSide: UIImageView!
    @IBOutlet var lblRecordingHeader: UILabel!
    @IBOutlet var btnLeftSideCancel: UIButton!
    @IBOutlet var lblDoneRightSide: UILabel!
    @IBOutlet var imgVwCancelRightSide: UIImageView!
    @IBOutlet var btnCancelRightSideTop: UIButton!
    @IBOutlet var imgVwWaveFormGif: UIImageView!
    @IBOutlet var lblTimeAudio: UILabel!
    @IBOutlet var stackVwAudio: UIStackView!
    @IBOutlet var vwContainRetakeBtn: UIView!
    @IBOutlet var vwRetakeBtnInside: UIView!
    @IBOutlet var btnRetake: UIButton!
    @IBOutlet var vwContainPlayBtn: UIView!
    @IBOutlet var vwContainPlayInside: UIView!
    @IBOutlet var btnPlayAudioSubVw: UIButton!
    
    
    //MARK:- Variables
    var objCallList:CallListModel?
    var strInfluencerAudioUrl = ""
    var strUserAudioUrl = ""
    var strInfluencerVideoUrl = ""
    var strUserVideoUrl = ""
    var strTutorialVideoUrl = ""
    var strCallType = ""
    var isAudioReceive = ""
    var mediaData:Data?
    //Video record variables
    open var bitRate = 192000
    open var sampleRate = 44100.0
    open var channels = 1
    var imagePicker = UIImagePickerController()
    var player : AVAudioPlayer?
    var videoURL : URL?
    var compressedURL : URL?
    var pickedImage:UIImage?
    var pickedVideoThumbData:Data?
    var pickedVideoData:Data?
    var pickedVideo = ""
    //Audo Variables
    var isRecording = false
    var isPlaying = false
    var displayLink:CADisplayLink!
    var audioData = Data()
    var counter = 15
    var timer = Timer()
    var playBackTimer = Timer()
       var playBackTime = Int()
       var playBacktimeCount = 0
    var isAudioRecordingGranted:Bool = false
    var recorder: AVAudioRecorder?
    var avPlayer : AVAudioPlayer!
    var recordingSession: AVAudioSession!
    var isComingFromCrossSubVw:Bool?
    var audioUrl:URL?
    var callID = ""
    var isComingFrom = ""
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setUserDataAndStyling()
        setviewbottomShadow()
        
        if objCallList != nil{
            if objCallList?.strCallID != nil{
                call_WsGetDetails(strCallDetailID: objCallList!.strCallID)
            }
        }else{
            if self.callID != ""{
                call_WsGetDetails(strCallDetailID: self.callID)
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.imgVwPlayStopUser.image = #imageLiteral(resourceName: "audio_play_ico")
        self.imgVwPlayStopInfluencer.image = #imageLiteral(resourceName: "audio_play_ico")
    }
    
    func setviewbottomShadow(){
        self.vwHeader.layer.shadowColor = UIColor(named: "Shadow")?.cgColor
        self.vwHeader.layer.masksToBounds = false
        self.vwHeader.layer.shadowOffset = CGSize(width: 0.0 , height: 5.0)
        self.vwHeader.layer.shadowOpacity = 0.2
        self.vwHeader.layer.shadowRadius = 4
    }
    
    
    func setUserDataAndStyling(){
        self.imgVwUserVideo.setCornerRadiusWithBoarder(color: .clear, cornerRadious: 7, photoImageView: self.imgVwUserVideo)
        self.imgVwInfluencerVideo.setCornerRadiusWithBoarder(color: .clear, cornerRadious: 7, photoImageView: self.imgVwInfluencerVideo)
        self.imgVwVideoTutorial.setCornerRadiusWithBoarder(color: .clear, cornerRadious: 7, photoImageView: self.imgVwVideoTutorial)
        self.vwConainUserAudioShadow.setViewShadowAndLessCorner()
        self.vwConatinInfluencerAudioShadow.setViewShadowAndLessCorner()
        self.imgVwUserHeader.setImgCircle()
        self.imgVwUserTutorial.setImgCircle()
        self.imgVwUserAudio.setImgCircle()
        self.imgVwUserProfileVideo.setImgCircle()
        self.vwContainUserVideo.isHidden = true
        self.vwContainInfluencerVideo.isHidden = true
        self.vwContainUserAudio.isHidden = true
        self.vwContainInfluencerAudio.isHidden = true
        
        self.btnPlayTutorialVideo.setImage(#imageLiteral(resourceName: "video_ico"), for: .normal)
        self.btnPlayUserVideo.setImage(#imageLiteral(resourceName: "video_ico"), for: .normal)
        self.btnPlayInfluencerVideo.setImage(#imageLiteral(resourceName: "video_ico"), for: .normal)
        
        //SubVw
        self.vwContainerAudio.isHidden = true
        self.vwContainPlayBtn.isHidden = true
        self.vwContainerInsiderAudio.setViewRadius()
        self.btnRetake.setTitle("RECORD", for: .normal)
        self.btnPlayAudioSubVw.setTitle("PLAY", for: .normal)
        self.vwContainPlayInside.setCornerRadius(radius: 3)
        self.vwRetakeBtnInside.setCornerRadius(radius: 3)
        self.lblDoneRightSide.isHidden = true
        self.imgVwCancelRightSide.isHidden = true
        self.btnCancelRightSideTop.isUserInteractionEnabled = false
        
        
        self.imgVwReadUnreadAudio.isHidden = true
        self.imgVwReadUnreadVideo.isHidden = true
        
        //UserData
//        if self.isComingFrom == "Notification"{
//
//        }else{
//            self.lblUserNameHeader.text = objCallList?.strUserName
//            if objCallList?.strUserImage != "" {
//                let url = URL(string: objCallList!.strUserImage)
//                self.imgVwUserHeader.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
//                self.imgVwUserAudio.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
//                self.imgVwUserProfileVideo.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
//            }else{
//                self.imgVwUserHeader.image = #imageLiteral(resourceName: "placeholder_img")
//            }
//        }
        
    }
    
    
    //MARK:- IBButton Action
    @IBAction func btnBackOnHeader(_ sender: Any) {
        self.isComingFrom = ""
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOnFollowUnfollow(_ sender: Any) {
        
    }
    
    @IBAction func btnPlayVideo(_ sender: AnyObject) {
        
        var strVideoUrl = ""
        
        switch sender.tag {
        case 0:
            strVideoUrl = strTutorialVideoUrl
        case 1:
            strVideoUrl = strUserVideoUrl
        case 2:
            strVideoUrl = strInfluencerVideoUrl
        default:
            break
        }
        
        if strVideoUrl != ""{
            if let videoURL = URL(string:strVideoUrl){
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }else{
                // AppSharedClass.shared.showAlert(title: "Alert", message: "Something went wrong!")
            }
        }
        
    }
    
    
    @IBAction func btnOnPlayInfluencerAudio(_ sender: Any) {
      //  print(strInfluencerAudioUrl)
        
        if self.imgVwPlayStopUser.image == #imageLiteral(resourceName: "audio_stop_ico"){
            self.player?.stop()
            self.imgVwPlayStopUser.image = #imageLiteral(resourceName: "audio_play_ico")
        }
        
        if self.player?.isPlaying == true{
            self.imgVwPlayStopInfluencer.image = #imageLiteral(resourceName: "audio_play_ico")
            self.player?.stop()
        }else{
            self.imgVwPlayStopInfluencer.image = #imageLiteral(resourceName: "audio_stop_ico")
            let url:URL? = URL(fileURLWithPath: strInfluencerAudioUrl)
            if url != nil{
                playMedia(strDestinationUrl: url!)
            }
        }
        
    }
    @IBAction func btnOnPlayUserAudio(_ sender: Any) {
        
        
        if self.imgVwPlayStopInfluencer.image == #imageLiteral(resourceName: "audio_stop_ico"){
            self.player?.stop()
            self.imgVwPlayStopInfluencer.image = #imageLiteral(resourceName: "audio_play_ico")
        }
        
        
        if self.player?.isPlaying == true{
            self.player?.stop()
            self.imgVwPlayStopUser.image = #imageLiteral(resourceName: "audio_play_ico")
        }else{
          self.imgVwPlayStopUser.image = #imageLiteral(resourceName: "audio_stop_ico")
            let url:URL? = URL(fileURLWithPath: strUserAudioUrl)
            if url != nil{
                // playAudio(strAudioUrl: url!)
              //  play(url: url! as NSURL)
                playMedia(strDestinationUrl: url!)
            }
        }
        
      
       // print(strUserAudioUrl)
        
      
        
    }
    
    func play(url:NSURL) {
        print("playing \(url)")
        
        do {
            self.player = try AVAudioPlayer(contentsOf: url as URL)
            player?.prepareToPlay()
            player?.volume = 1.0
            player?.play()
        } catch let error as NSError {
            //self.player = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
        
    }
    
    @IBAction func btnOnAudioVideoBottom(_ sender: Any) {
        check_record_permission()
        if self.isAudioReceive == "1"{
            if self.isAudioRecordingGranted{
                self.vwContainerAudio.isHidden = false
            }
        }else{
            checkCameraPermissions(handler: { [weak self](isGranted) in
                guard self != nil else{return}
                if isGranted{
                    //pickVideo()
                    gotoCameraVC()
                }
            })
        }
        
    }
    
    //MARK:- SubVwButton Action
    @IBAction func btnOnCloseLeftSideSubVw(_ sender: Any) {
         self.btnPlayAudioSubVw.isUserInteractionEnabled = true
        playBacktimeCount = 0
               playBackTimer.invalidate()
        self.imgVwWaveFormGif.image = nil
        self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
        self.player?.stop()
        self.vwContainerAudio.isHidden = true
        self.vwContainPlayBtn.isHidden = true
        self.vwContainInfluencerAudio.isHidden = true
        self.btnRetake.setTitle("RECORD", for: .normal)
        isComingFromCrossSubVw = true
        isRecording = false
        self.StopRecordingAndNavigation()
        self.timer.invalidate()
        self.lblTimeAudio.text = "00:00"
        self.lblDoneRightSide.isHidden = true
        self.vwContainPlayBtn.isHidden = true
    }
    @IBAction func btnOnCloseRightSideSubVw(_ sender: Any) {
         self.btnPlayAudioSubVw.isUserInteractionEnabled = true
        playBacktimeCount = 0
               playBackTimer.invalidate()
        self.imgVwWaveFormGif.image = nil
        self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
        self.player?.stop()
        self.vwContainerAudio.isHidden = true
        self.vwContainPlayBtn.isHidden = true
        self.isComingFromCrossSubVw = false
        self.btnRetake.setTitle("RECORD", for: .normal)
        self.timer.invalidate()
        self.lblTimeAudio.text = "00:00"
        self.vwContainPlayBtn.isHidden = true
        self.strCallType = "1"
        if self.objCallList?.strCallID != nil{
            self.call_WsCallReply(strCallID: self.objCallList!.strCallID)
        }else{
            self.call_WsCallReply(strCallID: self.callID)
        }
    }
    
    @IBAction func btnOnRetakeStartStop(_ sender: Any) {
        
        if self.btnRetake.currentTitle == "RECORD"{
            playBacktimeCount = 0
                   playBackTimer.invalidate()
            self.imgVwWaveFormGif.image = nil
            self.imgVwWaveFormGif.image = UIImage.gifImageWithName("audio")
            self.isComingFromCrossSubVw = false
            self.btnRetake.setTitle("STOP", for: .normal)
            self.vwContainPlayBtn.isHidden = true
            self.btnCancelRightSideTop.isUserInteractionEnabled = false
            if(isRecording)
            {
                isRecording = false
            }
            else
            {
                //self.imgVwWaveGIF.image = UIImage.gifImageWithName("sound_img")
                setup_recorder()
                counter = 15
                timer.invalidate()
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
                isRecording = true
            }
        }else if self.btnRetake.currentTitle == "STOP"{
            playBacktimeCount = 0
                   playBackTimer.invalidate()
            self.imgVwWaveFormGif.image = nil
            self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
            self.isComingFromCrossSubVw = false
            self.btnRetake.setTitle("RETAKE", for: .normal)
            self.StopRecordingAndNavigation()
            self.lblDoneRightSide.isHidden = false
            self.imgVwCancelRightSide.isHidden = true
            self.vwContainPlayBtn.isHidden = false
            self.btnCancelRightSideTop.isUserInteractionEnabled = true
            
        }else if self.btnRetake.currentTitle == "RETAKE"{
             self.btnPlayAudioSubVw.isUserInteractionEnabled = true
            playBacktimeCount = 0
                   playBackTimer.invalidate()
            self.imgVwWaveFormGif.image = nil
            self.imgVwWaveFormGif.image = UIImage.gifImageWithName("audio")
            self.player?.stop()
            self.isComingFromCrossSubVw = false
            self.btnCancelRightSideTop.isUserInteractionEnabled = false
            self.lblDoneRightSide.isHidden = true
            self.btnRetake.setTitle("STOP", for: .normal)
            self.vwContainPlayBtn.isHidden = true
            setup_recorder()
            counter = 15
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
            isRecording = true
        }
        
        
    }
    @IBAction func btnOnPlaySubVwAudio(_ sender: Any) {
        if self.audioUrl != nil{
            self.imgVwWaveFormGif.image = UIImage.gifImageWithName("audio")
            self.btnPlayAudioSubVw.isUserInteractionEnabled = false
                playBackTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(playBackTimerAction), userInfo: nil, repeats: true)
            
            playAudio(strAudioUrl: self.audioUrl!)
        }else{
            print("Url Not Found")
        }
        
        
    }
    
    //MARK:- Play Audio
    func playAudio(strAudioUrl:URL){
        
        //12000
        _ = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 44100.0,
            AVNumberOfChannelsKey: 1,
            AVEncoderBitRateKey: 192000,
            
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        let settings: [String: AnyObject] = [
            AVFormatIDKey : NSNumber(value: Int32(kAudioFormatAppleLossless) as Int32),
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue as AnyObject,
            AVEncoderBitRateKey: bitRate as AnyObject,
            AVNumberOfChannelsKey: channels as AnyObject,
            AVSampleRateKey: sampleRate as AnyObject
        ]
        do
        {
            self.player = try AVAudioPlayer(contentsOf: strAudioUrl as URL, fileTypeHint: AVFileType.mp3.rawValue)
            //self.player = try AVAudioPlayer(url: self.strAudioUrl as URL, settings: settings)
            
        }catch let error{
            print(error.localizedDescription)
        }
        
        
        do {
            
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            
            
        } catch let error{
            print(error.localizedDescription)
        }
        self.player?.delegate = self
        self.player?.prepareToPlay()
        self.player?.isMeteringEnabled = true
        self.player?.play()
        self.player?.volume = 1
        
        // displayLink = CADisplayLink(target: self, selector: #selector(updateMetersPlay))
        //displayLink.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
    }
    
}


extension InfluencerCallDetails_VC{
    //Check Permission
    func checkCameraPermissions(handler:(Bool)->Void) {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized:
            print("Allowed")
            handler(true)
        case .denied:
            handler(false)
            alertPromptToAllowCameraAccessViaSetting(strTitle:"Camera access required",strMessage: "Camera access is disabled please allow from Settings.")
        default:
            print("Allowed")
            handler(true)
        }
    }
    
    //Default PopUp
    func alertPromptToAllowCameraAccessViaSetting(strTitle:String,strMessage:String) {
        
        let alert = UIAlertController(title: strTitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel) { (alert) -> Void in
            
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        })
        
        self.present(alert, animated: true, completion: nil)
    }
}
//MARK:- Video Record Work
extension InfluencerCallDetails_VC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    //MARK:- Pick video
    func gotoCameraVC() {
        
        let vc = UIStoryboard.init(name: "Discover", bundle: Bundle.main).instantiateViewController(withIdentifier: "CameraVC")as! CameraVC
        
        vc.closerForDictMedia = { dict

        in
            print(dict)
            if dict.count != 0{
                self.mediaData = dict["mediaData"]as! Data
                self.pickedVideoThumbData = (dict["videoThumb"]as! Data)
                let videoUrl = dict["videoUrl"]as! URL
                self.strInfluencerVideoUrl = "\(String(describing: videoUrl))"
                self.generateImage(videoUrl)
                self.vwContainInfluencerVideo.isHidden = false
                self.vwContainInfluencerAudio.isHidden = true
                self.strCallType = "2"
                if self.objCallList?.strCallID != ""{
                    self.call_WsCallReply(strCallID: self.objCallList!.strCallID)
                }
            }
            
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    func pickVideo(){
        objAppShareData.openImagePickerForVideo(controller: self, success: { (imageInfo) in
            if var videoUrl = imageInfo["imageUrl"] as? URL{
                let strUrl = "\(videoUrl)"
                
                self.imagePicker.videoQuality = UIImagePickerController.QualityType.typeHigh
                self.imagePicker.videoQuality = .typeHigh
                self.imagePicker.delegate = self
                self.imagePicker.videoMaximumDuration = 5.0
                self.imagePicker.allowsEditing = false
                
                self.encodeVideo(videoUrl: videoUrl) { (convertedUrl) in
                    videoUrl = convertedUrl!
                    print("MP4---VideoUrl>>>>>\(videoUrl)")
                    let data = NSData(contentsOf: videoUrl as URL)!
                    self.mediaData = data as Data
                    print("MediaData>>>>",self.mediaData)
                }
                
                self.vwContainInfluencerVideo.isHidden = false
                self.vwContainInfluencerAudio.isHidden = true
                //  self.btnPlayMyVideo.isHidden = true
                self.strCallType = "2"
                let thumbData = self.generateThumbImageData(videoUrl)
                print("Thumb Data>>>>>\(thumbData)")
                self.pickedVideoThumbData = thumbData
                self.compressedURL = videoUrl
                self.generateImage(videoUrl)
                self.strInfluencerVideoUrl = "\(videoUrl)"
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                    if self.objCallList?.strCallID != ""{
                        self.call_WsCallReply(strCallID: self.objCallList!.strCallID)
                    }
                })
                
            }
        }){ (Error) in
            print("Error Occured")
        }
        
    }
    */
    
    
    //MARK:- Generate video thumbnail image
    func generateImage(_ URL: URL) -> UIImage {
        var image = UIImage()
        let asset = AVAsset.init(url: URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true;
        
        let maxSize = CGSize(width: 400, height: 400)
        assetImageGenerator.maximumSize = maxSize
        var time = asset.duration
        time.value = min(time.value, 2)
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            image =  UIImage.init(cgImage: imageRef)
            DispatchQueue.main.async {
                self.imgVwInfluencerVideo.image = image
                //self.pickedImage = image
            }
            
            return image
        } catch {
        }
        return image
    }
    
    func generateThumbImageData(_ URL: URL) -> Data {
        var data:Data?
        let asset = AVAsset.init(url: URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true;
        
        let maxSize = CGSize(width: 300, height: 350)
        assetImageGenerator.maximumSize = maxSize
        var time = asset.duration
        time.value = min(time.value, 2)
        do {
            
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            let image =  UIImage.init(cgImage: imageRef)
            data = image.pngData()
            return data!
        } catch {
        }
        return data!
    }
    
    
    
    func encodeVideo(videoUrl: URL, outputUrl: URL? = nil, resultClosure: @escaping (URL?) -> Void ) {
        
        var finalOutputUrl: URL? = outputUrl
        
        if finalOutputUrl == nil {
            var url = videoUrl
            url.deletePathExtension()
            url.appendPathExtension("mp4")
            finalOutputUrl = url
        }
        
        if FileManager.default.fileExists(atPath: finalOutputUrl!.path) {
            print("Converted file already exists \(finalOutputUrl!.path)")
            resultClosure(finalOutputUrl)
            return
        }
        
        let asset = AVURLAsset(url: videoUrl)
        if let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetPassthrough) {
            exportSession.outputURL = finalOutputUrl!
            exportSession.outputFileType = AVFileType.mp4
            let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
            let range = CMTimeRangeMake(start: start, duration: asset.duration)
            exportSession.timeRange = range
            exportSession.shouldOptimizeForNetworkUse = true
            exportSession.exportAsynchronously() {
                
                switch exportSession.status {
                case .failed:
                    print("Export failed: \(exportSession.error != nil ? exportSession.error!.localizedDescription : "No Error Info")")
                case .cancelled:
                    print("Export canceled")
                case .completed:
                    resultClosure(finalOutputUrl!)
                default:
                    break
                }
            }
        } else {
            resultClosure(nil)
        }
    }
    
}

//MARK:-  AudioRecording Work
extension InfluencerCallDetails_VC: AVAudioPlayerDelegate,AVAudioRecorderDelegate{
    
    func check_record_permission()
    {
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSessionRecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSessionRecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (allowed) in
                if allowed {
                    self.isAudioRecordingGranted = true
                } else {
                    self.isAudioRecordingGranted = false
                }
            })
            break
        default:
            break
        }
    }
    
    
    func setup_recorder()
    {
        
        
        if isAudioRecordingGranted
        {
            let session = AVAudioSession.sharedInstance()
            do
            {
                
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord)
                //try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.record)
                
                try session.setActive(true)
                
                let settings: [String: AnyObject] = [
                    AVFormatIDKey : NSNumber(value: Int32(kAudioFormatAppleLossless) as Int32),
                    AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue as AnyObject,
                    AVEncoderBitRateKey: bitRate as AnyObject,
                    AVNumberOfChannelsKey: channels as AnyObject,
                    AVSampleRateKey: sampleRate as AnyObject
                ]
                
                
                do{
                    
                    self.recorder = try AVAudioRecorder(url: self.getFileUrl(), settings: settings)
                    print(self.recorder)
                    
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
                
                self.recorder?.delegate = self
                self.recorder?.prepareToRecord()
                self.recorder?.isMeteringEnabled = true
                self.recorder?.record()
                
                
            }
                
            catch let error {
                //  display_alert(msg_title: "Error", msg_desc: error.localizedDescription, action_title: "OK")
            }
            
        }
        else
        {
            //  display_alert(msg_title: "Error", msg_desc: "Don't have access to use your microphone.", action_title: "OK")
        }
    }
    
    func getDocumentsDirectory() -> URL {
        
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        
        let url = NSURL(fileURLWithPath: path )
        return url as URL
        
        
    }
    
    func getFileUrl() -> URL
    {
        let filename = "myRecording.m4a"
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        self.audioUrl = filePath
        return filePath
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    @objc func timerAction() {
        print(counter)
        if counter >= 1{
            counter -= 1
            self.lblTimeAudio.text = "\(self.timeFormatted(counter))"
            
        }
        else
        {
            
            self.StopRecordingAndNavigation()
        }
    }
    
    @objc func playBackTimerAction() {
          print(playBacktimeCount)
          if playBacktimeCount < self.playBackTime{
              playBacktimeCount += 1
              self.lblTimeAudio.text = "\(self.timeFormatted(playBacktimeCount))"
              
          }
          else
          {
            self.btnPlayAudioSubVw.isUserInteractionEnabled = true
              playBacktimeCount = 0
              playBackTimer.invalidate()
              self.imgVwWaveFormGif.image = nil
              self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
          }
      }
    
    
    func StopRecordingAndNavigation()
    {
        self.timer.invalidate()
        
        //self.imgVwWaveGIF.image = gif
        //  self.imgVwWaveGIF.image = nil
        // self.imgVwWaveGIF.image = #imageLiteral(resourceName: "zic_zac-line")
        if self.isComingFromCrossSubVw == true{
            self.imgVwWaveFormGif.image = nil
            self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
            self.isComingFromCrossSubVw = false
            recorder?.stop()
            self.vwContainInfluencerAudio.isHidden = true
            self.vwContainInfluencerVideo.isHidden = true
            recorder = nil
            
        }else{
            if recorder != nil{
                
                self.imgVwWaveFormGif.image = nil
                self.imgVwWaveFormGif.image = #imageLiteral(resourceName: "zic_zac-line")
                let last2 = self.lblTimeAudio.text!.suffix(2)
                let remaining  = Int(last2) ?? 0
                let clipTime = 15 - remaining
                 self.playBackTime = clipTime
                let clip = "\(clipTime)"
                if clip.count != 2{
                    self.lblTimeAudio.text  = "00:0\(clipTime)"
                }else{
                    self.lblTimeAudio.text  = "00:\(clipTime)"
                }
                
                recorder?.stop()
                print(recorder?.url.absoluteString ?? "")
                print(recorder.self ?? "")
                print(recorder?.url ?? "")
                if FileManager.default.fileExists(atPath: recorder?.url.path ?? ""){
                    
                    if let cert = NSData(contentsOfFile: recorder?.url.path ?? "") {
                        
                        self.audioData = cert as Data
                        self.mediaData = cert as Data
                        // print(cert)
                    }
                    print("Audio Recorder finished successfully")
                    //   self.vwAudioRecorder.isHidden = true
                    self.lblTimeAudio.text = "00:00"
                    recorder = nil
                    self.strCallType = "1"
                    self.btnRetake.setTitle("RETAKE", for: .normal)
                    self.lblDoneRightSide.isHidden = false
                    self.imgVwCancelRightSide.isHidden = true
                    self.vwContainPlayBtn.isHidden = false
                    self.btnCancelRightSideTop.isUserInteractionEnabled = true
                    self.vwContainInfluencerAudio.isHidden = false
                    self.vwContainInfluencerVideo.isHidden = true
                }
                
                
            }
            timer.invalidate()
            
            if displayLink !=  nil
            {
                displayLink.invalidate()
            }
            isRecording = false
            
        }
    }
    
}

//MARK:- WEBSERVICE CALLING

extension InfluencerCallDetails_VC{
    //MARK:- Call WS Get Call Datails
    func call_WsGetDetails(strCallDetailID: String){
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        
        objWebServiceManager.requestGet(strURL: BASE_URL + "call/\(strCallDetailID)", params: nil, strCustomValidation: "", success: { (response) in
            print(response)
            objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            
            if status == "success"{
                
                if let data = response["data"]as? [String:Any]{
                    
                    if let callDetails = data["call_detail"]as? [String:Any]{
                        
                        let objData = CallDetailsModal.init(dict: callDetails)
                        
                            self.lblUserNameHeader.text = objData.strUserName
                            if objData.strUserImageUrl != "" {
                                let url = URL(string: objData.strUserImageUrl)
                                self.imgVwUserHeader.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
                                self.imgVwUserAudio.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
                                self.imgVwUserProfileVideo.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
                            }else{
                                self.imgVwUserHeader.image = #imageLiteral(resourceName: "placeholder_img")
                            }
                    
                       
                            if objData.strTutorialVideoThumb != "" || objData.strTutorialVideoThumb != "null" || objData.strTutorialVideoThumb != nil{
                                self.vwContainTutorial.isHidden = false
                                let url = URL(string: objData.strTutorialVideoThumb)
                                self.imgVwVideoTutorial.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "inactive_profile_ico"))
                                self.strTutorialVideoUrl = objData.strTutorialVideoUrl
                            }else{
                                self.vwContainTutorial.isHidden = true
                                self.imgVwVideoTutorial.image = #imageLiteral(resourceName: "image_placeholder")
                            }
                        
                        
                        
                        if objData.strCallType == "1"{
                            self.isAudioReceive = "1"
                            self.imgVwBottomAudioVideo.image = #imageLiteral(resourceName: "voice_call_ico_shadow")
                            self.vwContainUserAudio.isHidden = false
                            // self.btnPayNowUser.isHidden = true
                            self.vwContainInfluencerVideo.isHidden = true
                            self.vwContainUserVideo.isHidden = true
                            self.vwContainInfluencerAudio.isHidden = false
                            self.vwContainBtnBottom.isHidden = false
                            //  self.strInfluencerAudioUrl = objData.strInfluencer_media
                            // self.btnMyAudioPlay.isHidden = false
                            //save media in local
                            self.saveUserAudioInLoacal(strUrl: objData.strUser_media)
                            self.strUserAudioUrl = objData.strUser_media
                            
                            if objData.strInfluencer_media != ""{
                                if objData.strIsSeen == "2"{
                                    self.imgVwReadUnreadAudio.image = #imageLiteral(resourceName: "tick_ico")
                                    self.imgVwReadUnreadAudio.isHidden = false
                                    
                                }else{
                                    self.imgVwReadUnreadAudio.image = #imageLiteral(resourceName: "gray_tick_ico")
                                    self.imgVwReadUnreadAudio.isHidden = false
                                }
                                self.strInfluencerAudioUrl = objData.strInfluencer_media
                                self.saveInfluencerAudioInLoacal(strUrl: objData.strInfluencer_media)
                                self.vwContainInfluencerAudio.isHidden = false
                                self.vwContainBtnBottom.isHidden = true
                                // self.btnInfluencerAudioPlay.isHidden = false
                            }else{
                                self.vwContainInfluencerAudio.isHidden = true
                                
                            }
                            
                        }else{
                            self.isAudioReceive = "2"
                            self.imgVwBottomAudioVideo.image = #imageLiteral(resourceName: "video_call_ico_shadow")
                            // self.btnPayNowVideo.isHidden = true
                            self.vwContainUserVideo.isHidden = false
                            self.strUserVideoUrl = objData.strUser_media
                            self.vwContainBtnBottom.isHidden = false
                            if objData.strUser_video_thumb != "" {
                                let url = URL(string: objData.strUser_video_thumb)
                                self.imgVwUserVideo.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "image_placeholder"))
                            }else{
                                self.imgVwUserVideo.image = #imageLiteral(resourceName: "image_placeholder")
                            }
                            
                            if objData.strInfluencer_media != ""{
                                if objData.strIsSeen == "2"{
                                    self.imgVwReadUnreadVideo.image = #imageLiteral(resourceName: "tick_ico")
                                    self.imgVwReadUnreadVideo.isHidden = false
                                }else{
                                    self.imgVwReadUnreadVideo.image = #imageLiteral(resourceName: "gray_tick_ico")
                                    self.imgVwReadUnreadVideo.isHidden = false
                                }
                                self.strInfluencerVideoUrl = objData.strInfluencer_media
                                self.vwContainInfluencerVideo.isHidden = false
                                self.vwContainBtnBottom.isHidden = true
                                if objData.strInfluencer_video_thumb != "" {
                                    let url = URL(string: objData.strInfluencer_video_thumb)
                                    self.imgVwInfluencerVideo.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "image_placeholder"))
                                }else{
                                    self.imgVwInfluencerVideo.image = #imageLiteral(resourceName: "image_placeholder")
                                }
                                
                            }else{
                                self.vwContainInfluencerVideo.isHidden = true
                            }
                            
                            self.vwContainInfluencerAudio.isHidden = true
                            self.vwContainUserAudio.isHidden = true
                        }
                        
                        
                        
                    }else{
                        objWebServiceManager.hideIndicator()
                        objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                    }
                }else{
                    objWebServiceManager.hideIndicator()
                    objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                }
            }else{
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
            }
        }) { (Error) in
            print(Error)
        }
        
    }
    
    
    //MARK:- Reply Call
    func call_WsCallReply(strCallID:String){
        if !objWebServiceManager.isNetworkAvailable(){
            objWebServiceManager.hideIndicator()
            objAlert.showAlert(message: k_NoConnection, title: kAlertTitle , controller: self)
            return
        }
        
        objWebServiceManager.showIndicator()
        self.view.endEditing(true)
        
        var mediaType = ""
        var strMediaFileName:String?
        var strThumbFileName:String?
        var strMimeType:String?
        var imageData:Data?
        
        
        if self.strCallType == "1"{
            mediaType = "Audio"
            strMediaFileName = "influencer_media"
            strThumbFileName = nil
            strMimeType = "audio/m4a"
            imageData = nil
        }else{
            mediaType = "Video"
            strMediaFileName = "influencer_media"
            strThumbFileName = "influencer_video_thumb"
            imageData = pickedVideoThumbData
            strMimeType = "video/MOV"
        }
        print(self.strCallType)
        let param = ["call_type":self.strCallType,
                     "callId":strCallID]as [String:Any]
        
        print(param)
        
        objWebServiceManager.uploadMultipartDataAudioVideo(strURL: WsUrl.Url_CallReply, params: param, showIndicator: false, strMediaType: mediaType, imageData: imageData, mediaData: self.mediaData, mediaFileName: strMediaFileName, thumbDataFileName: strThumbFileName, mimeType: strMimeType, success: { (response) in
            objWebServiceManager.hideIndicator()
            let status = response["status"]as? String ?? ""
            let message = response["message"]as? String ?? ""
            print(response)
            if status == "success"{
                
                self.vwContainBtnBottom.isHidden = true
                if self.strCallType == "1"{
                    self.vwContainInfluencerAudio.isHidden = false
                    self.vwContainInfluencerVideo.isHidden = true
                }else{
                    self.vwContainInfluencerVideo.isHidden = false
                    self.vwContainInfluencerAudio.isHidden = true
                }
                
                
                //                if let data = response["data"]as? [String:Any]{
                //                    if let callDetailID = data["callID"]as? String{
                //                        self.call_WsGetDetails(strCallDetailID: callDetailID)
                //                    }else if let callDetailID = data["callID"]as? Int{
                //                        self.call_WsGetDetails(strCallDetailID: "\(callDetailID)")
                //                    }
                //                }else{
                //                    objWebServiceManager.hideIndicator()
                //                    objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
                //                }
            }else{
                self.vwContainInfluencerAudio.isHidden = true
                objWebServiceManager.hideIndicator()
                objAlert.showAlert(message: message , title: kAlertTitle, controller: self)
            }
        }) { (Error) in
            
            print(Error)
        }
        
        
    }
    
}

//MARK:- Audio Playback Work
extension InfluencerCallDetails_VC{
    
    func saveUserAudioInLoacal(strUrl:String){
        
        if let audioUrl = URL(string: strUrl) {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                self.strUserAudioUrl = "\(destinationUrl)"
              //  self.playMedia(strDestinationUrl: destinationUrl)
                
                // if the file doesn't exist
            } else {
                
                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        self.strUserAudioUrl = "\(destinationUrl)"
                       // self.playMedia(strDestinationUrl: destinationUrl)
                        print("File moved to documents folder")
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }).resume()
            }
        }
        
    }
    
    func saveInfluencerAudioInLoacal(strUrl:String){
           
           if let audioUrl = URL(string: strUrl) {
               
               // then lets create your document folder url
               let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
               
               // lets create your destination file url
               let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
               print(destinationUrl)
               
               // to check if it exists before downloading it
               if FileManager.default.fileExists(atPath: destinationUrl.path) {
                   print("The file already exists at path")
                   self.strInfluencerAudioUrl = "\(destinationUrl)"
                  // self.playMedia(strDestinationUrl: destinationUrl)
                   
                   // if the file doesn't exist
               } else {
                   
                   // you can use NSURLSession.sharedSession to download the data asynchronously
                   URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                       guard let location = location, error == nil else { return }
                       do {
                           // after downloading your file you need to move it to your destination url
                           try FileManager.default.moveItem(at: location, to: destinationUrl)
                          // self.playMedia(strDestinationUrl: destinationUrl)
                         self.strInfluencerAudioUrl = "\(destinationUrl)"
                           print("File moved to documents folder")
                       } catch let error as NSError {
                           print(error.localizedDescription)
                       }
                   }).resume()
               }
           }
           
       }
    
    
    func playMedia(strDestinationUrl:URL){
        //if let audioUrl = URL(string: "http://freetone.org/ring/stan/iPhone_5-Alarm.mp3") {
        if let audioUrl:URL = strDestinationUrl {
            self.player?.stop()
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            
            //let url = Bundle.main.url(forResource: destinationUrl, withExtension: "mp3")!
            
            do {
                self.player = try AVAudioPlayer(contentsOf: destinationUrl)
                guard let player = self.player else { return }
                self.player?.delegate = self
                player.prepareToPlay()
                player.play()
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.imgVwPlayStopUser.image = #imageLiteral(resourceName: "audio_play_ico")
        self.imgVwPlayStopInfluencer.image = #imageLiteral(resourceName: "audio_play_ico")
    }
    
}
