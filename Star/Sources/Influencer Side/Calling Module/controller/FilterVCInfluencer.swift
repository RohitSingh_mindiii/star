//
//  ViewController.swift
//  Design2
//
//  Created by IOS-Sagar on 30/01/20.
//  Copyright © 2020 IOS-Sagar. All rights reserved.
//

import UIKit

class FilterVCInfluencer: UIViewController, BottomSheetVCDelegate{
    //MARK: Variables
    var selectedGender:String = ""
    var selectedAge:String = ""
    //MARK: Outlet
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblAgeRightSide: UILabel!
    @IBOutlet weak var lblAgeLeftSide: UILabel!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnApplyFIlter: UIButton!
    @IBOutlet weak var txtAllCountry: UITextField!
    @IBOutlet weak var imgVwMale: UIImageView!
    
    @IBOutlet weak var imgVwFemale: UIImageView!
    @IBOutlet weak var viewSelectCountry: UIView!
    @IBOutlet weak var viewHeader: UIView!
    var arrCountry = [CommonModal]()
    var arrSelectedGender = [String]()
    var arrSelectedAgeGroup = [String]()
    var strCountry = ""
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.UiDesign()
        self.arrCountry = objAppShareData.arrCountryData
        if objAppShareData.isCallFilterApply{
            self.setFilteredDataDesign()
        }else{
            self.setDefaultDataDesign()
        }
        DispatchQueue.main.async
            {
           // self.viewHeader.setviewbottomShadow()
                self.btnReset.backgroundColor = .clear
                self.btnApplyFIlter.backgroundColor = UIColor(named: "FliterButton")
                self.btnApplyFIlter.setTitleColor(UIColor(named: "button_text"), for: .normal)
                self.btnReset.setTitleColor(UIColor(named: "BookedTableCell_SecondryTXT"), for: .normal)
            }
     }
    
    func setDefaultDataDesign(){
       ////
        self.arrSelectedGender.append("1")
        self.arrSelectedGender.append("2")
        self.lblMale.textColor = UIColor(named: "Text_Color")
        self.imgVwMale.image = UIImage(named:"active_male_ico")
        self.lblFemale.textColor = UIColor(named: "Text_Color")
        self.imgVwFemale.image = UIImage(named:"active_female_ico")
        
        self.arrSelectedAgeGroup.append("1")
        self.arrSelectedAgeGroup.append("2")
        self.lblAgeLeftSide.textColor = UIColor(named: "FilterSCRSelectedTxtClr")
        self.lblAgeLeftSide.backgroundColor = UIColor(named: "FilterSCRSelectedClr")
        self.lblAgeRightSide.textColor = UIColor(named: "FilterSCRSelectedTxtClr")
        self.lblAgeRightSide.backgroundColor = UIColor(named: "FilterSCRSelectedClr")
        ////
    }
    func setFilteredDataDesign(){
        if objAppShareData.objCallFilter.strGender.contains("1"){
            self.arrSelectedGender.append("1")
            self.lblMale.textColor = UIColor(named: "Text_Color")
            self.imgVwMale.image = UIImage(named:"active_male_ico")
        }
        if objAppShareData.objCallFilter.strGender.contains("2"){
            self.arrSelectedGender.append("2")
            self.lblFemale.textColor = UIColor(named: "Text_Color")
            self.imgVwFemale.image = UIImage(named:"active_female_ico")
        }
        if objAppShareData.objCallFilter.strAgeGroup.contains("1"){
            self.arrSelectedAgeGroup.append("1")
            self.lblAgeLeftSide.textColor = UIColor(named: "FilterSCRSelectedTxtClr")
            self.lblAgeLeftSide.backgroundColor = UIColor(named: "FilterSCRSelectedClr")
        }
        if objAppShareData.objCallFilter.strAgeGroup.contains("2"){
            self.arrSelectedAgeGroup.append("2")
            self.lblAgeRightSide.textColor = UIColor(named: "FilterSCRSelectedTxtClr")
            self.lblAgeRightSide.backgroundColor = UIColor(named: "FilterSCRSelectedClr")
        }
        let varText = objAppShareData.objCallFilter.strCountryName
        self.txtAllCountry.text = varText
        if varText.count == 0{
           self.txtAllCountry.text = "All Countries"
        }
        self.strCountry = objAppShareData.objCallFilter.strCountry
    }
    
    //MARK: UIButton Action method
    @IBAction func btnActionSelectCountry(_ sender: Any)
    {
        self.view.endEditing(true)
        let str = "Select Country"
        print("self.arrCountry count is \(self.arrCountry.count)")
               
    ////
    let arr = self.strCountry.components(separatedBy: ",")
    if arr.count>0{
        let arrInt = arr.map { Int($0) }
        if arrInt.count>0{
            let int = arrInt[0]
            if int != nil{
                objAppShareData.arrSelectedBottomIds = arrInt as! [Int]
            }
        }
    }
    let arrName = self.txtAllCountry.text!.components(separatedBy: ",")
    if arrName.count>0{
        objAppShareData.arrSelectedBottomNames = arrName
    }
    ////
        self.showBotttomSheet(arr: self.arrCountry,str: str)
   }
   
    func showBotttomSheet(arr : Array<Any>, str: String ){
       let sb = UIStoryboard.init(name: "Main", bundle:Bundle.main)
       let vc = sb.instantiateViewController(withIdentifier:"BottomSheetVC") as! BottomSheetVC

       vc.arrBottom = arr as! [CommonModal]
       vc.strHeader = str
       vc.modalPresentationStyle = .overCurrentContext
       vc.delegate = self
       self.present(vc, animated: false, completion: nil)
   }
    
    //MARK: step 6 finally use the method of the contract
    func sendDataToFirstViewController(arrId:[Int], arrName:[String]) {
            print(" my country is \(arrId)")
            print(" my Country is \(arrName)")
            let varText = arrName.joined(separator:",")
            self.txtAllCountry.text = varText
        if varText.count == 0{
           self.txtAllCountry.text = "All Countries"
        }
            self.strCountry = (arrId.map{String($0)}).joined(separator: ",")
    }
    
    @IBAction func btnActionReset(_ sender: Any) {
        
        /*btnReset.backgroundColor = UIColor(named: "FliterButton")
        btnApplyFIlter.backgroundColor = .clear
        btnApplyFIlter.setTitleColor(UIColor(named: "BookedTableCell_SecondryTXT"), for: .normal)
        btnReset.setTitleColor(UIColor(named: "button_text"), for: .normal)
        btnApplyFIlter.borderClr = .gray
        self.lblAgeRightSide.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
        self.lblAgeLeftSide.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
        self.lblMale.textColor = UIColor(named: "BookedTableCell_SecondryTXT")
        self.lblFemale.textColor = UIColor(named: "BookedTableCell_SecondryTXT")
        self.imgVwMale.image = UIImage(named:"inactive_male_ico")
        self.imgVwFemale.image = UIImage(named:"inactive_female_ico")
        self.lblAgeRightSide.backgroundColor = UIColor(named: "FilterSCRUnselectedClr")
        self.lblAgeLeftSide.backgroundColor = UIColor(named: "FilterSCRUnselectedClr")
        */
        objAppShareData.objCallFilter = CallFilterModal.init(dict: [:])
        objAppShareData.isCallFilterApply = false
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionApplyFilter(_ sender: Any) {
//        btnReset.backgroundColor = .clear
//        btnApplyFIlter.backgroundColor = UIColor(named: "FliterButton")
//        btnApplyFIlter.setTitleColor(UIColor(named: "button_text"), for: .normal)
//        btnReset.setTitleColor(UIColor(named: "BookedTableCell_SecondryTXT"), for: .normal)
        objAppShareData.objCallFilter = CallFilterModal.init(dict: [:])
        objAppShareData.objCallFilter.strAgeGroup = self.arrSelectedAgeGroup.joined(separator: ",")
        objAppShareData.objCallFilter.strGender = self.arrSelectedGender.joined(separator: ",")
        objAppShareData.objCallFilter.strCountry = self.strCountry
        objAppShareData.isCallFilterApply = true
        objAppShareData.objCallFilter.strCountryName = self.txtAllCountry.text ?? ""
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
    
    @IBAction func btnActionSelectMale(_ sender: Any) {
        //self.lblFemale.textColor = UIColor(named: "BookedTableCell_SecondryTXT")
        //self.imgVwFemale.image = UIImage(named:"inactive_female_ico")
        //self.selectedGender = "Male"
        if !self.arrSelectedGender.contains("1"){
            self.arrSelectedGender.append("1")
            self.lblMale.textColor = UIColor(named: "Text_Color")
            self.imgVwMale.image = UIImage(named:"active_male_ico")
        }else{
            if self.arrSelectedGender.count == 1{
                return
            }
            let ind = self.arrSelectedGender.firstIndex(of:"1")
            self.arrSelectedGender.remove(at: ind ?? 0)
            self.lblMale.textColor = UIColor(named: "BookedTableCell_SecondryTXT")
            self.imgVwMale.image = UIImage(named:"inactive_male_ico")
        }
    }
    
    @IBAction func btnActionSelectFemale(_ sender: Any) {
        //self.lblMale.textColor = UIColor(named: "BookedTableCell_SecondryTXT")
        //self.imgVwMale.image = UIImage(named:"inactive_male_ico")
        //self.selectedGender = "Female"
        if !self.arrSelectedGender.contains("2"){
            self.arrSelectedGender.append("2")
            self.lblFemale.textColor = UIColor(named: "Text_Color")
            self.imgVwFemale.image = UIImage(named:"active_female_ico")
        }else{
            if self.arrSelectedGender.count == 1{
                return
            }
            let ind = self.arrSelectedGender.firstIndex(of:"2")
            self.arrSelectedGender.remove(at: ind ?? 0)
            self.lblFemale.textColor = UIColor(named: "BookedTableCell_SecondryTXT")
            self.imgVwFemale.image = UIImage(named:"inactive_female_ico")
        }
    }

    @IBAction func AgeLeftSideSelect(_ sender:UIButton)
    {
//        self.lblAgeLeftSide.textColor = UIColor(named: "FilterSCRSelectedTxtClr")
//        self.lblAgeLeftSide.backgroundColor = UIColor(named: "FilterSCRSelectedClr")
//        self.lblAgeRightSide.backgroundColor = UIColor(named: "FilterSCRUnselectedClr")
//        self.lblAgeRightSide.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
//        self.selectedAge = lblAgeLeftSide.text ?? ""
        
        if !self.arrSelectedAgeGroup.contains("1"){
            self.arrSelectedAgeGroup.append("1")
            self.lblAgeLeftSide.textColor = UIColor(named: "FilterSCRSelectedTxtClr")
            self.lblAgeLeftSide.backgroundColor = UIColor(named: "FilterSCRSelectedClr")
        }else{
            if self.arrSelectedAgeGroup.count == 1{
                return
            }
            let ind = self.arrSelectedAgeGroup.firstIndex(of:"1")
            self.arrSelectedAgeGroup.remove(at: ind ?? 0)
            self.lblAgeLeftSide.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblAgeLeftSide.backgroundColor = UIColor(named: "FilterSCRUnselectedClr")
        }
    }
    
    @IBAction func AgeRightSideSelect(_ sender:UIButton)
    {
//        self.lblAgeLeftSide.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
//        self.lblAgeLeftSide.backgroundColor = UIColor(named: "FilterSCRUnselectedClr")
//        self.lblAgeRightSide.backgroundColor = UIColor(named: "FilterSCRSelectedClr")
//        self.lblAgeRightSide.textColor = UIColor(named: "FilterSCRSelectedTxtClr")
//        self.selectedAge = lblAgeLeftSide.text ?? ""
        
        if !self.arrSelectedAgeGroup.contains("2"){
            self.arrSelectedAgeGroup.append("2")
            self.lblAgeRightSide.textColor = UIColor(named: "FilterSCRSelectedTxtClr")
            self.lblAgeRightSide.backgroundColor = UIColor(named: "FilterSCRSelectedClr")
        }else{
            if self.arrSelectedAgeGroup.count == 1{
                return
            }
            let ind = self.arrSelectedAgeGroup.firstIndex(of:"2")
            self.arrSelectedAgeGroup.remove(at: ind ?? 0)
            self.lblAgeRightSide.textColor = UIColor(named: "FilterSCRUnselectedTxtClr")
            self.lblAgeRightSide.backgroundColor = UIColor(named: "FilterSCRUnselectedClr")
        }
    }
    
    //MARK: Local Methods-
    func UiDesign(){
        self.viewSelectCountry.setBorder()
        self.lblAgeLeftSide.setCornerRadius(radius: 4)
        self.lblAgeRightSide.setCornerRadius(radius: 4)

    }
}
