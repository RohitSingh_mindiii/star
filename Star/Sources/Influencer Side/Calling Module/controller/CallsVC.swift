//
//  CallsVC.swift
//  Star
//
//  Created by IOS-Sagar on 17/02/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class CallsVC: UIViewController {
    
    @IBOutlet weak var lblLinePending: UILabel!
    @IBOutlet weak var lblLineCompleted: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnCompleted: UIButton!
    @IBOutlet weak var btnPending: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var viewProfileImg: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
     @IBOutlet weak var viewNoDataFound: UIView!
    
    var isPending:Bool = true
    var count = 10
    var arrCallList = [CallListModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        if objAppShareData.isFromNotification{
            if objAppShareData.strNotificationType == "Calls"{
                self.goToCallDetailFromNotification()
                return
            }
        }
//        DispatchQueue.main.async
//            {
//                self.viewStatus.setviewbottomShadow()
//            }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.callWebserviceGetCallList()
        self.viewNoDataFound.isHidden = true
        objAppShareData.arrSelectedBottomNames.removeAll()
        objAppShareData.arrSelectedBottomIds.removeAll()
    }
   
}

extension CallsVC{
    
    @IBAction func btnActionPending(_ sender: Any)
    {
        self.count = 10
        self.isPending = true
        self.lblLinePending.isHidden = false
        self.lblLineCompleted.isHidden = true
        btnCompleted.setTitleColor(UIColor(named: "Text_Color"), for: .normal)
        btnPending.setTitleColor(UIColor(named: "Total_Balance_Color"), for: .normal)
        self.callWebserviceGetCallList()
    }
    
    @IBAction func btnActionCompleted(_ sender: Any)
    {
        count = 5
        self.isPending = false
        self.lblLinePending.isHidden = true
        self.lblLineCompleted.isHidden = false
        self.btnCompleted.setTitleColor(UIColor(named: "Total_Balance_Color"), for: .normal)
        self.btnPending.setTitleColor(UIColor(named: "Text_Color"), for: .normal)
        self.callWebserviceGetCallList()
    }
    
    @IBAction func btnActionFilter(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVCInfluencer") as! FilterVCInfluencer
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
        
extension CallsVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrCallList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CallsTableCell", for: indexPath) as! CallsTableCell
        
        let obj = self.arrCallList[indexPath.row]
        let ageGroup = obj.strUserGender + ", " + obj.strAge

        if self.isPending
        {
            cell.lblAmount.isHidden = true
            cell.imgViewStarIcon.isHidden = true
          //  cell.imgViewCenterConstraint.constant = 0
        }
        else
        {
            cell.lblAmount.isHidden = false
            cell.imgViewStarIcon.isHidden = false
                 //      cell.imgViewCenterConstraint.constant = -6

        }
        cell.lblUserName.text = obj.strUserName
       cell.lblAge.text = ageGroup
        cell.imgUserFlag.image = obj.imgCountryLogo
        cell.lblAmount.text = obj.strAmount
     //  cell.lblAmount.text = "500.00"
      //  cell.imgAudioVideo.image = #imageLiteral(resourceName: "voice_ico")
        
        
        
        let profilePic = obj.strUserImage
        
            if profilePic != "" {
                let url = URL(string: profilePic)
                cell.imgProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_img"))
            }
            else{
                cell.imgProfile.image = #imageLiteral(resourceName: "placeholder_img")

        }

        if obj.strCallType == "2"
        {
            cell.imgAudioVideo.image = #imageLiteral(resourceName: "video_call_ico")
            //cell.viewVoice.isHidden = false
            //cell.viewVideo.isHidden = false
        }else{
            cell.imgAudioVideo.image = #imageLiteral(resourceName: "voice_ico")
            //cell.viewVideo.isHidden = true
            //cell.viewVoice.isHidden = false
        }

        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.arrCallList[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InfluencerCallDetails_VC")as! InfluencerCallDetails_VC
        vc.objCallList = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func goToCallDetailFromNotification(){
        let obj = CallListModel.init(dict: [:])
        if let callId = objAppShareData.notificationDict["reference_id"] as? String{
            obj.strCallID = callId
        }else if let callId = objAppShareData.notificationDict["reference_id"] as? Int{
            obj.strCallID = String(callId)
        }
        objAppShareData.isFromNotification = false
        objAppShareData.strNotificationType = ""
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InfluencerCallDetails_VC")as! InfluencerCallDetails_VC
        vc.objCallList = obj
        self.navigationController?.pushViewController(vc, animated: false)
    }
}


extension CallsVC
{
   func callWebserviceGetCallList(){
    
         self.arrCallList.removeAll()
          objWebServiceManager.showIndicator()
    
    var strProgressStats = ""
    
    if self.isPending{
        strProgressStats = "1"
    }else{
         strProgressStats = "2"
    }
    
    // let CallUrl = "https://dev.stars.app/api/v1/call/list?" + "is_booked=false"
    ///api/v1/call/list?offset=0&limit=20&progress_status=1&age_group=1&gender=2&is_booked=true     age_group: 0:All, 1:17-24, 2:25+
    //let CallUrl = "https://dev.stars.app/api/v1/call/list?" + "is_booked=false&" + "progress_status=" + strProgressStats + "&age_group=" + objAppShareData.objCallFilter.strAgeGroup + "&gender=" + objAppShareData.objCallFilter.strGender + "&country=" + objAppShareData.objCallFilter.strCountry
    let CallUrl = BASE_URL + "call/list?" + "is_booked=false&" + "progress_status=" + strProgressStats + "&age_group=" + objAppShareData.objCallFilter.strAgeGroup + "&gender=" + objAppShareData.objCallFilter.strGender + "&country=" + objAppShareData.objCallFilter.strCountry
    
    objWebServiceManager.requestGet(strURL:CallUrl,params: nil, strCustomValidation: "", success: {response in
              print(response)
              let status = (response["status"] as? String)
              let message = (response["message"] as? String)
              if status == k_success{
                  var dataFound:Int?
                  objWebServiceManager.hideIndicator()
                  if let data = response["data"]as? [String:Any]{
                       dataFound = data["data_found"] as? Int ?? 0
                    
                      if status == "success" ||  status == "Success"
                      {
                         if dataFound == 0
                         {
                              self.viewNoDataFound.isHidden = false
                             return
                         }else{
                             let arrAllReqData = data["call_list"] as! [[String: Any]]
                               
                               //  let countryCode = Locale.current.regionCode
                               for dictGetData in arrAllReqData
                               {
                                let objCall = CallListModel.init(dict: dictGetData)
                                
                                let imagestring = objCall.strCountryCode
                                let imagePath = "CountryPicker.bundle/\(imagestring).png"
                                objCall.imgCountryLogo = UIImage.init(named: imagePath) ?? UIImage()
                                
                                
                                self.arrCallList.append(objCall)
                               }
                             self.tableView.reloadData()
                         }
                       
                        if self.arrCallList.count == 0
                        {
                            self.viewNoDataFound.isHidden = false
                        }else{
                            self.viewNoDataFound.isHidden = true
                        }
                        
                      }else{
                          objWebServiceManager.hideIndicator()
                          objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                      }
                  }
              }
              else{
                  objWebServiceManager.hideIndicator()
                 if message == "Your token has been expired"
                  {self.postAlert(strMessage: message ?? "")
                  }else{
                      objAlert.showAlert(message: message ?? "", title: kAlertTitle, controller: self)
                  }
              }
          }, failure: { (error) in
              print(error)
              objWebServiceManager.hideIndicator()
              objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
          })
      }

func postAlert(strMessage: String)
   {
       // Create the alert controller
       let alertController = UIAlertController(title: "", message: strMessage , preferredStyle: .alert)
       
       let subview = alertController.view.subviews.first! as UIView
       let alertContentView = subview.subviews.first! as UIView
       alertContentView.layer.cornerRadius = 10
       alertContentView.alpha = 1
       alertContentView.layer.borderWidth = 1
       alertContentView.layer.borderColor = UIColor.gray.cgColor
      // alertController.view.tintColor =  UIColor.red.cgColor
       
       // Create the actions
       let okAction = UIAlertAction(title:"Ok", style: UIAlertAction.Style.default) {
           UIAlertAction in
           objAppDelegate.showLogInNavigation()
       }
       
       // Add the actions
       
       alertController.addAction(okAction)
       
       // Present the controller
       self.present(alertController, animated: true, completion: nil)
   }
}

