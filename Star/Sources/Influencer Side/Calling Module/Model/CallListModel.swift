//
//  CallListModel.swift
//  Star
//
//  Created by Mindiii on 2/22/20.
//  Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit

class CallListModel: NSObject {
    
    var strUserName = ""
    var strAgeGroup = ""
    var strAge = ""
    var strUserGender = ""
    var strUserImage = ""
    var strAmount = ""
    var strDOB = ""
    var strCallType = ""
    var strProgressStatus = ""
    var strCountryCode = ""
    var strCategory: String = ""
    var strSubCategoryName: String = ""
    var imgCountryLogo = UIImage()
    var strCallID = ""
    var strTutorialVideoUrl = ""
    var strTutorialVideoThumbUrl = ""
    
    init(dict:[String:Any]) {
        
        if let name = dict["name"] as? String{
            self.strUserName = name
        }
        
        if let dob = dict["dob"] as? String{
            self.strDOB = dob
            //  let cal = Calendar.current
            // let components = cal.dateComponents([.hour], from: self.strDOB, to: "")
            // let diff = components.hour!
        }
        
        if let amount = dict["amount"] as? String{
            let value = Double(amount)
            let newValue = value?.clean
            let strFinalValeu = String(newValue!)
            self.strAmount = strFinalValeu
        }
        else if let amount = dict["amount"] as? Int{
            let value = Double(amount)
            let newValue = value.clean
            let strFinalValeu = String(newValue)
            self.strAmount = String(strFinalValeu)
        }
        if let callID = dict["callID"] as? String{
            self.strCallID = callID
        }
        else if let callID = dict["callID"] as? Int{
            self.strCallID = String(callID)
        }
        
        
        if let call_type = dict["call_type"] as? String{
            self.strCallType = call_type
        }
        else if let call_type = dict["call_type"] as? Int{
            self.strCallType = String(call_type)
        }
        
        if let progress_status = dict["progress_status"] as? String{
            self.strProgressStatus = progress_status
        }
        else if let progress_status = dict["progress_status"] as? Int{
            self.strProgressStatus = String(progress_status)
        }
        
        
        if let gender = dict["gender"] as? String{
            self.strUserGender = gender
        }
        
        
        if let avatar = dict["avatar"] as? String{
            self.strUserImage = avatar
        }
        
        if let ageGroup = dict["age_group"] as? String{
            self.strAgeGroup = ageGroup
        }
        if let countryCode = dict["country_code"] as? String{
            strCountryCode = countryCode
        }
        
        if let category = dict["category"] as? String{
            strCategory = category
        }else if let category = dict["category"]  as? Int {
            strCategory = "\(category)"
        }
        if let subCategoryName = dict["subcategory_name"] as? String{
            strSubCategoryName = subCategoryName
        }
        if let age = dict["age"] as? String{
            self.strAge = age
        }
        else if let age = dict["age"] as? Int{
            self.strAge = String(age)
        }
        
    }
}


//extension Date {
//
//static func getCurrentDate() -> String {
//
//let dateFormatter = DateFormatter()
//
//dateFormatter.dateFormat = "yyyy-MM-dd"
//
//return dateFormatter.string(from: Date())
//
//}
//
//}
