//
// CustomView.swift
// MualabCustomer
//
// Created by Mac on 09/02/18.
// Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

import UIKit

class customView: UIView {
    
    
    //border Color
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    //border width
    @IBInspectable var borderWidth: CGFloat = 1 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    //border Bound
    @IBInspectable var borderBound: CGFloat = 10 {
        didSet {
            layer.cornerRadius = borderBound
        }
    }
    override open func layoutSubviews() {
        super.layoutSubviews()
        //layer.cornerRadius = 0.5 * bounds.size.width
        //clipsToBounds = true
    }
}

extension UIView {
//    func setviewbottomShadow(){
//        layer.shadowColor = UIColor.lightGray.cgColor
//        layer.masksToBounds = false
//        layer.shadowOffset = CGSize(width: 0.0 , height: 5.0)
//        layer.shadowOpacity = 0.2
//        layer.shadowRadius = 4
//    }
    // func fadeInView(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
    // UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
    // self.alpha = 1.0
    // }, completion: completion) }
    
    // func fadeOutView(_ duration: TimeInterval = 0.5, delay: TimeInterval = 1.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
    // UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
    // self.alpha = 0.3
    // }, completion: completion)
    // }
    @IBInspectable
    var borderWidth1: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    @IBInspectable
    var borderClr: UIColor {
        get {
            return layer.borderColor as! UIColor
        }
        set {
            layer.borderColor = newValue.cgColor
        }
    }
    @IBInspectable
    var borderRadius: CGFloat {
        get {
            return layer.cornerRadius }
        set {
            layer.cornerRadius = newValue
        }
    }
    func setViewShadow1()
    {
        layer.shadowColor=UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: -5.0, height: 5.0)
        layer.shadowOpacity = 0.9
        layer.cornerRadius = 5
    }
//    func setViewprofileBorderEdit()
//    {
//        layer.cornerRadius = 65
//        layer.masksToBounds = true
//        layer.borderWidth = 1
//        layer.borderColor = #colorLiteral(red: 0.8901208043, green: 0.8902459741, blue: 0.8900814056, alpha: 1)
//    }
    
    
    
    func createDashedLine(from point1: CGPoint, to point2: CGPoint, color: UIColor, strokeLength: NSNumber, gapLength: NSNumber, width: CGFloat) {
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = width
        shapeLayer.lineDashPattern = [strokeLength, gapLength]
        
        let path = CGMutablePath()
        path.addLines(between: [point1, point2])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
    
}
class customLabel: UILabel {
    
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    
    @IBInspectable var borderWidth: CGFloat = 1 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderBound: CGFloat = 10 {
        didSet {
            layer.cornerRadius = borderBound
        }
    }
    override open func layoutSubviews() {
        super.layoutSubviews()
    }
}

class customImage: UIImageView {
    
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderBound: CGFloat = 10 {
        didSet {
            layer.cornerRadius = borderBound
        }
    }
    override open func layoutSubviews() {
        super.layoutSubviews()
    }
}
class customButton: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderBound: CGFloat = 10 {
        didSet {
            layer.cornerRadius = borderBound
        }
    }
    override open func layoutSubviews() {
        super.layoutSubviews()
    }
}


class customTextField: UITextField {
    
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderBound: CGFloat = 10 {
        didSet {
            layer.cornerRadius = borderBound
        }
    }
    override open func layoutSubviews() {
        super.layoutSubviews()
    }
}
