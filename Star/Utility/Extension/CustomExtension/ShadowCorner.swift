//
//  ShadowCorner.swift
//  Trade
//
//  Created by Mindiii on 5/8/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ShadowCorner: NSObject {
    
}
extension UIView
{
//    func setviewbottomShadow()
//      {
//          layer.shadowColor = UIColor(named: "Shadow")?.cgColor
//          layer.masksToBounds = false
//          layer.shadowOffset = CGSize(width: -0.5 , height: 5.0)
//          layer.shadowOpacity = 0.3
//          layer.shadowRadius = 5
//
//      }
   
    func setviewbottomShadow(){

             layer.shadowColor =  UIColor(named: "Shadow")?.cgColor
             layer.masksToBounds = false
             layer.shadowOffset = CGSize(width: 0.0 , height: 5.0)
             layer.shadowOpacity = 0.2
             layer.shadowRadius = 4
         }
    
    func setviewBottomShadowLight()
      {
          layer.shadowColor = UIColor(named: "Shadow")?.cgColor
          layer.masksToBounds = false
         //layer.shadowOffset = CGSize(width: -0.5 , height: 5.0)
        layer.shadowOffset = CGSize(width: -0.5 , height: 4.0)
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 2.5
      }

    func setviewShadowLightDollerView()
      {
          layer.shadowColor = UIColor(named: "Shadow")?.cgColor
          layer.masksToBounds = false
          layer.shadowOffset = .zero
        layer.shadowOpacity = 0.5
          layer.shadowRadius = 4
      }

//        func createDashedLine(from point1: CGPoint, to point2: CGPoint, color: UIColor, strokeLength: NSNumber, gapLength: NSNumber, width: CGFloat)
//        {
//            let shapeLayer = CAShapeLayer()
//            shapeLayer.strokeColor = color.cgColor
//            shapeLayer.lineWidth = width
//            shapeLayer.lineDashPattern = [strokeLength, gapLength]
//            let path = CGMutablePath()
//            path.addLines(between: [point1, point2])
//            shapeLayer.path = path
//            layer.addSublayer(shapeLayer)
//        }
    func setCornerRadBoarder(color : UIColor,cornerRadious : Int){
           layer.cornerRadius = CGFloat(cornerRadious)
           layer.masksToBounds = true
           layer.borderWidth = 1
           layer.borderColor = color.cgColor
       }
        
    func setCornerRadius(radius : Int){
        layer.cornerRadius = CGFloat(radius)
        self.clipsToBounds = true
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.masksToBounds = true
    }
    func setViewborder(color : UIColor){
           //layer.cornerRadius = 10
           layer.borderWidth = 2
           layer.borderColor = color.cgColor
    }
    
    func setViewShadowCornerRadius(color : UIColor,cornerRadious : Int,shadowOpacity : Int) {
                layer.shadowColor =  color.cgColor //UIColor.lightGray.cgColor
                layer.shadowOffset = CGSize(width: -0.5, height: 0.3)
                layer.shadowOpacity = Float(CGFloat(shadowOpacity))
                layer.cornerRadius = CGFloat(cornerRadious) // 10
            
    //        vwContainTfSearch.layer.shadowColor = UIColor.lightGray.cgColor
    //        vwContainTfSearch.layer.shadowOffset = CGSize(width: -0.5, height: 0.3)
    //        vwContainTfSearch.layer.shadowOpacity = 0.5
    //        vwContainTfSearch.layer.cornerRadius = 20
            }
    
    func setShadow() {
           DispatchQueue.main.async {
               self.layer.masksToBounds = false
            self.layer.shadowColor =  UIColor(named: "Shadow")?.cgColor
                   self.layer.shadowOffset = CGSize(width: -1, height: 1)
                      self.layer.shadowOpacity = 0.5
                      //self.layer.cornerRadius = 10
                      self.layer.shadowRadius = 7
           }
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {

        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer = CAShapeLayer()
            maskLayer.frame = self.bounds
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true){
      layer.masksToBounds = false
      layer.shadowColor = color.cgColor
      layer.shadowOpacity = opacity
      layer.shadowOffset = offSet
      layer.shadowRadius = radius
      layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
      layer.shouldRasterize = true
      layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
   
    
func setBorder(){
    layer.shadowColor = UIColor.black.cgColor
    layer.borderWidth = 2
    layer.cornerRadius = 5
    layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
}
    func setviewCirclewhite(){
              // for set view circle
              layer.cornerRadius = layer.frame.size.height/2
              layer.masksToBounds = true
              layer.borderWidth = 1
              layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
          }

    func setviewCircle(){
                 // for set view circle
                 layer.cornerRadius = layer.frame.size.height/2
                 layer.masksToBounds = true
                 
             }

     func setshadowViewCircle2(){
              //only top left and top right of the view
              layer.masksToBounds = false
              //layer.borderColor = UIColor.gray.cgColor
              layer.shadowColor = UIColor(named: "Shadow")?.cgColor
              layer.shadowOffset = .zero
              layer.shadowOpacity = 0.9
              layer.shadowRadius = 5
              layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner,.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
          }
    func setViewRadius(){
        // set view radius oly top left and top right
        layer.masksToBounds = false
        layer.cornerRadius = 15
        layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    func setshadowViewCircle(){
        //only top left and top right of the view
        layer.masksToBounds = false
        //layer.borderColor = UIColor.gray.cgColor
        layer.shadowColor = UIColor.white.cgColor
        layer.shadowOffset = .zero
        layer.shadowOpacity = 0.5
        layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner,.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
    }
    
}

extension UIButton
{
    func setCornerRadius(cornerRadious : Int){
        layer.cornerRadius = CGFloat(cornerRadious)
        layer.masksToBounds = true
        layer.borderWidth = 0
        layer.borderColor = UIColor.clear.cgColor
    }
    func setCornerRediues(){
           layer.cornerRadius = 8
           layer.masksToBounds = true
           layer.borderWidth = 1
           layer.borderColor = UIColor.clear.cgColor
       }
    
    func setCornerRadWithBoarder(color : UIColor,cornerRadious : Int){
        layer.cornerRadius = CGFloat(cornerRadious)
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = color.cgColor
    }
    

    
    
//    func setCornerRediues(){
//        layer.cornerRadius = 8
//        layer.masksToBounds = true
//        layer.borderWidth = 1
//        layer.borderColor = UIColor.clear.cgColor
//    }
//
//    func setCornerGrayRediues(){
//        layer.cornerRadius = 8
//        layer.masksToBounds = true
//        layer.borderWidth = 1
//        layer.borderColor = UIColor.lightGray.cgColor
//    }
//    func setClickColor(){
//        layer.borderColor = #colorLiteral(red: 0.137254902, green: 0.768627451, blue: 0.4, alpha: 1)
//        setTitleColor(#colorLiteral(red: 0.137254902, green: 0.768627451, blue: 0.4, alpha: 1), for: .normal)
//    }
//    func deSelectColor(){
//        layer.borderColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
//        setTitleColor(#colorLiteral(red: 0.1411764706, green: 0.1411764706, blue: 0.1411764706, alpha: 1), for: .normal)
//    }
//    func setShadowButton(){
//        layer.cornerRadius = 4
//        layer.borderColor = UIColor.lightGray.cgColor
//        layer.borderWidth = 0.7
//        layer.shadowColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
//        layer.shadowOffset = CGSize(width: 1, height: 1)
//        layer.shadowRadius = 2
//        layer.shadowOpacity = 0.7
//    }
//    func setShadowBtn(){
//        layer.shadowColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
//        layer.shadowOffset = CGSize(width: 1, height: 1)
//        layer.shadowRadius = 2
//        layer.shadowOpacity = 0.7
//    }


    func applyGradient() {

     let gradient = CAGradientLayer()
         gradient.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: 35)
         gradient.startPoint = CGPoint(x:0.0, y:0.5)
         gradient.endPoint = CGPoint(x:1.0, y:0.5)

        gradient.colors = [UIColor.systemPink.cgColor, UIColor.blue.cgColor]
          // gradient.colors = [UIColor.init(red: 179, green: 12, blue: 14, alpha: 1).cgColor, UIColor.init(red: 2, green: 3, blue: 158, alpha: 1).cgColor]
          //  #b30c29
          // #02039e
        self.layer.addSublayer(gradient)
}
    func setCornerRadiusWithBoarder(color : UIColor,cornerRadious : Int){
           layer.cornerRadius = CGFloat(cornerRadious)
           layer.masksToBounds = true
           layer.borderWidth = 1
           layer.borderColor = color.cgColor
       }
    
    func setCornerBlackRediues(){
        layer.cornerRadius = 8
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.cgColor
    }
    
     func setCornerRedRediues(){
              layer.cornerRadius = 8
              layer.masksToBounds = true
              layer.borderWidth = 1
              layer.borderColor = UIColor.red.cgColor
          }
      
  
}
extension UITextField {
   func addDoneButtonOnKeyboard() {
       let keyboardToolbar = UIToolbar()
       keyboardToolbar.sizeToFit()
       let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
           target: nil, action: nil)
       let doneButton = UIBarButtonItem(barButtonSystemItem: .done,
           target: self, action: #selector(resignFirstResponder))
       keyboardToolbar.items = [flexibleSpace, doneButton]
       self.inputAccessoryView = keyboardToolbar
   }
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
extension UIImage {
    func toString() -> String? {
        let data: Data? = self.pngData()
        return data?.base64EncodedString(options: .endLineWithLineFeed)
    }
//    func circularImage(photoImageView: UIImageView?)
//    {
//        photoImageView!.layer.frame = photoImageView!.layer.frame.insetBy(dx: 0, dy: 0)
//        photoImageView!.layer.borderColor = UIColor.gray.cgColor
//        photoImageView!.layer.cornerRadius = photoImageView!.frame.height/2
//        photoImageView!.layer.masksToBounds = false
//        photoImageView!.clipsToBounds = true
//        photoImageView!.layer.borderWidth = 0.5
//        photoImageView!.contentMode = UIView.ContentMode.scaleAspectFill
//    }
    
  
    
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio)
        {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    var roundedImage: UIImage {
        let rect = CGRect(origin:CGPoint(x: 0, y: 0), size: self.size)
        UIGraphicsBeginImageContextWithOptions(self.size, false, 1)
        UIBezierPath(
            roundedRect: rect,
            cornerRadius: self.size.height
            ).addClip()
        self.draw(in: rect)
        
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    func resizedImage(newWidth: CGFloat) -> UIImage {
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    }
    extension UIImage {
        var isPortrait:  Bool    { size.height > size.width }
        var isLandscape: Bool    { size.width > size.height }
        var breadth:     CGFloat { min(size.width, size.height) }
        var breadthSize: CGSize  { .init(width: breadth, height: breadth) }
        var breadthRect: CGRect  { .init(origin: .zero, size: breadthSize) }
        func rounded(with color: UIColor, width: CGFloat) -> UIImage? {

            guard let cgImage = cgImage?.cropping(to: .init(origin: .init(x: isLandscape ? ((size.width-size.height)/2).rounded(.down) : .zero, y: isPortrait ? ((size.height-size.width)/2).rounded(.down) : .zero), size: breadthSize)) else { return nil }

            let bleed = breadthRect.insetBy(dx: -width, dy: -width)
            let format = imageRendererFormat
            format.opaque = false

            return UIGraphicsImageRenderer(size: bleed.size, format: format).image { context in
                UIBezierPath(ovalIn: .init(origin: .zero, size: bleed.size)).addClip()
                var strokeRect =  breadthRect.insetBy(dx: -width/2, dy: -width/2)
                strokeRect.origin = .init(x: width/2, y: width/2)
                UIImage(cgImage: cgImage, scale: 1, orientation: imageOrientation)
                .draw(in: strokeRect.insetBy(dx: width/2, dy: width/2))
                context.cgContext.setStrokeColor(color.cgColor)
                let line: UIBezierPath = .init(ovalIn: strokeRect)
                line.lineWidth = width
                print("image width \(width)")
                print("Founded line width \(line.lineWidth)")
                line.stroke()
            }
        }
    
         
}
extension UIImageView {
    func setCornerRadiusWithBoarder(color : UIColor,cornerRadious : Int, photoImageView: UIImageView?){
    photoImageView?.layer.cornerRadius = CGFloat(cornerRadious)
           photoImageView?.layer.masksToBounds = true
           photoImageView?.layer.borderWidth = 2
           photoImageView?.layer.borderColor = color.cgColor
      }
    
    func setImgCircleColor(){
             
             layer.cornerRadius = layer.frame.size.height/2
             layer.masksToBounds = true
             layer.borderWidth = 1
             layer.borderColor = #colorLiteral(red: 0.1140634629, green: 0.2149929786, blue: 0.3579177461, alpha: 1)
         }
    func setImgCircle(){
           
           layer.cornerRadius = layer.frame.size.height/2
           layer.masksToBounds = true
           layer.borderWidth = 1.5
           layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
           
       }
    func setImg(url:String)->Data?
    {
        var req:URLRequest = URLRequest(url: URL(string: url)!)
        req.httpMethod = "GET"
        var imgData:Data? = Data()
        let dataTask = URLSession.shared.dataTask(with: req) { (data, response, err) in
            imgData = data
            if err == nil && data != nil
            {
                print("inside if block print img \(imgData)")
                
            }
            else
            {
                print("found error : ",err)
            }
        }.resume()
        return imgData
    }

    
    
}


extension UIColor {
    class func colorConvert(withData data:Data) -> UIColor {
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? UIColor ?? UIColor(red: 23.0 / 255.0, green: 29.0 / 255.0, blue: 49.0 / 255.0, alpha: 1.0)
    }

    func encode() -> Data {
         return NSKeyedArchiver.archivedData(withRootObject: self)
    }
}
extension UILabel{
    
    func setCornerRadiusWithBoarder(color : UIColor,cornerRadious : Int){
        layer.cornerRadius = CGFloat(cornerRadious)
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = color.cgColor
    }
}
extension NSAttributedString {

class func attributedPlaceholderString(string: String) -> NSAttributedString {
    let color = #colorLiteral(red: 0.6588235294, green: 0.6588235294, blue: 0.6588235294, alpha: 1)
    let attributedString =  NSAttributedString(string:string, attributes:[NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font :UIFont(name: "Montserrat-Regular", size: 16)!])
    return attributedString
    }

}
