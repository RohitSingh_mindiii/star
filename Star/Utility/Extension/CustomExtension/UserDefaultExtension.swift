//
//  UserDefaultExtension.swift
//  Habito
//
//  Created by MINDIII on 11/9/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation
import UIKit

var deviceToken: String? {
    get {
        return UserDefaults.standard.value(forKey: UserDefaults.KeysDefault.deviceToken) as? String ?? "abc"
    }
}

//extension UserDefaults {
//    
//    enum Keys {
//   
//    static let strAccessToken = "access_token"
//    static let AuthToken = "AuthToken"
//    static let user_type = "user_type"
//    static let email = "email"
//    static let full_name = "full_name"
//    static let userID = "userID"
//    static let status = "status"
//    static let businessInfo = "business_info"
//    static let profileImg = "profileImg"
//    static let password = "password"
//    }
//}

extension UserDefaults{
    enum KeysDefault {
        //user Info
        static let strVenderId = "udid"
        static let deviceToken = "device_token"
        static let contentURLs = "content"
        static let kAuthToken = "authToken"
        static  let kUserId = "userId"
        static  let kUserName = "userName"
        static let kFullName = "fullName"
        static  let kEmail = "email"
        static  let kContact = "contact"
        static let kCountryCode = "countryCode"
        static  let kProfileImage = "profileImage"
        static  let kCoverImage = "coverImage"
        static let kAddress = "address"
        static  let kLatitude = "latitude"
        static  let kLongitude = "longitude"
        static let kUserType = "userType"
        static  let kDiscription = "description"
        static  let kDOB = "DOB"
        static let isCatagorySelected = "catagorySelected"
        static  let kPassword = "password"
        static  let kGender = "gender"
        static let kIsRemember = "remember"
        static  let kStatus = "remember"
        static  let kCrd = "remember"
       
        
        static let ktermsCondition = "termsCondition"
        static let ksharedUrl = "sharedUrl"
        static let kaboutUs = "aboutUs"
        static  let kReminderStatus = "reminderStatus"
        static let userInfo = "userInfo"
        static let AuthToken = "AuthToken"
        static let Stars = "Stars"
        static let ProfilePic = "ProfilePic"
        static let freeStars = "free"
        static let paidStars = "paid"
        static let availableStars = "available"
        
        static let credit = "credit"
        static let isDarkMode = "isDarkMode"
        
        
    }
    
}


extension UserDefaults {
    
    enum InfulenceKeys {
        
        static let user_type = "user_type"
        static let InfulencerProfileImage = "avatar"
        static let InfulencerName = "name"
        
    }
}

