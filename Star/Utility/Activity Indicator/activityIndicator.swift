//
//  activityIndicator.swift
//  CrimeSpotter
//
//  Created by Mindiii on 1/10/20.
//  Copyright © 2020 MS-H110. All rights reserved.
//

import UIKit
import RSLoadingView

let objIndicator = activityIndicator()

class activityIndicator: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func startIndicator(indicatorView : UIView)
    {
       let loadingView = RSLoadingView()
       loadingView.shouldTapToDismiss = false
       loadingView.show(on: indicatorView)
    }
    
    func stopIndicator(indicatorView : UIView)
       {
          let loadingView = RSLoadingView()
          loadingView.shouldTapToDismiss = true
          loadingView.hide()
       }
}
