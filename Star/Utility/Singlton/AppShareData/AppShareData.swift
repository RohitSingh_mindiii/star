import UIKit
import AVFoundation
import MobileCoreServices
import Foundation

class AppShareData: NSObject{
    
    var backgroundColor: UIColor?
    var fromPasswordreminder = false
    
    var UserDetail = userDetailModel(dict: [:])
    var dictUserLoction = [String:Any]()
    var isNotificationToggleOn = Bool()
    //For Image Picker
    var successBlock : ((_ placeInfo: [String: Any])->())?
    var failureBlock : ((_ error: Error)->())?
    var controller: UIViewController?
    var imagePicker = UIImagePickerController()
    var strDeviceToken = ""
    var isCountryInBottomSheet = false
    var isDiscoverFilterApply = false
    var isCallFilterApply = false
    var arrSelectedBottomIds = [Int]()
    var arrSelectedBottomNames = [String]()
    var arrLanguageData = [CommonModal]()
    var arrCountryData = [CommonModal]()
    var arrGenderData = [CommonModal]()
    var arrAgeGroupData = [CommonModal]()
    var arrCategoryData = [CommonModal]()
    var arrSubCategoryData = [CommonModal]()
    var arrCreditData = [CommonModal]()
    var objCallFilter = CallFilterModal(dict: [:])
    var objDiscoverFilter = DiscoverFilterModal(dict: [:])
   
    var strNotificationType = ""
    var isFromNotification = false
    var notificationDict = [String:Any]()
   
    //MARK: - Shared object
    
    private static var sharedManager: AppShareData = {
        let manager = AppShareData()
        return manager
    }()
    
    // MARK: - Accessors
    class func sharedObject() -> AppShareData {
        return sharedManager
    }
    
    
    // MARK: - saveUpdateUserInfoFromAppshareData ---------------------
    func SaveUpdateUserInfoFromAppshareData(userDetail:[String:Any])
    {
        let outputDict = self.removeNSNull(from:userDetail)
        
        UserDefaults.standard.set(outputDict, forKey: UserDefaults.KeysDefault.userInfo)
        
    }
    
    // MARK: - FetchUserInfoFromAppshareData -------------------------
    func fetchUserInfoFromAppshareData()
    {
        if let userDic = UserDefaults.standard.value(forKey:  UserDefaults.KeysDefault.userInfo) as? [String : Any]{
            UserDetail = userDetailModel.init(dict: userDic)
        }
    }
    
    func resetDefaultsAlluserInfo()
    {
        let defaults = UserDefaults.standard
        let myVenderId = defaults.string(forKey:UserDefaults.KeysDefault.strVenderId)
        let Email = defaults.value(forKey:"AlwasysEmail") as? String ?? ""
        let Password = defaults.value(forKey:"AlwaysPassword") as? String ?? ""
        let AlwasysEmail = defaults.value(forKey:"AlwasysEmail") as? String ?? ""
       
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        
        if let bundleID = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundleID)
        }
        
        defaults.set(myVenderId ?? "", forKey:UserDefaults.KeysDefault.strVenderId)
        defaults.set(Email ?? "", forKey:"AlwasysEmail")
        defaults.set(Password ?? "", forKey:"AlwaysPassword")
        defaults.set(AlwasysEmail ?? "", forKey:"AlwasysEmail")
        
        UserDetail = userDetailModel(dict: [:])
    }
    
    func removeNSNull(from dict: [String: Any]) -> [String: Any] {
        var mutableDict = dict
        
        
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
            print(key)
            
            
        }
        return mutableDict
    }
}

//MARK:- Logout Api
extension AppShareData {
    func callWebserviceForLogout(){
        objWebServiceManager.showIndicator()
        objWebServiceManager.requestDelete(strURL: WsUrl.Logout, params: nil, strCustomValidation: "", success: {response in
            
            let status = (response["status"] as? String)
            // let message = (response["message"] as? String)
            if status == k_success{
                objWebServiceManager.hideIndicator()
                userDefaults.removeObject(forKey: objAppShareData.UserDetail.straAuthToken)
                UserDefaults.standard.removeObject(forKey: "Stars")
                self.resetDefaultsAlluserInfo()
                UserDefaults.standard.setValue(false, forKey: "isDarkMode")
                UserDefaults.standard.synchronize()
                if #available(iOS 13.0, *) {
                   objAppDelegate.window?.overrideUserInterfaceStyle = .light
                }
                objAppDelegate.showLogInNavigation()
            }
        }, failure: { (error) in
            print(error)
            objWebServiceManager.hideIndicator()
            // objAppShareData.showAlert(title: kAlertTitle, message: message ?? "", view: self)
        })
    }
}

//MARK:- Image Picker for Video
extension AppShareData {
    
    func openImagePickerForVideo(controller: UIViewController, success: @escaping ([String: Any])->(), failure: @escaping (Error)-> ()) {
        self.imagePicker.delegate = self
        self.successBlock = success
        self.failureBlock = failure
        self.controller = controller
        self.imagePickForVideo()
    }
    
    func imagePickForVideo() {
        let alert:UIAlertController=UIAlertController(title: "Choose Video", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openVideoGallery()
        }
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCameraForVideo()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        // alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        self.controller?.present(alert, animated: true, completion: nil)
    }
    
    func openCameraForVideo(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            
            
            self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.videoMaximumDuration = 300.0
            self.imagePicker.allowsEditing = false
            self.imagePicker.delegate = self
            self.imagePicker.mediaTypes = [kUTTypeMovie as String]
            self.controller?.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func openVideoGallery(){
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.imagePicker.allowsEditing = true
            self.imagePicker.delegate = self
            self.imagePicker.mediaTypes = [kUTTypeMovie as String]
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            self.controller?.present(self.imagePicker, animated: true, completion: nil)
        }
    }
}

//MARK:- Image picker Controller Delegate
extension AppShareData:UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.controller?.dismiss(animated: true, completion: nil)
        self.successBlock = nil
        self.failureBlock = nil
    }
    
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        self.controller?.dismiss(animated: true, completion: nil)
        var dict  = [String: Any]()
        dict["image"] = info[.originalImage]
        dict["imageUrl"] = info[.mediaURL]
        print("media info",dict)
        self.successBlock?(dict)
        self.successBlock = nil
        self.failureBlock = nil
        
        /// Save Video
        guard
            let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String,
            mediaType == (kUTTypeMovie as String),
            let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL,
            UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
            else {
                return
        }
        
        // Handle a movie capture
        UISaveVideoAtPathToSavedPhotosAlbum(
            url.path,
            self,
            #selector(video(_:didFinishSavingWithError:contextInfo:)),
            nil)
        
        self.controller?.dismiss(animated: true, completion: nil)
    }
    
    @objc func video(_ videoPath: String, didFinishSavingWithError error: Error?, contextInfo info: AnyObject) {
        //  let title = (error == nil) ? "Success" : "Error"
        // let message = (error == nil) ? "Video was saved" : "Video failed to save"
        
    }
}
