
//
//  WebServiceClass.swift
//  Link
//
//  Created by MINDIII on 10/3/17.
//  Copyright © 2017 MINDIII. All rights reserved.


import UIKit
import Alamofire
import SVProgressHUD


var strAuthToken : String = ""
let stripeKey = "Bearer sk_test_5LBctxgLZlmgYzB2oCWlw36O00OIq2Kfgg"

let objWebServiceManager = WebServiceManager.sharedObject()

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}


class WebServiceManager: NSObject {
    
    //MARK: - Shared object
    fileprivate var window = UIApplication.shared.keyWindow
    
    private static var sharedNetworkManager: WebServiceManager = {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.setMinimumDismissTimeInterval(1)
        SVProgressHUD.setRingThickness(3)
        SVProgressHUD.setRingRadius(22)
        
        let networkManager = WebServiceManager()
        return networkManager
    }()
    
    // MARK: - Accessors
    class func sharedObject() -> WebServiceManager {
        return sharedNetworkManager
    }
    
    private let manager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        return Alamofire.SessionManager(configuration: configuration)
    }()
    
    
    //MARK:-
    public func requestgetDistance(strURL:String, params : [String:Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        let url = strURL
        print("url = \(url)")
        
        // manager.retrier = OAuth2Handler()
        manager.request(url, method: .get, parameters: params, headers:nil).responseJSON { responseObject in
            
            print(responseObject)
            if responseObject.result.isSuccess {
                do {
                    SVProgressHUD.show(withStatus: "Please wait..")
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    // print(dictionary)
                    SVProgressHUD.dismiss()
                }catch{
                    SVProgressHUD.dismiss()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
            if responseObject.result.isFailure {
                SVProgressHUD.dismiss()
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    //MARK: - Convert String to Dict
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func isNetworkAvailable() -> Bool{
        if !NetworkReachabilityManager()!.isReachable{
            return false
        }else{
            return true
        }
    }
    
}

//MARK :- retrier

class OAuth2Handler : RequestRetrier {
    
    public func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        
        if error.localizedDescription.count > 0 && request.retryCount < 5{
            
            if error.localizedDescription == "The request timed out."{
                completion(false, 0.0) // don't retry
            }else{
                print("Retrying..\(String(describing: request.request?.url!)))")
                completion(true, 5.0)  // retry after 5 second
            }
            
        }else{
            completion(false, 0.0) // don't retry
        }
    }
}



//MARK:- Webservice methods
extension WebServiceManager{
    
    //MARK: - Request Post method ----
    public func requestPost(strURL:String, params : [String:Any], strCustomValidation:String , showIndicator:Bool, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void) {
        if !NetworkReachabilityManager()!.isReachable{
            // self.showNetworkAlert()
            DispatchQueue.main.async {
                //   self.StopIndicator()
            }
            return
        }
        
        strAuthToken = ""
        strAuthToken =  UserDefaults.standard.string(forKey:"Authtoken") ?? ""
        //        if UserDefaults.standard.string(forKey:"Authtoken")=="" {
        //            strAuthToken = "Bearer" +  " " +  ""
        //               }else{
        //            strAuthToken = "Bearer" +  " " +  UserDefaults.standard.string(forKey:"Authtoken")!
        //        }
        
        if let token = UserDefaults.standard.string(forKey: "Authtoken"){
            strAuthToken = "Bearer" + " " + token
        }
        
        var strUdidi = ""
        if let MyUniqueId = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.strVenderId) {
            print("defaults VenderID: \(MyUniqueId)")
            strUdidi = MyUniqueId
        }
        
        let currentTimeZone = getCurrentTimeZone()
        
        let header: HTTPHeaders = [
            "Authorization": strAuthToken,
            "Accept": "application/json",
            WsHeader.deviceId:strUdidi,
            WsHeader.deviceType:"1",
            WsHeader.deviceTimeZone: currentTimeZone
        ]
        print("header----",header)
        //           if showIndicator == true{ SVProgressHUD.show(withStatus: "Please wait..")}
        Alamofire.upload(multipartFormData:{ multipartFormData in
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }},
                         usingThreshold:UInt64.init(),
                         to:strURL,
                         method:.post,
                         headers: header,
                         
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    
                                    do {
                                        let dictionary = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                        if let errorCode = dictionary["status_code"] as? String{
                                            if errorCode == "400"{
                                                let strErrorType = dictionary["error_type"] as? String
                                                if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                                    objAlert.showSessionFailAlert()
                                                    return
                                                }
                                            }
                                        }else if let errorCode = dictionary["status_code"] as? Int{
                                            if errorCode == 400{
                                                let strErrorType = dictionary["error_type"] as? String
                                                if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                                    objAlert.showSessionFailAlert()
                                                    return
                                                }
                                            }
                                        }
                                        success(dictionary as! Dictionary<String, Any>)
                                    }catch{
                                        
                                    }
                                    
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                                failure(encodingError)
                            }
        })
    }
    
    //MARK: - Request Put method ----
    public func requestPut(strURL:String, params : [String:Any]?,strCustomValidation:String,showIndicator:Bool, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        if !NetworkReachabilityManager()!.isReachable{
            
            let app = UIApplication.shared.delegate as? AppDelegate
            //  let window = app?.window
            //    objAlert.showAlertVc(title: NoNetwork, controller: window!)
            //    self.StopIndicator()
            return
        }
        strAuthToken = ""
        
        if UserDefaults.standard.string(forKey:"Authtoken")==nil {
            strAuthToken = "Bearer" +  " " +  ""
        }else{
            strAuthToken = "Bearer" +  " " +  UserDefaults.standard.string(forKey:"Authtoken")!
        }
        
        var strUdidi = ""
        if let MyUniqueId = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.strVenderId) {
            print("defaults VenderID: \(MyUniqueId)")
            strUdidi = MyUniqueId
        }
        let currentTimeZone = getCurrentTimeZone()
        let header: HTTPHeaders = ["Authorization":strAuthToken,
                                   WsHeader.deviceId:strUdidi,
                                   WsHeader.deviceType:"1",
                                   WsHeader.deviceTimeZone: currentTimeZone,
                                   WsHeader.ContentType: "application/x-www-form-urlencoded"]
        print(header)
        print(params)
        Alamofire.request(strURL, method: .put, parameters:  params, encoding: URLEncoding.default, headers: header).responseString{ responseObject in
            // if !objAppShareData.isPutApiFromUpload{
            // self.StopIndicator()
            // }
            print(responseObject )
            if responseObject.result.isSuccess {
                
                
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    if let errorCode = dictionary["status_code"] as? String{
                        if errorCode == "400"{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }else if let errorCode = dictionary["status_code"] as? Int{
                        if errorCode == 400{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }
                    success(dictionary as! Dictionary<String, Any>)
                    print(dictionary)
                }catch{
                    
                }
            }
            if responseObject.result.isFailure {
                // self.StopIndicator()
                let error : Error = responseObject.result.error!
                failure(error)
                
            }
        }
    }
    // deepak
    
    public func uploadMultipartData(strURL:String, params :  [String:Any]?,showIndicator:Bool, imageData:Data?, fileName:String?, mimeType:String?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void){
        
        if !NetworkReachabilityManager()!.isReachable{
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            //   objAlert.showAlert(message: k_NoConnection, title:"Alert" , controller: self)
            DispatchQueue.main.async {
                //   self.StopIndicator()
            }
            return
        }
        
        
        strAuthToken = ""
        
        if UserDefaults.standard.string(forKey:"Authtoken")==nil {
            strAuthToken = "Bearer" +  " " + ""
        }else{
            strAuthToken = "Bearer" +  " " +  UserDefaults.standard.string(forKey:"Authtoken")!
        }
        
        
        var strUdidi = ""
        if let MyUniqueId = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.strVenderId) {
            print("defaults VenderID: \(MyUniqueId)")
            strUdidi = MyUniqueId
        }
        
        let currentTimeZone = getCurrentTimeZone()
        
        let header: HTTPHeaders = [
            "Authorization": strAuthToken,
            "Accept": "application/json",
            WsHeader.deviceId:strUdidi,
            WsHeader.deviceType:"1",
            WsHeader.deviceTimeZone: currentTimeZone
        ]
        Alamofire.upload(multipartFormData:{ multipartFormData in
            if let data = imageData {
                multipartFormData.append(data,
                                         withName:fileName!,
                                         fileName: "file.jpeg",
                                         mimeType:mimeType!)
            }
            
            for (key, value) in params ?? [:] {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        },
                         usingThreshold:UInt64.init(),
                         to:strURL,
                         method:.post,
                         headers: header,
                         
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    
                                    do {
                                        let dictionary = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                        if let errorCode = dictionary["status_code"] as? String{
                                            if errorCode == "400"{
                                                let strErrorType = dictionary["error_type"] as? String
                                                if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                                    objAlert.showSessionFailAlert()
                                                    return
                                                }
                                            }
                                        }else if let errorCode = dictionary["status_code"] as? Int{
                                            if errorCode == 400{
                                                let strErrorType = dictionary["error_type"] as? String
                                                if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                                    objAlert.showSessionFailAlert()
                                                    return
                                                }
                                            }
                                        }
                                        success(dictionary as! Dictionary<String, Any>)
                                    }catch{
                                    }
                                    
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                                failure(encodingError)
                            }
        })
    }
    
    //MARK:- Rohit Upload Multipart AudioVidoe
    public func uploadMultipartDataAudioVideo(strURL:String, params :  [String:Any]?,showIndicator:Bool,strMediaType:String?, imageData:Data?, mediaData:Data?, mediaFileName:String?,thumbDataFileName:String?, mimeType:String?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void){
        
        if !NetworkReachabilityManager()!.isReachable{
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            //   objAlert.showAlert(message: k_NoConnection, title:"Alert" , controller: self)
            DispatchQueue.main.async {
                //   self.StopIndicator()
            }
            return
        }
        
        
        strAuthToken = ""
        
        if UserDefaults.standard.string(forKey:"Authtoken")==nil {
            strAuthToken = "Bearer" +  " " + ""
        }else{
            strAuthToken = "Bearer" +  " " +  UserDefaults.standard.string(forKey:"Authtoken")!
        }
        
        
        var strUdidi = ""
        if let MyUniqueId = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.strVenderId) {
            print("defaults VenderID: \(MyUniqueId)")
            strUdidi = MyUniqueId
        }
        
        let currentTimeZone = getCurrentTimeZone()
        
        let header: HTTPHeaders = [
            "Authorization": strAuthToken,
            "Accept": "application/json",
            WsHeader.deviceId:strUdidi,
            WsHeader.deviceType:"1",
            WsHeader.deviceTimeZone: currentTimeZone
        ]
        Alamofire.upload(multipartFormData:{ multipartFormData in
            if let data = imageData {
                multipartFormData.append(data,
                                         withName:thumbDataFileName!,
                                         fileName: "file.png",
                                         mimeType:"image/png")
            }
            
            if strMediaType == "Audio"{
                if let audioData = mediaData {
                    multipartFormData.append(audioData,
                                             withName:mediaFileName!,
                                             fileName: "file.m4a",
                                             mimeType:mimeType!)
                }
                
            }else{
                if let videoData = mediaData {
                    multipartFormData.append(videoData,
                                             withName:mediaFileName!,
                                             fileName: "file.MOV",
                                             mimeType:mimeType!)
                }
            }
            
            
            for (key, value) in params ?? [:] {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        },
                         usingThreshold:UInt64.init(),
                         to:strURL,
                         method:.post,
                         headers: header,
                         
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    
                                    do {
                                        let dictionary = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                        if let errorCode = dictionary["status_code"] as? String{
                                            if errorCode == "400"{
                                                let strErrorType = dictionary["error_type"] as? String
                                                if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                                    objAlert.showSessionFailAlert()
                                                    return
                                                }
                                            }
                                        }else if let errorCode = dictionary["status_code"] as? Int{
                                            if errorCode == 400{
                                                let strErrorType = dictionary["error_type"] as? String
                                                if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                                    objAlert.showSessionFailAlert()
                                                    return
                                                }
                                            }
                                        }
                                        success(dictionary as! Dictionary<String, Any>)
                                    }catch{
                                    }
                                    
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                                failure(encodingError)
                            }
        })
    }
    //
    
    //MARK: - Request get method ----
    public func requestGet(strURL:String, params : [String : AnyObject]?, strCustomValidation:String , success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        if !NetworkReachabilityManager()!.isReachable{
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            //    objAlert.showAlertVc(title: NoNetwork, controller: window!)
            //    DispatchQueue.main.async {
            //    self.StopIndicator()
            //    }
            return
        }
        
        strAuthToken = ""
        
        if UserDefaults.standard.string(forKey:"Authtoken")==nil {
            strAuthToken = "Bearer" +  " " +  ""
        }else{
            strAuthToken = "Bearer" +  " " +  UserDefaults.standard.string(forKey:"Authtoken")!
        }
        
        let currentTimeZone = getCurrentTimeZone()
        
        var strUdidi = ""
        if let MyUniqueId = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.strVenderId) {
            print("defaults VenderID: \(MyUniqueId)")
            strUdidi = MyUniqueId
        }
        
        let headers: HTTPHeaders = [
            "Authorization": strAuthToken ,
            "Accept": "application/json",
            WsHeader.deviceId:strUdidi,
            WsHeader.deviceType:"1",
            WsHeader.deviceTimeZone: currentTimeZone
        ]
        var urlString = strURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        print("url....\(strURL)")
        print("header....\(headers)")
        Alamofire.request(urlString, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            //self.StopIndicator()
            
            if responseObject.result.isSuccess {
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    if let errorCode = dictionary["status_code"] as? String{
                        if errorCode == "400"{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }else if let errorCode = dictionary["status_code"] as? Int{
                        if errorCode == 400{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }
                    success(dictionary as! Dictionary<String, Any>)
                    print(dictionary)
                }catch{
                    
                    let error : Error = responseObject.result.error!
                    failure(error)
                    let str = String(decoding: responseObject.data!, as: UTF8.self)
                    print("PHP ERROR : \(str)")
                }
            }
            if responseObject.result.isFailure {
                //self.StopIndicator()
                let error : Error = responseObject.result.error!
                failure(error)
                
                let str = String(decoding: responseObject.data!, as: UTF8.self)
                print("PHP ERROR : \(str)")
            }
        }
    }
    
    public func uploadMultipartDataPut(strURL:String, params :  [String:Any]?,showIndicator:Bool, imageData:Data?, fileName:String?, mimeType:String?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void){
        
        if !NetworkReachabilityManager()!.isReachable{
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            //   objAlert.showAlert(message: k_NoConnection, title:"Alert" , controller: self)
            DispatchQueue.main.async {
                //   self.StopIndicator()
            }
            return
        }
        
        
        strAuthToken = ""
        
        if UserDefaults.standard.string(forKey:"Authtoken")==nil {
            strAuthToken = "Bearer" +  " " + ""
        }else{
            strAuthToken = "Bearer" +  " " +  UserDefaults.standard.string(forKey:"Authtoken")!
        }
        
        
        var strUdidi = ""
        if let MyUniqueId = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.strVenderId) {
            print("defaults VenderID: \(MyUniqueId)")
            strUdidi = MyUniqueId
        }
        
        let currentTimeZone = getCurrentTimeZone()
        
        let header: HTTPHeaders = [
            "Authorization": strAuthToken,
            "Accept": "application/json",
            WsHeader.deviceId:strUdidi,
            WsHeader.deviceType:"1",
            WsHeader.deviceTimeZone: currentTimeZone
        ]
        Alamofire.upload(multipartFormData:{ multipartFormData in
            if let data = imageData {
                multipartFormData.append(data,
                                         withName:fileName!,
                                         fileName: "file.jpeg",
                                         mimeType:mimeType!)
            }
            
            for (key, value) in params ?? [:] {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        },
                         usingThreshold:UInt64.init(),
                         to:strURL,
                         method:.put,
                         headers: header,
                         
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    
                                    do {
                                        let dictionary = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                        if let errorCode = dictionary["status_code"] as? String{
                                            if errorCode == "400"{
                                                let strErrorType = dictionary["error_type"] as? String
                                                if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                                    objAlert.showSessionFailAlert()
                                                    return
                                                }
                                            }
                                        }else if let errorCode = dictionary["status_code"] as? Int{
                                            if errorCode == 400{
                                                let strErrorType = dictionary["error_type"] as? String
                                                if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                                    objAlert.showSessionFailAlert()
                                                    return
                                                }
                                            }
                                        }
                                        success(dictionary as! Dictionary<String, Any>)
                                    }catch{
                                    }
                                    
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                                failure(encodingError)
                            }
        })
    }
    
    //MARK: - Request Delete method ----
    public func requestDelete(strURL:String, params : [String : AnyObject]?, strCustomValidation:String , success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        if !NetworkReachabilityManager()!.isReachable{
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            //    objAlert.showAlertVc(title: NoNetwork, controller: window!)
            //    DispatchQueue.main.async {
            //    self.StopIndicator()
            //    }
            return
        }
        
        strAuthToken = ""
        
        if UserDefaults.standard.string(forKey:"Authtoken")==nil {
            strAuthToken = "Bearer" +  " " +  ""
        }else{
            strAuthToken = "Bearer" +  " " +  UserDefaults.standard.string(forKey:"Authtoken")!
        }
        print(strAuthToken)
        
        let currentTimeZone = getCurrentTimeZone()
        
        var strUdidi = ""
        if let MyUniqueId = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.strVenderId) {
            print("defaults VenderID: \(MyUniqueId)")
            strUdidi = MyUniqueId
        }
        
        let headers: HTTPHeaders = [
            "Authorization": strAuthToken ,
            "Accept": "application/json",
            WsHeader.deviceId:strUdidi,
            WsHeader.deviceType:"1",
            WsHeader.deviceTimeZone: currentTimeZone
        ]
        
        print("url....\(strURL)")
        print("header....\(headers)")
        Alamofire.request(strURL, method: .delete, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            //self.StopIndicator()
            
            if responseObject.result.isSuccess {
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    if let errorCode = dictionary["status_code"] as? String{
                        if errorCode == "400"{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }else if let errorCode = dictionary["status_code"] as? Int{
                        if errorCode == 400{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }
                    success(dictionary as! Dictionary<String, Any>)
                    print(dictionary)
                }catch{
                    
                    let error : Error = responseObject.result.error!
                    failure(error)
                    let str = String(decoding: responseObject.data!, as: UTF8.self)
                    print("PHP ERROR : \(str)")
                }
            }
            if responseObject.result.isFailure {
                //self.StopIndicator()
                let error : Error = responseObject.result.error!
                failure(error)
                
                let str = String(decoding: responseObject.data!, as: UTF8.self)
                print("PHP ERROR : \(str)")
            }
        }
    }
    
    
    //MARK:- Show/hide Indicator
//    func showIndicator(){
//        if #available(iOS 13.0, *) {
//            SVProgressHUD.setOffsetFromCenter(.init(horizontal: (UIApplication.shared.keyWindow?.center.x)!, vertical: (UIApplication.shared.keyWindow?.center.y)!))
//        }else {
//        }
//        SVProgressHUD.show()
//    }
    
    func showIndicator(){
          SVProgressHUD.show()
      }
      
      func hideIndicator(){
          SVProgressHUD.dismiss()
      }
    
    
    //get current timezone
    func getCurrentTimeZone() -> String{
        return TimeZone.current.identifier
    }
    
}

//MARK:-  stripe payment method
extension WebServiceManager{
    public func requestAddCardOnStripe(strURL:String, params :[String : Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        let url = strURL
        print("url = \(url)")
        
        let headers = ["Authorization" :  stripeKey,"Content-Type":"application/x-www-form-urlencoded"]
        
        
        Alamofire.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in
            
            print(responseObject)
            if responseObject.result.isSuccess {
                do {
                    SVProgressHUD.show(withStatus: "Please wait..")
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    if let errorCode = dictionary["status_code"] as? String{
                        if errorCode == "400"{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }else if let errorCode = dictionary["status_code"] as? Int{
                        if errorCode == 400{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }
                    success(dictionary as! Dictionary<String, Any>)
                    // print(dictionary)
                    SVProgressHUD.dismiss()
                }catch{
                    SVProgressHUD.dismiss()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
            if responseObject.result.isFailure {
                SVProgressHUD.dismiss()
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    // Delete card
    public func requestDeleteCardFromStripe(strURL:String, params :[String : Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        let url = strURL
        print("url = \(url)")
        
        let headers = ["Authorization" :  stripeKey,"Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request(url, method: .delete, parameters: params, headers: headers).responseJSON { responseObject in
            
            print(responseObject)
            if responseObject.result.isSuccess {
                do {
                    SVProgressHUD.show(withStatus: "Please wait..")
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    if let errorCode = dictionary["status_code"] as? String{
                        if errorCode == "400"{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }else if let errorCode = dictionary["status_code"] as? Int{
                        if errorCode == 400{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }
                    success(dictionary as! Dictionary<String, Any>)
                    // print(dictionary)
                    SVProgressHUD.dismiss()
                }catch{
                    SVProgressHUD.dismiss()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
            if responseObject.result.isFailure {
                SVProgressHUD.dismiss()
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    // Get all card List
    public func requestGetCardsFromStripe(strURL:String, params :[String : Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        let url = strURL
        print("url = \(url)")
        
        let headers = ["Authorization" : stripeKey,"Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request(url, method: .get, parameters: params, headers: headers).responseJSON { responseObject in
            
            // print(responseObject)
            if responseObject.result.isSuccess {
                do {
                    SVProgressHUD.show(withStatus: "Please wait..")
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    if let errorCode = dictionary["status_code"] as? String{
                        if errorCode == "400"{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }else if let errorCode = dictionary["status_code"] as? Int{
                        if errorCode == 400{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }
                    success(dictionary as! Dictionary<String, Any>)
                    // print(dictionary)
                    SVProgressHUD.dismiss()
                }catch{
                    SVProgressHUD.dismiss()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
            if responseObject.result.isFailure {
                SVProgressHUD.dismiss()
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    // MARK: - Request Patch method ----
    public func requestPatch(strURL:String, params : [String : AnyObject]?, strCustomValidation:String , success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        if !NetworkReachabilityManager()!.isReachable{
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            objAlert.showAlertVc(title: NoNetwork, controller: window!)
            DispatchQueue.main.async {
               // self.StopIndicator()
            }
            return
        }
        
        strAuthToken = ""
        
        strAuthToken = "Bearer" + " " + objAppShareData.UserDetail.straAuthToken
        
        let currentTimeZone = getCurrentTimeZone()
        
        var strUdidi = ""
        if let MyUniqueId = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.strVenderId) {
            print("defaults VenderID: \(MyUniqueId)")
            strUdidi = MyUniqueId
        }
        
        let headers: HTTPHeaders = [
            "Authorization": strAuthToken ,
            "Accept": "application/json",
            WsHeader.deviceId:strUdidi,
            WsHeader.deviceType:"1",
            WsHeader.deviceTimeZone: currentTimeZone
        ]
        
        print("url....\(strURL)")
        print("header....\(headers)")
        Alamofire.request(strURL, method: .patch, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
          //  self.StopIndicator()
            
            if responseObject.result.isSuccess {
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    if let errorCode = dictionary["status_code"] as? String{
                        if errorCode == "400"{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }else if let errorCode = dictionary["status_code"] as? Int{
                        if errorCode == 400{
                            let strErrorType = dictionary["error_type"] as? String
                            if strErrorType == "USER_NOT_FOUND" || strErrorType == "INVALID_TOKEN" || strErrorType == "SESSION_EXPIRED"{
                                objAlert.showSessionFailAlert()
                                return
                            }
                        }
                    }
                    success(dictionary as! Dictionary<String, Any>)
                    print(dictionary)
                    let status = dictionary["status"] as? String
                    if status == "error"
                    {
                        let error = dictionary["error_type"] as? String
                        
                        if error == "SESSION_EXPIRED" || error == "USER_NOT_FOUND"
                        {
                           // self.setRootSessionExpired()
                         //  self.StopIndicator()
                        }
                    }
                    
                }catch{
                    
                    let error : Error = responseObject.result.error!
                    failure(error)
                    let str = String(decoding: responseObject.data!, as: UTF8.self)
                    print("PHP ERROR : \(str)")
                }
            }
            if responseObject.result.isFailure {
               // self.StopIndicator()
                let error : Error = responseObject.result.error!
                failure(error)
                
                let str = String(decoding: responseObject.data!, as: UTF8.self)
                print("PHP ERROR : \(str)")
            }
        }
    }
}

