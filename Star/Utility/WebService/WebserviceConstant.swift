//
//  WebserviceConstant.swift
//  Hoggz
//
//  Created by MACBOOK-SHUBHAM V on 27/12/19.
//  Copyright © 2019 MACBOOK-SHUBHAM V. All rights reserved.
//
//
//  WebserviceConstant.swift
//  Mualab
//
//  Created by MINDIII on 10/25/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation
import UIKit

//let BASE_URL = "https://stars.app/api/v1/"   // live
let BASE_URL = "https://www.stars.app/api/v1/" //Live
//let BASE_URL = "https://dev.stars.app/api/v1/"  // Dev


// Api Base url with api name 
struct WsUrl {
    
    static let SendVerificationCode     = BASE_URL + "auth/send-verification-code"
    static let VerifyCode = BASE_URL + "auth/verify-code"
    static let Signup = BASE_URL + "auth/signup"
    static let AvatarUpload = BASE_URL + "user/avatar-upload"
    static let Login = BASE_URL + "auth/login"
    static let Languages = BASE_URL + "user/languages"
    static let CommonList = BASE_URL + "general/list"
    static let Countries  = BASE_URL + "user/countries"
    static let Logout = BASE_URL + "auth/logout"
    static let CheckSocialSignup = BASE_URL + "auth/check-social-signup"
    static let SocialSignup = BASE_URL + "auth/social-signup"
    static let Url_updateProfile = BASE_URL + "user/profile"
    static let Url_GetUserProfile = BASE_URL + "user/profile?user_id="
    static let Url_GetDiscoverList = BASE_URL + "discover/list?"
    static let Url_FollowUnfollow = BASE_URL + "user/following/"
    static let Url_BalancePlanList = BASE_URL + "plan/list"
    static let Url_PurchasePlan = BASE_URL + "plan/purchase"
    static let Url_GetCallList = BASE_URL + "call/list?is_booked="
    static let Url_GetCallDetails = BASE_URL + "call/"
    static let Url_CallReply = BASE_URL + "call/call-reply"
    
    //Forgot Password
    static let Url_ForgotPassword = BASE_URL + "auth/reset-password"
    static let Url_NotificationStatus = BASE_URL + "alert/settings"
    
    //Infulencer Side
    static let ScheduleList = BASE_URL + "schedule/list?"
    static let AddSchedule = BASE_URL + "schedule"
    static let DeleteSchedule = BASE_URL + "schedule/"
    
    static let Url_GetCredits = BASE_URL + "user/credits"
    static let Free_Credits = BASE_URL + "plan/free-credits"
    static let Get_Notification = BASE_URL + "alert/list"
    static let checkUserName = BASE_URL + "user/check-username"
     
    }


//Api Header
struct WsHeader {
    //Login
    static let deviceId = "Device-Id"
    static let deviceType = "Device-Type"
    static let deviceTimeZone = "Timezone"
    static let ContentType = "Content-Type"
}

//Api parameters
struct WsParam {
    
    // Verification Code
    static let Phone = "phone"
    static let Dial_code = "dial_code"
    static let Phone_country_code = "phone_country_code"
    static let Verify_code = "verify_code"
    
    // SingUp
    static let Phone_number = "phone_number"
    static let Phone_dial_code = "phone_dial_code"
    static let UserName = "name"
    static let Email = "email"
    static let Password = "password"
    static let Gender = "gender"
    static let Language = "language"
    static let deviceToken = "device_token"
    static let DoB = "dob"
    static let Country_code = "country_code"
    static let Profile_picture = "profile_picture"
    static let Country_Id = "country_id"
    
    //Login
    static let userId = "user_id"
    static let deviceId = "device_id"
    static let deviceType  = "device_type"
    static let socialNetwork_type = "social_network_type"
    static let socialNetworkId = "social_network_id"
    ///
    static let Login_identifier = "login_identifier"
    static let Login_type = "login_type"
    static let User_type = "user_type"
   
}




