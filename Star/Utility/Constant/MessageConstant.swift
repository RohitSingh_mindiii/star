//
//  MessageConstant.swift
//  Hoggz
//
//  Created by MACBOOK-SHUBHAM V on 27/12/19.
//  Copyright © 2019 MACBOOK-SHUBHAM V. All rights reserved.
//
//
//  MessageConstant.swift
//  Mualab
//
//  Created by MINDIII on 10/16/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation
import UIKit



//pragma mark - Field validation
let ValidEmail: String = "Email Address is not valid, Please provide a valid Email"
let ValidPhone: String = "Phone number is not valid, Please provide a valid phone number"
let ValidEmailPhone: String = "Email Address / Phone number is not valid, Please provide a valid Email or phone number"
let BlankEmail: String = "Please enter your Email"
let BlankEmailAndPhoneNumber: String = "Please enter your email or phone number"
let BlankEmailPass: String = "Please enter your email and password "
let BlankPhoneNo: String = "Please enter your Phone number "
let BlankLanguage: String = "Please Select Language "
let BlankUsername: String = "Please enter full name"
let BlankVoiceVideo: String = "Please enter video or voice message "
let BlankGender: String = "Please Select Gender"
let BlankDOB: String = "Please enter DOB"
let InvalidName: String = "Only alphabeticals allowed in Username"
let UploadImage: String = "Please upload image"
let InvalidDOB: String = "DOB is not valid, Please provide a valid DOB in formate dd/MMM/yyyy"
let BlankAge: String = "Please Select Age "
let UsernameLength: String = "Username must be atleast 4 characters long"
let BlankPassword: String = "Please enter your Password"
let LengthPassword: String = "Password is too short (minimum 6 characters)"
let LengthName: String = "Username is too short (minimum 3 characters)"
let BlankConfirmPassword: String = "Please enter confirm password"
let NoNetwork: String = "No network connection"
let SameConfirmPassword: String = "password and confirm password does not match"
let VerifyCode: String = "Please enter the 4 digit code below to verify your mobile number and create your account"
let ErrorEmail: String = "Email does not exist in our database, please try alternative."
let SentEmail: String = "Email sent. Check your inbox"
let SMSSent: String = "SMS sent. Check your phone"

let Name_Title : String = "Please enter the Name/Title"
let NameValidation : String = "Name and title should be between 3 to 50 characters"
let Development: String = "Under Development"
let CountrySelection: String = "Please select the country"



//Color constant
let colorTheame = UIColor(red: 14.0 / 255.0, green: 123.0 / 255.0, blue: 122.0 / 255.0, alpha: 1.0)
let colorTheameGreeen = UIColor(red: 70.0 / 255.0, green: 170.0 / 255.0, blue: 129.0 / 255.0, alpha: 1.0)
let colorTheameYellow = UIColor(red: 239.0 / 255.0, green: 187.0 / 255.0, blue: 107.0 / 255.0, alpha: 1.0)
let colorTheameRed = UIColor(red: 222.0 / 255.0, green: 113.0 / 255.0, blue: 127.0 / 255.0, alpha: 1.0)

//pragma mark -Alert validation
let kAlertMessage: String = "Message"
let kAlertTitle: String = "Alert"
let kErrorMessage: String = "Something went wrong"
let k_success = "success"
let k_NoConnection: String = "No network connection"

//MARK: - Alerts
func showAlertVC(title:String,message:String,controller:UIViewController) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let subView = alertController.view.subviews.first!
    let alertContentView = subView.subviews.first!
    alertContentView.backgroundColor = UIColor.gray
    alertContentView.layer.cornerRadius = 20
    let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(OKAction)
    controller.present(alertController, animated: true, completion: nil)
}

func sessionExpireAlertVC(controller:UIViewController) {
    let alertController = UIAlertController(title: kAlertTitle, message: "Your session is expired , please login again", preferredStyle: .alert)
    let subView = alertController.view.subviews.first!
    let alertContentView = subView.subviews.first!
    alertContentView.backgroundColor = UIColor.gray
    alertContentView.layer.cornerRadius = 20
    let OKAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
        //objAppShareData.objAppdelegate.logOut()
    })
    alertController.addAction(OKAction)
    controller.present(alertController, animated: true, completion: nil)
}

func checkForNULL(obj:Any?) -> Any {
    return obj ?? ""
}
