////
////  SceneDelegate.swift
////  Star
////
////  Created by ios-deepak b on 27/01/20.
////  Copyright © 2020 ios-deepak b. All rights reserved.
////
//
//import UIKit
//import GoogleSignIn
//import FBSDKCoreKit
//import FBSDKLoginKit
//
//@available(iOS 13.0, *)
//let objSceneDelegate = SceneDelegate.SceneDelegateObject()
//
//@available(iOS 13.0, *)
//class SceneDelegate: UIResponder, UIWindowSceneDelegate {
//    
//    var window: UIWindow?
//    
//    //MARK: - Shared object
//    private static var SceneDelegateManager: SceneDelegate = {
//        let manager = UIApplication.shared.keyWindow?.windowScene?.delegate as? SceneDelegate
//        
//        return manager!
//    }()
//    
//    // MARK: - Accessors
//    class func SceneDelegateObject() -> SceneDelegate {
//        return SceneDelegateManager
//    }
//    
//    func application(
//        _ app: UIApplication,
//        open url: URL,
//        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
//        
//    ) -> Bool {
//        ApplicationDelegate.shared.application(
//            app,
//            open: url,
//            options: options
//        )
//        GIDSignIn.sharedInstance().handle(url)
//        return true
//    }
//    
//    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
//        guard let url = URLContexts.first?.url else {
//            return
//        }
//        let _ = ApplicationDelegate.shared.application(
//            UIApplication.shared,
//            open: url,
//            sourceApplication: nil,
//            annotation: [UIApplication.OpenURLOptionsKey.annotation])
//    }
//    
//    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
//        
//        guard let _ = (scene as? UIWindowScene)else { return }
//        if let windowScene = scene as? UIWindowScene {
//            self.window = UIWindow(windowScene: windowScene)
//            self.checkLoginStatus()
//            self.WebserviceForGetlanguages()
//        }
//    }
//    
//    
//    
//    func sceneDidDisconnect(_ scene: UIScene) {
//        // Called as the scene is being released by the system.
//        // This occurs shortly after the scene enters the background, or when its session is discarded.
//        // Release any resources associated with this scene that can be re-created the next time the scene connects.
//        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
//    }
//    
//    func sceneDidBecomeActive(_ scene: UIScene) {
//        // Called when the scene has moved from an inactive state to an active state.
//        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
//    }
//    ///*
//    func showLogInNavigation() {
//        
//        // switch root view controllers
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let nav = storyboard.instantiateViewController(withIdentifier: "MainNavigation")
//        
//        self.window?.rootViewController = nav
//        //  self.window?.makeKeyAndVisible()
//        
//    }
//    
//    
//    func showUserTabbar() {
//        
//        let storyboard:UIStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
//        let navigationController = storyboard.instantiateViewController(withIdentifier: "UserNav") as? UINavigationController
//        
//        self.window?.rootViewController = navigationController
//        self.window?.makeKeyAndVisible()
//    }
//    
//    func showInfluencerTabbar() {
//        
//        if #available(iOS 13.0, *) {
//            print("ios 13")
//        }else{
//            let storyboard:UIStoryboard = UIStoryboard(name: "Influencer_Tabbar", bundle: nil)
//            let navigationController = storyboard.instantiateViewController(withIdentifier: "InfluencerNav") as? UINavigationController
//            self.window?.rootViewController = navigationController
//            self.window?.makeKeyAndVisible()
//        }
//    }
//    
//    func checkLoginStatus(){
//        
//        
//        if UserDefaults.standard.string(forKey:"Authtoken")==nil {
//            self.showLogInNavigation()
//        }else{
//            if objAppShareData.UserDetail.stronboarding_step == "2"{
//                // self.showLogInNavigation()
//                self.showUserTabbar()
//            }else{
//                
//                self.showUserTabbar()
//                
//            }
//        }
//    }
//    
//    // */
//    
//    func sceneWillResignActive(_ scene: UIScene) {
//        // Called when the scene will move from an active state to an inactive state.
//        // This may occur due to temporary interruptions (ex. an incoming phone call).
//    }
//    
//    func sceneWillEnterForeground(_ scene: UIScene) {
//        // Called as the scene transitions from the background to the foreground.
//        // Use this method to undo the changes made on entering the background.
//    }
//    
//    func sceneDidEnterBackground(_ scene: UIScene) {
//        // Called as the scene transitions from the foreground to the background.
//        // Use this method to save data, release shared resources, and store enough scene-specific state information
//        // to restore the scene back to its current state.
//    }
//    
//    func WebserviceForGetlanguages(){
//        
//        //  objWebServiceManager.showIndicator()
//        objWebServiceManager.requestGet(strURL: WsUrl.CommonList, params: nil, strCustomValidation: "", success: {response in
//            print(response)
//            let status = (response["status"] as? String)
//            let message = (response["message"] as? String)
//            if status == k_success{
//                objWebServiceManager.hideIndicator()
//                if let data = response["data"]as? [String:Any]{
//                    
//                    if status == "success" ||  status == "Success"
//                    {
//                        let arrCountryData = data["countryData"] as! [[String: Any]]
//                        for dictCountryData in arrCountryData
//                        {
//                            let objPendingData = CountryData.init(dict: dictCountryData)
//                            objAppShareData.arrCountryData.append(objPendingData)
//                            
//                        }
//                        
//                        let arrLanguageData = data["languageData"] as! [[String: Any]]
//                        for dictLanguageData in arrLanguageData
//                        {
//                            let objData = LanguageData.init(dict: dictLanguageData)
//                            objAppShareData.arrLanguageData.append(objData)
//                            
//                        }
//                        
//                        let arrGenderData = data["genderData"] as! [String]
//                        objAppShareData.arrGenderData = arrGenderData
//                        //print("arrGenderData count is \(arrGenderData.count)")
//                        // print("objAppShareData arrGenderData count is \(objAppShareData.arrGenderData.count)")
//                        
//                        let arrAgeGroupData = data["ageGroupData"] as! [String]
//                        objAppShareData.arrAgeGroupData = arrAgeGroupData
//                         print("objAppShareData arrAgeGroupData count is \(objAppShareData.arrAgeGroupData.count)")
//                    }else{
//                    }
//                }
//            }
//            else{
//            }
//        }, failure: { (error) in
//            print(error)
//            //  objWebServiceManager.hideIndicator()
//            //  objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
//        })
//    }
//}
//
//
