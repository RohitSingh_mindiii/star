//
// AppDelegate.swift
// Star
//
// Created by ios-deepak b on 27/01/20.
// Copyright © 2020 ios-deepak b. All rights reserved.
//

import UIKit
import Firebase
//import FirebaseCore
//import FirebaseMessaging
import UserNotifications
import IQKeyboardManagerSwift
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import CoreLocation
let objAppDelegate = AppDelegate.AppDelegateObject()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {
    
    //MARK: - Shared object
    private static var AppDelegateManager: AppDelegate = {
        let manager = UIApplication.shared.delegate as! AppDelegate
        return manager
    }()
    // MARK: - Accessors
    class func AppDelegateObject() -> AppDelegate {
        return AppDelegateManager
    }
    
    var window: UIWindow?
    var navController: UINavigationController?
    var currentTimeZone:String = ""
    //Location Work //Rohit
    var locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        GIDSignIn.sharedInstance().clientID = "25526584939-r8batp0fongqn1ml0j35mqbnl2nh6hfo.apps.googleusercontent.com"
        self.configureNotification()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        let getudid = getUUID()
        let defaults = UserDefaults.standard
        defaults.set(getudid ?? "", forKey:UserDefaults.KeysDefault.strVenderId)
        if let myString = defaults.string(forKey:UserDefaults.KeysDefault.strVenderId) {
            print("defaults savedString: \(myString)")
        }
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
//            if granted {
//                self.registerCategory()
//                self.scheduleNotification(event: "test", interval: 3)
//                self.scheduleNotification(event: "test2", interval: 5)
//            }
        }
        //MARK:- Get Location Permission Work //Rohit
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        //locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = false
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        //============================XXXX==========================//
        currentTimeZone = getCurrentTimeZone()
        objAppShareData.fetchUserInfoFromAppshareData()
     
      //  self.checkLoginStatus()
        self.WebserviceForCommonList()
       
        let isDarkMode:Bool = userDefaults.bool(forKey: "isDarkMode")
        //    print("isDarkMode  is \(isDarkMode)")
           if isDarkMode == true{
               if #available(iOS 13.0, *) {
                  objAppDelegate.window?.overrideUserInterfaceStyle = .dark
               }
           }else{
               if #available(iOS 13.0, *) {
                  objAppDelegate.window?.overrideUserInterfaceStyle = .light
               }
        }
        return true
    }
    
    // @available(iOS 9.0, *)
    // func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
    // return GIDSignIn.sharedInstance().handle(url)
    // }
    
    
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {
        ApplicationDelegate.shared.application(
            app,
            open: url,
            options: options
        )
        
        GIDSignIn.sharedInstance().handle(url)
        return true
    }
    
    ///*
    func showLogInNavigation() {
        
        // switch root view controllers
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "MainNavigation")
        
        self.window?.rootViewController = nav
        // self.window?.makeKeyAndVisible()
        
    }
    
    
    func showUserTabbar() {
        
        let storyboard:UIStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "UserNav") as? UINavigationController
        
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
    }
    func showInfluencerTabbar() {
        
            let storyboard:UIStoryboard = UIStoryboard(name: "Influencer_Tabbar", bundle: nil)
            let navigationController = storyboard.instantiateViewController(withIdentifier: "InfluencerNav") as? UINavigationController
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        
    }
    
    func checkLoginStatus(){
        if UserDefaults.standard.string(forKey:"Authtoken")==nil {
            self.showLogInNavigation()
        }else{
            if objAppShareData.UserDetail.stronboarding_step == "2"{
               // self.showLogInNavigation()
                objAppDelegate.showUserTabbar()
            }else{
                
                let UserType = UserDefaults.standard.string(forKey:UserDefaults.InfulenceKeys.user_type)
                if UserType == "influencer"{
                    self.showInfluencerTabbar()
                }else{
                    self.showUserTabbar()
                }
                
            }
        }
    }
    
    func getCurrentTimeZone() -> String{
        return TimeZone.current.identifier
    }
    
    func getUUID() -> String? {
        // create a keychain helper instance
        let keychain = KeychainAccess()
        // this is the key we'll use to store the uuid in the keychain
        let uuidKey = "DeviceUniqueId"
        // check if we already have a uuid stored, if so return it
        if let uuid = try? keychain.queryKeychainData(itemKey: uuidKey), uuid != nil {
            return uuid
        }
        // generate a new id
        guard let newId = UIDevice.current.identifierForVendor?.uuidString else {
            return nil
        }
        
        // store new identifier in keychain
        try? keychain.addKeychainData(itemKey: uuidKey, itemValue: newId)
        
        // return new id
        return newId
    }
    
    
    //MARK:- Get User Current Location Work
    
    ///Call Webservice
    func WebserviceForCommonList(){

    // objWebServiceManager.showIndicator()
    objWebServiceManager.requestGet(strURL: WsUrl.CommonList, params: nil, strCustomValidation: "", success: {response in
    print(response)
    let status = (response["status"] as? String)
    let message = (response["message"] as? String)
    if status == k_success{
    objWebServiceManager.hideIndicator()
    if let data = response["data"]as? [String:Any]{

    if status == "success" || status == "Success"
    {
    
    // TODO: Get Countries
    let arrCountryData = data["countries"] as! [[String: Any]]
    for dictCountryData in arrCountryData
    {
        let objPendingData = CommonModal.init(dict: dictCountryData)
        let imagestring = objPendingData.strCountryCode
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        objPendingData.imgCountryLogo = UIImage.init(named: imagePath) ?? UIImage()
        objAppShareData.arrCountryData.append(objPendingData)
    }
        let objCountry:[String:Any] = ["countryID": 0, "country_name": "All Countries"]
        let objAllCountry:CommonModal = CommonModal(dict: objCountry)
        objAppShareData.arrCountryData = [objAllCountry] + objAppShareData.arrCountryData
    
    // TODO: Get langauges
    let arrLanguageData = data["langauges"] as! [[String: Any]]
    for dictLanguageData in arrLanguageData
    {
    let objData = CommonModal.init(dict: dictLanguageData)
    objAppShareData.arrLanguageData.append(objData)
    }
    let objAllLang:[String:Any] = ["languageID": 0, "code": "allLang", "name": "All Languages"]
    let objAllLngBasicInfo:CommonModal = CommonModal(dict: objAllLang)
    objAppShareData.arrLanguageData = [objAllLngBasicInfo] + objAppShareData.arrLanguageData

    // TODO: Get Genders
    let arrgendersData = data["genders"] as! [[String: Any]]
    for dictgendersData in arrgendersData
    {
    let objgendersData = CommonModal.init(dict: dictgendersData)
    if objgendersData.strID == "0"{
    }else{
    objAppShareData.arrGenderData.append(objgendersData)
    }
    }
    let objGender:[String:Any] = ["id": 0, "name": "All Gender"]
    let objGenderInfo:CommonModal = CommonModal(dict: objGender)
    objAppShareData.arrGenderData = [objGenderInfo] + objAppShareData.arrGenderData

    // TODO: Get Age Groups
    let arrAgeData = data["age_groups"] as! [[String: Any]]
    for dictAgeData in arrAgeData
    {
    let objAgeData = CommonModal.init(dict: dictAgeData)
    if objAgeData.strID == "0"{
    }else{
    objAppShareData.arrAgeGroupData.append(objAgeData)
    }
    }
    let objAgeData:[String:Any] = ["id": 0, "name": "All Age"]
    let objAgeInfo:CommonModal = CommonModal(dict: objAgeData)
    objAppShareData.arrAgeGroupData = [objAgeInfo] + objAppShareData.arrAgeGroupData

        // TODO: Get Credits
        let arrCreditData = data["free_credit_types"] as! [[String: Any]]
        for dictCreditData in arrCreditData
        {
        let objData = CommonModal.init(dict: dictCreditData)
        objAppShareData.arrCreditData.append(objData)

        }
    
    // TODO: Get Category and SubCategory
    let arrCatData = data["categories"] as! [[String: Any]]
    for dictCatData in arrCatData{
        let objCatData = CommonModal.init(dict: dictCatData)
        objAppShareData.arrCategoryData.append(objCatData)
    }
        let objCatData:[String:Any] = ["id": 0, "name": "All Categories"]
        let objCatInfo:CommonModal = CommonModal(dict: objCatData)
        objAppShareData.arrCategoryData = [objCatInfo] + objAppShareData.arrCategoryData

    print("objAppShareData arrCategoryData count is \(objAppShareData.arrCategoryData.count)")
    print("objAppShareData arrCountryData count is \(objAppShareData.arrCountryData.count)")
    print("objAppShareData arrLanguageData count is \(objAppShareData.arrLanguageData.count)")
    print("objAppShareData arrGenderData count is \(objAppShareData.arrGenderData.count)")
    print("objAppShareData arrAgeGroupData count is \(objAppShareData.arrAgeGroupData.count)")


    }else{
    }
    }
    }
    else{
    }
    }, failure: { (error) in
    print(error)
    // objWebServiceManager.hideIndicator()
    // objAlert.showAlert(message:kErrorMessage, title: kAlertTitle, controller: self)
    })
    }
    
    //Get Current Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        let latitude = String(locValue.latitude)
        let longitude = String(locValue.longitude)
        getAddressFromLatLon(pdblLatitude: latitude, withLongitude: longitude)
        locationManager.stopUpdatingLocation()
        
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        
        let lon: Double = Double("\(pdblLongitude)")!
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if let pm = placemarks{
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country ?? "")
                        print(pm.locality ?? "")
                        print(pm.subLocality ?? "")
                        print(pm.thoroughfare ?? "")
                        print(pm.postalCode ?? "")
                        print(pm.subThoroughfare ?? "")
                        var strfulladdress : String = ""
                        var strCity : String = ""
                        var strCountry : String = ""
                        var strPostalCode : String = ""
                        var strCountryCode : String = ""
                        
                        if pm.subLocality != nil {
                            // addressString = addressString + pm.subLocality! + ", "
                            strfulladdress = pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            // addressString = addressString + pm.thoroughfare! + ", "
                            strfulladdress = strfulladdress + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            //addressString = addressString + pm.locality! + ", "
                            strCity = pm.locality!
                            strfulladdress = strfulladdress + pm.locality!
                            
                        }
                        if pm.country != nil {
                            // addressString = addressString + pm.country! + ", "
                            strCountry = pm.country!
                        }
                        if pm.postalCode != nil {
                            // addressString = addressString + pm.postalCode! + " "
                            strPostalCode = pm.postalCode!
                        }
                        if pm.isoCountryCode != nil{
                            strCountryCode = pm.isoCountryCode!
                        }
                        
                        
                        let param = ["latitude":pdblLatitude,"longitude":pdblLongitude,"country":strCountry,"city":strCity,"fullAddress":strfulladdress,"postalCode":strPostalCode,"countryCode":strCountryCode]as [String:Any]
                        
                        AppShareData.sharedObject().dictUserLoction = param
                        print(objAppShareData.dictUserLoction)
                    }
                }
        })
        
    }
}
    
//MARK: - FireBase Methods / FCM Token
extension AppDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        objAppShareData.UserDetail.strDeviceToken = fcmToken
        objAppShareData.strDeviceToken = fcmToken
        print("objAppShareData.firebaseToken = \(objAppShareData.UserDetail.strDeviceToken)")
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        objAppShareData.UserDetail.strDeviceToken = fcmToken
        objAppShareData.strDeviceToken = fcmToken
        print("objAppShareData.firebaseToken = \(objAppShareData.UserDetail.strDeviceToken)")
        ConnectToFCM()
    }
    
    func tokenRefreshNotification(_ notification: Notification) {

        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            }else if let result = result {
                print("Remote instance ID token: \(result.token)")
                objAppShareData.UserDetail.strDeviceToken = result.token
                objAppShareData.strDeviceToken = result.token
                print("objAppShareData.firebaseToken = \(result.token)")
            }
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        ConnectToFCM()
    }
    
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
    }
    
    func ConnectToFCM() {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            }else if let result = result {
                print("Remote instance ID token: \(result.token)")
                objAppShareData.UserDetail.strDeviceToken = result.token
                objAppShareData.strDeviceToken = result.token
                print("objAppShareData.firebaseToken = \(result.token)")
            }
        }
    }
    func configureNotification() {
           let center = UNUserNotificationCenter.current()
           center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
               // If granted comes true you can enabled features based on authorization.
               guard granted else { return }

            DispatchQueue.main.async {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
                UIApplication.shared.registerForRemoteNotifications()
            }
       }
    }
    func application(application: UIApplication,  didReceiveRemoteNotification userInfo: [NSObject : AnyObject],  fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        print("Recived: \(userInfo)")
        completionHandler(.newData)
    }
    @available(iOS 10.0, *)
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    willPresent notification: UNNotification,
                                    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
            
            if let userInfo = notification.request.content.userInfo as? [String : Any]{
                print(userInfo)
                var notifincationType : Int?
                if let notiType = userInfo["notifincationType"] as? Int{
                    notifincationType = notiType
                }else{
                    if let notiType = userInfo["notifincationType"] as? String{
                        notifincationType = Int(notiType)
                    }
                }
           }
        }
        
        @available(iOS 10.0, *)
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    didReceive response: UNNotificationResponse,
                                    withCompletionHandler completionHandler: @escaping () -> Void) {
            
            print(response)
          
            
            switch response.actionIdentifier {
                
            case UNNotificationDismissActionIdentifier:
                print("Dismiss Action")
            case UNNotificationDefaultActionIdentifier:
                print("Open Action")
                if let userInfo = response.notification.request.content.userInfo as? [String : Any]{
                    print(userInfo)
                    self.handleNotificationWithNotificationData(dict: userInfo)
                }
            case "Snooze":
                print("Snooze")
            case "Delete":
                print("Delete")
            default:
                print("default")
            }
            completionHandler()
        }
        func handleNotificationWithNotificationData(dict:[String:Any]){
            var notifincationType = ""
            if let notiType = dict["type"] as? String{
                notifincationType = notiType
            }
            if notifincationType == "Calls"{
            }else if notifincationType == "Balance"{
            }
             objAppShareData.isFromNotification = true
             objAppShareData.strNotificationType = notifincationType
             objAppShareData.notificationDict = dict
             self.checkLoginStatus()
        }
        func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
            if let navigationController = controller as? UINavigationController {
                return topViewController(controller: navigationController.visibleViewController)
            }
            if let tabController = controller as? UITabBarController {
                if let selected = tabController.selectedViewController {
                    return topViewController(controller: selected)
                }
            }
            if let presented = controller?.presentedViewController {
                return topViewController(controller: presented)
            }
            return controller
        }
}

